<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class SettingsModel extends Model
{
    public $table = 'settings';
    
    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'unique_link',
        'net_promoter_score',
        'second_chance',
        'play_reminder',
        'campaign_reminder',
        'get_discover_color',
        'get_discover_text',
        'get_discover',
        'facebook_link',
        'instagram_link',
        'twitter_link',
        'linked_link',
        'store_promo_slider',
        'store_name',
        'store_url',
        'terms_condition'
    ];
    
    function add_data($table,$data)
    {
        DB::table($table)->insert(
            $data
        );
    }
}
