<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductsModel extends Model {

    public $table = 'products';

    function get_products() {

        $users = DB::table('products')
                ->select(DB::raw('*'))
                ->where('status', 1)
                ->orderBy('id', 'desc')
                ->get();
        return $users;
    }

    function get_product_detail($whr) {
        //Case Added by Nikunj Dhimar
        if (isset($whr['select'])) {
            $select = DB::raw($whr['select']);
            unset($whr['select']);
        } else {
            $select = DB::raw('*');
        }

        $users = DB::table('products')
                ->select($select)
                ->where($whr)
                ->get();
        return $users;
    }

    function get_products_foruser($whr) {
        if ($whr['location'] != '') {
            $users = DB::table('products')
                    ->select(DB::raw('*'))
                    ->whereRaw("status = 1 and location = '" . $whr['location'] . "' ")
                    ->orderBy('id', 'desc')
                    ->get();
        } else {
            $users = DB::table('products')
                    ->select(DB::raw('*'))
                    ->whereRaw("status = 1 and company = '" . $whr['company'] . "'")
                    ->orderBy('id', 'desc')
                    ->get();
        }

        return $users;
    }

    function get_products_foruser_filter($whr) {
        $cat = $whr['category'];
        $users = DB::table('products')
                ->select(DB::raw('*'))
                ->whereRaw("status = 1 and (location = '" . $whr['location'] . "' or company = '" . $whr['company'] . "') and category like '%" . $cat . "%'")
                ->orderBy('id', 'desc')
                ->get();

        return $users;
    }
    
    function select_data_wherein($table, $data, $whrCol, $whr) {
        $d = implode(',', $data);
        $responseData = DB::table($table)
                ->select(DB::raw($d))
                ->whereIn($whrCol,$whr)
                ->get()
                ->toArray();
        return $responseData;
    }

}
