<?php
namespace App\Helpers;

class Helper {

    public static function getcurrency($currency, $pricetoconvert) {
        $req_url = 'https://v6.exchangerate-api.com/v6/2f0a0e14099c1e179266548f/latest/USD';
        $response_json = file_get_contents($req_url);
        // Continuing if we got a result
        if (false !== $response_json) {

            // Try/catch for json_decode operation
            try {
                // Decoding
                $response = json_decode($response_json);
                // Check for success
                if ('success' === $response->result) {
                    // YOUR APPLICATION CODE HERE, e.g.
                    $base_price = $pricetoconvert; // Your price in USD
                    $EUR_price = round(($base_price * $response->conversion_rates->$currency), 2);
                    return $EUR_price;
                }
            } catch (Exception $e) {
                // Handle JSON parse error...
            }
        }
    }
}
