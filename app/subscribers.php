<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subscribers extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'subscribers';
	
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
}
