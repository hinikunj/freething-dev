<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class PhoneNumberModel extends Model
{
    public $table = 'phone_numbers';
}
