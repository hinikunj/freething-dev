<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ConditionModel extends Model
{
      protected $table = 'flow_conditions';
      protected $primaryKey = 'condition_id';

      function add_data($table, $data)
      {
            $insert = DB::table($table)->insert(
                  $data
            );
            return $insert;
      }


      function select_data($table, $whr)
      {
            $users = DB::table($table)
                  ->select(DB::raw('*'))
                  ->where($whr)
                  ->get();
            return $users;
      }
      function select_data_count($table, $whr)
      {
            $users = DB::table($table)
                  ->select(DB::raw('count(*) as count'))
                  ->where($whr)
                  ->get();
            return $users;
      }
      
      

}
