<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class QuestionModel extends Model
{
      
      protected $table = 'questions';
      protected $primaryKey = 'question_id';

      function add_data($table, $data)
      {
            $insert = DB::table($table)->insert(
                  $data
            );
            return $insert;
      }

      function check_existing_user($data)
      {
            $users = DB::table('login')
                  ->select(DB::raw('first_name,last_name,phone,role'))
                  ->where('phone', '=', $data['phone'])
                  ->get();
            return $users;
      }

      function check_existing_email($data)
      {
            $users = DB::table('login')
                  ->select(DB::raw('first_name,last_name,phone,role'))
                  ->where('email', '=', $data['email'])
                  ->get();
            return $users;
      }


      function select_data($table, $whr)
      {
            $users = DB::table($table)
                  ->select(DB::raw('*'))
                  ->where($whr)
                  ->get();
            return $users;
      }
      function select_data_count($table, $whr)
      {
            $users = DB::table($table)
                  ->select(DB::raw('count(*) as count'))
                  ->where($whr)
                  ->get();
            return $users;
      }
      function search_company($table, $whr)
      {

            $users = DB::table($table)
                  ->select(DB::raw('id,company_name'))
                  ->where('company_name', 'like', $whr['company_name'] . '%')
                  ->get();
            return $users;
      }
      function get_company()
      {

            $users = DB::table('company')
                  ->select(DB::raw('id,company_name'))
                  ->get();
            return $users;
      }
      function login($whr)
      {
            $p = $whr['phone'];
            $users = DB::table('login')
                  ->select(DB::raw('id,first_name,last_name,phone,role'))
                  ->whereRaw("password = '" . $whr['password'] . "' and (phone = '" . $whr["phone"] . "' or email = '" . $whr["phone"] . "') and status=1 and auth_type = ''")


                  ->get();
            return $users;
      }
      function user_details($whr)
      {

            $users = DB::table('login')
                  ->select(DB::raw('id,first_name,last_name,phone,role'))
                  ->where($whr)
                  ->get();
            return $users;
      }

      function get_locations()
      {

            $users = DB::table('location')
                  ->select(DB::raw('id,location,currency,symbol'))
                  ->get();
            return $users;
      }

      function get_dataset()
      {

            $users = DB::table('datasets')
                  ->join('company', 'datasets.company_id', '=', 'company.id', 'left')
                  ->select(DB::raw('*,datasets.id as datasets_id'))
                  ->orderBy('datasets.id', 'desc')
                  ->get();
            return $users;
      }

      function get_category()
      {
            $users = DB::table('datasets')
                  ->select(DB::raw('*'))
                  ->groupBy('category')
                  ->get();
            return $users;
      }

      function get_subcategory($whr)
      {
            $users = DB::table('datasets')
                  ->select(DB::raw('*'))
                  ->where($whr)
                  ->get();
            return $users;
      }

      function get_product_detail($whr)
      {

            $users = DB::table('products')
                  ->select(DB::raw('*'))
                  ->where($whr)
                  ->get();
            return $users;
      }

      function get_profile($whr)
      {

            $users = DB::table('login')
                  ->select(DB::raw('id,first_name,last_name,phone,role,email,status,company,languages, location,website'))
                  ->whereRaw("phone = '" . $whr["phone"] . "' ")
                  ->get();
            return $users;
      }

      function update_data($table, $data, $whr)
      {
            $affected = DB::table($table)
                  ->where($whr)
                  ->update($data);
      }

      function delete_dataset($dataset_id)
      {
            DB::table('datasets')->where('id', '=', $dataset_id)->delete();
      }

      function delete_multiple($question_id) {
            DB::table('question_response')->where('question_id', '=', $question_id)->delete();
      }
}
