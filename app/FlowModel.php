<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class FlowModel extends Model
{
      protected $table = 'flow';
      protected $primaryKey = 'flow_id';

      function add_data($table, $data)
      {
            $insert = DB::table($table)->insert(
                  $data
            );
            return $insert;
      }


      function select_data($table, $whr)
      {
            $users = DB::table($table)
                  ->select(DB::raw('id,first_name,last_name,phone,role'))
                  ->where($whr)
                  ->get();
            return $users;
      }
      function select_data_count($table, $whr)
      {
            $users = DB::table($table)
                  ->select(DB::raw('count(*) as count'))
                  ->where($whr)
                  ->get();
            return $users;
      }
      
      function get_company()
      {

            $users = DB::table('company')
                  ->select(DB::raw('id,company_name'))
                  ->get();
            return $users;
      }
      
      

      function flow_question($flow_id)
      {

            $users = DB::table('flow')
                  ->join('questions', 'flow.flow_id', '=', 'questions.flow_id', 'inner')
                  ->select(DB::raw('*'))
                  ->where('flow.flow_id',$flow_id)
                  ->get();
            return $users;
      }
      
      function create_flow_questions($whereCase = array())
      {
            $flow_questions = DB::table('datasets')
                    ->join('questions', 'questions.save_response_to', '=', 'datasets.id', 'inner')
                  ->select(DB::raw('questions.question_id,questions.question,sub_category'))
                  ->where($whereCase)
                  ->orderBy('sub_category')
                  ->get();
            return $flow_questions;
      }

}
