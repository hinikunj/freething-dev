<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\LoginModel;
use App\ProductsModel;
use App\FlowModel;
use App\DatasetModel;
use Session;
use Illuminate\Support\Facades\Validator;
use Config;

class ProductController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    function list() {
        $av = new LoginModel();
        $res = $av->get_products();
        $location = $av->simple_select('currency', array('country', 'code', 'symbol'));

        if (count($res) > 0) {
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['list'] = $res;
        } else {
            $returnData['success'] = false;
            $returnData['code'] = 402;
            $returnData['description'] = 'No Matching Records.';
        }
        $returnData['location'] = $location;
        $returnData['flows'] = FlowModel::select('flow_id', 'flow_name','questions')->where('is_published', '=', 1)->orderBy('flow_id', 'DESC')->get()->all();
        $data_captures = DatasetModel::select('sub_category')->where('category', '!=', 'Settings')->whereIn('sub_category', array('First Name', 'Phone number', 'Last name'))->orderBy('id', 'DESC')->get()->toArray();
        $returnData['data_capture'] = implode(',', array_column($data_captures, 'sub_category'));

        return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('product/sp-products', $returnData) . view('product/modals', $returnData) . view('common/footer');
    }

    function add_product() {
        $data = $_POST;
        $av = new LoginModel();
        $data['category'] = implode(',', $data['category']);
        $data['type'] = implode(',', $data['type']);
        $data['shipping_by'] = implode(',', $data['shipping_by']);
        $data['attach'] = ($data['attach'] == '') ? '1.png' : $data['attach'];
        $data['created_date'] = date('Y-m-d H:i:s');
        $inserted_id = $av->add_data('products', $data);
        if ($inserted_id > 0) {
            if (!empty($data['odds'])) {
                $oddRatio = explode(':', $data['odds']);
                if (!empty($oddRatio[1])) {
                    $select = ['code'];
                    $whereCase['product_id'] = $inserted_id;
                    $existCodes = $av->select_data_common('odd_codes', $select, $whereCase);
                    $code_arr = array_map(function($e) {
                        return is_object($e) ? $e->code : $e['code'];
                    }, $existCodes);

                    for ($i = 0; $i < $oddRatio[1]; $i++) {
                        $codename = substr(str_shuffle(str_repeat("0123456789ABCDEFGHIJKLMNOPQRSTYUVWXYZ", 6)), 0, 5);
                        if (!in_array(trim($codename), $code_arr)) {
                            $parsed_val['product_id'] = $inserted_id;
                            $parsed_val['code'] = trim($codename);
                            $parsed_val['created_at'] = date('Y-m-d H:i:s');
                            $insertArr[] = $parsed_val;
                        } else {
                            $i--; //because of duplicate value
                        }
                    }
                }
                $inserted_id = $av->add_data('odd_codes', $insertArr, true);
            }
        }
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Product Added Successfully.';
        return json_encode($returnData);
    }

    function upload_product(Request $request) {
        $data = $_POST;
        $file = $request->file('file');

        // $validator = Validator::make(
        //      array('file' => $request,
        //      ),
        //    array(
        //         'file' => 'dimensions:min_width=1000,min_height=1000',
        //     )
        // );
        //  if ($validator->fails())
        // {
        //   $returnData['success'] = false;
        //   $returnData['code'] = 402;
        //   $messages = $validator->messages();
        //   foreach ($messages->all() as $message)
        //   {
        //     $returnData['success'] = false;
        //     $returnData['code'] = 402;
        //     $returnData['description'] = $message;
        //     return json_encode($returnData);
        //   }
        // }
        //Move Uploaded File
        $destinationPath = 'resources/assets/uploads';
        $timeupload = time();
        $file->move($destinationPath, $timeupload . $file->getClientOriginalName());
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['filename'] = $timeupload . $file->getClientOriginalName();
        return json_encode($returnData);
    }

    function search_company(Request $request) {
        $company = $request->input('company');
        $av = new LoginModel();
        $whr['company_name'] = $company;
        $res = $av->search_company('company', $whr);

        if (count($res) > 0) {
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['list'] = $res;
        } else {
            $returnData['success'] = false;
            $returnData['code'] = 402;
            $returnData['description'] = 'No Matching Records.';
        }
        return json_encode($returnData);
    }

    /*     * **************************************************************** */

    /// ADD LOCATION IN ADD PRODUCT PAGE ///////////////////////////////
    /*     * **************************************************************** */

    function add_location(Request $request) {

        $data = $_POST;
        $av = new LoginModel();

        /* VALIDATION START */
        $validator = Validator::make(
                        array('location' => $request->input('location'),
                            'currency' => $request->input('currency'),
                            'symbol' => $request->input('symbol'),
                        ),
                        array(
                            'location' => 'required',
                            'currency' => 'required',
                            'symbol' => 'required',
                        )
        );
        if ($validator->fails()) {
            $returnData['success'] = false;
            $returnData['code'] = 402;
            $messages = $validator->messages();
            foreach ($messages->all() as $message) {
                $returnData['description'] = $message;
                return json_encode($returnData);
            }
        }
        /* VALIDATION END */

        $data['created_date'] = Config::get('constants.current_datetime');
        $data['modified_date'] = Config::get('constants.current_datetime');
        $count = $av->select_data_count('location', array('location' => $data['location']));
        if ($count[0]->count > 0) {
            $returnData['success'] = false;
            $returnData['code'] = 402;
            $returnData['description'] = 'Location already present.';
        } else {
            $av->add_data('location', $data);
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['description'] = 'Location Added Successfully.';
        }

        return json_encode($returnData);
    }

    /*     * **************************************************************** */

    ///GET LOCATIONS ///////////////////////////////
    /*     * **************************************************************** */

    function get_locations(Request $request) {

        $av = new LoginModel();
        $res = $av->get_locations();

        if (count($res) > 0) {
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['list'] = $res;
        } else {
            $returnData['success'] = false;
            $returnData['code'] = 402;
            $returnData['description'] = 'No Matching Records.';
        }
        return json_encode($returnData);
    }

    /*     * **************************************************************** */

    ///GET Product Detail ///////////////////////////////
    /*     * **************************************************************** */

    function get_product_detail(Request $request, $id) {
        $symbol['inr'] = 'fa fa-inr';
        $symbol['eur'] = 'fa fa-eur';
        $symbol['usd'] = 'fa fa-usd';
        $symbol['ruble'] = 'fa fa-ruble';
        $symbol['cny'] = 'fa fa-cny';

        $av = new LoginModel();
        $whr['id'] = $id;
        $res = $av->get_product_detail($whr);

        $locations = $av->get_locations(); // GET LOCATIONS

        if (count($res) > 0) {
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['list'] = $res;
            $returnData['locations'] = $locations;
            $returnData['symbol'] = $symbol;
        } else {
            $returnData['success'] = false;
            $returnData['code'] = 402;
            $returnData['description'] = 'No Matching Records.';
        }
        $returnData['flows'] = FlowModel::select('flow_id', 'flow_name','questions')->where('is_published', '=', 1)->orderBy('flow_id', 'DESC')->get()->all();
        $data_captures = DatasetModel::select('sub_category')->where('category', '!=', 'Settings')->whereIn('sub_category', array('First Name', 'Phone number', 'Last name'))->orderBy('id', 'DESC')->get()->toArray();
        $returnData['data_capture'] = implode(',', array_column($data_captures, 'sub_category'));

        return json_encode($returnData);
    }

    /*     * **************************************************************** */

    ///Delete Product ///////////////////////////////
    /*     * **************************************************************** */

    function delete_product(Request $request, $id) {
        $av = new LoginModel();
        $data['status'] = 0;
        $data['delete_by'] = $request->session()->get('user_id');
        $whr['id'] = $id;
        $res = $av->update_data('products', $data, $whr);
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Product has been deactivated.';
        return json_encode($returnData);
    }

    /*     * **************************************************************** */

    ///Update Product ///////////////////////////////
    /*     * **************************************************************** */

    function update_product() {
        $data = $_POST;
        $av = new LoginModel();
        if (array_key_exists('category', $data)) {
            $data['category'] = implode(',', $data['category']);
        }
        if (array_key_exists('type', $data)) {
            $data['type'] = implode(',', $data['type']);
        }
        if (array_key_exists('shipping_by', $data)) {
            $data['shipping_by'] = implode(',', $data['shipping_by']);
        }

        $data['created_date'] = date('Y-m-d H:i:s');
        $whr['id'] = $data['id'];
        $av->update_data('products', $data, $whr);

        if (!empty($data['odds'])) {

            $oddRatio = explode(':', $data['odds']);
            if (!empty($oddRatio[1])) {
                $select = ['code'];
                $whereCase['product_id'] = $data['id'];
                $existCodes = $av->select_data_common('odd_codes', $select, $whereCase);
                $code_arr = array_map(function($e) {
                    return is_object($e) ? $e->code : $e['code'];
                }, $existCodes);

                if (!empty($existCodes) && count($existCodes) == $oddRatio[1]) {
                    // no change request.
                } else {
                    //delete existing rows
                    $av = new LoginModel();
                    $av->delete_data('odd_codes', $whereCase);

                    //inserting new row
                    for ($i = 0; $i < $oddRatio[1]; $i++) {
                        $codename = substr(str_shuffle(str_repeat("0123456789ABCDEFGHIJKLMNOPQRSTYUVWXYZ", 6)), 0, 5);
                        if (!in_array(trim($codename), $code_arr)) {
                            $parsed_val['product_id'] = $data['id'];
                            $parsed_val['code'] = trim($codename);
                            $parsed_val['created_at'] = date('Y-m-d H:i:s');
                            $insertArr[] = $parsed_val;
                        } else {
                            $i--; //because of duplicate value
                        }
                    }
                    $av->add_data('odd_codes', $insertArr, true);
                }
            }
        }

        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Product Updated Successfully.';
        return json_encode($returnData);
    }

    function delete_product_image(Request $request, $id) {
        $av = new LoginModel();
        $data['modified_date'] = date('Y-m-d H:i:s');
        $data['attach'] = $request->attach;
        $whr['id'] = $id;
        $imageName = $request->image;
        $res = $av->update_data('products', $data, $whr);
        unlink(url('/') . '/resources/assets/uploads/' . $imageName);
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Product has been deactivated.';
        return json_encode($returnData);
    }

    public function destroy(Request $request) {
        $file_path = './resources/assets/uploads/' . $request->name; #public_path('uploads/files/'.$id->filename);
        #$thumb = public_path('uploads/files/thumb_'.$id->filename);
        if ($request->name) {
            echo $file_path; //die;
            // Check if file is exists
            if (file_exists($file_path)) {

                // Delete the file
                unlink($file_path);

                // Be sure we deleted the file
                if (!file_exists($file_path)) {
                    $response = array(
                        'status' => 'success',
                        'info' => 'Successfully Deleted.'
                    );
                    /* Update DB table */
                    if (isset($request->prod_id) && $request->prod_id != '') {
                        $existProduct = DB::table('products')
                                        ->select(DB::raw('id,attach'))
                                        ->where('id', '=', $request->prod_id)
                                        ->get()->first();
                        if (!empty($existProduct)) {
                            $updateArr['modified_date'] = date('Y-m-d H:i:s');
                            $prodListImg = explode(',', $existProduct->attach);
                            $index = array_search($request->name, $prodListImg);
                            if ($index !== false) {
                                unset($prodListImg[$index]);
                            }
                            $prodListImg = implode(',', $prodListImg);
                            $updateArr['attach'] = $prodListImg;
                            DB::table('products')->where("id", '=', $request->prod_id)->update($updateArr);
                        }
                    }
                } else {
                    // Check the directory's permissions
                    $response = array(
                        'status' => 'error',
                        'info' => 'We screwed up, the file can\'t be deleted.'
                    );
                }
            } else {
                // Something weird happend and we lost the file
                $response = array(
                    'status' => 'error',
                    'info' => 'Couldn\'t find the requested file :('
                );
            }
            echo json_encode($response);
            exit;
        }
    }
    
    
    function get_datasset(Request $request) {
        $datasets = [];
        $queList = explode(',', $request->question_ids);
        if(!empty($queList)){
            $datasets = DatasetModel::select('datasets.sub_category')
                        ->join('questions', 'datasets.id', '=', 'questions.save_response_to')
                        ->whereIn('questions.question_id', $queList)->get()->toArray();
        }
        
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['data'] = implode(',',array_column($datasets, 'sub_category'));
        return json_encode($returnData);
    }

}
