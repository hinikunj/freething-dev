<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WinnersModel;
use App\LoginModel;

class WinnersController extends Controller {
    /*     * **************************************************************** */

    ///Winners work done by Nikunj Dhimar ///////////////////////////////
    /*     * **************************************************************** */

    protected $av;

    public function __construct() {
        $this->av = new LoginModel();
    }

    public function index(Request $request) {

        //$subscribers = subscribers::where('deleted_at', '=', NULL)->where('active', '=', 1) ->get(); 
        //$data['subscribers'] = $subscribers;
        $winners = WinnersModel::where('deleted_at', '=', NULL)->get();
        foreach ($winners as $key => $product) {
            $whr['id'] = $product->product_id;
            $whr['select'] = 'name,quantity,displayed_price,currency,shipping_by';
            $productdetails = $this->av->get_product_detail($whr)->first();
            $data['product'][] = $productdetails;
        }
        $winners->symbol = $request->session()->get('symbols');
        $data['winners'] = $winners;
        $header['title'] = 'Winners';
        return view('common/header', $header) . view('common/inner-top') . view('common/sp-sidenav') . view('winners/sp-winner', $data) . view('common/footer');
    }

    public function get_winners(Request $request) {
        if (isset($request->camp_id) && isset($request->prod_id)) {
            $existWinners = WinnersModel::select('winning_id', 'first_name', 'company_name', 'flow_number')
                    ->where('campaign_id', '=', $request->camp_id)
                    ->where('product_id', '=', $request->prod_id)
                    ->get();
            if (!empty($existWinners->first())) {
                $returnData['status'] = true;
                $returnData['winData'] = $existWinners;
                return (json_encode($returnData));
                exit;
            }
        }
        $returnData['status'] = false;
        return (json_encode($returnData));
    }
    public function get_countdown_time(Request $request){
        $returnData['status'] = true;
        $res = array();
        foreach($request->current_timer as $key => $v){
            $cid = $v['cid'];
            $pid = $v['pid'];
            $sdate = $v['sdate'];
            $freq = $v['freq'];
            $every = $v['every'];
            $last_winnder = WinnersModel::select('created_at')
                    ->where('campaign_id', '=', $cid)
                    ->where('product_id', '=', $pid)
                    ->where('is_winner', '=', '1')
                    ->orderBy('created_at', 'desc')
                    ->first();   
            $res[$key]["id"] = $v['id'];
            if(!empty($last_winnder->created_at)){
                $res[$key]["is_countdown"] = "1";
                $res[$key]["date1"] = date("Y-m-d H:i:s",strtotime($last_winnder->created_at));
                $res[$key]["date2"] = date("Y-m-d H:i:s",strtotime("-{$every} {$freq}"));
                if(strtotime($last_winnder->created_at)>strtotime("-{$every} {$freq}")){
                    $res[$key]["last_time"] = date("M j, Y H:i:s",strtotime("+{$every} {$freq}".$last_winnder->created_at));
                    $res[$key]["last_time2"] = date("Y-m-d H:i:s",strtotime("+{$every} {$freq}".$last_winnder->created_at));
                    $res[$key]["is_countdown"] = "1";
                }else {
                    $res[$key]["is_countdown"] = "0";
                }
            }else{
                $res[$key]["is_countdown"] = "0";
            }
        }
        $returnData['winData'] = $res;
        //$returnData['winData'] = $request->current_timer;
        return (json_encode($returnData));
    }

}
