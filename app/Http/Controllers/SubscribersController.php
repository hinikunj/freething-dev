<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\subscribers;
use App\SettingsModel;
use App\DatasetModel;
use DB;

class SubscribersController extends Controller
{
    protected $dataset_m;
    
    public function __construct() {
        $this->dataset_m = new DatasetModel();
    }
    
    public function index(Request $request) {
        
        $subscribers = subscribers::where('deleted_at', '=', NULL)->where('active', '=', 1) ->get(); 
        $data['subscribers'] = $subscribers;
        $settings = SettingsModel::where('deleted_at', '=', NULL)
                ->where('user_id', '=', $request->session()->get('user_id'))->orderBy('id', 'desc') ->get()->first(); 
        $data['settings'] = $settings;
        $header['title'] = 'Subscribers';
        return view('common/header',$header) . view('common/inner-top') . view('common/sp-sidenav') . view('dataset/subscriber', $data) . view('common/footer');
    }
    
    public function profile(Request $request, $id) {
        /*Set Goal Works starts here*/
        $catArr = array();
        $datasetCategories = $this->dataset_m->get_category();
        foreach($datasetCategories as $key => $val){
            //$catArr[] = $val->category;
                /*Adding subcategory*/
            if($val->category != 'Settings'){ //Skipping settings as there were for admin use
                $catWhr['category'] = $val->category;
                $datasetSubCategories = $this->dataset_m->get_subcategory($catWhr);
                if(!empty($datasetSubCategories)){
                    foreach($datasetSubCategories as $k => $v){
                        $catArr[$val->category][] = $v->sub_category;
                        /*$selectColumn = str_replace(' ', '_', $v->sub_category);
                        if($selectColumn == 'Phone_number')
                            $selectColumn = 'phone';
                        try{
                            $existSubFeild = subscribers::select($selectColumn)->where('id','=',$id)->get()->first();
                        } catch (Exception $e){
                            $existSubFeild = false;
                        }
                        $catArr[$v->sub_category][] = $existSubFeild->$selectColumn;*/
                    }
                }
            }
        }
        $data['setGoal'] = $catArr;
        /*Check existing goal set by user*/
        $subscriber = subscribers::where('id','=',$id)->get()->first();
        $data['subscriber'] = $subscriber;
        $header['title'] = 'Subscriber Profile';
        
        /* Check existing goal set by user */
        $whrGoal = ['user_id' => $request->session()->get('user_id'), 'active' => 1];
        $goalRow = DB::table('goals')->select(DB::raw('set_goals'))->where($whrGoal)->get();
        if (!empty($goalRow->first())) {
            $data['existGoal'] = $goalRow->first();
        } else {
            $data['existGoal'] = array();
        }
        
        return view('common/header',$header) . view('common/inner-top') . view('common/sp-sidenav') . view('dataset/subscriber-profile', $data) . view('common/footer');
    }
}
