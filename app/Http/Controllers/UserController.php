<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*

  Register : index
  Login : login
  Sign out : sign_out

 */

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\LoginModel;
use App\SettingsModel;
use App\DatasetModel;
use App\QuestionModel;
use Session;
use Illuminate\Support\Facades\Validator;
use Mail;
use Config;
use Stripe;
use Helper;
use Twilio\Rest\Client;

class UserController extends Controller {

    protected $av;
    protected $setting_m;
    protected $dataset_m;

    public function __construct() {
        $this->av = new LoginModel();
        $this->setting_m = new SettingsModel();
        $this->dataset_m = new DatasetModel();

        $this->sid = 'AC641053c311414250aaad5ea28524ad0f';#getenv('TWILIO_ACCOUNT_SID');
        $this->token = '182426241c5a965fcccc9559ea190a3e';#getenv('TWILIO_AUTH_TOKEN');

        /** Initialize the Twilio client so it can be used */
        $this->twilio = new Client($this->sid, $this->token);
    }

    function admin_dashboard(Request $request) {
        $userModel = new User();
        $campdetails = $userModel->campaign_details();
        $data['campaign_detail'] = $campdetails->all();
        if (!empty($campdetails)) {
            foreach ($campdetails as $k => $v) {
                $productDetails = (json_decode($v->product_details));
                foreach ($productDetails as $key => $val) {
                    $whr['id'] = $val->id;
                    $whr['select'] = 'id,name,quantity,displayed_price';
                    $product = $this->av->get_product_detail($whr)->first();

                    /* get countdown row */
                    $whrCount['campaign_id'] = $v->id;
                    $whrCount['product_id'] = $val->id;
                    $whrCount['deleted_at'] = NULL;
                    $select = ['countdown.*'];
                    $savedCountDown = $this->av->select_data_common('countdown', $select, $whrCount);

                    #$product->countdown_id = (!empty($savedCountDown)) ? $savedCountDown[0]->id : 0;
                    #$product->startdate = (!empty($savedCountDown)) ? $savedCountDown[0]->startdate : false;
                    if (!empty($savedCountDown)) {
                        $data['campaign_detail'][$k]->countdown[$key] = $savedCountDown[0];
                    } else {
                        unset($data['campaign_detail'][$k]->countdown[$key]);
                    }
                    /**/

                    $product->winning_freq = $val->winning_freq;
                    $product->winning = $val->winning;
                    $data['campaign_detail'][$k]->productdetails[] = $product;
                }
            }
        }
        $header['title'] = 'Admin';
        return view('common/header', $header) . view('common/inner-top') . view('common/sp-sidenav') . view('admin/sp-dashboard', $data) . view('common/footer');
    }

    function dashboard(Request $request) {

        $whr['user_id'] = $request->session()->get('user_id');
        $whr['deleted_at'] = NULL;
        $campdetails = $this->av->checkout_process($whr);

        $userWhr['id'] = $request->session()->get('user_id');
        $userdetails = $this->av->user_details($userWhr)->first();
        $userdetails->symbol = $request->session()->get('symbols');

        unset($whr['deleted_at']);
        $campReport = $this->av->checkout_process($whr);
        $data['campaign_detail'] = $campdetails;
        $data['campaign_report'] = $campReport;
        $data['userdetails'] = $userdetails;

        /* Set Goal Works starts here */
        $catArr = array();
        $datasetCategories = $this->dataset_m->get_category();
        foreach ($datasetCategories as $key => $val) {
            //$catArr[] = $val->category;
            /* Adding subcategory */
            if ($val->category != 'Settings') { //Skipping settings as there were for admin use
                $catWhr['category'] = $val->category;
                $datasetSubCategories = DatasetModel::select('id', 'sub_category', 'is_default')->where('category', '=', $val->category)->get();
                if (!empty($datasetSubCategories)) {
                    foreach ($datasetSubCategories as $k => $v) {
                        $questions = QuestionModel::where('save_response_to', $v->id)->get();
                        if (isset($questions[0]->question_id))
                            $questionID = $questions[0]->question_id;
                        else
                            $questionID = 0;
                        $catArr[$val->category][] = $v->sub_category . '::' . $v->id . '::' . $questions->count() . '::' . $questionID . '::' . $v->is_default;
                    }
                }
            }
        }
        /* Check existing goal set by user */
        $whrGoal = ['user_id' => $request->session()->get('user_id'), 'active' => 1];
        $goalRow = DB::table('goals')->select(DB::raw('id,set_goals,questions'))->where($whrGoal)->get();
        if (!empty($goalRow->first())) {
            $data['existGoal'] = $goalRow->first();
        } else {
            $data['existGoal'] = array();
        }
        $data['setGoal'] = $catArr;
        /* Set Goal Works ends here */
        return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('user/dashboard', $data) . view('common/footer');
    }

    function all_product_list(Request $request) {
        $countOrders = $this->av->cart_count(array('status' => 0, 'user_id' => $request->session()->get('user_id')));
        $whr['user_id'] = $request->session()->get('user_id');
        $whr['status'] = 0;
        $cartdetails = $this->av->cart($whr);
        /* Filter */
        if ($request->isMethod('post')) {

            $w['location'] = ($request->session()->get('location') == '') ? null : $request->session()->get('location');
            $w['company'] = ($request->session()->get('company') == '') ? null : $request->session()->get('company');
            $w['category'] = $_POST['typefilter'];
            $res = $this->av->get_products_foruser_filter($w);
            /* Filter end */
        } else {
            $whr['location'] = ($request->session()->get('location') == '') ? '' : $request->session()->get('location');
            $whr['company'] = ($request->session()->get('company') == '') ? '' : $request->session()->get('company');

            $res = $this->av->get_products_foruser($whr);
        }


        // echo "<pre>";print_r($res);
        if (count($res) > 0) {
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['list'] = $res;
            $returnData['cartcount'] = $countOrders[0]->count;
            $returnData['cart_details'] = $cartdetails;
        } else {
            $returnData['success'] = false;
            $returnData['code'] = 402;
            $returnData['description'] = 'No Matching Records.';
            $returnData['cartcount'] = 0;
            $returnData['cart_details'] = $cartdetails;
        }
        return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('user/checkout-process', $returnData) . view('product/modals') . view('common/footer');
    }

    function add_to_cart($id, Request $request) {
        $data['product_id'] = $id;
        $data['odds'] = $request->input('odds');
        $data['winning_freq'] = $request->input('winning_freq');
        $data['user_id'] = $request->session()->get('user_id');
        $data['created_date'] = Config::get('constants.current_datetime');
        $cartcount = $this->av->cart_count(array('user_id' => $data['user_id'], 'product_id' => $id, 'status' => 0));
        if ($cartcount[0]->count > 0) {
            $returnData['success'] = false;
            $returnData['code'] = 402;
            $returnData['description'] = 'Product already added in cart.';
            print_r(json_encode($returnData));
            die;
        }
        $rescount = $this->av->cart_count(array('user_id' => $data['user_id'], 'status' => 0));

        if ($rescount[0]->count < 5) {
            $this->av->add_data('cart', $data);
            $whr1['user_id'] = $request->session()->get('user_id');
            $whr1['status'] = 0;
            $cartdetails = $this->av->cart($whr1);
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['description'] = 'Added to cart';
            $returnData['cartdetails'] = $cartdetails;
            print_r(json_encode($returnData));
        } else {
            $whr1['user_id'] = $request->session()->get('user_id');
            $whr1['status'] = 0;
            $cartdetails = $this->av->cart($whr1);
            $returnData['success'] = false;
            $returnData['code'] = 402;
            $returnData['description'] = 'You can add upto 5 products only.';
            $returnData['cartdetails'] = $cartdetails;
            print_r(json_encode($returnData));
        }
    }

    function cart(Request $request) {
        $whr['user_id'] = $request->session()->get('user_id');
        $whr['payment_status'] = 1;

        $countOrders = $this->av->cart_count(array('status' => 0, 'user_id' => $request->session()->get('user_id')));
        if ($countOrders[0]->count == 0) {
            $returnData['onetimecost'] = 14;
        } else {
            $returnData['onetimecost'] = 0;
        }
        unset($whr['payment_status']);
        $res = $this->av->cart($whr);
        if (count($res) > 0) {
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['list'] = $res;
            $returnData['cartcount'] = $countOrders[0]->count;
        } else {
            $returnData['success'] = false;
            $returnData['code'] = 402;
            $returnData['description'] = 'Cart is empty.';
            $returnData['cartcount'] = 0;
        }

        // echo "<pre>"; print_r($res);
        $header['title'] = 'Cart';
        return view('common/header', $header) . view('common/inner-top') . view('common/sp-sidenav') . view('user/cart', $returnData) . view('product/modals') . view('common/footer');
    }

    function checkout_process(Request $request) {
        $id = DB::select("SHOW TABLE STATUS LIKE 'checkout_process'");
        $next_id = $id[0]->Auto_increment;
        $campID = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 5);
        for ($i = 0; $i < count($_POST['id']); $i++) {
            $arr[$i]['id'] = $_POST['id'][$i];
            $arr[$i]['price'] = $_POST['price'][$i];
            $arr[$i]['winning_freq'] = $_POST['winning_freq'][$i];
            $arr[$i]['shipping_cost'] = $_POST['shipping_cost'][$i];
            $arr[$i]['winning'] = $_POST['winning'][$i];
            $arr[$i]['today'] = $_POST['today'][$i];
            if (isset($_POST['coupon_image'][$i]))
                $arr[$i]['coupon_image'] = $_POST['coupon_image'][$i];
        }
        $data['product_details'] = json_encode($arr);
        $data['onetimecost'] = $_POST['onetimecost'];
        $data['totalusd'] = $_POST['totalusd'];
        $data['totalcharges'] = $_POST['totalcharges'];
        $data['created_date'] = Config::get('constants.current_datetime');
        $data['campaign_id'] = $campID . $next_id;
        $data['user_id'] = $request->session()->get('user_id');
        $this->av->add_data('checkout_process', $data);
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'complete';
        print_r(json_encode($returnData));
    }

    function checkout(Request $request) {
        $w['status'] = 1;
        $fields = array('id', 'plan', 'details', 'sum', 'totalsms', 'perprice');
        $plans = $this->av->select_c('plans', $fields, $w);

        // foreach ($plans as $key => $value) {
        // 	$currency = $request->session()->get('currency');
        // 	$priceRes =  Helper::getcurrency($currency,$value->perprice);
        // 	// $plansArr[$key] = $value->$key
        // 	$plansArr[$key]['id'] = $value->id;
        // 	$plansArr[$key]['plan'] = $value->plan;
        // 	$plansArr[$key]['details'] = $value->details;
        // 	$plansArr[$key]['sum'] = $value->sum;
        // 	$plansArr[$key]['totalsms'] = $value->totalsms;
        // 	$plansArr[$key]['usdprice'] = $value->perprice;
        // 	$plansArr[$key]['price'] = $priceRes;
        // 	$plansArr[$key]['symbol'] =$request->session()->get('symbols');
        // }
        // echo "<pre>";print_r($plansArr);

        $whr['user_id'] = $request->session()->get('user_id');
        $whr['status'] = 0;
        $data['res'] = $this->av->checkout_process($whr);
        $data['card_details'] = $this->av->select_c('card_details', '*', array('user_id' => $request->session()->get('user_id')));
        $data['plans'] = $plans;


        $request->session()->put('amount', $data['res'][0]->totalusd);
        $request->session()->put('order_id', $data['res'][0]->id);

        return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('user/checkout', $data) . view('product/modals') . view('common/footer');
    }

    function stripe(Request $request) {
        // if(array_key_exists('save', $_POST))
        // {
        $data['card_number'] = $_POST['card-number'];
        $data['card_expiry_month'] = $_POST['card-expiry-month'];
        $data['card_expiry_year'] = $_POST['card-expiry-year'];
        $data['name'] = $_POST['name'];
        $data['user_id'] = $request->session()->get('user_id');
        $data['stripeToken'] = $_POST['stripeToken'];
        $data['created_date'] = Config::get('constants.current_datetime');
        $data['modified_date'] = Config::get('constants.current_datetime');
        $data['amount'] = $request->session()->get('amount');
        $this->av->add_data('card_details', $data);
        // }

        $whr['user_id'] = $request->session()->get('user_id');
        $updateArr['payment_status'] = 1;
        $updateArr['payment_date'] = Config::get('constants.current_datetime');
        ;
        $updateArr['status'] = 1;
        if (array_key_exists('plan_no', $_POST)) {

            $insertArr['user_id'] = $request->session()->get('user_id');
            $insertArr['plan'] = $_POST['plan_no'];
            $insertArr['created_date'] = Config::get('constants.current_datetime');
            $this->av->add_data('user_plans', $insertArr);

            $updateArr['card_id'] = $_POST['card_id'];
        }


        $this->av->update_data('cart', array('status' => 1), $whr);
        $this->av->update_data('checkout_process', $updateArr, array('id' => $request->session()->get('order_id')));
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create([
            "amount" => $request->session()->get('amount') * 100,
            "currency" => "usd",
            "source" => $request->stripeToken,
            "description" => "Test payment from techstersweb.com."
        ]);

        Session::flash('success', 'Payment successful!');
        return redirect()->action(
                        'UserController@settings'
        );

        // return back();
    }

    function payment(Request $request) {


        $smsurl = url('/') . "/incomingsms";

        $w['id'] = $request->session()->get('user_id');

        $fields = array('country_code', 'twilio_number');

        $userinfo = $this->av->select_c('login', $fields, $w);

        foreach ($userinfo as $key => $value) {

            $country_code = $value->country_code;
            $existingtwilio_number = $value->twilio_number;
        }
        if ($existingtwilio_number == '') {

            $numbers = $this->twilio->availablePhoneNumbers($country_code)->local->stream();

            $phone = '';

            foreach ($numbers as $number) {

                $phone = $number->phoneNumber;
                break;
            }

            if ($phone != '') {

                $incoming_phone_number = $this->twilio->incomingPhoneNumbers->create(["phoneNumber" => $phone, "SmsUrl" => $smsurl]);

                if ($incoming_phone_number->sid) {

                    $whr['id'] = $request->session()->get('user_id');

                    $this->av->update_data('login', array('twilio_number' => $phone), $whr);
                }
            }
        }

// echo "dsffd".$request->session()->get('amount');die;
        $whr['user_id'] = $request->session()->get('user_id');
        $updateArr['payment_status'] = 1;
        $updateArr['payment_date'] = Config::get('constants.current_datetime');
        $updateArr['status'] = 1;
        if (array_key_exists('plan_no', $_POST)) {
            $insertArr['user_id'] = $request->session()->get('user_id');
            $insertArr['plan'] = $_POST['plan_no'];
            $insertArr['created_date'] = Config::get('constants.current_datetime');
            if (array_key_exists('typeofend', $_POST)) {
                $insertArr['type'] = $_POST['typeofend'];
            }

            $this->av->add_data('user_plans', $insertArr);
            $updateArr['card_id'] = $_POST['card_id'];
            $updateArr['totalusd'] = $_POST['totalusd'];
        }



        $this->av->update_data('cart', array('status' => 1), $whr);
        $this->av->update_data('checkout_process', $updateArr, array('id' => $request->session()->get('order_id')));
        $amount = ($request->session()->get('amount') != '') ? $request->session()->get('amount') : $_POST['billingtotal'];

        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create([
            "amount" => $amount * 100,
            "currency" => "usd",
            "source" => $request->stripeToken,
            "description" => "Test payment from techstersweb.com."
        ]);

        Session::flash('success', 'Payment successful!');
        return redirect()->action(
                        'UserController@settings'
        );
    }

    function settings(Request $request) {
        $header['title'] = 'Settings';
        $data['camp_id'] = $request->session()->get('order_id');
        return view('common/header', $header) . view('common/inner-top') . view('common/sp-sidenav') . view('user/settings', $data) . view('product/modals') . view('common/footer');
    }

    public function settings_save(Request $request) {

        $insertArr = array(
            'user_id' => $request->session()->get('user_id'),
            'campaign_id' => $request->camp_id,
            'unique_link' => $request->unique_link,
            'net_promoter_score' => $request->net_promoter_score,
            'second_chance' => $request->second_chance,
            'play_reminder' => $request->play_reminder,
            'campaign_reminder' => $request->campaign_reminder,
            'get_discover_color' => $request->get_discover_color,
            'get_discover_text' => $request->get_discover_text,
            'facebook_link' => $request->facebook_link,
            'instagram_link' => $request->instagram_link,
            'twitter_link' => $request->twitter_link,
            'linked_link' => $request->linked_link,
            'store_promo_slider' => (empty($request->attach) || $request->attach == '') ? '1.png' : $request->attach, #$request->store_promo_slider,
            'store_name' => $request->store_name,
            'store_url' => $request->store_url,
            'terms_condition' => $request->terms_condition,
            'created_at' => date('Y-m-d H:i:s'),
        );

        #$input = $request->all();

        /* $validator = Validator::make($input, [
          'title' => 'required|unique:channels,title',
          ]);

          if($validator->fails()){
          return $this->sendError('Validation Error.', $validator->errors());
          } */

        $this->setting_m->add_data('settings', $insertArr);

        Session::flash('success', 'Settings saved successful!');
        return redirect()->action(
                        'UserController@complete'
        );
    }

    function upload_store_image(Request $request) {
        $file = $request->file('file');
        //Move Uploaded File
        $destinationPath = 'resources/assets/uploads';
        $timeupload = time();
        $file->move($destinationPath, $timeupload . $file->getClientOriginalName());
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['filename'] = $timeupload . $file->getClientOriginalName();
        return json_encode($returnData);
    }

    function complete(Request $request) {
        $header['title'] = 'Complete';
        $data['user_id'] = $request->session()->get('user_id');
        $data['camp_id'] = $request->session()->get('order_id');
        return view('common/header', $header) . view('common/inner-top') . view('common/sp-sidenav') . view('user/complete', $data) . view('common/footer');
    }

    function payment_done() {
        return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('user/payment_done') . view('product/modals') . view('common/footer');
    }

    /*     * **************************************************************** */

    ///Delete cart Product ///////////////////////////////
    /*     * **************************************************************** */

    function delete_cart_product(Request $request, $id) {
        $av = new LoginModel();
        $whr['product_id'] = $id;
        $whr['user_id'] = $request->session()->get('user_id');
        ;
        $res = $this->av->delete_data('cart', $whr);
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Product has been deleted from the cart.';
        return json_encode($returnData);
    }

    function getsymbol(Request $request, $price) {
        $request->session()->put('plan_amount', $price);
        $currency = $request->session()->get('currency');
        $priceRes = Helper::getcurrency($currency, $price);
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['price'] = $priceRes;
        return json_encode($returnData);
    }

    function plan_details(Request $request, $plan) {
        $w['status'] = 1;
        $w['plan'] = $plan;
        $fields = array('id', 'plan', 'details', 'sum', 'totalsms', 'perprice', 'featured');
        $plans = $this->av->select_c('plans', $fields, $w, '1');


        foreach ($plans as $key => $value) {
            $currency = $request->session()->get('currency');
            $priceRes = Helper::getcurrency($currency, $value->perprice);
            $sumRes = Helper::getcurrency($currency, $value->sum);

            // $plansArr[$key] = $value->$key
            $plansArr[$key]['id'] = $value->id;
            $plansArr[$key]['plan'] = $value->plan;
            $plansArr[$key]['details'] = $value->details;
            $plansArr[$key]['usdsum'] = $value->sum;
            $plansArr[$key]['sum'] = $sumRes;
            $plansArr[$key]['totalsms'] = $value->totalsms;
            $plansArr[$key]['usdprice'] = $value->perprice;
            $plansArr[$key]['price'] = $priceRes;
            $plansArr[$key]['symbol'] = $request->session()->get('symbols');
            $plansArr[$key]['fixprice'] = $value->featured;
        }

        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['plan'] = $plansArr;
        return json_encode($returnData);
    }

    public function fileUpload(Request $request) {

        $input = $request->all();
        $validator = Validator::make($input, [
                    'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        if ($request->hasFile('file')) {
            $image = $request->file('file');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = 'resources/assets/uploads';
            $image->move($destinationPath, $name);
            return json_encode(array('status' => true, 'code' => 201, 'data' => $name, 'message' => "Image upload Success."));
        } else {
            return json_encode(array('status' => false, 'message' => "Something went wrong!"));
        }
    }

    public function save_goal(Request $request) {
        $questionsIDs = [];
        $data = $request->all();
        $insertArr['user_id'] = $request->session()->get('user_id');
        $insertArr['created_at'] = date('Y-m-d H:i:s');
        foreach ($data as $key => $value) {
            if (strpos($key, 'checkbox-') !== false) {
                $column = str_replace('checkbox-', '', $key);
                $goalArr[$column] = $value;
            }
        }
        $goals = json_encode($goalArr);
        $insertArr['set_goals'] = $goals;
        /* Saving order start here */
        $question_ids = $request->input('question_ids');
        if (!empty($question_ids)) {
            foreach ($question_ids as $que) {
                if (!empty($que)) {
                    if (strpos($que, ',') !== false) {
                        $explode = explode(',', $que);
                        if (!empty($explode)) {
                            foreach ($explode as $exp) {
                                $questionsIDs[] = $exp;
                            }
                        }
                    } else {
                        $questionsIDs[] = $que;
                    }
                }
            }
            $impQueIds = implode(',', $questionsIDs);
        }
        $insertArr['questions'] = $impQueIds;

        $existGoal = DB::table('goals')
                ->select(DB::raw('id'))
                ->where('user_id', '=', $insertArr['user_id'])
                ->where('active', '=', 1)
                ->orderBy('id', 'desc')
                ->get();
        if (!empty($existGoal->first())) {
            $updateArr['updated_at'] = date('Y-m-d H:i:s');
            $updateArr['active'] = 0;
            DB::table('goals')->where("user_id", '=', $insertArr['user_id'])->update($updateArr);
        } #else {
        $this->setting_m->add_data('goals', $insertArr);

        /* Savinng into Flow table as well for twilio reference */
        $insertFlowArr['flow_name'] = "setGoalFlow_" . $insertArr['user_id'];
        $insertFlowArr['questions'] = $impQueIds;
        $insertFlowArr['is_published'] = 1;
        $insertFlowArr['created_at'] = date('Y-m-d H:i:s');
        $this->setting_m->add_data('flow', $insertFlowArr);
        #}

        Session::flash('success', 'Goals saved successful!');
        return redirect()->action('UserController@dashboard');
    }

    public function get_time(Request $request) {
        //$data = $request->all();
        if (isset($request->countdown_id) && $request->countdown_id > 0) {
            $existGoal = DB::table('countdown')
                    ->select(DB::raw('id,campaign_id,product_id,startdate,every_time,frequency,winning_status'))
                    ->where('id', '=', $request->countdown_id)
                    ->get();
            if (!empty($existGoal->first())) {
                $returnData['status'] = true;
                $returnData['campData'] = $existGoal->first();
                return (json_encode($returnData));
                exit;
            }
        } else {
            $returnData['status'] = false;
            return (json_encode($returnData));
        }
    }

    public function set_time(Request $request) {
        //$data = $request->all();
        if (isset($request->demo3) && $request->demo3 > 0) {
            $updateArr['startdate'] = date('Y-m-d', strtotime($request->startdate));
            $updateArr['every_time'] = $request->demo3;
            $updateArr['frequency'] = $request->frequency;
            $updateArr['winning_status'] = ($request->winning_status) ? 1 : 0;
        }

        $existCountDown = DB::table('countdown')
                ->select(DB::raw('id'))
                ->where('id', '=', $request->countdown_id)
                ->get();
        if (!empty($existCountDown->first())) {
            #$updateArr['updated_by'] = $request->session()->get('user_id');
            $updateArr['updated_at'] = date('Y-m-d H:i:s');
            DB::table('countdown')->where("id", '=', $request->countdown_id)->update($updateArr);
        } else {
            $updateArr['campaign_id'] = $request->camp_id;
            $updateArr['product_id'] = $request->prod_id;
            $updateArr['created_date'] = date('Y-m-d H:i:s');
            $this->setting_m->add_data('countdown', $updateArr);
        }

        Session::flash('success', 'Campaign details saved successful!');
        return redirect()->action('UserController@admin_dashboard');
    }

    function verifyphone(Request $request) {

        $this->layout = null;
        //check if its our form
        if ($request->ajax()) {
            $phonenumber = str_replace("-", "", $request->phonenumber);
          try {

                $phone_number = $this->twilio->lookups->v1->phoneNumbers($phonenumber)->fetch(["type" => ["carrier"]]);

                if ($phone_number->carrier['type'] != 'mobile') {

                    return 'not a mobile number';
                    
                }
            } catch (\Exception $e) {
                return 'no';
            }  
            $subscriber = DB::table('subscribers')
                    ->select(DB::raw('id,updated_at,created_at'))
                    ->where('phone', '=', $phonenumber)
                    ->get();

            $code = rand(111111, 999999);
            if (!empty($subscriber->first())) {

                $updateArr['verificationcode'] = $code;

                 foreach ($subscriber as $key => $value) {

                    $subscriberid = $value->id;
                    $updated_at = $value->updated_at;
                    $created_at = $value->created_at;
                }
				if($created_at == null || $created_at == ''){
					
					$updateArr['created_at'] = date("Y-m-d H:i:s");
				}
				if($updated_at == null || $updated_at == ''){
					
					$updateArr['updated_at'] = date("Y-m-d H:i:s");
				}

                $this->av->update_data('subscribers', $updateArr, array('id' => $subscriberid));
            } else {

                $insertArr['phone'] = $phonenumber;

                $insertArr['verificationcode'] = $code;
                $insertArr['updated_at'] = date("Y-m-d H:i:s");
                $insertArr['created_at'] = date("Y-m-d H:i:s");

                $this->av->add_data('subscribers', $insertArr);
            }
            try {
                $message = $this->twilio->messages->create(
                        $phonenumber,
                        [
                            'from' => "+14153478646",
                            'body' => "Your verification code is " . $code,
                        ]
                );
            } catch (\Exception $e) {
                return 'no';
                //return 'yes';
            }
            return 'yes';
        } else {

            return 'no';
        }
    }
	
	function savefirstname(Request $request){
		
		$this->layout = null;
        //check if its our form
        if ($request->ajax()) {
            $phonenumber = str_replace("-", "", $request->phonenumber);
            $firstname = $request->firstname;
           
            $subscriber = DB::table('subscribers')
                    ->select(DB::raw('id'))
                    ->where('phone', '=', $phonenumber)
                    ->get();

            
            if (!empty($subscriber->first())) {

                $updateArr['first_name'] = $firstname;
                $updateArr['updated_at'] = date("Y-m-d H:i:s");

                foreach ($subscriber as $key => $value) {

                    $subscriberid = $value->id;
                }

                $this->av->update_data('subscribers', $updateArr, array('id' => $subscriberid));
            }
            return 'yes';
        } else {

            return 'no';
        }
	}
	
	function resendcode(Request $request) {

        $this->layout = null;
        //check if its our form
        if ($request->ajax()) {
            $phonenumber = str_replace("-", "", $request->phonenumber);
            try {

                $phone_number = $this->twilio->lookups->v1->phoneNumbers($phonenumber)->fetch(["type" => ["carrier"]]);

                if ($phone_number->carrier['type'] != 'mobile') {

                    return 'not a mobile number';
                }
            } catch (\Exception $e) {
                return 'no';
            } 
            $subscriber = DB::table('subscribers')
                    ->select(DB::raw('id,created_at,updated_at'))
                    ->where('phone', '=', $phonenumber)
                    ->get();

            $code = rand(111111, 999999);
            if (!empty($subscriber->first())) {

                $updateArr['verificationcode'] = $code;

                foreach ($subscriber as $key => $value) {

                    $subscriberid = $value->id;
                    $updated_at = $value->updated_at;
                    $created_at = $value->created_at;
                }
				if($created_at == null || $created_at == ''){
					
					$updateArr['created_at'] = date("Y-m-d H:i:s");
				}
				if($updated_at == null || $updated_at == ''){
					
					$updateArr['updated_at'] = date("Y-m-d H:i:s");
				}
                $this->av->update_data('subscribers', $updateArr, array('id' => $subscriberid));
            } else {

                $insertArr['phone'] = $phonenumber;

                $insertArr['verificationcode'] = $code;
                $insertArr['created_at'] = date("Y-m-d H:i:s");
                $insertArr['updated_at'] = date("Y-m-d H:i:s");

                $this->av->add_data('subscribers', $insertArr);
            }
            try {
                $message = $this->twilio->messages->create(
                        $phonenumber,
                        [
                            'from' => "+14153478646",
                            'body' => "Your verification code is " . $code,
                        ]
                );
            } catch (\Exception $e) {
               // return 'yes';
                return 'no';
            }
            return 'yes';
        } else {

            return 'no';
        }
    }

    function verificationcode(Request $request) {

        $this->layout = null;
        //check if its our form
        if ($request->ajax()) {
            $phonenumber = str_replace("-", "", $request->phonenumber);
            $verificationcodeinput = $request->verificationcode;

            $subscriber = DB::table('subscribers')
                    ->select(DB::raw('id,verificationcode,first_name,updated_at'))
                    ->where('phone', '=', $phonenumber)
                    ->get();

            $verificationcode = '';
            if (!empty($subscriber->first())) {

                foreach ($subscriber as $key => $value) {

                    $verificationcode = $value->verificationcode;
                    $first_name = $value->first_name;
                    $updated_at = $value->updated_at;
                }

                if ($verificationcodeinput == $verificationcode) {
					
					if($first_name == ''){
					
                    return 'yes';
					
					}
					
					$now = time(); 
					$your_date = strtotime($updated_at);
					 $datediff = $now - $your_date;
					$datediff = round($datediff / (60 * 60 * 24));
					if($datediff >= 32){
						
						return 'yes';
					}
					return "namenotrequired";
                } else {

                    return 'no';
                }
            } else {

                return 'no';
            }
        } else {

            return 'no';
        }
    }

    function startgame(Request $request) {

        if ($request->ajax()) {
            $step = str_replace("-", "", $request->step);
            $msg = $request->msg;
            $phone = $request->phone;
            $phone = str_replace("-", "", $phone);
            if ($step == 1) {
                // check product code in database.

                $odd_code = DB::table('odd_codes')
                        ->select(DB::raw('odd_codes.id,odd_codes.product_id,products.name,products.template'))
                        ->join('products', 'products.id', '=', 'odd_codes.product_id')
                        ->where('odd_codes.code', '=', $msg)
                        ->get();

                $verificationcode = '';
                if (!empty($odd_code->first())) {

                    foreach ($odd_code as $key => $value) {

                        $product_id = $value->product_id;
                        $name = $value->name;
                        $flow_id = $value->template;
                    }

                    $countdown = DB::table('countdown')
                            ->select(DB::raw('countdown.id,countdown.campaign_id,countdown.product_id,countdown.winning_status,countdown.frequency,countdown.every_time'))
                            ->where('countdown.product_id', '=', $product_id)
                            ->get();

                    if (!empty($countdown->first())) {

                        foreach ($countdown as $key => $value) {

                            $winning_status = $value->winning_status;                            
                            $frequency = $value->frequency;
                            $every_time = $value->every_time;
                            $campaign_id = $value->campaign_id;
                        }

                        if ($winning_status != 1) {
                            
                            $last_winner_date = date("Y-m-d H:i:s",strtotime("-{$every_time} {$frequency}"));
                            $last_winner_data = DB::select("select * from winners where campaign_id='{$campaign_id}' and product_id='{$product_id}' and is_winner=1 and created_at>'{$last_winner_date}'");
                            $flow = DB::table('flow')
                                    ->select(DB::raw('flow.flow_id,flow.questions'))
                                    ->where('flow.flow_id', '=', $flow_id)
                                    ->get();
                            if (!empty($flow->first())) {

                                foreach ($flow as $key => $value) {

                                    $questions = $value->questions;
                                }

                                $question = explode(",", $questions);
                                $questionfirstkey = key($question);
                                $questionfirst = reset($question);
                                unset($question[$questionfirstkey]);
                                $questionfirstt = DB::table('questions')
                                        ->select(DB::raw('questions.question_id,questions.question_type,questions.question'))
                                        ->where('questions.question_id', '=', $questionfirst)
                                        ->get();
                                if (!empty($questionfirstt->first())) {

                                    foreach ($questionfirstt as $key => $value) {

                                        $questionstr = $value->question;
                                        $question_type = $value->question_type;
                                        $question_id = $value->question_id;
                                    }
                                }
                            }

                            $arr['name'] = $name;
                            $arr['namestep'] = 0;
                            $arr['question'] = $question;
                           // $arr['questionstr'] = "What is your first name ?";

                             if($question_type == 1){
                              $arr['questionstr'] = $questionstr;
                              }else{



                              $question_response =  DB::table('question_response')
                              ->select(DB::raw('question_response.question_id,question_response.expected_selection'))
                              ->where('question_response.question_id', '=', $question_id)
                              ->get();
                              $questionstr .= "<ul class='options'>";

                              foreach ($question_response as $key => $value) {
                              $expected_selection = $value->expected_selection;
                              $questionstr .= "<li onclick='selectoption(\"$expected_selection\")'>$expected_selection</li>";


                              }
                              $questionstr .= "</ul>";
                              $arr['questionstr'] = $questionstr;
                              } 

                            $insertArr['flow_number'] = $phone;

                            $insertArr['winning_id'] = $msg;

                            $insertArr['campaign_id'] = $campaign_id;
                            $insertArr['product_id'] = $product_id;
                            if(count($last_winner_data)==0){
                                $insertArr['is_winner'] = 1;
                            }

                            $winner_id = $this->av->add_data('winners', $insertArr);

                            $insertArrnew['winner_id'] = $winner_id;

                            $insertArrnew['question'] = $questionstr;
                             
							$insertArrnew['question_id'] = $question_id;

                            $insertArrnew['step'] = 1;

                            $insertArrnew['flow_id'] = $flow_id;

                            $this->av->add_data('logs', $insertArrnew); 

                            return json_encode($arr);
                        } else {

                            return 'notready';
                        }
                    } else {

                        return 'notready';
                    }
                } else {

                    return 'notmatch';
                }
            } else {
                //if ($step <= 3) {

                    /* if ($step == 3) {

                        $updateArr['first_name'] = $msg;

                        $subscriber = DB::table('subscribers')
                                ->select(DB::raw('id'))
                                ->where('phone', '=', $phone)
                                ->get();


                        if (!empty($subscriber->first())) {

                            foreach ($subscriber as $key => $value) {

                                $subscriberid = $value->id;
                            }

                            $this->av->update_data('subscribers', $updateArr, array('id' => $subscriberid));
                        }

                        $arr['questionstr'] = "What is your last name ?";

                        echo json_encode($arr);
                        exit;
                    }
                    if ($step == 4) {

                        $updateArr['last_name'] = $msg;

                        $subscriber = DB::table('subscribers')
                                ->select(DB::raw('id'))
                                ->where('phone', '=', $phone)
                                ->get();


                        if (!empty($subscriber->first())) {

                            foreach ($subscriber as $key => $value) {

                                $subscriberid = $value->id;
                            }

                            $this->av->update_data('subscribers', $updateArr, array('id' => $subscriberid));
                        }
                        $arr['questionstr'] = "What is your email address ?";

                        echo json_encode($arr);
                        exit;
                    } */
                   /*  if ($step == 3) {

                        $updateArr['first_name'] = $msg;

                        $subscriber = DB::table('subscribers')
                                ->select(DB::raw('id'))
                                ->where('phone', '=', $phone)
                                ->get();


                        if (!empty($subscriber->first())) {

                            foreach ($subscriber as $key => $value) {

                                $subscriberid = $value->id;
                            }

                            $this->av->update_data('subscribers', $updateArr, array('id' => $subscriberid));
                        }

                        //$arr['questionstr'] = "What is your last name ?";
                        $winners = DB::table('winners')
                                ->select(DB::raw('winners.id,winners.product_id'))
                                ->where('winners.flow_number', '=', $phone)
                                ->orderBy('id', 'desc')
                                ->limit(1)
                                ->get();

                        if (!empty($winners->first())) {

                            foreach ($winners as $key => $value) {

                                $winner_id = $value->id;
                                $product_id = $value->product_id;
                            }
                        }

                        $products = DB::table('products')
                                ->select(DB::raw('products.id,products.name,products.template'))
                                ->where('products.id', '=', $product_id)
                                ->get();
                        if (!empty($products->first())) {

                            foreach ($products as $key => $value) {

                                $flow_id = $value->template;
                            }
                        }



                        $flow = DB::table('flow')
                                ->select(DB::raw('flow.flow_id,flow.questions'))
                                ->where('flow.flow_id', '=', $flow_id)
                                ->get();
                        if (!empty($flow->first())) {

                            foreach ($flow as $key => $value) {

                                $questions = $value->questions;
                            }

                            $question = explode(",", $questions);
                        }
                        $step = 0;
                        if (!empty($question)) {
                            if (isset($question[$step])) {
                                $questionfirstt = DB::table('questions')
                                        ->select(DB::raw('questions.question_id,questions.question_type,questions.question'))
                                        ->where('questions.question_id', '=', $question[$step])
                                        ->get();
                                if (!empty($questionfirstt->first())) {

                                    foreach ($questionfirstt as $key => $value) {

                                        $questionstr = $value->question;
                                        $question_type = $value->question_type;
                                        $question_id = $value->question_id;
                                    }

                                    if ($question_type == 1) {

                                        $arr['questionstr'] = $questionstr;
                                    } else {

                                        $question_response = DB::table('question_response')
                                                ->select(DB::raw('question_response.question_id,question_response.expected_selection'))
                                                ->where('question_response.question_id', '=', $question_id)
                                                ->get();
                                        $questionstr .= "<ul class='options'>";

                                        foreach ($question_response as $key => $value) {
                                            $expected_selection = $value->expected_selection;
                                            $questionstr .= "<li onclick='selectoption(\"$expected_selection\")'>$expected_selection</li>";
                                            //echo $value->expected_selection;
                                        }
                                        $questionstr .= "</ul>";
                                        $arr['questionstr'] = $questionstr;
                                    }


                                    $insertArrnew['winner_id'] = $winner_id;

                                    $insertArrnew['question'] = $questionstr;

                                    $insertArrnew['step'] = 1;

                                    $insertArrnew['flow_id'] = $flow_id;

                                    $this->av->add_data('logs', $insertArrnew);

                                    echo json_encode($arr);
                                    exit;
                                }
                            }
                        }







                        echo json_encode($arr);
                        exit;
                    } */
                /* } else { */
                    $winners = DB::table('winners')
                            ->select(DB::raw('winners.id'))
                            ->where('winners.flow_number', '=', $phone)
                            ->orderBy('id', 'desc')
                            ->limit(1)
                            ->get();

                    if (!empty($winners->first())) {

                        foreach ($winners as $key => $value) {

                            $winner_id = $value->id;
                        }
                    }
                    //  echo $winner_id;
                    $logs = DB::table('logs')
                            ->select(DB::raw('logs.flow_id,logs.step,logs.id'))
                            ->where('logs.winner_id', '=', $winner_id)
                            ->orderBy('id', 'desc')
                            ->limit(1)
                            ->get();
                    if (!empty($logs->first())) {

                        foreach ($logs as $key => $value) {

                            $id = $value->id;
                            $step = $value->step;
                            $flow_id = $value->flow_id;
                        }
                        $updateArr['response'] = $msg;
                        //$updateArr['step'] = $step+1;
                        $this->av->update_data('logs', $updateArr, array('id' => $id));
                    }


                    $flow = DB::table('flow')
                            ->select(DB::raw('flow.flow_id,flow.questions'))
                            ->where('flow.flow_id', '=', $flow_id)
                            ->get();
                    if (!empty($flow->first())) {

                        foreach ($flow as $key => $value) {

                            $questions = $value->questions;
                        }

                        $question = explode(",", $questions);
                    }
                    if (!empty($question)) {
                        if (isset($question[$step])) {
                            $questionfirstt = DB::table('questions')
                                    ->select(DB::raw('questions.question_id,questions.question_type,questions.question'))
                                    ->where('questions.question_id', '=', $question[$step])
                                    ->get();
                            if (!empty($questionfirstt->first())) {

                                foreach ($questionfirstt as $key => $value) {

                                    $questionstr = $value->question;
                                    $question_type = $value->question_type;
                                    $question_id = $value->question_id;
                                }

                                if ($question_type == 1) {

                                    $arr['questionstr'] = $questionstr;
                                } else {

                                    $question_response = DB::table('question_response')
                                            ->select(DB::raw('question_response.question_id,question_response.expected_selection'))
                                            ->where('question_response.question_id', '=', $question_id)
                                            ->get();
                                    $questionstr .= "<ul class='options'>";

                                    foreach ($question_response as $key => $value) {
                                        $expected_selection = $value->expected_selection;
                                        $questionstr .= "<li onclick='selectoption(\"$expected_selection\")'>$expected_selection</li>";
                                        //echo $value->expected_selection;
                                    }
                                    $questionstr .= "</ul>";
                                    $arr['questionstr'] = $questionstr;
                                }


                                $insertArrnew['winner_id'] = $winner_id;
								
								$insertArrnew['question_id'] = $question_id;

                                $insertArrnew['question'] = $questionstr;

                                $insertArrnew['step'] = $step + 1;

                                $insertArrnew['flow_id'] = $flow_id;

                                $this->av->add_data('logs', $insertArrnew);

                                echo json_encode($arr);
                                exit;
                            }
                        } else {

                            $arr['questionstr'] = "Thanks for the information";
                            $arr['end'] = "1";
                            echo json_encode($arr);
                            exit;
                        }
                    }
                //}
            }
        }
    }

}
