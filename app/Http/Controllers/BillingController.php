<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*

  Register : index
  Login : login
  Sign out : sign_out

 */

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\LoginModel;
use Session;
use Illuminate\Support\Facades\Validator;
use Mail;
use Config;
use Stripe;
use Helper;

class BillingController extends Controller {

    protected $av;

    public function __construct() {
        $this->av = new LoginModel();
    }

    function history(Request $request) {
        $whr['user_id'] = $request->session()->get('user_id');
        $history = $this->av->history($whr);
        if (count($history) > 0) {
            $data['history'] = $history;
            $currency = $request->session()->get('currency');
            if(($history[0]->sum) > 0)
                $data['current_plan_price'] = Helper::getcurrency($currency, $data['history'][0]->sum);
            else
                $data['current_plan_price'] = 0;
        } else {
            $data['history'] = array();
        }

        $w['status'] = 1;
        $fields = array('id', 'plan', 'details', 'sum', 'totalsms', 'perprice');
        $data['plans'] = $this->av->select_c('plans', $fields, $w);
        $data['card_details'] = $this->av->select_c('card_details', '*', array('user_id' => $request->session()->get('user_id')));
        

        if (count($history) > 0) {
            return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('billing/history', $data) . view('billing/billingmodal', $data) . view('common/footer');
        } else {
            return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('billing/history', $data) . view('common/footer');
        }
    }

    function payment(Request $request) {
        $whr['user_id'] = $request->session()->get('user_id');
        $history = $this->av->history($whr);
        if (count($history) > 0) {
            $data['history'] = $history;
            $currency = $request->session()->get('currency');
            $data['current_plan_price'] = Helper::getcurrency($currency, $data['history'][0]->sum);
        } else {
            $data['history'] = array();
        }

        $w['status'] = 1;
        $fields = array('id', 'plan', 'details', 'sum', 'totalsms', 'perprice');
        $data['plans'] = $this->av->select_c('plans', $fields, $w);


        $data['card_details'] = $this->av->select_c('card_details', '*', array('user_id' => $request->session()->get('user_id')));

        $add = $this->av->select_c('billing_address', '*', array('user_id' => $request->session()->get('user_id')));
        $data['address'] = (count($add) > 0 ) ? $add : array();

        if (count($history) > 0) {
            return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('billing/payment', $data) . view('billing/billingmodal', $data) . view('common/footer');
        } else {
            return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('billing/payment', $data) . view('common/footer');
        }
    }

    function delete_card(Request $request, $id) {
        $whr['id'] = $id;
        $whr['user_id'] = $request->session()->get('user_id');
        $res = $this->av->delete_data('card_details', $whr);
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Card deleted successfully.';
        return json_encode($returnData);
    }

    function add_address(Request $request) {
        $data['user_id'] = $request->session()->get('user_id');
        $data['billing_address'] = $request->input('address');
        $data['title'] = $request->input('title');
        $data['created_date'] = Config::get('constants.current_datetime');
        $this->av->add_data('billing_address', $data);
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Address added successfully.';
        return json_encode($returnData);
    }

    function cancel_subscription(Request $request, $id) {
        $whr['id'] = $id;
        $whr['user_id'] = $request->session()->get('user_id');
        $update['cancel'] = 1;
        $update['cancel_date'] = Config::get('constants.current_datetime');
        $res = $this->av->update_data('user_plans', $update, $whr);
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Your subscription has been cancelled.';
        return json_encode($returnData);
    }

    function invoice(Request $request, $id) {
        $whr['user_id'] = $request->session()->get('user_id');
        $whr['id'] = $id;
        $history = $this->av->history_each($whr);
        if (count($history) > 0) {
            $data['history'] = $history;
            $fields = array('id', 'plan', 'details', 'sum', 'totalsms', 'perprice', 'featured');
            $data['fixprice'] = $this->av->select_c('plans', $fields, array('plan' => $history[0]->plan), 'featured')[0]->featured;
        } else {
            $data['history'] = array();
        }

        return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('billing/invoice', $data) . view('common/footer');
    }

}
