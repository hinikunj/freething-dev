<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\LoginModel;
use App\DatasetModel;
use Session;
use Illuminate\Support\Facades\Validator;
use Config;

class DatasetController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */

    function list()
    {
    	$av = new DatasetModel();
        $res = $av->get_dataset();
        $cat = $av->get_category();
        $company = $av->get_company();
    	if(count($res) > 0)
    	{
    		$returnData['success'] = true;
	        $returnData['code'] = 200;
	        $returnData['list'] = $res;
    	}
    	else
    	{
    		$returnData['success'] = false;
	        $returnData['code'] = 402;
	        $returnData['description'] = 'No Matching Records.';
        }
        $returnData['company'] = $company;
        $returnData['category'] = $cat;
    	return view('common/header').view('common/inner-top').view('common/sp-sidenav').view('dataset/sp-dataset',$returnData).view('common/footer').view('dataset/dataset_js');
    }

    

     function save()
    {
    	//$data = $_POST;
        $av = new DatasetModel();
        $add = true;
        if($_POST['dataset_id'] != '') {
            $add = false;
        }
        
        $data['is_company_specific']=($_POST['is_company_specific']=='spec-com-search') ? 1 : 0;
        $data['company_id']=($_POST['is_company_specific']=='spec-com-search') ? $_POST['company_id'] : 0;
        $data['category'] = $_POST['category'];
        $data['sub_category'] = $_POST['sub_category'];

        $duplicate = $this->check_duplicate_dataset($data['category'],$data['sub_category']);

        if($duplicate) {
            $returnData['success'] = false;
            $returnData['code'] = 200;
            $returnData['description'] = 'Duplicate Entry';
            echo json_encode($returnData); exit;
        }

        if($add == true) {
            $data['created_date'] = date('Y-m-d H:i:s');
        }
        
        $data['modified_date'] = date('Y-m-d H:i:s');
        $data['explanation'] = $_POST['explanation'];
        $data['link_required'] = $_POST['link_required'];
        $data['text_entry'] = $_POST['text_entry'];
        $data['modified_date'] = date('Y-m-d H:i:s');
        
        if($add == true) {
        $av->add_data('datasets',$data);
        } else {
            $whr['id'] = $_POST['dataset_id'];
            $av->update_data('datasets',$data,$whr);    
        }
    	$returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Dataset Added Successfully.';
        echo json_encode($returnData); exit;
    }

    function check_duplicate_dataset($cat,$sub_cat) {
        $obj = new DatasetModel();
        $whr['category'] = $cat;
        $whr['sub_category'] = $sub_cat;
        $count = $obj->select_data_count('datasets',$whr);
        if($count[0]->count > 0) {
            return true;
        } else {
            return false;
        }
    }
    function delete_dataset() {
        $av = new DatasetModel(); 
        $dataset_id = $_POST['dataset_id'];
        $av->delete_dataset($dataset_id);
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Dataset Added Successfully.';
        return json_encode($returnData);
    }

     function upload_product(Request $request)
    {
    	$data = $_POST;
    	$file = $request->file('file');
   
      	//Move Uploaded File
      	$destinationPath = 'resources/assets/uploads';
      	$timeupload= time();
      	$file->move($destinationPath,$timeupload.$file->getClientOriginalName());
      	$returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['filename'] = $timeupload.$file->getClientOriginalName();
        return json_encode($returnData);
    }

    function search_company(Request $request)
    {
    	$company = $request->input('company');
    	$av = new LoginModel();
    	$whr['company_name'] = $company;
    	$res = $av->search_company('company', $whr);
    	
    	if(count($res) > 0)
    	{
    		$returnData['success'] = true;
	        $returnData['code'] = 200;
	        $returnData['list'] = $res;
    	}
    	else
    	{
    		$returnData['success'] = false;
	        $returnData['code'] = 402;
	        $returnData['description'] = 'No Matching Records.';
    	}	
    	return json_encode($returnData);
    }

    /*******************************************************************/
    /// ADD LOCATION IN ADD PRODUCT PAGE ///////////////////////////////
    /*******************************************************************/

    function add_location(Request $request)
    {

    	$data = $_POST;
    	$av = new LoginModel();

    	/* VALIDATION START */
        $validator = Validator::make(
             array('location' => $request->input('location'),
                   'currency' => $request->input('currency'),
                   'symbol' => $request->input('symbol'),
              ),
    
           array(
                'location' => 'required',
                'currency' => 'required',
                'symbol' => 'required',
            )
        );
        if ($validator->fails())
        {
          $returnData['success'] = false;
          $returnData['code'] = 402;
          $messages = $validator->messages();
          foreach ($messages->all() as $message)
          {
                $returnData['description'] = $message;
                return json_encode($returnData);
          }
        
          
        }
        /* VALIDATION END */

    	$data['created_date'] = Config::get('constants.current_datetime');;
    	$data['modified_date'] = Config::get('constants.current_datetime');;;
    	$count = $av->select_data_count('location', array('location'=>$data['location']));
    	if($count[0]->count > 0)
    	{
	    	$returnData['success'] = false;
	        $returnData['code'] = 402;
	        $returnData['description'] = 'Location already present.';
    	}
    	else
    	{
    		$av->add_data('location',$data);
	    	$returnData['success'] = true;
	        $returnData['code'] = 200;
	        $returnData['description'] = 'Location Added Successfully.';
    	}
    	
        return json_encode($returnData);
    }

    /*******************************************************************/
    ///GET LOCATIONS ///////////////////////////////
    /*******************************************************************/

    function get_locations(Request $request)
    {
    	
    	$av = new LoginModel();
    	$res = $av->get_locations();
    	
    	if(count($res) > 0)
    	{
    		$returnData['success'] = true;
	        $returnData['code'] = 200;
	        $returnData['list'] = $res;
    	}
    	else
    	{
    		$returnData['success'] = false;
	        $returnData['code'] = 402;
	        $returnData['description'] = 'No Matching Records.';
    	}	
    	return json_encode($returnData);
    }

    /*******************************************************************/
    ///GET Product Detail ///////////////////////////////
    /*******************************************************************/

    function get_product_detail(Request $request,$id)
    {
        
        $av = new LoginModel();
        $whr['id'] = $id;
        $res = $av->get_product_detail($whr);
        
        $locations = $av->get_locations(); // GET LOCATIONS

        if(count($res) > 0)
        {
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['list'] = $res;
            $returnData['locations'] = $locations;
        }
        else
        {
            $returnData['success'] = false;
            $returnData['code'] = 402;
            $returnData['description'] = 'No Matching Records.';
        }   
        return json_encode($returnData);
    }

    /*******************************************************************/
    ///Delete Product ///////////////////////////////
    /*******************************************************************/

    function delete_product(Request $request,$id)
    {
        $av = new LoginModel();
        $data['status'] = 0;
        $data['delete_by'] = $request->session()->get('user_id');
        $whr['id'] = $id;
        $res = $av->update_data('products',$data,$whr);
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Product has been deactivated.';
        return json_encode($returnData);
    }

    /*******************************************************************/
    ///Update Product ///////////////////////////////
    /*******************************************************************/

     function update_product()
    {
        $data = $_POST;
        $av = new LoginModel();
        if(array_key_exists('category', $data))
        {
             $data['category'] = implode(',', $data['category']);
        }
        if(array_key_exists('type', $data))
        {
             $data['type'] = implode(',', $data['type']);
        }
        if(array_key_exists('shipping_by', $data))
        {
             $data['shipping_by'] = implode(',', $data['shipping_by']);
        }
       
        $data['created_date'] = date('Y-m-d H:i:s'); 
        $whr['id'] = $data['id']; 
        $av->update_data('products',$data,$whr);
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Product Updated Successfully.';
        return json_encode($returnData);
    }
    
    public function set_default_goal($id) {
        
        $datasetModel = DatasetModel::where('id',$id)->first();
        if ($datasetModel->is_default == 1)
            $is_default = 0;
        else
            $is_default = 1;
        $datasetModel->is_default = $is_default;
        $datasetModel->save();
        
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['data'] = $is_default;
        $returnData['message'] = 'Category has been changed successfully.';
        return json_encode($returnData);
    }
    
    
}