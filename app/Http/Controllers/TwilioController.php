<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\PhoneNumberModel;
use App\LoginModel;
use App\DatasetModel;
use App\ConditionModel;
use App\SmsLogsModel;
use App\CompanyModel;
use App\UserQuestionsFlowModel;
use App\FlowModel;
use App\FlowquestionsModel;
use App\QuestionModel;
use Session;
use Illuminate\Support\Facades\Validator;
use Config;
use Twilio\Rest\Client;
use Twilio\TwiML\MessagingResponse;
class TwilioController extends Controller
{
     protected $av;
    public function __construct() {
         $this->av = new LoginModel();
		$this->sid = getenv('TWILIO_ACCOUNT_SID');
		$this->token = getenv('TWILIO_AUTH_TOKEN');

	   /** Initialize the Twilio client so it can be used */
	   $this->twilio = new Client($this->sid, $this->token);
    }
	
	
    public function send_sms() {
        // Your Account SID and Auth Token from twilio.com/console
        $sid = 'ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';
        $token = 'your_auth_token';
        $client = new Client($sid, $token);
        $client->messages->create(
            // the number you'd like to send the message to
            '+15558675309',
            [
                // A Twilio phone number you purchased at twilio.com/console
                'from' => '+15017250604',
                // the body of the text message you'd like to send
                'body' => 'Hey Jenny! Good luck on the bar exam!'
            ]
        );
    }

    public function flow_condition () {
        $flow_condition = ConditionModel::get();


        foreach($flow_condition as $cond) {
            if($cond->label == 'win') {
                if($phone_number_registered) {
                    $message = "Hello {{Subject name}} thank you for vising {{Store name}}";
                } else {
                    $message = "Thank you visiting {{Store name}}. So we know how to address you, what is your {{first name}}?";
                }
            }

            if($cond->label == 'NPS') {

            }

            if($cond->label == 'second_chance') {
                
            }

            if($cond->label == 'second_chance') {
                
            }

            if($cond->label == 'Play_reminder') {
                
            }

            if($cond->label == 'campaign_reminder') {
                
            }
            
        }
    }

    public function getIncomingSms(Request $request){

        $SmsStatus =$request->SmsStatus;
        $SmsSid =$request->SmsSid;
        $num_segments =$request->NumSegments;
        $From =str_replace('+','',$request->From);
        $To =str_replace('+','',$request->To);
        $Body =$request->Body;

        $numberDetails = CompanyModel::where('twilio_number','=',$To)->first();
        if(!empty($numberDetails)){
            $company_id = $numberDetails->id;

            $flowDetails = FlowModel::where('company_id','=',$company_id)->first();
            if(!empty($flowDetails)){
                $flow_id = $flowDetails->id;
                $flowquestionDetails = FlowquestionsModel::where('flow_id','=',$flow_id)->get();
                if(!empty($flowquestionDetails)){
                    foreach ($flowquestionDetails as $flowqkey => $flowq_value) {
                        $questions_id = $flowq_value->questions_id;
                        $questionDetails = QuestionModel::where('question_id','=',$questions_id)->first();
                        if(!empty($questionDetails)){

                            $user_question_flow_Details = UserQuestionsFlowModel::where('question_id','=',$questions_id)->where('flow_id','=',$flow_id)->where('company_id','=',$company_id)->where('user_number','=',$From)->first();
                            if(!empty($user_question_flow_Details)){
                                // user wise flow question add
                                $userFlowQuestion = UserQuestionsFlowModel::create([
                                    'flow_id'=>$flow_id,
                                    'questions_id'=>$questions_id,
                                    'company_id'=>$company_id,
                                    'user_number'=>$From,
                                    'question_type'=>$questionDetails->question_type,
                                    'question'=>$questionDetails->question,
                                    'send_status'=>'0'
                                ]);
                            }
                        }
                    }

                    // receice sms logs store-----
                    $smslogs = SmsLogs::create([
                        'company_id'=>$company_id,
                        'flow_id'=>$flow_id,
                        'questions_id'=>'',
                        'to_number'=>$To,
                        'from_number'=>$From,
                        'user_number'=>$From,
                        'sms_body'=>$Body,
                        'sms_type'=>'0',
                        'sms_sid'=>$SmsSid,
                        'sms_status'=>$SmsStatus
                    ]);

                    $send_user_question_flow_Details = UserQuestionsFlowModel::where('flow_id','=',$flow_id)->where('company_id','=',$company_id)->where('user_number','=',$From)->where('send_status','=','0')->first();
                    if(!empty($send_user_question_flow_Details)){

                        $send_question = $send_user_question_flow_Details->question;

                            $CALLURL  = config('app.url').'';
                            $sid    = config('app.TWILIO_ACCOUNT_SID');
                            $token  = config('app.TWILIO_AUTHTOKEN');
                            $client = new Client($sid, $token);
                            try{
                                $message = $client->messages
                                ->create('+'.$From, // to
                                    array(
                                        "body" => $send_question,
                                        "from" => '+'.$To,
                                        "statusCallback" => $CALLURL."getSmsResponse"
                                    )
                                );
                            }catch (\Throwable $th)  {
                            
                                return $this->sendError(trans('Number is not valid'),[],200);
                            }

                    }
                    
                }                

            }
        }

    }

    public function getSmsResponse(Request $request){

        $SmsStatus =$request->SmsStatus;
        $SmsSid =$request->SmsSid;

        $smsDetails = SmsLogs::where('sms_sid','=',$SmsSid)->first();
        if(!empty($getLastSms)){
            $smsDetails->sms_status=$SmsStatus;
            $smsDetails->save();
        }else{
            $smsDetails->sms_status=$SmsStatus;
            $smsDetails->save();
        }

    }


    public function buyNumber(Request $request){

        $number  = $request->number;
        $sid = 'AC641053c311414250aaad5ea28524ad0f';
        $token = '182426241c5a965fcccc9559ea190a3e';
        $client = new Client($sid, $token);

        $CALLURL  = config('app.call_url').'';
        try {
            $incoming_phone_number = $client->incomingPhoneNumbers->create(
                array(
                    "voiceUrl" => $CALLURL . 'api/incomingCall',
                    "voiceFallbackUrl" => $CALLURL . 'api/fallBackurl',
                    "StatusCallback" => $CALLURL . 'api/callHangup',
                    "smsUrl" => $CALLURL . 'api/getIncomingSms',
                    "phoneNumber" => $number
                )
            );
            $sid= $incoming_phone_number->sid;

            // $gettwilionumberbynum=Numbers::where('Number','=',$number)->first();
            // if(empty($gettwilionumberbynum)){
            //     $number = Numbers::create([
            //         'NumberSid' => $sid,
            //         'Number' => $number,
            //         'CreatedAt' => date('Y-m-d h:i:s'),
            //     ]);
            //     $number->save();
            //     return response()->json(['error' => false, 'message'  => 'Number purchsed successfully.']);
            // }else{
            //     return response()->json(['error' => true, 'message'  => 'Number add issue.']);
            // }
        } catch (Exception $e) {
            $err = explode(':', $e->getMessage());
            return response()->json(['error' => true, 'message'  => $err[1]]);
        }


    }
	
	function incomingsms(){
		
		
		
		/* $from   = $request->input('From');
        $msg       = $request->input('Body');
        $To       = $request->input('To'); */
		 $msg = $_REQUEST['Body'];
		
		$from = $_REQUEST['From'];
		$To = $_REQUEST['To']; 
		$from = str_replace("+1","",$from);
		
		
		
		$fieldsarr = array('first_name','last_name','phone');
			
		$warr['phone'] = str_replace("+1","",$from);
			
		$phonenumberdata = $this->av->select_c('subscribers', $fieldsarr, $warr);
		
		$firstname = '';
		
		$lastname = '';
		
		$email = '';
		
		$phonenumber = '';
		
		foreach($phonenumberdata as $rec){
			
			$phonenumber = $rec->phone;
			
			$firstname = $rec->first_name;
			
			$lastname = $rec->last_name;
			
		}
		
		$w['twilio_number'] = $To;
		
		$fields = array('twilio_number');
		
		$userinfo = $this->av->select_c('login', $fields, $w);
		
		$existingtwilio_number = '';
		
		 foreach ($userinfo as $key => $value) {
			
			$existingtwilio_number = $value->twilio_number;
		
		} 
	
		if($existingtwilio_number != ''){
		$phone_number = $this->twilio->lookups->v1->phoneNumbers($from)->fetch(["type" => ["carrier"]]);
		
		if($phone_number->carrier['type'] !='mobile'){
			
			$response = new MessagingResponse();
			$response->message('We do not accept this phone type. Please use a phone number from your mobile phone carrier. ');
			echo $response;
			exit;
		}else{
			
			if(strtoupper($msg) == 'WIN'){
		
		if($phonenumber == ''){
			
			$data['phone'] = $from;
			
			$this->av->add_data('subscribers', $data);
			
			$response = new MessagingResponse();
			$response->message('Thank you for visiting {{Store name}}. So we know how to address you, what is your first name?');
			echo $response;
			exit;
		
		}else{
			if($firstname == ''){
			
				$response = new MessagingResponse();
				$response->message('Thank you for visiting {{Store name}}. So we know how to address you, what is your first name?');
				echo $response;
				exit;
			
			}elseif($lastname == ''){
				
				$response = new MessagingResponse();
				
				$response->message('Thank you for visiting {{Store name}}. So we know how to address you, what is your last name?');
				echo $response;
				exit;
			}
		}
		
		}
		if($phonenumber == ''){
			
			$response = new MessagingResponse();
				
			$response->message('Invalid Keyword, Please send WIN to start.');
			
			echo $response;
			
			exit;
		}
		if($firstname == ''){
			
			$whrcond['phone'] = $from;
			
			$updateArr['first_name'] = ucfirst($msg);
			
			$this->av->update_data('subscribers', $updateArr, $whrcond);
				
			$response = new MessagingResponse();
				
			$response->message('what is your last name?');
			
			echo $response;
			
			exit;
		}
		
		if($lastname == ''){
				
			$whrcond['phone'] = $from;
			
			$updateArr['last_name'] = ucfirst($msg);
			
			$this->av->update_data('subscribers', $updateArr, $whrcond);
				
			$response = new MessagingResponse();
				
			$response->message('Thank you '.$firstname.'! Now, enter your {{Winning code}}');
			
			echo $response;
			
			exit;
		}
			
			
		}
		
		
		
		}
	}
    
}