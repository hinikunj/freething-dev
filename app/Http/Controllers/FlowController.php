<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\LoginModel;
use App\DatasetModel;
use App\QuestionModel;
use App\FlowModel;
use App\ConditionModel;
use Session;
use Illuminate\Support\Facades\Validator;
use Mail;
use Config;
use Twilio\Rest\Client;

class FlowController extends Controller {

    public function flow() {

        $res = DB::table('datasets')->select('datasets.sub_category', 'datasets.id as datasets_id')
                ->whereNotIn('datasets.id', function($query) {
                    $query->select('save_response_to')->from('questions');
                })
                ->orderBy('datasets.id', 'desc')
                ->groupBy('datasets.id')
                ->get();
        $data['dataset'] = $res;
        $data['questions'] = QuestionModel::orderBy('question_id', 'DESC')->get()->all();

        return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('flow/sp-add-question', $data) . view('common/footer') . view('dataset/dataset_js') . view('flow/flow-js');
    }

    public function add_question(Request $request) {

        if (null != $request->input('question_id') && $request->input('question_id') > 0) {
            $av = QuestionModel::find($request->input('question_id'));
        } else {
            $av = new QuestionModel();
        }

        $av->question_type = $request->input('question_type');
        $av->question = $request->input('question');
        $av->answer = $request->input('answer');
        $av->save_response_to = $request->input('save_response_to');
        $av->save();
        $question_id = $av->question_id;

        $expected_selection = $request->input('expected_selection');
        $acceptable_response = $request->input('acceptable_response');
        $widget_box = $request->input('widget_bot');
        $reply = $request->input('reply');

        if ($av->question_type == 2) {
            $av = new QuestionModel();
            $av->delete_multiple($question_id);
            foreach ($expected_selection as $key => $val) {
                $res_data['question_id'] = $question_id;
                if ($val != '') {
                    $res_data['expected_selection'] = $val;
                    $res_data['acceptable_response'] = $acceptable_response[$key];
                    $res_data['widget_bot'] = $widget_box[$key];
                    $res_data['reply'] = $reply[$key];
                    $res_data['created_date'] = date('Y-m-d H:i:s');
                    $av->add_data('question_response', $res_data);
                }
            }
        }



        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['question_id'] = $question_id;
        echo json_encode($returnData);
    }

    public function delete_questions(Request $request) {
        $question_id = $request->input('question_id');
        $user = QuestionModel::find($question_id);
        $user->delete();
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['data'] = "Deleted";
        echo json_encode($returnData);
    }

    public function edit_questions(Request $request) {
        $question_id = $request->input('question_id');
        $user = QuestionModel::find($question_id);
        $av = new QuestionModel();
        $multiple = $av->select_data('question_response', array('question_id' => $question_id));
        $dataset = $av->select_data('datasets', array('id' => $user->save_response_to))->first();
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['data'] = $user;
        $returnData['multiple'] = $multiple;
        $returnData['dataset'] = $dataset;
        echo json_encode($returnData);
    }

    public function get_questions(Request $request) {
        $html = false;
        $goal_id = $request->input('goal_id');
        $questions = QuestionModel::where('save_response_to', '=', $goal_id)->get();
        if (!empty($questions->first())) {
            $dataset = DatasetModel::where('id', '=', $goal_id)->first();
            foreach ($questions as $question) {
                if ($question->question_type == 1) {
                    $html = "<tr>
                        <td>Question</td>
                        <td><p>$question->question</p></td>
                    </tr>
                    <tr>
                        <td>Short Answers</td>
                        <td><p>Wait for response</p></td>
                    </tr>";
                } else if ($question->question_type == 2) {
                    $html = "<tr>
                                <td>Question</td>
                                <td><p>$question->question</p></td>
                            </tr>
                            <tr>
                                <td>Multiple Choice</td>
                                <td>";
                    $av = new QuestionModel();
                    $multiple = $av->select_data('question_response', array('question_id' => $question->question_id));
                    if (!empty($multiple->first())) {
                        $html .= "<table><tr>
                                    <th>Expected response</th>
                                    <th>Acceptable response</th>
                                    <th>Widget Bot</th>
                                    <th>Reply</th>
                                </tr>";
                        foreach ($multiple as $key => $value) {
                            $html .= "<tr>
                                        <td>$value->expected_selection</td>
                                        <td>$value->acceptable_response</td>
                                        <td>$value->widget_bot</td>
                                        <td>$value->reply</td>
                                    </tr>";
                        }
                        $html .= "</table>";
                    } else {
                        $html .= "No Choice Found!</td></tr>";
                    }
                } else if ($question->question_type == 3) {
                    $html = "<tr>
                        <td>Instructions</td>
                        <td><p>$question->question</p></td>
                    </tr>";
                }

                $req_sign = "";
                $required = "";
                $dataset->link_required;


                if (!empty($dataset->explanation)) {
                    $html .= "<tr>";
                    $html .= "<td>Explanation</td><td class='font-weight-normal'>$dataset->explanation</td>";
                    $html .= "</tr>";
                }

                if ($dataset->link_required == 1) {
                    $req_sign = "*";
                    $required = "required";
                    $html .= "<tr>
                        <td>Enter Link{$req_sign}</td>
                        <td><input type='text' class='form-control'
                                                id='question_link' name='question_link' placeholder='https://' {$required} data-required='{$required}' value='{$question->link}' />
                                                    <span style='color:red;font-weight:normal;'>This link will be delivered to get with the message above?</span></td>
                    </tr>";
                }
                if ($dataset->text_entry == 1) {
                    $req_sign = "*";
                    $required = "required";
                    $html .= "<tr>
                        <td>Enter Text{$req_sign}</td>
                        <td>
                            <input type='text' class='form-control' id='text_entry' name='text_entry' placeholder='Enter text entry' {$required} data-required='{$required}' max-length='50' value='{$question->text_entry}' />
                        </td>
                    </tr>";
                }
                //$question->suggested_edit
                $html .= "<tr>";
                $html .= "<td>Suggested Edit</td><td><textarea id='summernote' name='suggested_edit' class='form-control' placeholder='Your Message Here ...'></textarea><button class='btn btn-danger mt-2' type='button' onclick='updateQuestion(\"{$question->question_id}\")'>Submit</button></td>";
                $html .= "</tr>";
                $html .= "<tr>
                        <td> </td>
                        <td></td>
                    </tr>";
            }
            $returnData['success'] = true;
            $returnData['code'] = 200;
        } else {
            $returnData['success'] = false;
            $returnData['code'] = 400;
        }
        #$av = new QuestionModel();
        #$multiple = $av->select_data('question_response', array('question_id' => $question_id));

        $returnData['data'] = $html;
        #$returnData['multiple'] = $multiple;
        echo json_encode($returnData);
    }

    public function update_question(Request $request) {
        try {
            if (isset($_POST['question_link']))
                $data["link"] = $_POST['question_link'];
            if (isset($_POST['text_entry']))
                $data["text_entry"] = $_POST['text_entry'];
            $data["suggested_edit"] = $_POST['suggested_edit'];
            $whr['question_id'] = $_POST['question_id'];
            $q = new QuestionModel();
            $q->update_data('questions', $data, $whr);
            $dataset_data = DB::table('datasets')
                    ->select(DB::raw('category,sub_category'))
                    ->where(array("id" => $_POST['dataset_id']))
                    ->get();
            $user_data = DB::table('login')
                    ->select(DB::raw('id,first_name,last_name,phone,role,email'))
                    ->where(array("id" => $request->session()->get('user_id')))
                    ->get();
            $email_user = isset($user_data[0]) ? $user_data[0]->email : $request->session()->get('first_name');
            $msg = "Suggest Edit:" . $_POST['suggested_edit'] . "
                \nDataset:" . (isset($dataset_data[0]) ? ($dataset_data[0]->sub_category . "(" . $dataset_data[0]->category . ")") : "-") . "
                \nEmail:" . $email_user . "
                \nDate:" . date("M d, Y");
            mail("team@freethings.shop", "Suggested edit received from " . $email_user, $msg);
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['data'] = $data;
            $returnData['where'] = $whr;
        } catch (Exception $e) {
            $returnData['success'] = false;
            $returnData['code'] = 400;
            $returnData['msg'] = $e->getMessage();
        }
        echo json_encode($returnData);
        die;
    }

    public function list() {
        $data['flows'] = FlowModel::orderBy('flow_id', 'DESC')->get()->all();
        #flow/question-list
        return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('flow/sp-flow-template', $data) . view('common/footer') . view('dataset/dataset_js') . view('flow/flow-js');
    }

    public function edit_flow($flow_id) {
        $av = new FlowModel();
        $flow = FlowModel::find($flow_id);
        $data['flows'] = $flow;

        $fm = new FlowModel();
        $res = $fm->create_flow_questions();
        $data['flowQues'] = $res;

        return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('flow/sp-create-flow', $data) . view('common/footer') . view('dataset/dataset_js') . view('flow/flow-js');
    }

    public function add_flow(Request $request) {
        $question_ids = $request->input('question_ids');
        $flow_name = $request->input('flow_name');
        $flow_id = $request->input('flow_id');
        $impQueIds = '';

        if (!empty($question_ids)) {
            foreach ($question_ids as $que) {
                if (!empty($que)) {
                    if (strpos($que, ',') !== false) {
                        $explode = explode(',', $que);
                        if (!empty($explode)) {
                            foreach ($explode as $exp) {
                                $questionsIDs[] = $exp;
                            }
                        }
                    } else {
                        $questionsIDs[] = $que;
                    }
                }
            }
            $impQueIds = implode(',', $questionsIDs);
        }

        if (null != $flow_id && $flow_id > 0) {
            $av = FlowModel::find($flow_id);
        } else {
            $av = new FlowModel();
        }
        //$av = new FlowModel();
        //$av = FlowModel::find($flow_id);
        $av->flow_name = $flow_name;
        $av->is_published = $request->input('published');
        $av->questions = $impQueIds;
        $av->save();
        $flow_id = $av->flow_id;

        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['data'] = $flow_id;
        echo json_encode($returnData);
    }

    public function delete_flow(Request $request) {
        $flow_id = $request->input('flow_id');
        $flow = FlowModel::find($flow_id);
        $flow->delete();
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['data'] = "Deleted";
        echo json_encode($returnData);
    }

    public function drafts() {
        $res = FlowModel::where("is_published", '0')->get();
        $data['flows'] = $res;
        return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('flow/draft-question-list', $data) . view('common/footer') . view('dataset/dataset_js') . view('flow/flow-js');
    }

    public function creating_flow() {
        $fm = new FlowModel();
        $res = $fm->create_flow_questions();
        $data['flowQues'] = $res;
        return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('flow/sp-create-flow', $data) . view('common/footer') . view('dataset/dataset_js') . view('flow/flow-js');
    }

    public function search_question(Request $request) {
        $searchText = $request->input('search_keyword');
        $whereCase[] = ['questions.question', 'like', '%' . $searchText . '%'];
        $fm = new FlowModel();
        $flowQues = $fm->create_flow_questions($whereCase);

        if (count($flowQues) > 0) {
            $html = '';
            foreach ($flowQues as $key => $flowQue) {
                $category = $flowQue->sub_category;
                if ($key == 0)
                    $html .= "<h6>" . $flowQue->sub_category . "</h6>";
                else if (($key != 0) && ($category != $flowQues[$key - 1]->sub_category))
                    $html .= "<h6>" . $flowQue->sub_category . "</h6>";
                $html .= '<li id="' . $flowQue->question_id . '" class="alert alert-warning">' . $flowQue->question . '<a href="#" class="remove_question pull-right"><i class="fa fa-trash text-danger font-18"></i></a></li>';
            }
            echo $html;
            return;
        } else {
            echo $html = 'No records found!';
        }

        return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('flow/sp-create-flow', $data) . view('common/footer') . view('dataset/dataset_js') . view('flow/flow-js');
    }

    public function search_flow(Request $request) {
        $searchText = $request->input('search_keyword');
        $flows = FlowModel::select('flow_name', 'flow_id')->where('flow_name', 'like', "%$searchText%")->orderBy('flow_id', 'DESC')->get()->all();

        if (!empty($flows)) {
            $i = 0;
            $html = '';
            foreach ($flows as $flow) {
                $i++;
                $html .= '<tr id="div_' . $flow->flow_id . '">
                        <td>' . $i . '</td>
                        <td>' . $flow->flow_name . '</td>
                        <td>
                            <a href="flow/' . $flow->flow_id . '" class="mr-2"><i class="fa fa-edit text-info font-18"></i></a>
                            <a href="#" data-id="' . $flow->flow_id . '" class="deleteFlow"><i class="fa fa-trash text-danger font-18"></i></a>
                        </td>
                    </tr>';
            }
            echo $html;
            return;
        } else {
            echo $html = 'No records found!';
        }
    }

    public function search_label(Request $request) {

        $searchText = $request->input('search_keyword');
        $whereCase[] = ['datasets.sub_category', 'like', '%' . $searchText . '%'];
        $fm = new FlowModel();
        $questions = $fm->create_flow_questions($whereCase);

        if (count($questions) > 0) {
            $html = '';
            foreach ($questions as $question) {
                $html .= '
                <div class="alert alert-warning" role="alert" id="div_' . $question->question_id . '">
                    ' . $question->question . '
                    <div class="float-right">
                        <a href="#" data-id="' . $question->question_id . '" class="edit_question mr-2"><i class="fa fa-edit text-info font-18"></i></a>
                        <a href="#" class="delete_question" data-id="' . $question->question_id . '"><i class="fa fa-trash text-danger font-18"></i></a>
                    </div>
                </div>';
            }
            echo $html;
            return;
        } else {
            echo $html = 'No records found!';
        }
    }

}
