<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*

User List : user_list
Update User Status : update_user_status

*/

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\LoginModel;
use Session;
use Illuminate\Support\Facades\Validator;
use Mail;
use Config;


class UserManagementController extends Controller
{
	protected $av;
	public function __construct()
    {
        $this->av = new LoginModel();
    }

    function user_list()
    {
    	$res = $this->av->user_list();
    	// echo "<pre>";print_r($res);die;
    	if(count($res) > 0)
    	{
    		$returnData['success'] = true;
	        $returnData['code'] = 200;
	        $returnData['list'] = $res;
    	}
    	else
    	{
    		$returnData['success'] = false;
	        $returnData['code'] = 402;
	        $returnData['description'] = 'No Matching Records.';
    	}
    	return view('common/header').view('common/inner-top').view('common/sp-sidenav').view('user-management/sp-users',$returnData).view('product/modals').view('common/footer');
    }

    ///////////////////////////////////////////////////////////////////////////////
    ////update user status form user list ///////////////////////////////////////////
    /// script : .userStatus ///////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////
    function update_user_status($id,$status)
    {
    	$this->av->update_data('login',array('status'=>$status,'modified_date'=>Config::get('constants.current_datetime')),array('id'=>$id));
    	$returnData['success'] = true;
	    $returnData['code'] = 200;
	    $returnData['description'] = 'Update Successfull.';
	    print_r(json_encode($returnData));
    }
}