<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*

  Register : index
  Login : login
  Sign out : sign_out
  forget password : forget_password

 */

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\LoginModel;
use Session;
use Illuminate\Support\Facades\Validator;
use Mail;
use Config;
use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;

class LoginController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    protected $av;

    public function __construct() {
        $this->av = new LoginModel();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    /*
      To register
      $('#register').submit(function()
     */
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    public function index(Request $request) {
        if ($request->isMethod('post')) {
            $location = '';
            if (!empty($request->input('location'))) {
                if ($request->input('location') == 'US')
                    $location = 'USA';
                else if ($request->input('location') == 'GB')
                    $location = 'UK';
                else if ($request->input('location') == 'CA')
                    $location = 'Canada';
            }

            $data['first_name'] = ucwords($request->input('first_name'));
            $data['last_name'] = ucwords($request->input('last_name'));
            $data['phonecode'] = $request->input('phonecode');
            $data['phone'] = $request->input('phone');
            $data['email'] = $request->input('email');
            $data['password'] = md5($request->input('password'));
            $data['industry'] = $request->input('industry');
            $data['company'] = $request->input('company');
            $data['location'] = $location;
            $data['country_code'] = $request->input('location');
            $data['role'] = 0;
            $data['status'] = 0;
            $data['created_date'] = Config::get('constants.current_datetime');

            if ($request->input('password') != $request->input('confirm_password')) {
                $returnData['success'] = false;
                $returnData['code'] = 402;
                $returnData['description'] = 'Password and Confirm Password does not match.';
                return json_encode($returnData);
            }
            $av = new LoginModel();

            $chk['phone'] = $request->input('phone');
            $chkres = $av->check_existing_user($chk);
            if (count($chkres) > 0) {
                $returnData['success'] = false;
                $returnData['code'] = 402;
                $returnData['description'] = 'User Already Exists.';
                return json_encode($returnData);
                // return response()->json($returnData);
            }

            /* end of checking */ else {

                $sid = Config::get('constants.sid');
                $token = Config::get('constants.token');
                $twilio = new Client($sid, $token);
                try {
                    $verification = $twilio->verify->v2->services(Config::get('constants.service_id'))
                            ->verifications
                            ->create($request->input('phonecode') . $request->input('phone'), "sms");
                } catch (RestException $e) {
                    $returnData['success'] = false;
                    $returnData['code'] = 200;
                    $returnData['description'] = 'Mobile number not valid';
                    return json_encode($returnData);
                }
                
                $res = $av->add_data('login', $data);

                if ($verification->status == 'pending') {
                    $returnData['success'] = true;
                    $returnData['code'] = 200;
                    $returnData['description'] = 'An OTP has been sent to your mobile number for verification';
                } else {
                    $returnData['success'] = false;
                    $returnData['code'] = 200;
                    $returnData['description'] = 'Some Error Occured';
                }

                return json_encode($returnData);
            }
        }

        return view('common/header') . view('register');
        // return view('register');
    }

//////////////////////////////////////////////////////////////////////////////////////////
    /*
      To Verify Phone NUmber

     */
/////////////////////////////////////////////////////////////////////////////////////////
    function verify_phone(Request $request) {
        $av = new LoginModel();
        $sid = Config::get('constants.sid');
        $token = Config::get('constants.token');
        $twilio = new Client($sid, $token);
        try {
            $verification_check = $twilio->verify->v2->services(Config::get('constants.service_id'))
                    ->verificationChecks
                    ->create($request->input('otp'), // code
                    ["to" => $request->input('code_to_verify') . $request->input('mobile_to_verify')]
            );
            if ($verification_check->status == 'approved') {

                $whrd['phone'] = $request->input('mobile_to_verify');
                $res = $av->update_data('login', array('status' => 1, 'role' => 1), $whrd);
                $returnData['success'] = true;
                $returnData['code'] = 200;
                $returnData['description'] = 'Mobile number verified successfully. You will be redirecting to login page.';
                return json_encode($returnData);
            } else {
                $returnData['success'] = false;
                $returnData['code'] = 402;
                $returnData['description'] = 'OTP is incorrect.';
                return json_encode($returnData);
            }
        } catch (Exception $e) {
            $returnData['success'] = false;
            $returnData['code'] = 402;
            $returnData['description'] = 'OTP is incorrect.';
            return json_encode($returnData);
        }
        print($verification_check->status);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    /*
      To Login
      $('#login').submit(function()
     */
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    function login(Request $request) {

        //Redirect to Dashboard if already logged in
        if ($request->session()->get('user_id') !== null) {
            if (($request->session()->get('role') !== null) && $request->session()->get('role') == 0) {
                return redirect()->action('UserController@admin_dashboard');
            } else {
                return redirect()->action('UserController@dashboard');
            }
        }

        if ($request->isMethod('post')) {
            $whr['phone'] = $request->input('phone');
            $whr['password'] = md5($request->input('password'));

            /* VALIDATION START */
            $validator = Validator::make(
                            array('phone' => $request->input('phone'),
                                'password' => $request->input('password'),
                            ),
                            array(
                                'phone' => 'required',
                                'password' => 'required',
                            )
            );
            if ($validator->fails()) {
                $returnData['success'] = false;
                $returnData['code'] = 402;
                $messages = $validator->messages();
                foreach ($messages->all() as $message) {
                    $returnData['description'] = $message;
                    return json_encode($returnData);
                }
            }
            /* VALIDATION END */

            $av = new LoginModel();
            $chkres = $av->login($whr);
            if (count($chkres) > 0) {
                /* get symbol */
                $getSybol = $this->av->select_c('location', array('currency', 'symbol'), array('location' => $chkres[0]->location));
                if (count($getSybol) > 0) {
                    $request->session()->put('currency', $getSybol[0]->currency);
                    $request->session()->put('symbols', $getSybol[0]->symbol);
                } else {
                    $request->session()->put('currency', 'USD');
                    $request->session()->put('symbols', '$');
                }

                /* end get symbol */
                $request->session()->put('first_name', ucwords($chkres[0]->first_name));
                $request->session()->put('last_name', ucwords($chkres[0]->last_name));
                $request->session()->put('role', $chkres[0]->role);
                $request->session()->put('phone', $chkres[0]->phone);
                $request->session()->put('user_id', $chkres[0]->id);
                $request->session()->put('location', $chkres[0]->location);
                $request->session()->put('company', $chkres[0]->company);
                $getaddress = $this->av->select_c('billing_address', array('billing_address'), array('user_id' => $chkres[0]->id));
                if (count($getaddress) > 0) {
                    $request->session()->put('address', $getaddress[0]->billing_address);
                }
                $returnData['success'] = true;
                $returnData['code'] = 200;
                $returnData['list']['role'] = $chkres[0]->role;
                $returnData['description'] = 'Login Successfully.';
                return json_encode($returnData);
            } else {

                $returnData['success'] = false;
                $returnData['code'] = 402;
                $returnData['description'] = 'Wrong Credentials.';
                return json_encode($returnData);
            }
        }
        return view('common/header') . view('login');
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    /*
      To Sign out
     */
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    function sign_out(Request $request) {
        $request->session()->flush();
        return redirect()->action(
                        'LoginController@login'
        );
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    /*
      To Edit Profile
     */
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    function edit_profile(Request $request) {
        if ($request->isMethod('post')) {
            $av = new LoginModel();
            $data = $_POST;
            $whr['id'] = $request->session()->get('user_id');
            $res = $av->update_data('login', $data, $whr);
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['description'] = 'Data Updated Successfully.';
            return json_encode($returnData);
        }
        $av = new LoginModel();
        $whr['phone'] = $request->session()->get('phone');
        $chkres = $av->get_profile($whr);

        /* GET LOCATION */
        $res = $av->get_locations();
        /* END GET LOCATION */
        if (count($chkres) > 0) {

            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['list'] = $chkres;
            $returnData['location'] = $res;
        } else {

            $returnData['success'] = false;
            $returnData['code'] = 402;
            $returnData['description'] = 'No Data.';
        }
        return view('common/header') . view('common/inner-top') . view('common/sp-sidenav') . view('profile/edit-profile', $returnData) . view('common/footer');
    }

    ////////////////////////////////////////////////////////////////////////
    /* Reset Password */
    /////////////////////////////////////////////////////////////////////////

    function reset_pwd(Request $request) {
        if ($request->isMethod('post')) {
            if ($request->input('password') != $request->input('confirm_password')) {
                $returnData['success'] = false;
                $returnData['code'] = 402;
                $returnData['description'] = 'Password and Confirm Password does not match.';
                return json_encode($returnData);
            } else {
                $av = new LoginModel();
                $data['password'] = md5($request->input('password'));
                $whr['id'] = $request->session()->get('user_id');
                $res = $av->update_data('login', $data, $whr);
                $returnData['success'] = true;
                $returnData['code'] = 200;
                $returnData['description'] = 'Data Updated Successfully.';
                return json_encode($returnData);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////
    /* Update location in profile */
    ////////////////////////////////////////////////////////////////////////
    function update_profile_location(Request $request) {
        if ($request->isMethod('post')) {
            $av = new LoginModel();
            $data = $_POST;
            $whr['id'] = $request->session()->get('user_id');
            $res = $av->update_data('login', $data, $whr);
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['description'] = 'Data Updated Successfully.';
            return json_encode($returnData);
        }
    }

    ////////////////////////////////////////////////////////////////////////
    /* Update lang in profile */
    ////////////////////////////////////////////////////////////////////////
    function update_profile_lang(Request $request) {
        if ($request->isMethod('post')) {
            $av = new LoginModel();
            $data = $_POST;
            $whr['id'] = $request->session()->get('user_id');
            $res = $av->update_data('login', $data, $whr);
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['description'] = 'Data Updated Successfully.';
            return json_encode($returnData);
        }
    }

    ////////////////////////////////////////////////////////////////////////
    /* Update industry in profile */
    ////////////////////////////////////////////////////////////////////////
    function update_profile_industry(Request $request) {
        if ($request->isMethod('post')) {
            $av = new LoginModel();
            $data = $_POST;
            $whr['id'] = $request->session()->get('user_id');
            $res = $av->update_data('login', $data, $whr);
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['description'] = 'Data Updated Successfully.';
            return json_encode($returnData);
        }
    }

    ////////////////////////////////////////////////////////////////////////
    /* Delete Account in profile */
    ////////////////////////////////////////////////////////////////////////
    function delete_account(Request $request) {

        $av = new LoginModel();
        $data['status'] = 0;
        $whr['id'] = $request->session()->get('user_id');
        $res = $av->update_data('login', $data, $whr);
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Your account has been inactive.';
        return json_encode($returnData);
    }

    ////////////////////////////////////////////////////////////////////////
    /* Update Email in profile */
    ////////////////////////////////////////////////////////////////////////
    function update_email(Request $request) {
        $av = new LoginModel();
        if ($request->input('otp') != '') {
            $whrs['otp'] = $request->input('otp');
            $whrs['id'] = $request->session()->get('user_id');
            $count = $av->select_data_count('login', $whrs);
            if ($count[0]->count > 0) {
                $data['email'] = $request->input('email');
                $whr['id'] = $request->session()->get('user_id');
                $res = $av->update_data('login', $data, $whr);
                $returnData['success'] = true;
                $returnData['code'] = 200;
                $returnData['description'] = 'Email updated successfully';
                return json_encode($returnData);
            } else {
                $returnData['success'] = false;
                $returnData['code'] = 402;
                $returnData['description'] = 'Please ente correct OTP.';
                return json_encode($returnData);
            }
        }

        // use wordwrap() if lines are longer than 70 characters
        $msg = rand(1999, 99999);
        // send email
        mail($request->input('email'), "Verify Email", $msg);

        $data['otp'] = $msg;
        $whr['id'] = $request->session()->get('user_id');
        $res = $av->update_data('login', $data, $whr);
        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Enter OTP recieved in your email.';
        return json_encode($returnData);
    }

    function google_register(Request $request) {

        $av = new LoginModel();
        $chk['email'] = $request->input('email');
        $chkres = $av->check_existing_email($chk);

        if (count($chkres) > 0) {


            $whr['email'] = $request->input('email');
            $chkres = $av->user_details($whr);
            if (count($chkres) > 0) {
                $request->session()->put('first_name', $chkres[0]->first_name);
                $request->session()->put('role', $chkres[0]->role);
                $request->session()->put('phone', $chkres[0]->phone);
                $request->session()->put('user_id', $chkres[0]->id);
            }
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['description'] = 'User Already Exists.';
            $returnData['role'] = $chkres[0]->role;
            return json_encode($returnData);
            // return response()->json($returnData);
        }
        $chks['phone'] = $request->input('phone');
        $chkres = $av->check_existing_user($chks);
        if (count($chkres) > 0) {
            $whr['phone'] = $request->input('phone');
            $chkres = $av->user_details($whr);
            if (count($chkres) > 0) {
                $request->session()->put('first_name', $chkres[0]->first_name);
                $request->session()->put('role', $chkres[0]->role);
                $request->session()->put('phone', $chkres[0]->phone);
                $request->session()->put('user_id', $chkres[0]->id);
            }
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['description'] = 'User Already Exists.';
            $returnData['role'] = $chkres[0]->role;
            return json_encode($returnData);
            // return response()->json($returnData);
        }

        /* end of checking */ else {
            $data = $_POST;
            $data['auth_type'] = 'google';
            $data['created_date'] = date('Y-m-d H:i:s');
            $data['modified_date'] = date('Y-m-d H:i:s');
            $res = $av->add_data('login', $data);

            $whr['email'] = $request->input('email');
            $chkres = $av->user_details($whr);
            if (count($chkres) > 0) {
                $request->session()->put('first_name', $chkres[0]->first_name);
                $request->session()->put('role', $chkres[0]->role);
                $request->session()->put('phone', $chkres[0]->phone);
                $request->session()->put('user_id', $chkres[0]->id);
            }
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['role'] = $chkres[0]->role;
            $returnData['description'] = 'User added Successfully.';
            return json_encode($returnData);
        }
    }

    /*     * **************************************************************** */

    /// FORGET PASSWORD  ///////////////////////////////
    /*     * **************************************************************** */

    function forget_password(Request $request) {
        if ($request->isMethod('post')) {

            if ($request->input('phone') != '') {
                $sid = Config::get('constants.sid');
                $token = Config::get('constants.token');
                $twilio = new Client($sid, $token);

                $verification = $twilio->verify->v2->services(Config::get('constants.service_id'))
                        ->verifications
                        ->create($request->input('phonecode') . $request->input('phone'), "sms");
            } else {
                $msg = rand(1999, 99999);
                $data['otp'] = $msg;
                $whr['email'] = $request->input('email');
                $res = $this->av->update_data('login', $data, $whr);
                // send email
                mail($request->input('email'), "Verify Email", $msg);
            }
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['description'] = 'Enter OTP recieved in your email.';
            return json_encode($returnData);
        }


        return view('common/header') . view('profile/forget-password');
    }

    /*     * **************************************************************** */

    /// RESET FORGET PASSWORD  ///////////////////////////////
    /*     * **************************************************************** */

    function reset_forget_password(Request $request) {
        if ($request->isMethod('post')) {

            if ($request->input('mobile_to_verify') != '') {

                $sid = Config::get('constants.sid');
                $token = Config::get('constants.token');
                $twilio = new Client($sid, $token);
                try {
                    $verification_check = $twilio->verify->v2->services(Config::get('constants.service_id'))
                            ->verificationChecks
                            ->create($request->input('otp'), // code
                            ["to" => $request->input('code_to_verify') . $request->input('mobile_to_verify')]
                    );
                    if ($verification_check->status == 'approved') {
                        if ($request->input('password') != $request->input('confirm_password')) {
                            $returnData['success'] = false;
                            $returnData['code'] = 402;
                            $returnData['description'] = 'Password and Confirm Password does not match.';
                            return json_encode($returnData);
                        }

                        $whr['phone'] = $request->input('mobile_to_verify');
                        $whr['phonecode'] = $request->input('code_to_verify');
                        $data['password'] = md5($request->input('password'));
                        $res = $this->av->update_data('login', $data, $whr);
                        $returnData['success'] = true;
                        $returnData['code'] = 200;
                        $returnData['description'] = 'Password reset successfully. You will be redirecting to login page.';
                        return json_encode($returnData);
                    } else {
                        $returnData['success'] = false;
                        $returnData['code'] = 402;
                        $returnData['description'] = 'OTP is incorrect.';
                        return json_encode($returnData);
                    }
                } catch (Exception $e) {
                    $returnData['success'] = false;
                    $returnData['code'] = 402;
                    $returnData['description'] = 'OTP is incorrect.';
                    return json_encode($returnData);
                }
            } else {
                $whrs['otp'] = $request->input('otp');
                $whrs['email'] = $request->input('email_to_verify');
                $count = $this->av->select_data_count('login', $whrs);
                if ($count[0]->count > 0) {
                    if ($request->input('password') != $request->input('confirm_password')) {
                        $returnData['success'] = false;
                        $returnData['code'] = 402;
                        $returnData['description'] = 'Password and Confirm Password does not match.';
                        return json_encode($returnData);
                    }
                    $whr['email'] = $request->input('email_to_verify');
                    $data['password'] = md5($request->input('password'));
                    $res = $this->av->update_data('login', $data, $whr);
                    $returnData['success'] = true;
                    $returnData['code'] = 200;
                    $returnData['description'] = 'Password reset successfully. You will be redirect to login page.';
                    return json_encode($returnData);
                } else {
                    $returnData['success'] = false;
                    $returnData['code'] = 402;
                    $returnData['description'] = 'Please enter correct OTP.';
                    return json_encode($returnData);
                }
            }
            $returnData['success'] = true;
            $returnData['code'] = 200;
            $returnData['description'] = 'Enter OTP recieved in your email.';
            return json_encode($returnData);
        }
        return view('common/header') . view('profile/forget-password');
    }

//end
}
