<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\LoginModel;
use App\ProductsModel;

class CampaignController extends Controller {
    /*     * **************************************************************** */

    ///Campaign work done by Nikunj Dhimar ///////////////////////////////
    /*     * **************************************************************** */

    protected $av;

    public function __construct() {
        $this->av = new LoginModel();
        $this->product_m = new ProductsModel();
    }

    public function index(Request $request) {
        $whr['user_id'] = $request->session()->get('user_id');
        $campdetails = $this->av->checkout_process($whr);
        $data['campaign_detail'] = $campdetails->all();
        $header['title'] = 'Campaign';
        return view('common/header', $header) . view('common/inner-top') . view('common/sp-sidenav') . view('product/product-orders', $data) . view('common/footer');
    }

    public function campaign_product($id) {
        $whr['id'] = $id;
        $campdetails = $this->av->checkout_process($whr)->first();
        $productDetails = (json_decode($campdetails->product_details));

        $data['campaign_detail'] = $campdetails;
        $data['product_details'] = $productDetails;
        foreach ($productDetails as $key => $product) {
            $whr['id'] = $product->id;
            $whr['select'] = 'name,attach,quantity,displayed_price';
            $productdetails = $this->av->get_product_detail($whr)->first();
            $data['product_details'][$key]->productdetails = $productdetails;
        }
        $header['title'] = 'Campaign Product';
        return view('common/header', $header) . view('common/inner-top') . view('common/sp-sidenav') . view('product/products-in-camp', $data) . view('common/footer');
    }

    public function delete_campaign(Request $request) {
        $updateArr['deleted_at'] = date('Y-m-d H:i:s');
        $whr['user_id'] = $request->session()->get('user_id');
        $whr['deleted_at'] = NULL;
        $this->av->update_data('checkout_process', $updateArr, $whr);

        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Campaign has been deleted successfully.';
        return json_encode($returnData);
        //return redirect()->action('UserController@dashboard');
    }

    // Play Pause campaign
    public function change_campaign(Request $request) {
        $whr['user_id'] = $request->session()->get('user_id');
        $whr['deleted_at'] = NULL;
        $campdetails = $this->av->checkout_process($whr);
        $camprow = $campdetails->first();
        if ($camprow->status == 0 || $camprow->status == 1)
            $updateArr['status'] = 2; //Pause
        else
            $updateArr['status'] = 0; //Genera.0

        $whr['id'] = $camprow->id;
        $this->av->update_data('checkout_process', $updateArr, $whr);

        $returnData['success'] = true;
        $returnData['code'] = 200;
        $returnData['description'] = 'Campaign has been changed successfully.';
        return json_encode($returnData);

        #return redirect()->action('UserController@dashboard');
    }

    public function store($id) {
        #sample ID : CPX38XBUX1
        if (!empty($id)) {
            $decodeID = explode('X', $id);
            $data['camp_id'] = $decodeID[1];
            $data['user_id'] = $decodeID[3];

            /* User Details */
            $userWhr['id'] = $data['user_id'];
            $userdetails = $this->av->user_details($userWhr)->first();
            #$userdetails->symbol = $request->session()->get('symbols');
            $data['userdetails'] = $userdetails;

            /* Campaign Details */
            $whr['id'] = $data['camp_id'];
            $whr['deleted_at'] = NULL;
            $campdetails = $this->av->checkout_process($whr)->first();
            $data['campaign_detail'] = $campdetails;

            /* Campaign product Details */
            $productDetails = (json_decode($campdetails->product_details));
            $data['product_details'] = $productDetails;
            
            foreach ($productDetails as $key => $product) {
                /*Check countdown Ready?
                $existCountDown =  DB::table('countdown')
                  ->select(DB::raw('id,startdate'))
                  ->where('product_id', '=', $product->id)
                  ->where('campaign_id', '=', $data['camp_id'])
                  ->where('winning_status', '=', 0)
                  ->get();
                if(!empty($existCountDown->first())){
                    if(!empty($existCountDown[0]->startdate)){
                        $timenow = date('Y-m-d H:i:s');
                        if(($existCountDown[0]->startdate.' 23:59:59') < $timenow){
                            $data['product_details'] = $productDetails;*/
                            
                            $whrProd['id'] = $product->id;
                            $productIDs[] = $product->id;
                            $whrProd['select'] = 'name,attach,quantity,displayed_price';
                            $productdetails = $this->av->get_product_detail($whrProd)->first();
                            $data['product_details'][$key]->productdetails = $productdetails;
                        /*}
                    }
                }*/
            }
            
            /* Random Codes */
            if (!empty($productIDs)) {
                $select = ['code'];
                $whrCol = 'product_id';
                $existCodes = $this->product_m->select_data_wherein('odd_codes', $select, $whrCol, $productIDs);
                $code_arr = array_map(function($e) {
                    return is_object($e) ? $e->code : $e['code'];
                }, $existCodes);
                $data['odd_codes'] = $code_arr;
            } else {
                $data['odd_codes'] = [];
            }
            /* Saved settings */
            $whrSetting['campaign_id'] = $data['camp_id'];
            $whrSetting['deleted_at'] = NULL;
            $select = ['settings.*'];
            $savedSettings = $this->av->select_data_common('settings', $select, $whrSetting);
            if(!empty($savedSettings))
                $savedSettings = $savedSettings[0];
            else
                $savedSettings = [];
            $data['settings'] = $savedSettings;
        }
        return view('product/store', $data). view('product/modals');
    }

}
