<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class WinnersModel extends Model
{
    public $table = 'winners';
}
