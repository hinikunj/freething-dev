<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class LoginModel extends Model {

    function add_data($table, $data,$batch = false) {
        if($batch)
            $last_id = DB::table($table)->insert($data);
        else
            $last_id = DB::table($table)->insertGetId($data);
        return $last_id;
    }

    function delete_data($table, $whr) {
        DB::table($table)
                ->where($whr)
                ->delete();
    }

    function simple_select($table, $field) {
        $users = DB::table($table)
                ->select($field)
                ->get();
        return $users;
    }

    function check_existing_user($data) {
        $users = DB::table('login')
                ->select(DB::raw('first_name,last_name,phone,role'))
                ->where('phone', '=', $data['phone'])
                ->get();
        return $users;
    }

    function check_existing_email($data) {
        $users = DB::table('login')
                ->select(DB::raw('first_name,last_name,phone,role'))
                ->where('email', '=', $data['email'])
                ->get();
        return $users;
    }

    function select_data($table, $whr) {
        $users = DB::table($table)
                ->select(DB::raw('id,first_name,last_name,phone,role'))
                ->where($whr)
                ->get();
        return $users;
    }

    function select_data_count($table, $whr) {
        $users = DB::table($table)
                ->select(DB::raw('count(*) as count'))
                ->where($whr)
                ->get();
        return $users;
    }

    function search_company($table, $whr) {

        $users = DB::table($table)
                ->select(DB::raw('id,company_name'))
                ->where('company_name', 'like', $whr['company_name'] . '%')
                ->get();
        return $users;
    }

    function login($whr) {
        $p = $whr['phone'];
        $users = DB::table('login')
                ->select(DB::raw('id,first_name,last_name,phone,role,location,company'))
                ->whereRaw("password = '" . $whr['password'] . "' and (phone = '" . $whr["phone"] . "' or email = '" . $whr["phone"] . "') and status=1 and auth_type = ''")
                ->get();
        return $users;
    }

    function user_details($whr) {

        $users = DB::table('login')
                ->select(DB::raw('id,first_name,last_name,phonecode,phone,role,company'))
                ->where($whr)
                ->get();
        return $users;
    }

    function get_locations() {

        $users = DB::table('location')
                ->select(DB::raw('id,location,currency,symbol'))
                ->get();
        return $users;
    }

    function get_products() {

        $users = DB::table('products')
                ->select(DB::raw('*'))
                ->where('status', 1)
                ->orderBy('id', 'desc')
                ->get();
        return $users;
    }

    function get_product_detail($whr) {
        //Case Added by Nikunj Dhimar
        if (isset($whr['select'])) {
            $select = DB::raw($whr['select']);
            unset($whr['select']);
        } else {
            $select = DB::raw('*');
        }

        $users = DB::table('products')
                ->select($select)
                ->where($whr)
                ->get();
        return $users;
    }

    function get_profile($whr) {

        $users = DB::table('login')
                ->select(DB::raw('id,first_name,last_name,phone,role,email,status,company,languages, location,website,industry'))
                ->whereRaw("phone = '" . $whr["phone"] . "' ")
                ->get();
        return $users;
    }

    function update_data($table, $data, $whr) {
        $affected = DB::table($table)
                ->where($whr)
                ->update($data);
    }

    function select_data_common($table, $data, $whr) {
        $d = implode(',', $data);
        $users = DB::table($table)
                ->select(DB::raw($d))
                ->where($whr)
                ->get()
                ->toArray();
        return $users;
    }

    function user_list() {
        $users = DB::table('login')
                ->select(DB::raw('*'))
                ->whereRaw("role != '0' and status != 0")
                ->get();
        return $users;
    }

    function get_products_foruser($whr) {
        if ($whr['location'] != '') {
            $users = DB::table('products')
                    ->select(DB::raw('*'))
                    ->whereRaw("status = 1 and location = '" . $whr['location'] . "' ")
                    ->orderBy('id', 'desc')
                    ->get();
        } else {
            $users = DB::table('products')
                    ->select(DB::raw('*'))
                    ->whereRaw("status = 1 and company = '" . $whr['company'] . "'")
                    ->orderBy('id', 'desc')
                    ->get();
        }

        return $users;
    }

    function get_products_foruser_filter($whr) {
        $cat = $whr['category'];
        $users = DB::table('products')
                ->select(DB::raw('*'))
                ->whereRaw("status = 1 and (location = '" . $whr['location'] . "' or company = '" . $whr['company'] . "') and category like '%" . $cat . "%'")
                ->orderBy('id', 'desc')
                ->get();

        return $users;
    }

    function select_c($table, $field, $whr, $orderBy = '') {
        if ($orderBy != '') {
            $users = DB::table($table)
                    ->select($field)
                    ->where($whr)
                    ->orderBy('featured', 'desc')
                    ->get();
        } else {
            $users = DB::table($table)
                    ->select($field)
                    ->where($whr)
                    ->get();
        }

        return $users;
    }

    function select_c_g($table, $field, $whr, $gfield) {
        $users = DB::table($table)
                ->select($field)
                ->where($whr)
                ->groupby($gfield)
                ->get();
        return $users;
    }

    function count_c($table, $whr) {
        $users = DB::table($table)
                ->select(DB::raw('count(*) as count'))
                ->where($whr)
                ->get();
        return $users;
    }

    function cart_count($whr) {
        $users = DB::table('cart')
                ->select(DB::raw('count(*) as count'))
                ->where($whr)
                ->get();
        return $users;
    }

    function cart($whr) {
        $users = DB::table('cart')
                ->select(DB::raw('products.id as product_id,products.name,products.attach,cart.odds,cart.winning_freq,products.price,products.shipping_cost,products.coupons,products.currency'))
                ->join('products', 'cart.product_id', '=', 'products.id', 'left')
                ->where('cart.user_id', $whr['user_id'])
                ->where('cart.status', 0)
                ->get();
        return $users;
    }

    function checkout_process($whr) {
        /* if(!isset($whr['status']))
          $whr['status'] = 0; */

        $users = DB::table('checkout_process')
                ->select(DB::raw('*'))
                #->where('user_id',$whr['user_id'])
                ->where($whr)
                #->where('status',0)
                ->orderBy('id', 'desc')
                #->limit(1)
                ->get();
        return $users;
    }

    function history($whr) {
        $users = DB::table('user_plans')
                ->select(DB::raw('user_plans.id,user_plans.plan as plan_id,plans.plan,plans.totalsms,plans.perprice,plans.sum,plans.featured,plans.expiry,
                     date(user_plans.created_date) as created_date,user_plans.expiry_date'))
                ->join('plans', 'user_plans.plan', '=', 'plans.id', 'left')
                ->where('user_plans.user_id', $whr['user_id'])
                ->orderBy('user_plans.id', 'desc')
                ->get();
        return $users;
    }

    function history_each($whr) {
        $users = DB::table('user_plans')
                ->select(DB::raw('user_plans.id,user_plans.plan as plan_id,plans.plan,plans.totalsms,plans.perprice,plans.sum,plans.featured,plans.expiry,
                     date(user_plans.created_date) as created_date,user_plans.expiry_date,user_plans.type'))
                ->join('plans', 'user_plans.plan', '=', 'plans.id', 'left')
                ->where('user_plans.user_id', $whr['user_id'])
                ->where('user_plans.id', $whr['id'])
                ->orderBy('user_plans.id', 'desc')
                ->get();
        return $users;
    }
}
?>