<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PhoneNumbersModel extends Model
{
    public $table = 'phone_numbers';
}
