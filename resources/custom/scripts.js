/*
 START REGISTRATION AND LOGIN 
 PRODUCT SECTION BEGINS
 PROFILE SECTION BEGINS
 
 */

$(document).ready(function () {
    $('.otpblock').css('display', 'none');
    $('#phoneOtpVerify').css('display', 'none');
    $('#resetForgetPwd').css('display', 'none');

    $(document).ajaxStart(function () {
        $("#wait").css("display", "block"), $(".window").hide();
    }),
            $(document).ajaxComplete(function () {
        $("#wait").css("display", "none");
    }),
            /*------------------- START REGISTRATION AND LOGIN --------------------------------*/

//////////////////////////////////////////////////////////////////////////////////////
            /* Registeration */
            /* controller - Login Controller/index */
/////////////////////////////////////////////////////////////////////////////////////
            $('#register').submit(function () { //enter form id 

        validate = 1;
        $(':input[required]', register).each(function () { //enter form id
            $(this).css('border', '1px solid #eee');
            $(this).next('span').remove();
            if (this.value.trim() === '') {
                $(this).css('border', '1px solid red');
                return false;
            }
        });
        $('input[type="text"]', register).each(function () { //enter form id
            $(this).css('border', '1px solid #eee');
            $(this).next('span').remove();
            str = $(this).attr('class');
            if (str.indexOf('txtField') >= 0)
            {
                var regex = /^[a-zA-Z\s]*$/;
                if (!regex.test($(this).val()))
                {
                    $(this).next('span').remove();
                    $(this).val('');
                    $(this).css('border', '1px solid red');
                    $(this).after('<span class="text-danger">Please enter only alphabets.</span>');
                    validate = 0;
                }
            }
            if (str.indexOf('emailFields') >= 0)
            {
                var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
                if (!filter.test($(this).val()))
                {
                    $(this).next('span').remove();
                    $(this).val('');
                    $(this).css('border', '1px solid red');
                    $(this).after('<span class="text-danger">Please enter valid email id.</span>');
                    validate = 0;
                    return false;
                }
            }
            if (str.indexOf('phoneFields') >= 0)
            {
                if ($(this).val() == 0)
                {
                    $(this).next('span').remove();
                    $(this).val('');
                    $(this).css('border', '1px solid red');
                    $(this).after('<span class="text-danger">Please enter valid mobile number.</span>');
                    validate = 0;
                    return false;
                }
                if (!$.isNumeric($(this).val()))
                {
                    $(this).next('span').remove();
                    $(this).val('');
                    $(this).css('border', '1px solid red');
                    $(this).after('<span class="text-danger">Please enter only numbers.</span>');
                    validate = 0;
                    return false;
                }
            }
            if (str.indexOf('mixTxtField') >= 0)
            {
                var regex = /^[a-zA-Z0-9.-\s]*$/;
                if (!regex.test($(this).val()))
                {
                    $(this).next('span').remove();
                    $(this).val('');
                    $(this).css('border', '1px solid red');
                    $(this).after('<span class="text-danger">Please enter only alphabets, numerics, dot(.) or hiphen (-)</span>');
                    validate = 0;
                    return false;
                }
            }

        });
        if (validate != 0)
        {
            var myform = document.getElementById('register'); //enter form id
            var fd = new FormData(myform);

            $.ajax({
                url: baseurl + 'register',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: fd,
                cache: false,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (result) {
                    console.log(result);
                    result = jQuery.parseJSON(result);
                    console.log(result.description);
                    //                             ph = '';
                    $('.message').removeClass('alert alert-success').removeClass('alert alert-danger');
                    if (result.success == true)
                    {
                        $('[name="code_to_verify"]').val($('[name="phonecode"]').val());
                        $('[name="mobile_to_verify"]').val($('[name="phone"]').val());
                        $('#phoneOtpVerify').css('display', 'block');
                        $('#register').css('display', 'none');
                        $('.otpmsg').html(result.description).css('color', 'blue');

                    } else
                    {
                        $('.message').html(result.description).addClass('alert alert-danger');
                    }

                }
            })

        }
        return false;
    });

//////////////////////////////////////////////////////////////////////////////////////
    /* Verify Phone */
    /* controller - Login Controller/login */
/////////////////////////////////////////////////////////////////////////////////////

    $('#phoneOtpVerify').submit(function () {
        var myform = document.getElementById('phoneOtpVerify'); //enter form id
        var fd = new FormData(myform);

        $.ajax({
            url: baseurl + 'verify-phone',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);
                $('.otpmsg').removeClass('alert alert-success').removeClass('alert alert-danger');

                if (result.success == true)
                {
                    $('#phoneOtpVerify').css('display', 'none');
                    $('.crtext').html(result.description);
                    setTimeout(function () {
                        window.location.href = baseurl + "login";
                    }, 3000);
                } else
                {

                    $('.otpmsg').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;

    });


//////////////////////////////////////////////////////////////////////////////////////
    /* Login */
    /* controller - Login Controller/login */
/////////////////////////////////////////////////////////////////////////////////////

    $('#login').submit(function () {
        var myform = document.getElementById('login'); //enter form id
        var fd = new FormData(myform);

        $.ajax({
            url: baseurl + 'login',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);

                $('.message').removeClass('alert alert-success').removeClass('alert alert-danger');
                if (result.success == true)
                {
                    if (result.list.role == 0)
                    {
                        window.location.href = baseurl + "sp-dashboard";
                    } else
                    {
                        window.location.href = baseurl + "dashboard";

                    }

                } else
                {
                    $('.message').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;
    });



    /*--------------------- END REGISTER AND LOGIN -----------------------------*
     
     /*--------------------- PRODUCT SECTION BEGINS -----------------------------*/

/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Add Product */
    /* controller - Product Controller/list */
/////////////////////////////////////////////////////////////////////////////////////

    $('#add-product').submit(function () {


        if ($('[name="category[]"]').is(":checked"))
        {

        } else
        {

            $('.message').html('Product category is required.').css('color', 'red');
            return false;
        }
        if ($('[name="type[]"]').is(":checked"))
        {

        } else
        {

            $('.message').html('Product Type is required.').css('color', 'red');
            return false;
        }
        if ($('[name="shipping_by[]"]').is(":checked"))
        {

        } else
        {

            $('.message').html('Shipping Responsibility is required.').css('color', 'red');
            return false;
        }


        var myform = document.getElementById('add-product'); //enter form id
        var fd = new FormData(myform);

        $.ajax({
            url: baseurl + 'add-product',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);

                $('.message').removeClass('alert alert-success').removeClass('alert alert-danger');
                if (result.success == true)
                {
                    $('.message').html(result.description).addClass('alert alert-success');
                } else
                {
                    $('.message').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;
    });



/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Search Company in Add Product */
    /* controller - Product Controller/addproduct */
/////////////////////////////////////////////////////////////////////////////////////

    $('.search-company').keyup(function () {
        $('.companylist').html('');
        if ($(this).val() != '')
        {
            $.ajax({
                url: baseurl + 'search-company',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: 'company=' + $(this).val(),
                cache: false,

                type: 'POST',
                success: function (result) {

                    result = jQuery.parseJSON(result);
                    $('.companylist').html('');
                    if (result.success == true)
                    {
                        $(result.list).each(function (k, v) {
                            $('.companylist').append(v.company_name);
                        })

                    } else
                    {
                        $('.companylist').html(result.description).addClass('alert alert-danger');
                    }

                }
            });
            return false;
        }
    });


/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Add Location in Add product Page */
/////////////////////////////////////////////////////////////////////////////////////

    $('.locselectd').change(function () {
        var symbol = $(".locselectd option:selected").attr('code');
        $('[name="location"]').val($(".locselectd option:selected").val());
        $('[name="currency"]').val($(".locselectd option:selected").attr('code'));
        $('[name="symbol"]').val($(".locselectd option:selected").attr('symbol'));
    })

    $('#add-locations').submit(function () {
        var myform = document.getElementById('add-locations'); //enter form id
        var fd = new FormData(myform);

        $.ajax({
            url: baseurl + 'add-location',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);
                $('.message').removeClass('alert alert-success').removeClass('alert alert-danger');
                if (result.success == true)
                {
                    $('#add-locations').find('input:text').val('');
                    ;
                    $('.message').html(result.description).addClass('alert alert-success');
                } else
                {
                    $('.message').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;
    });

/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Open Add Location Model */
/////////////////////////////////////////////////////////////////////////////////////

    $('.add-location-modal').click(function () {
        $('.message').html('').removeClass('alert alert-danger').removeClass('alert alert-success');
        $('#add-locations').find('input:text').val('');

        $('#add-location').modal('show');
    })

/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Open Add Products Modal */
/////////////////////////////////////////////////////////////////////////////////////

    $('.addproductpopup').click(function () {


        $('.dropzone').css('display', 'block');

        $('[name="location_type"]').prop('checked', false).removeAttr('readonly').removeAttr('disabled');
        $('[name="coupons"]').prop('checked', false).removeAttr('readonly').removeAttr('disabled');
        $('[name="category[]"]').prop('checked', false).removeAttr('readonly').removeAttr('disabled');
        $('[name="type[]"]').prop('checked', false).removeAttr('readonly').removeAttr('disabled');
        $('[name="shipping_by[]"]').prop('checked', false).removeAttr('readonly').removeAttr('disabled');

        $('.locsearch').removeAttr('readonly').removeAttr('disabled').css('display', 'none');
        $('.comsearch').removeAttr('readonly').removeAttr('disabled').css('display', 'none');
        $('[name="quantity"]').removeAttr('readonly').removeAttr('disabled');
	 
        $('.message').html('').removeClass('alert alert-danger').removeClass('alert alert-success');

        $('#addproductpopup .q_addons').html(0);
        $('#addproductpopup .p_addons').html(0);
        $('#addproductpopup .s_addons').html(0);
        $('#addproductpopup .sc_addons').html(0);
        $('#addproductpopup .pz_addons').html(0);
        $('#addproductpopup .d_addons').html(0);
        $('#addproductpopup .w_addons').html('Select');
        $('#addproductpopup .o_addons').html('Select');
        $('#addproductpopup .pr_addons').html('Select');
        $('#addproductpopup .t_addons').html('Select');



        $('#addproductpopup').modal('show');
    })

/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Open Add Products Modal with location from db */
/////////////////////////////////////////////////////////////////////////////////////

    $('.selectloc input[type="radio"]').click(function () {
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $.ajax({
            url: baseurl + 'get-locations',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);

                html = '<option value="">All Location</option>';
                $(result.list).each(function (k, v) {

                    html += '<option name="' + v.symbol + '">' + v.location + '</option>';

                })
                $('.locationbox').html(html);

            }
        });

        $(".box").not(targetBox).hide();
        $(targetBox).show();
    });

    /* --------------  ADD PRODUCT POPUP BOX FIELDS --------------------------*/
    $(document).delegate('.locationbox', 'change', function () {
        var symbol = $(".locationbox option:selected").attr('name');
        $('.symbol').html('<i class="fa fa-' + symbol + '" aria-hidden="true"></i>');
        $('#currencyfield').val(symbol);
    })

    $('[name="price"]').focusout(function () {
        $('.p_addons').html($(this).val());
    })

    $('[name="quantity"]').focusout(function () {
        $('.q_addons').html($(this).val());
    })
    $('[name="shipping_cost"]').focusout(function () {
        $('.s_addons').html($(this).val());
    })
    $('[name="shipping_cost"]').keyup(function () {
        var a = parseFloat($(this).val()) + parseFloat($('[name="price"]').val());

        $('[name="price_after_win"]').val(a);
        $('.price_after_win').val(a);
        $('.sc_addons').html(a);
    })
    $('.shipping_cost').keyup(function () {
        var a = parseFloat($(this).val()) + parseFloat($('.price').val());

        $('.price_after_win').val(a);
        $('.sc_addons').html(a);
    })
    $('[name="displayed_price"]').focusout(function () {
        $('.d_addons').html($(this).val());
    })
    $('[name="prize"]').focusout(function () {
        $('.pz_addons').html($(this).val());
    })
    $('[name="determine_winning"]').change(function () {
        $('.w_addons').html($(this).val().toUpperCase());
        if ($(this).val().toLowerCase() == 'freethings')
        {
            $('.odds-section').css('display', 'flex');
        } else
        {
            $('.odds-section').css('display', 'none');
        }
    })
    $('[name="odds"]').change(function () {
        $('.o_addons').html($(this).val().toUpperCase());
    })
    $('[name="provide_by"]').change(function () {
        $('.pr_addons').html($(this).val().toUpperCase());
    })
    $('[name="template"]').change(function () {
        $('.t_addons').html($("#flow_template option:selected").text());
        //$('.t_addons').html($(this).val().toUpperCase());
    })

    /* --------------  ADD POPUP BOX end --------------------------*/

/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Search Company in Add Product */
    /* controller - Product Controller/addproduct */
/////////////////////////////////////////////////////////////////////////////////////

    $('.view-product-details').click(function () {
        id = this.id;
        $('.dropzone').css('display', 'none');
        $('.showimg').html('');
        $.ajax({
            url: baseurl + 'get-product-detail/' + id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,

            type: 'GET',
            success: function (result) {

                result = jQuery.parseJSON(result);


                if (result.success == true)
                {
                    /* location from db to dropdown */
                    lochtml = '<option value="">All Location</option>';
                    $(result.locations).each(function (k, v) {

                        lochtml += '<option name="' + v.symbol + '">' + v.location + '</option>';

                    })
                    $('.locationbox').html(lochtml);
                    /* end location from db to dropdown */
                    $('#each-product-detail').modal('show');
                    $(result.list).each(function (k, v) {

                        $('.name').val(v.name).prop('readonly', 'readonly');
                        $('.description').val($(v.description).text()).prop('readonly', 'readonly');
                        $('.displayed_price').val(v.displayed_price).prop('readonly', 'readonly');
                        $('.odds').val(v.odds).prop('disabled', 'disabled');
                        $('.price').val(v.price).prop('readonly', 'readonly');
                        $('.price_after_win').val(v.price_after_win).prop('readonly', 'readonly');
                        $('.provide_by').val(v.provide_by).prop('disabled', 'disabled');
                        $('.quantity').val(v.quantity).prop('readonly', 'readonly');
                        $('.prize').val(v.prize).prop('readonly', 'readonly');

                        $('.shipping_cost').val(v.shipping_cost).prop('readonly', 'readonly');
                        $('.template').val(v.template).prop('disabled', 'disabled');
                        $('.determine_winning').val(v.determine_winning).prop('disabled', 'disabled');

                        $('#each-product-detail .q_addons').html(v.quantity);
                        $('#each-product-detail .p_addons').html(v.price);
                        $('#each-product-detail .s_addons').html(v.shipping_cost);
                        $('#each-product-detail .sc_addons').html(v.price_after_win);
                        $('#each-product-detail .d_addons').html(v.displayed_price);
                        $('#each-product-detail .w_addons').html(v.determine_winning);
                        $('#each-product-detail .o_addons').html(v.odds);
                        $('#each-product-detail .pr_addons').html(v.provide_by);
                        $('#each-product-detail .t_addons').html(v.template);
                        $('#tags').val(v.data);
                        $('#each-product-detail .pz_addons').html(v.prize);

                        display_loc = (v.location_type == "locsearch") ? 'block' : 'none';
                        display_com = (v.location_type == "comsearch") ? 'block' : 'none';

                        $('.locsearch').css('display', display_loc);
                        $('.comsearch').css('display', display_com);
                        $('.location').css('display', display_loc);
                        $('.company').css('display', display_com);

                        $('.locsearch').val(v.location).prop('disabled', 'disabled');
                        $('.comsearch').val(v.company).prop('disabled', 'disabled');

                        $('.category').prop('disabled', true);
                        $('.type').prop('disabled', true);
                        $('.shipping_by').prop('disabled', true);
                        $('.coupons').prop('disabled', true);
                        $('.location_type').prop('disabled', true);

                        $(v.category.split(',')).each(function (a, b) {

                            $('[value="' + b + '"]').prop('checked', true);
                        })

                        $(v.type.split(',')).each(function (a, b) {
                            console.log(b);
                            $('[value="' + b + '"]').prop('checked', true);
                        })

                        $(v.shipping_by.split(',')).each(function (a, b) {
                            console.log(b);
                            $('[value="' + b + '"]').prop('checked', true);
                        })

                        $('[value="' + v.location_type + '"]').prop('checked', true).prop('readonly', 'readonly');
                        $('[value="' + v.category + '"]').prop('checked', true).prop('readonly', 'readonly');
                        $('[value="' + v.coupons + '"]').prop('checked', true).prop('readonly', 'readonly');
                        $('[value="' + v.shipping_by + '"]').prop('checked', true).prop('readonly', 'readonly');
                        $('[value="' + v.type + '"]').prop('checked', true).prop('readonly', 'readonly');


                        var symbol = $(".locationbox option:selected").attr('name');
                        $('.symbol').html('<i class="fa fa-' + symbol + '" aria-hidden="true"></i>');

                        $('.btnsubmit').css('display', 'none');
                        htmlImg = '';
                        $(v.attach.split(',')).each(function (k, j) {
                            htmlImg += '<img src="' + baseurl + productImg + j + '" width="100" height="100"/>';
                        })
                        $('.showimg').html(htmlImg);

                    })

                } else
                {
                    $('.companylist').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;

    });


/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Delete Product in Product List */

/////////////////////////////////////////////////////////////////////////////////////

    $('.delProduct').click(function () {
        id = this.id;
          $.confirm({
                        title: 'Confirmation',
                        content: 'Are you sure you want to delete the product?',
                        buttons: {
                                formSubmit: {
                                        text: 'Yes',
                                        btnClass: 'btn-blue',
                                        action: function () {
                       
                                                $.ajax({
                            url: baseurl + 'delete-product/' + id,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            cache: false,

                            type: 'GET',
                            success: function (result) {
                                result = jQuery.parseJSON(result);

                                $('#pr_' + id).remove();
                                 $.confirm({
                                               title: 'Completed',
                                               content: result.description,
                                               type: 'blue',
                                               typeAnimated: true,
                                               buttons: {
                                                        close: function () {
                                                            jconfirm.instances[0].close();
                                                            }
                                                 }
                                          });
                            }

                        });
                          return false;
                                        }
                                },
                                cancel: function () {
                                        //close
                                },
                        },
           
                });
    })


/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Edit Product */

/////////////////////////////////////////////////////////////////////////////////////

    $('.edit-product').click(function () {
        id = this.id;
        $('.showimg').html('');
        $('.message').html('').removeClass('alert alert-success').removeClass('alert alert-danger');
        $('.dropzone').css('display', 'block');
        $.ajax({
            url: baseurl + 'get-product-detail/' + id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,

            type: 'GET',
            success: function (result) {

                result = jQuery.parseJSON(result);
                if (result.success == true)
                {
                    $('#each-product-detail').modal('show');
                    $(result.list).each(function (k, v) {
                        /* location from db to dropdown */
                        lochtml = '<option value="">All Location</option>';
                        $(result.locations).each(function (k, v) {

                            lochtml += '<option name="' + v.symbol + '">' + v.location + '</option>';

                        })
                        $('.locationbox').html(lochtml);
                        /* end location from db to dropdown */

                        $('.name').val(v.name).removeAttr('readonly');
                        $('.description').val(v.description).removeAttr('readonly');
                        $('.displayed_price').val(v.displayed_price).removeAttr('readonly');
                        $('.odds').val(v.odds).removeAttr('readonly').removeAttr('disabled');
                        $('.price').val(v.price).removeAttr('readonly');
                        $('.price_after_win').val(v.price_after_win).removeAttr('readonly');
                        $('.provide_by').val(v.provide_by).removeAttr('disabled');
                        $('.quantity').val(v.quantity).removeAttr('readonly');
                        $('.prize').val(v.prize).removeAttr('readonly');

                        $('#each-product-detail .q_addons').html(v.quantity);
                        $('#each-product-detail .p_addons').html(v.price);
                        $('#each-product-detail .s_addons').html(v.shipping_cost);
                        $('#each-product-detail .sc_addons').html(v.price_after_win);
                        $('#each-product-detail .d_addons').html(v.displayed_price);
                        $('#each-product-detail .w_addons').html(v.determine_winning);
                        $('#each-product-detail .o_addons').html(v.odds);
                        $('#each-product-detail .pr_addons').html(v.provide_by);
                        $('#each-product-detail .t_addons').html(v.template);
                        $('#each-product-detail .pz_addons').html(v.prize);
                        $('#tags_addons').val(v.data);

                        $('.shipping_cost').val(v.shipping_cost).removeAttr('readonly');
                        $('.template').val(v.template).removeAttr('disabled');


                        $('.determine_winning').val(v.determine_winning).removeAttr('disabled');

                        display_loc = (v.location_type == "locsearch") ? 'block' : 'none';
                        display_com = (v.location_type == "comsearch") ? 'block' : 'none';

                        $('.locsearch').css('display', display_loc);
                        $('.comsearch').css('display', display_com);
                        $('.location').css('display', display_loc);
                        $('.company').css('display', display_com);

                        $('.locsearch').val(v.location).removeAttr('readonly');
                        $('.comsearch').val(v.company).removeAttr('readonly');

                        $('.category').removeAttr('disabled');
                        $('.type').removeAttr('disabled');
                        $('.shipping_by').removeAttr('disabled');
                        $('.coupons').removeAttr('disabled');
                        $('.location_type').removeAttr('disabled');

                        $('[value="' + v.location_type + '"]').prop('checked', true).removeAttr('readonly');
                        $('[value="' + v.coupons + '"]').prop('checked', true).removeAttr('readonly');

                        var symbol = $(".locationbox option:selected").attr('name');
                        $('.symbol').html('<i class="fa fa-' + symbol + '" aria-hidden="true"></i>');

                        $(v.category.split(',')).each(function (a, b) {

                            $('[value="' + b + '"]').prop('checked', true).removeAttr('readonly');
                        })

                        $(v.type.split(',')).each(function (a, b) {

                            $('[value="' + b + '"]').prop('checked', true).removeAttr('readonly');
                        })

                        $(v.shipping_by.split(',')).each(function (a, b) {

                            $('[value="' + b + '"]').prop('checked', true).removeAttr('readonly');
                        })

                        $('[name="id"]').val(v.id);
                        $('[name="attach"]').val(v.attach);
                        $('.btnsubmit').css('display', 'block');

                        htmlImg = '';
                        $(v.attach.split(',')).each(function (k, j) {
                            htmlImg += '<div class="img-wrap"><span class="close removeImage" data-prod_id="' + id + '" data-src="' + j + '">&times;</span><img src="' + baseurl + productImg + j + '" width="100" height="100"/></div>';
                        })
                        $('.showimg').html(htmlImg);
                    })

                } else
                {
                    $('.companylist').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;

    });


/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Update Product */

/////////////////////////////////////////////////////////////////////////////////////

    $('#update-product').submit(function () {
        id = $('[name="id"]').val();
        if ($('[name="category[]"]').is(":checked"))
        {

        } else
        {

            $('.message').html('Product category is required.').css('color', 'red');
            return false;
        }
        if ($('[name="type[]"]').is(":checked"))
        {

        } else
        {

            $('.message').html('Product Type is required.').css('color', 'red');
            return false;
        }
        if ($('[name="shipping_by[]"]').is(":checked"))
        {

        } else
        {

            $('.message').html('Shipping Responsibility is required.').css('color', 'red');
            return false;
        }


        var myform = document.getElementById('update-product'); //enter form id
        var fd = new FormData(myform);

        $.ajax({
            url: baseurl + 'update-product',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);

                $('.message').removeClass('alert alert-success').removeClass('alert alert-danger');
                if (result.success == true)
                {
                    $('.disprice_' + id).html($('#each-product-detail .price').val());
                    $('.disshipping_' + id).html($('#each-product-detail .shipping_cost').val());
                    $('.disodds_' + id).html($('#each-product-detail .odds').val());
                    $('.disafterwin_' + id).html($('#each-product-detail .price_after_win').val());

                    $('.message').html(result.description).addClass('alert alert-success');
                } else
                {
                    $('.message').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;
    });

//////////////////////////////////////////////////////////////////////////////////////
    /* Quick edit quantity from product list */

/////////////////////////////////////////////////////////////////////////////////////

    $('[name="quantities"]').focusout(function () {
        id = this.id;
        $.ajax({
            url: baseurl + 'update-product',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: 'quantity=' + $(this).val() + '&id=' + this.id,

            type: 'POST',
            success: function (result) {
                result = jQuery.parseJSON(result);
                $('.message').removeClass('alert alert-success').removeClass('alert alert-danger');
                if (result.success == true)
                {
                    console.log('.q_' + this.id);
                    $('.q_' + id).html(result.description);
                } else
                {
                    $('.q_' + id).html(result.description);
                }
            }
        });
        return false;
    })


    /*------------------ PRODUCT SECTION ENDS ------------------------------*/

    /*------------------ PROFILE SECTION BEGINS -----------------------------*/

    //////////////////////////////////////////////////////////////////////////////////////
    /* General Profile */
    /* controller - Login Controller/edit-profile */
/////////////////////////////////////////////////////////////////////////////////////
    $('#gnrlProfile').submit(function () { //enter form id 

        validate = 1;
        $(':input[required]', gnrlProfile).each(function () { //enter form id
            $(this).css('border', '1px solid #eee');
            $(this).next('span').remove();
            if (this.value.trim() === '') {
                $(this).css('border', '1px solid red');
                return false;
            }
        });
        $('input[type="text"]', gnrlProfile).each(function () { //enter form id
            $(this).css('border', '1px solid #eee');
            $(this).next('span').remove();
            str = $(this).attr('class');
            if (str.indexOf('txtField') >= 0)
            {
                var regex = /^[a-zA-Z\s]*$/;
                if (!regex.test($(this).val()))
                {
                    $(this).next('span').remove();
                    $(this).val('');
                    $(this).css('border', '1px solid red');
                    $(this).after('<span class="text-danger">Please enter only alphabets.</span>');
                    validate = 0;
                }
            }
            if (str.indexOf('emailFields') >= 0)
            {
                var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
                if (!filter.test($(this).val()))
                {
                    $(this).next('span').remove();
                    $(this).val('');
                    $(this).css('border', '1px solid red');
                    $(this).after('<span class="text-danger">Please enter valid email id.</span>');
                    validate = 0;
                    return false;
                }
            }
            if (str.indexOf('phoneFields') >= 0)
            {
                if ($(this).val() == 0)
                {
                    $(this).next('span').remove();
                    $(this).val('');
                    $(this).css('border', '1px solid red');
                    $(this).after('<span class="text-danger">Please enter valid mobile number.</span>');
                    validate = 0;
                    return false;
                }
                if (!$.isNumeric($(this).val()))
                {
                    $(this).next('span').remove();
                    $(this).val('');
                    $(this).css('border', '1px solid red');
                    $(this).after('<span class="text-danger">Please enter only numbers.</span>');
                    validate = 0;
                    return false;
                }
            }
            if (str.indexOf('mixTxtField') >= 0)
            {
                var regex = /^[a-zA-Z0-9.-\s]*$/;
                if (!regex.test($(this).val()))
                {
                    $(this).next('span').remove();
                    $(this).val('');
                    $(this).css('border', '1px solid red');
                    $(this).after('<span class="text-danger">Please enter only alphabets, numerics, dot(.) or hiphen (-)</span>');
                    validate = 0;
                    return false;
                }
            }

        });
        if (validate != 0)
        {
            var myform = document.getElementById('gnrlProfile'); //enter form id
            var fd = new FormData(myform);

            $.ajax({
                url: baseurl + 'edit-profile',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: fd,
                cache: false,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (result) {
                    console.log(result);
                    result = jQuery.parseJSON(result);
                    console.log(result.description);
                    //                             ph = '';
                    $('.message').removeClass('alert alert-success').removeClass('alert alert-danger');
                    if (result.success == true)
                    {
                        $('.message').html(result.description).addClass('alert alert-success');

                    } else
                    {
                        $('.message').html(result.description).addClass('alert alert-danger');
                    }

                }
            })

        }
        return false;
    });


//////////////////////////////////////////////////////////////////////////////////////
    /* Reset Password from edit profile */
    /* controller - Login Controller/edit-profile */
/////////////////////////////////////////////////////////////////////////////////////
    $('#resetpwd').submit(function () { //enter form id 

        validate = 1;
        $(':input[required]', resetpwd).each(function () { //enter form id
            $(this).css('border', '1px solid #eee');
            $(this).next('span').remove();
            if (this.value.trim() === '') {
                $(this).css('border', '1px solid red');
                return false;
            }
        });

        if (validate != 0)
        {
            var myform = document.getElementById('resetpwd'); //enter form id
            var fd = new FormData(myform);

            $.ajax({
                url: baseurl + 'reset-password',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: fd,
                cache: false,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (result) {
                    console.log(result);
                    result = jQuery.parseJSON(result);
                    console.log(result.description);
                    //                             ph = '';
                    $('.messagepwd').removeClass('alert alert-success').removeClass('alert alert-danger');
                    if (result.success == true)
                    {
                        $('.messagepwd').html(result.description).addClass('alert alert-success');

                    } else
                    {
                        $('.messagepwd').html(result.description).addClass('alert alert-danger');
                    }

                }
            })

        }
        return false;
    });


/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Update Location in edit profile Page */
/////////////////////////////////////////////////////////////////////////////////////

    $('#profileloc').submit(function () {
        var myform = document.getElementById('profileloc'); //enter form id
        var fd = new FormData(myform);

        $.ajax({
            url: baseurl + 'update-profile-location',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);
                $('.locmessage').removeClass('alert alert-success').removeClass('alert alert-danger');
                if (result.success == true)
                {
                    $('#add-locations').find('input:text').val('');
                    ;
                    $('.locmessage').html(result.description).addClass('alert alert-success');
                } else
                {
                    $('.locmessage').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;
    });

/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Update Language in edit profile Page */
/////////////////////////////////////////////////////////////////////////////////////

    $('#langProfile').submit(function () {
        var myform = document.getElementById('langProfile'); //enter form id
        var fd = new FormData(myform);

        $.ajax({
            url: baseurl + 'update-profile-lang',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);
                $('.langmessage').removeClass('alert alert-success').removeClass('alert alert-danger');
                if (result.success == true)
                {
                    $('#add-locations').find('input:text').val('');
                    ;
                    $('.langmessage').html(result.description).addClass('alert alert-success');
                } else
                {
                    $('.langmessage').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;
    });

/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Update Industry in edit profile Page */
/////////////////////////////////////////////////////////////////////////////////////

    $('#industryAdd').submit(function () {
        var myform = document.getElementById('industryAdd'); //enter form id
        var fd = new FormData(myform);

        $.ajax({
            url: baseurl + 'update-profile-industry',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);
                $('.langmessage').removeClass('alert alert-success').removeClass('alert alert-danger');
                if (result.success == true)
                {
                    $('#add-locations').find('input:text').val('');
                    ;
                    $('.indmessage').html(result.description).addClass('alert alert-success');
                } else
                {
                    $('.indmessage').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;
    });


/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* deactivate account edit profile Page */
/////////////////////////////////////////////////////////////////////////////////////

    $('.delete-acc').click(function () {

        $.ajax({
            url: baseurl + 'delete-account',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            type: 'GET',
            success: function (result) {

                result = jQuery.parseJSON(result);
                $('.delmessage').removeClass('alert alert-success').removeClass('alert alert-danger');
                if (result.success == true)
                {
                    $('.delmessage').html(result.description).addClass('alert alert-success');
                    setTimeout(function () {
                        window.location.href = baseurl + "login";
                    }, 2000);

                } else
                {
                    $('.delmessage').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;
    });

/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* deactivate account edit profile Page */
/////////////////////////////////////////////////////////////////////////////////////

    $('#change-email').submit(function () {

        var myform = document.getElementById('change-email'); //enter form id
        var fd = new FormData(myform);

        $.ajax({
            url: baseurl + 'update-email',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);
                $('.emailmessage').removeClass('alert alert-success').removeClass('alert alert-danger');
                $('.otpmessage').html('');
                if (result.success == true)
                {
                    $('.otpblock').css('display', 'block');
                    $('.otpsub').html('Verify OTP');
                    if ($('[name="otp"]').val() == '')
                    {
                        $('.otpmessage').html(result.description).css('color', 'blue');
                    } else
                    {
                        $('[name="otp"]').val('');
                        $('.otpblock').css('display', 'none');
                        $('.emailmessage').html(result.description).addClass('alert alert-success');
                    }

                } else
                {
                    $('.otpblock').css('display', 'block');
                    $('.emailmessage').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;
    });

    $('#forget-password').submit(function () {
        var myform = document.getElementById('forget-password'); //enter form id
        var fd = new FormData(myform);

        $.ajax({
            url: baseurl + 'forget-password',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);
                $('.langmessage').removeClass('alert alert-success').removeClass('alert alert-danger');
                if (result.success == true)
                {
                    if ($('[name="phone"]').val() != '')
                    {
                        $('[name="code_to_verify"]').val($('[name="phonecode"]').val());
                        $('[name="mobile_to_verify"]').val($('[name="phone"]').val());
                    } else
                    {
                        $('[name="email_to_verify"]').val($('[name="email"]').val());
                    }
                    $('#add-locations').find('input:text').val('');
                    ;
                    $('.indmessage').html(result.description).addClass('alert alert-success');
                    $('#forget-password').css('display', 'none');
                    $('#resetForgetPwd').css('display', 'block');
                } else
                {
                    $('.indmessage').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;
    });

//////////////////////////////////////////////////////////////////////////////////////
    /* Reset password phone verify */
    /* controller - Login Controller/login */
/////////////////////////////////////////////////////////////////////////////////////

    $('#resetForgetPwd').submit(function () {
        $('.crtext').html('').removeClass('alert alert-danger').removeClass('alert alert-success');
        $('.otpmsg').html('').removeClass('alert alert-danger').removeClass('alert alert-success');
        var myform = document.getElementById('resetForgetPwd'); //enter form id
        var fd = new FormData(myform);

        $.ajax({
            url: baseurl + 'reset-forget-password',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);
                $('.otpmsg').removeClass('alert alert-success').removeClass('alert alert-danger');

                if (result.success == true)
                {
                    $('#phoneOtpVerify').css('display', 'none');
                    $('.crtext').html(result.description);
                    setTimeout(function () {
                        window.location.href = baseurl + "login";
                    }, 3000);
                } else
                {

                    $('.otpmsg').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;

    });


    /*------------------ PROFILE SECTION ENDS -----------------------------*/

    /*------------------------ USER MANAGEMENT START ---------------------*/

////////////////////////////////////////////////////////////////////////////
//////////////////////// UPDATE USER STATUS ////////////////////////////////
////////////////////////////////////////////////////////////////////////////
    $('.userStatus').click(function () {
        status = $(this).attr('status');
        if ($(this).attr('status') == 1)
        {
            text = 'Activate';
        } else if ($(this).attr('status') == 0)
        {
            text = 'Delete';
        } else
        {
            text = 'Hold';
        }
        id = this.id;
          $.confirm({
                        title: 'Confirmation',
                        content: 'Are you sure you want to ' + text + ' the user?',
                        buttons: {
                                formSubmit: {
                                        text: 'Yes',
                                        btnClass: 'btn-blue',
                                        action: function () {
                       
                                                $.ajax({
                            url: baseurl + 'update-user-status/' + id + '/' + status,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            cache: false,

                            type: 'GET',
                            success: function (result) {
                                $('#status_' + id).html(text);
                                result = jQuery.parseJSON(result);


                                 $.confirm({
                                               title: 'Completed',
                                               content: result.description,
                                               type: 'blue',
                                               typeAnimated: true,
                                               buttons: {
                                                        close: function () {
                                                            jconfirm.instances[0].close();
                                                            }
                                                 }
                                          });
                            }

                        });
                          return false;
                                        }
                                },
                                cancel: function () {
                                        //close
                                },
                        },
           
                });

    })





    /*------------------------ USER MANAGEMENT END ---------------------*/

    /*------------------------ CHECKOUT PROCESS START ---------------------*/

////////////////////////////////////////////////////////////////////////////
//////////////////////// ADD TO CART ////////////////////////////////
////////////////////////////////////////////////////////////////////////////

    $('.addtocart').click(function () {
        id = this.id;
        odds = ($('[name="odds"]').val() == undefined) ? '' : $('[name="odds"]').val();
        winning_freq = $('[name="winning_freq"]').val();
            $.ajax({
            url: baseurl + 'add-to-cart/' + id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
            data: 'id=' + id + '&odds=' + odds + '&winning_freq=' + winning_freq,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);
                if (result.success == true)
                {
                    hh = '';
                    $(result.cartdetails).each(function (k, v) {

                        hh += '<li class="list-group-item">' + v.name + '</li>';
                    })
                    // alert(hh);
                    $('.cartname').html(hh);
                    k = parseInt($('.cart-bubble').html()) + 1;
                    $('.cart-bubble').html(k);
                    $('.cartbtn_' + id).html('Added').removeClass('addtocart');
                } else
                {
                     $.confirm({
                                   title: 'Completed',
                                   content: result.description,
                                   type: 'blue',
                                   typeAnimated: true,
                                   buttons: {
                                            close: function () {
                                                jconfirm.instances[0].close();
                                                }
                                     }
                              });
                }




            }

        });
          return false;
    })


    $('.storecheck').click(function () {
        key = $(this).attr('key');
        console.log(key);
        if ($(this).val() == 'addtostore')
        {
            winning = $('#winning_' + key).val();
            today = 0;
        } else
        {

            winning = $('#winning_' + key).val();
            winning_freq = $('#winning_freq_' + key).val();
            today = winning * winning_freq;
        }

        $('.today_' + $(this).attr('key')).html(today);
        $('#today_' + $(this).attr('key')).val(today);
        $('#today_' + $(this).attr('key')).val(today);

        /* calculating total charges for total usd */
        totalcal = 0;
        $('.today_cal').each(function (k, v) {
            console.log("nim" + k);
            totalcal = parseFloat(totalcal) + parseFloat($('#today_' + k).val());
            console.log($('#today_' + k).val() + "=" + totalcal);
            $('[name="totalcharges"]').val(totalcal);
            $('.distot').html(totalcal);
            $('.totalchargesdisplay').html(totalcal);
        })
        tj = parseFloat($('.one').html()) + parseFloat($('.two').html());

        $('.totalusddisplay').html(tj);
        $('.totalusd').val(tj);
        /* end calculating total charges for total usd */
    })


    $('.wwfreq').keyup(function () {

        key = $(this).attr('key');

        // console.log($('input[name="storecheck'+key+'"]:checked').val());
        if ($('input[name="storecheck' + key + '"]:checked').val() == 'addtostore')
        {
            winning = $('#winning_' + key).val();
            today = 0;
        } else
        {
            console.log("yes");
            winning = $('#winning_' + key).val();
            winning_freq = $('#winning_freq_' + key).val();
            today = winning * winning_freq;
        }
        console.log(today);
        $('.today_' + $(this).attr('key')).html(today);
        $('#today_' + $(this).attr('key')).val(today);
        $('#today_' + $(this).attr('key')).val(today);

        /* calculating total charges for total usd */
        totalcal = 0;
        $('.today_cal').each(function (k, v) {

            totalcal = parseFloat(totalcal) + parseFloat($('#today_' + k).val());

            $('[name="totalcharges"]').val(totalcal);
            $('.distot').html(totalcal);
            $('.totalchargesdisplay').html(totalcal);
        })
        tj = parseFloat($('.one').html()) + parseFloat($('.two').html());

        $('.totalusddisplay').html(tj);
        $('.totalusd').val(tj);
        /* end calculating total charges for total usd */
    })

    $('#checkoutform').submit(function () {
        var myform = document.getElementById('checkoutform'); //enter form id
        var fd = new FormData(myform);

        $.ajax({
            url: baseurl + 'checkout_process',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);

                if (result.success == true)
                {
                    window.location.href = baseurl + "checkout";
                } else
                {
                    $('.indmessage').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;
    })

    $('.cardselect').click(function () {
        $('.currentid').val(this.id);
        $('.f').removeAttr('id').removeClass('require-validation');
        $('.f_' + this.id).attr('id', 'payment-form').addClass('require-validation');
        $('.e').removeClass('error');
        $('#e_' + this.id).addClass('error');
    })

    $('.placeorder').click(function () {
        $('.placeorderdata_' + $('.currentid').val()).click();

    })

    $('[name="card-number"]').keyup(function () {
        $('.require-validation').removeAttr('id').removeClass('require-validation');
        $('.f').attr('id', 'payment-form').addClass('require-validation');


    })

/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Delete Product in Product List */

/////////////////////////////////////////////////////////////////////////////////////

    $('.delcartitem').click(function () {
        id = this.id;
          $.confirm({
                        title: 'Confirmation',
                        content: 'Are you sure you want to delete the product?',
                        buttons: {
                                formSubmit: {
                                        text: 'Yes',
                                        btnClass: 'btn-blue',
                                        action: function () {
                       
                                                $.ajax({
                            url: baseurl + 'delete-cart-product/' + id,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            cache: false,

                            type: 'GET',
                            success: function (result) {
                                result = jQuery.parseJSON(result);
                                k = parseInt($('.cart-bubble').html()) - 1;
                                $('.cart-bubble').html(k);
                                if (k == 0)
                                {
                                    $('.consolidate').remove();
                                    $('.totalchargesdisplay').html(0);
                                    $('.totalusddisplay').html(0);
                                    $('.chkoutbtn').remove();
                                }
                                $('#c_' + id).remove();
                                 $.confirm({
                                               title: 'Completed',
                                               content: result.description,
                                               type: 'blue',
                                               typeAnimated: true,
                                               buttons: {
                                                        close: function () {
                                                            jconfirm.instances[0].close();
                                                            }
                                                 }
                                          });
                            }

                        });
                          return false;
                                        }
                                },
                                cancel: function () {
                                        //close
                                },
                        },
           
                });




    });
    
        /////////////////////////////////////////////////////////////////////////////////////////////////////
    /* User to view product images */

/////////////////////////////////////////////////////////////////////////////////////
 
      $('.user-to-view-product-images').click(function () {
         id = this.id;
          $.ajax({
            url: baseurl + 'get-product-detail/' + id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,

            type: 'GET',
            success: function (result) {

                result = jQuery.parseJSON(result);


                if (result.success == true)
                {

                    imageArr = result.list[0].attach.split(',');
                    dataArr = result.list[0].data.split(',');
                    if (result.list[0].currency == 'inr')
                    {
                        symbol = '<i class="fa fa-inr"></i>'
                    }
                    // console.log(result.list);

                    html = '<div>'; 
                    $(imageArr).each(function (k, v) {
                        html += '<img src="' + baseurl + productImg + v + '" alt="Product" >';
                    })

              
                    html += '</div>';
                    $('.product-info-box').html(html);
                    $('#product-info').modal('show');


                } else
                {
                    $('.companylist').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;
         

     });
     
  

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /* User to view product details */

/////////////////////////////////////////////////////////////////////////////////////

    $('.user-to-view-product-details').click(function () {
        id = this.id;

        $.ajax({
            url: baseurl + 'get-product-detail/' + id,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,

            type: 'GET',
            success: function (result) {

                result = jQuery.parseJSON(result);


                if (result.success == true)
                {

                    imageArr = result.list[0].attach.split(',');
                    dataArr = result.list[0].data.split(',');
                    if (result.list[0].currency == 'inr')
                    {
                        symbol = '<i class="fa fa-inr"></i>'
                    }
                    // console.log(result.list);

                    html = '<div class="row">' +
                            '<div class="col-sm-12 col-lg-4 col-xl-4">' +
                            '<div class="product-name mb-20">' +
                            '<h3>' + result.list[0]['name'] + '</h3>' +
                            '</div>' +
                            '<div class="product-gallery" style="width:100%;">';
                    $(imageArr).each(function (k, v) {
                        html += '<img src="' + baseurl + productImg + v + '" alt="Product" class="" style="width:30%;">&nbsp;&nbsp;';
                    })

                    html += '</div>' +
                            '<div class="product-name mb-20">' +
                            '<h4 class="font-18 mt-20">Description :</h4>' +
                            '<p>' + result.list[0]['description'] + '</p>' +
                            '<h4 class="font-18 mt-20">Product Prize Value :</h4>' +
                            '<p><i class="' + result.symbol[result.list[0]['currency']] + '"></i>' + result.list[0]['prize'] + '</p>' +
                            '<h4 class="font-18 mt-20">Odd Of Winning :</h4>' +
                            '<p>' + result.list[0]['odds'] + '</p>' +
                            '</div>' +
                            '</div>' +
                            '<div class="col-sm-12 col-lg-4 col-xl-4">' +
                            '<div class="product-name mb-20">' +
                            '<h4 class="font-18 mt-20">Delivery Method :</h4>' +
                            '<p>SMS and Email</p>' +
                            '<h4 class="font-18 mt-20">Product Category</h4>' +
                            '<p>' + result.list[0]['category'] + '</p>' +
                            '<h4 class="font-18 mt-20">Product Charges</h4>' +
                            '<p><i class="' + result.symbol[result.list[0]['currency']] + '"></i>' + result.list[0]['price_after_win'] + '</p>' +
                            '<h4 class="font-18 mt-20">Product Shipping</h4>' +
                            '<p><i class="' + result.symbol[result.list[0]['currency']] + '"></i>' + result.list[0]['shipping_cost'] + '</p>' +
                            '<h4 class="font-18 mt-20">Product ID</h4>' +
                            '<p>' + result.list[0]['id'] + '</p>' +
                            '</div>' +
                            '</div>' +
                            '<div class="col-sm-12 col-lg-4 col-xl-4">' +
                            '<div class="product-name mb-20">' +
                            '<h4 class="font-18 mt-20">Product provided by</h4>' +
                            '<p>' + result.list[0]['provide_by'] + '</p>' +
                            '<h4 class="font-18 mt-20">Product fullment by</h4>' +
                            '<p>' + result.list[0]['determine_winning'] + '</p>' +
                            '<h4 class="font-18 mt-20">Product location</h4>' +
                            '<p>' + result.list[0]['location'] + '</p>' +
                            '<h4 class="font-18 mt-20">Data Capture</h4>' +
                            '<p>';
                    $(dataArr).each(function (k, v) {
                        html += '<a href="#" class="download-link badge badge-success badge-pill">' + v + '</a>';
                    })
                    html += '</p>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    $('.product-info-box').html(html);
                    $('#product-info').modal('show');


                } else
                {
                    $('.companylist').html(result.description).addClass('alert alert-danger');
                }

            }
        });
        return false;

    });

/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Product filter in User side */

/////////////////////////////////////////////////////////////////////////////////////
    $('.typefilter').click(function () {
        // alert($(this).html())
        $('[name="typefilter"]').val($(this).html());
        $('.typefilterform').click();
    })

    $('.select_plan').change(function () {

        type = $(".select_plan option:selected").val();

        $.ajax({
            url: baseurl + 'plan-details/' + type,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            type: 'GET',
            success: function (result) {
                $('.showplansinbilling').css('display', 'block');
                result = jQuery.parseJSON(result);
                $('.fixprice').html(result.plan[0].fixprice);
                html = '';
                $(result.plan).each(function (k, v) {
                    chk = (k == 0) ? 'checked' : '';
                    html += '<input ' + chk + ' shtype="' + v.plan + '" num="' + v.id + '" fix="' + result.plan[0].fixprice + '" id="pd_' + v.id + '" sum="' + v.sum + '" type="radio" name="drone" class="slctplan">' +
                            '<span shtype="' + v.plan + '" > ' + v.totalsms + '/' + v.symbol + v.price + '/sms' + '<br></span>';
                })
                $('.planprice').html(0);
                total_usd = parseFloat($('.tusd').html()) + parseFloat($('.fixprice').html());
                $('.tusd').html(total_usd);
                $('[name="totalusd"]').val(total_usd);
                $('.showpland').html(html);
            }
        });
        $('[shtype="Free"]').css('display', 'none');
        $('[shtype="Basic"]').css('display', 'none');
        $('[shtype="Premium"]').css('display', 'none');
        $('[shtype="Custom"]').css('display', 'none');
        $('[shtype="' + type + '"]').css('display', 'block');
        code = $(".select_plan option:selected").attr('code');


        // $('#pd_'+code).attr('checked','checked');
        $('[name="plan_no"]').val(code);


    })

    $(document).delegate('.slctplan', 'click', function () {
// $('.slctplan').click(function(){
        tusd = $(this).attr('sum');
        fixed = $(this).attr('fix');
        $('[name="plan_no"]').val($(this).attr('num'));
        $.ajax({
            url: baseurl + 'getsymbol/' + tusd,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            type: 'GET',
            success: function (result) {
                result = jQuery.parseJSON(result);

                $('.planprice').html(tusd);
                j = parseFloat(tusd) + parseFloat(fixed);
                $('.billingtotal').html(j)
                $('.billingtotal').val(j)
                tusd = result.price;

                total_usd = parseFloat($('.tusd').html()) + parseFloat(tusd);
                $('.tusd').html(total_usd);
                $('[name="totalusd"]').val(total_usd);
            }
        })


    })
    /*------------------------ CHECKOUT PROCESS END ---------------------*/

    $('.autofill').click(function () {
        $('.typeofpayment').val('auto');
    })
    $('.endcampaign').click(function () {
        $('.typeofpayment').val('end');
    })
 

/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Delete Product in Product List */

/////////////////////////////////////////////////////////////////////////////////////

    $('.delCard').click(function () {
        id = this.id;
          $.confirm({
                        title: 'Confirmation',
                        content: 'Are you sure you want to delete the card?',
                        buttons: {
                                formSubmit: {
                                        text: 'Yes',
                                        btnClass: 'btn-blue',
                                        action: function () {
                       
                                                $.ajax({
                            url: baseurl + 'delete-card/' + id,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            cache: false,

                            type: 'GET',
                            success: function (result) {
                                result = jQuery.parseJSON(result);

                                $('#cr_' + id).remove();
                                 $.confirm({
                                               title: 'Completed',
                                               content: result.description,
                                               type: 'blue',
                                               typeAnimated: true,
                                               buttons: {
                                                        close: function () {
                                                            jconfirm.instances[0].close();
                                                            }
                                                 }
                                          });
                            }

                        });
                          return false;
                                        }
                                },
                                cancel: function () {
                                        //close
                                },
                        },
           
                });




    })

    /* Add Billing Address */

    $('#add_address').submit(function () {
        var myform = document.getElementById('add_address'); //enter form id
        var fd = new FormData(myform);

        $.ajax({
            url: baseurl + 'add-address',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (result) {

                result = jQuery.parseJSON(result);

                if (result.success == true)
                {
                    $('.addmsg').html(result.description).addClass('text text-success');
                } else
                {
                    $('.addmsg').html(result.description).addClass('text text-danger');
                }

            }
        });
        return false;
    })


    /* end of Billing Address */

/////////////////////////////////////////////////////////////////////////////////////////////////////
    /* Cancel Subscription */

/////////////////////////////////////////////////////////////////////////////////////

    $('.cancelsubs').click(function () {
        id = this.id;
          $.confirm({
                        title: 'Confirmation',
                        content: 'Are you sure you want to cancel the subscription?',
                        buttons: {
                                formSubmit: {
                                        text: 'Yes',
                                        btnClass: 'btn-blue',
                                        action: function () {
                       
                                                $.ajax({
                            url: baseurl + 'cancel-subscription/' + id,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            cache: false,

                            type: 'GET',
                            success: function (result) {
                                result = jQuery.parseJSON(result);

                                $('#cr_' + id).remove();
                                 $.confirm({
                                               title: 'Completed',
                                               content: result.description,
                                               type: 'blue',
                                               typeAnimated: true,
                                               buttons: {
                                                        close: function () {
                                                            jconfirm.instances[0].close();
                                                            }
                                                 }
                                          });
                            }

                        });
                          return false;
                                        }
                                },
                                cancel: function () {
                                        //close
                                },
                        },
           
                });




    })

    $('.nodata').click(function () {
         $.confirm({
                       title: 'Warning',
                       content: "You don't have any plan yet. Please create Campaign to buy plan.",
                       type: 'blue',
                       typeAnimated: true,
                       buttons: {
                                close: function () {
                                    jconfirm.instances[0].close();
                                    }
                         }
                  });
    })
    /* end of document */


    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /* To attach file in add product */
    /* controller - Product Controller/upload_product */
/////////////////////////////////////////////////////////////////////////////////////


    $(".fileattach").dropzone({

        url: baseurl + 'upload-product',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (file, response) {


            var obj = jQuery.parseJSON(response);
            file.id = obj.id;
            file.filename = obj.filename;
            // Create the remove button
            var removeButton = Dropzone.createElement("<button style='margin: 10px 0 0 15px;'>Remove file</button>");

            // Capture the Dropzone instance as closure.
            var _this = this;

            // Listen to the click event
            removeButton.addEventListener("click", function (e) {
                // Make sure the button click doesn't submit the form:
                e.preventDefault();
                e.stopPropagation();
                $.ajax({
                    url: "file/delete",
                    type: "GET",
                    data: {"name": file.filename}
                }).done(function (response) {
                    var existAttach = $('[name="attach"]').val();
                    var values = existAttach.split(',');
                    for (var i = 0; i < values.length; i++) {
                        if (values[i] == file.filename) {
                            values.splice(i, 1);
                            values = values.join(',');
                        }
                    }
                    $('[name="attach"]').val(values);
                });
                // Remove the file preview.
                _this.removeFile(file);
            });

            // Add the button to the file preview element.
            file.previewElement.appendChild(removeButton);

            if (file.height < 1000 || file.width < 1000)
            {
                 $.confirm({
                               title: 'Invalid',
                               content: 'Image must be of 1000 x 1000',
                               type: 'blue',
                               typeAnimated: true,
                               buttons: {
                                        close: function () {
                                            jconfirm.instances[0].close();
                                        }
                                 }
                          });

                $.ajax({
                    url: "file/delete",
                    type: "GET",
                    data: {"name": file.filename}
                });
                _this.removeFile(file);
                return false;
            }
            if (file.size < 2000)
            {
                 $.confirm({
                               title: 'Invalid',
                               content: 'Image must be of more than 100 MB.',
                               type: 'blue',
                               typeAnimated: true,
                               buttons: {
                                        close: function () {
                                            jconfirm.instances[0].close();
                                            }
                                 }
                          });
                this.removeFile(file);
                return false;
            }
            result = jQuery.parseJSON(response);
            if (result.success == true)
            {
                if ($('[name="attach"]').val() == '')
                {
                    $('[name="attach"]').val(result.filename);
                } else
                {
                    $('[name="attach"]').val($('[name="attach"]').val() + ',' + result.filename);
                }
            } else
            {
                $(this).remove();
            }

        }
    });


    $(document).on("click", ".removeImage", function () {
        //$('.removeImage').click(function () {
        var filename = $(this).data('src');
        var prod_id = $(this).data('prod_id');
          $.confirm({
                        title: 'Confirmation',
                        content: 'Are you sure you want to delete the product?',
            autoClose: 'cancel|7000',
                        buttons: {
                                formSubmit: {
                                        text: 'Yes',
                                        btnClass: 'btn-blue',
                                        action: function () {

                        return $.ajax({
                            url: "file/delete",
                            type: "GET",
                            data: {"name": filename, "prod_id": prod_id}
                        }).done(function (response) {
                            var existAttach = $('[name="attach"]').val();
                            var values = existAttach.split(',');
                            for (var i = 0; i < values.length; i++) {
                                if (values[i] == filename) {
                                    values.splice(i, 1);
                                    values = values.join(',');
                                }
                            }
                            $('[name="attach"]').val(values);
                            $(".img-wrap").filter(":has(img[src*='" + filename + "'])").remove();
                        }).fail(function () {
                            //self.setContent('Something went wrong.');
                        });
                                        }
                                },
                                cancel: function () {
                                        //close
                                },
                        },
                });
    });

    /* END */


})
