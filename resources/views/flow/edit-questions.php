<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-0">SMS FLOW</h4>


                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col -->


            </div>
            <div class="row">
                <div class="col-xl-6 height-card box-margin">
                    <ul class="nav nav-pills navtab-bg">
                        <li class="nav-item">
                            <a href="sms-flow" class="nav-link active mb-3">
                                Creating Flow
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="flow" class="nav-link mb-3">
                                Flow Template List
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="questions" class="nav-link mb-3">
                                Add Questions
                            </a>
                        </li>


                    </ul>
                </div> <!-- end col -->
                
                

            </div>
            <div class="row">
                <div class="col-xl-4 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="kt-portlet__head-toolbar">
                                <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-bold" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#kt_portlet_tabs_1_1_1_content" role="tab" aria-selected="true">
                                            Questions
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_tabs_1_1_3_content" role="tab" aria-selected="false">
                                            Conditions
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- <h4 class="card-title">Questions</h4> -->
                            <div class="row">
                                <div class="tab-content">
                                    <div class="tab-pane fade active show" id="kt_portlet_tabs_1_1_1_content" role="tabpanel">
                                                                
                                        <div class="col-sm-12 col-xs-12" ondrop="drop(event)" ondragover="allowDrop(event)">
                                            <?php foreach($questions as $question) { ?>
                                            <div class="alert alert-warning nodrop" id="div_<?php echo $question->question_id;?>" role="alert" draggable="true" ondragstart="drag(event)" >
                                                <?php echo $question->question;?>
                                                <!-- <div class="float-right">
                                                    <a href="#" data-id="<?php echo $question->question_id;?>" class="mr-2 edit_question"><i class="fa fa-edit text-info font-18"></i></a>
                                                    <a href="#" data-id="<?php echo $question->question_id;?>" class="delete_question"><i class="fa fa-trash text-danger font-18"></i></a>
                                                </div> -->
                                                <input type="hidden" name="question_ids[]" value="<?php echo $question->question_id;?>">
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="kt_portlet_tabs_1_1_3_content" role="tabpanel">
                                        <div class="col-sm-12 col-xs-12" ondrop="drop(event)" ondragover="allowDrop(event)">
                                            
                                            <div class="alert alert-info nodrop" id="div_cond_1" role="alert" draggable="true" ondragstart="drag(event)" >
                                                Check for New/Old Customer
                                            </div>
                                            
                                        </div>    
                                    </div>      
                                </div>      
                            </div>


                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col -->

                <div class="col-xl-8 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12" style="height:500px" style="overflow:auto">
                                    <form id="form_flow">
                                        <input type="hidden" name="flow_id" id="flow_id" value="<?php echo $data[0]->flow_id; ?>">

                                        <div class="form-group mb-3">
                                            <label for="exampleInputEmail111">Name Flow</label>
                                            <input type="text" id="flow_name" name="flow_name" class="form-control"
                                                placeholder="Flow Name Goes Here" value="<?php echo $data[0]->flow_name; ?>">
                                        </div>


                                        <div id="div_questions" ondrop="drop(event)" ondragover="allowDrop(event)" draggable="true" ondragstart="drag(event)" style="height:400px;overflow:auto">
                                        <span>Drag and Drop questions here</span>
                                        <?php foreach($data as $question) { ?>
                                            <div class="alert alert-warning" id="div_<?php echo $question->question_id; ?>" role="alert"><?php echo $question->question; ?>
                                            <div class="float-right"><a href="#" data-id="<?php echo $question->question_id; ?>" class="mr-2 edit_question"><i class="fa fa-edit text-info font-18"></i></a>
                                            <input type="hidden" name="question_ids[]" value="<?php echo $question->question_id; ?>">
                                            <a href="#" data-id="<?php echo $question->question_id; ?>" class="delete_question"><i class="fa fa-trash text-danger font-18"></i></a></div></div>
                                       <?php } ?>
                                        </div>
                                    </form>
                                </div> <!-- end col -->


                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <input type="button" class="btn btn-info mr-2 btn-sm mb-3" id="publish_flow"
                                        value="Publish Flow">
                                </div>
                            </div>
                            <!-- end row-->
                        </div> <!-- end card-body -->
                    </div> <!-- end card-->
                </div> <!-- end col -->
            </div>


        </div>