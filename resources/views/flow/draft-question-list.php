
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12 height-card box-margin">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title mb-0">Questions</h4>


                                </div>
                            </div> <!-- end card-->
                        </div> <!-- end col -->


                    </div>
                    <div class="row">
                        <div class="col-xl-10 height-card box-margin">
                            <ul class="nav nav-pills navtab-bg">
                                <li class="nav-item">
                                    <a href="sms-flow" class="nav-link mb-3">
                                        Creating Flow
                                    </a>
                                </li>



                            </ul>
                        </div> <!-- end col -->
                        <div class="col-xl-2 height-card box-margin">
                            <div class="form-group w-100">

                                <input type="text" class="form-control" id="exampleInputEmail111" placeholder="Search">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-xl-12 height-card box-margin">
                            <div class="card">
                                <div class="card-body">
                                    <table class="table table-hover" id="dragable-xxxxxx">

                                        <tbody>
                                        <?php foreach($flows as $flow) { ?>
                                            <tr>
                                                
                                                <td><?php echo $flow->flow_id ?></td>
                                                <td><?php echo $flow->flow_name ?></td>

                                                <td>
                                                    <a href="flow/<?php echo $flow->flow_id ?>" class="mr-2"><i class="fa fa-edit text-info font-18"></i></a>
                                                    <a href="#" ><i class="fa fa-trash text-danger font-18"></i></a>
                                                </td>
                                            </tr>
                                        <?php } ?>    
                                        </tbody>
                                    </table>


                                </div>
                            </div> <!-- end card-->
                        </div> <!-- end col -->


                    </div>


                </div>

                