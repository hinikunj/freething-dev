<script>

$('#question_type').change(function() {
    var question_type = $(this).val();
    if (question_type == 2) {
        $('.cloned-row').show();
        $('#clone').show();
        $('#addQueLabel').text('Adding Question');
        $('#replyLabel').text('Error message'); //$('.shortAns').hide();
    } else if (question_type == 3) {
        $('.cloned-row').hide();
        $('#clone').hide();
        $('.shortAns').hide();
        $('#addQueLabel').text('Enter Instruction');
    } else {
        $('.cloned-row').hide();
        $('#clone').hide();
        $('#addQueLabel').text('Adding Question');
        $('#replyLabel').text('Reply'); //$('.shortAns').show();
    }
});

$(document).on('click','.text-danger',function(){
    $(this).closest('.cloned-row').remove();
})

$('#btn_question').click(function() {
    var flow_id = $('#flow_id').val();
    var x = $("#form_question").serialize();
    $.ajax({
        url: baseurl+'add-questions',
        type: 'POST',
        "dataType": "json",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        //data: {"is_company_specific":is_company_specific,"company_id":company_id,"category":category,"sub_category":sub_category},
        data: x+"&flow_id="+flow_id,
        success: function(data) {
            //called when successful
            $('#flow_id').val(data.flow_id);
            if ($('#hidden_question_id').val() > 0) {
                
                var question_id = data.question_id;
               var question_text = $('#question_text').val();
               question_text += '<div class="float-right"><a href="#" data-id="'+question_id+'" class="mr-2 edit_question"><i class="fa fa-edit text-info font-18"></i></a>';
               question_text += '<input type="hidden" name="question_ids[]" value="'+question_id+'">';
               question_text += '<a href="#" data-id="'+question_id+'" class="delete_question"><i class="fa fa-trash text-danger font-18"></i></a></div></div>';
               $('#div_'+question_id).html(question_text);
               $('#form_question')[0].reset();
               $('#btn_question').text('Submit');
               $('#hidden_question_id').remove();
               $('#multiple_options').empty();
               $('.cloned-row').hide();
                $('#clone').hide();

            } else {
                var question_id = data.question_id;
               var question_text = '<div class="alert alert-warning" id="div_'+question_id+'" role="alert">'+$('#question_text').val();
               question_text += '<div class="float-right"><a href="#" data-id="'+question_id+'" class="mr-2 edit_question"><i class="fa fa-edit text-info font-18"></i></a>';
               question_text += '<input type="hidden" name="question_ids[]" value="'+question_id+'">';
               question_text += '<a href="#" data-id="'+question_id+'" class="delete_question"><i class="fa fa-trash text-danger font-18"></i></a></div></div>';
               $('#div_questions').append(question_text);
               $('#form_question')[0].reset();
               $('.cloned-row').hide();
                $('#clone').hide();
                
            }
                $option_html = '<div class="cloned-row w-100">';
                $option_html += '<div class="form-row"><div class="form-group col-md-5">';
                $option_html += '<label for="exampleInputPassword11">Expected Selection</label>';
                $option_html += '<input type="text" class="form-control expected_selection" id="exampleInputPassword11" name="expected_selection[]" ></div>';
                $option_html += '<div class="form-group col-md-1 text-center pt-4"><label class="mt-2" for="exampleInputPassword11 ">OR</label><br></div>';
                $option_html += '<div class="form-group col-md-5"><label for="exampleInputPassword12">Acceptable Response</label>';
                $option_html += '<input type="text" class="form-control acceptable_response" id="exampleInputPassword12" name="acceptable_response[]" ></div>';
                $option_html += '<div class="form-group col-md-1 text-center pt-4 "><a href="#" class=""><i class="fa fa-trash text-danger font-18 mt-2"></i></a>';
                $option_html += '</div></div></div>';
                $('#copy_div').html($option_html);
                $('.cloned-row').hide();
        },
        error: function(e) {
            //called when there is an error
            //console.log(e.message);
        }
    });
});
$(document).on('click','a.delete_question',function(){
var question_id = $(this).attr('data-id');
//Delete Question
$.confirm({
    title: 'Delete Question?',
    content: 'You will not recover your question again.',
    autoClose: 'cancel|8000',
    buttons: {
        deleteQuestion: {
            text: 'delete question',
            action: function () {
                //var question_id = this.$target.attr('data-id');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                return $.ajax({
                    url: baseurl+'delete-questions',
                    dataType: 'json',
                    method: 'post',
                    data: 'question_id=' + question_id
                }).done(function (response) {
                    if(response.success)
                        $('#div_'+question_id).remove();
                }).fail(function () {
                    //self.setContent('Something went wrong.');
                });
            }
        },
        cancel: function () {
            //$.alert('action is canceled');
        }
    }
});
});

$(document).on("click",".edit_question",function() {
    $('#multiple_options').empty();
    var question_id = $(this).data('id');
    $.ajax({
        url: baseurl+'edit-questions',
        type: 'POST',
        "dataType": "json",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {"question_id":question_id},
        
        success: function(data) {
            //called when successful
            //console.log(data);
            $('#question_type').val(data.data.question_type);
            $('#question_text').val(data.data.question);
            if(data.data.question_type == 2){
                $('#addQueLabel').text('Adding Question');
                $('#replyLabel').text('Error message'); //$('.shortAns').hide();
            } else if(data.data.question_type == 3){
                $('.shortAns').hide();
                $('#addQueLabel').text('Enter Instruction');
            } else {
                $('#addQueLabel').text('Adding Question');
                $('#replyLabel').text('Reply'); //$('.shortAns').show();
            }
            $('#answer_text').val(data.data.answer);
            var saveResponse = '<option value="'+data.dataset.id+'">'+data.dataset.sub_category+'</option>';
            $('#save_response_to').append(saveResponse);
            $('#save_response_to').val(data.data.save_response_to);
            var question_type = data.data.question_type;
            if (question_type == 2) {
                //$('.cloned-row').show();
                $('#clone').show();
            } else {
                $('.cloned-row').hide();
                $('#clone').hide();
            }
            if(data.data.question_type == '2') {
                for(var i=0;i<data.multiple.length;i++) {
                    
                $option_html = '<div class="cloned-row w-100">';
                $option_html += '<div class="d-flex justify-content-start">';
                $option_html += '<div class="form-group mr-2">';
                $option_html += '<label for="expected_selection">Expected Selection</label>';
                $option_html += '<input type="text" class="form-control expected_selection" id="expected_selection" name="expected_selection[]" value="'+data.multiple[i].expected_selection+'"></div>';
                $option_html += '<div class="form-group text-center pt-4 mr-2">';
                $option_html += '<label class="mt-2" for="exampleInputPassword11 ">OR</label><br></div>';
                $option_html += '<div class="form-group mr-2">';
                $option_html += '<label for="acceptable_response">Acceptable Response</label>';
                $option_html += '<input type="text" class="form-control acceptable_response" id="acceptable_response" name="acceptable_response[]" value="'+data.multiple[i].acceptable_response+'"></div>';
                
                $option_html += '<div class="form-group mr-2">';
                $option_html += '<label for="widget_bot">Widget Bot</label>';
                $option_html += '<input type="text" class="form-control widget_bot" id="widget_bot" name="widget_bot[]" value="'+data.multiple[i].widget_bot+'"></div>';
                
                $option_html += '<div class="form-group mr-2">';
                $option_html += '<label for="reply">Reply</label>';
                $option_html += '<input type="text" class="form-control reply" id="reply" name="reply[]" value="'+data.multiple[i].reply+'"></div>';
                
                $option_html += '<div class="form-group text-center pt-4 mr-2 ">';
                $option_html += '<a href="#" class=""><i class="fa fa-trash text-danger font-18 mt-2"></i></a></div>';
                $option_html += '</div></div>';
                $('#multiple_options').append($option_html);
                }
            }



            $("#form_question").append('<input type="hidden" id="hidden_question_id" name="question_id" value="'+question_id+'">');
            $('#btn_question').text('Update');
        },
        error: function(e) {
            //called when there is an error
            //console.log(e.message);
        }
    });
});


//Delete Flow
$('a.deleteFlow').confirm({
    title: 'Delete Flow?',
    content: 'You will not recover your flow again.',
    autoClose: 'cancel|8000',
    buttons: {
        deleteFlow: {
            text: 'delete flow',
            action: function () {
                var flow_id = this.$target.attr('data-id');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                return $.ajax({
                    url: baseurl+'delete-flow',
                    dataType: 'json',
                    method: 'post',
                    data: 'flow_id=' + flow_id
                }).done(function (response) {
                    if(response.success)
                        $('#div_'+flow_id).remove();
                }).fail(function () {
                    //self.setContent('Something went wrong.');
                });
            }
        },
        cancel: function () {
            //$.alert('action is canceled');
        }
    }
});

$('#publish_flow').click(function() {
    var flow_name = $('#flow_name').val();
    //var question_ids = $('#question_ids').val();  || question_ids == ''
    if (flow_name == '') {
        $.alert({
            title: 'Flow validation!',
            content: 'Please enter flow name and add questions!',
        });
        return false;
    }
    var x = $("#form_flow").serialize();
    $.ajax({
        url: baseurl+'add-flow',
        type: 'POST',
        "dataType": "json",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        //data: {"is_company_specific":is_company_specific,"company_id":company_id,"category":category,"sub_category":sub_category},
        data: x+"&published=1",
        success: function(data) {
            //called when successful

            if (data.success == false) {
                alert('Duplicate Dataset Entry')
            } else {
               //location.reload();
               location.href=baseurl+"flow";
            }
        },
        error: function(e) {
            //called when there is an error
            //console.log(e.message);
        }
    });
});

$('a.remove_question').click(function(){
   var question_id = $(this).attr("data-id");
   if(question_id > 0)
        $('#div_'+question_id).remove(); 
   else
        $(this).parent().remove();
});

</script>