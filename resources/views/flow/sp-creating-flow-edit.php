<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-0">SMS FLOW</h4>


                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col -->


            </div>
            <div class="row">
                <div class="col-xl-6 height-card box-margin">
                    <ul class="nav nav-pills navtab-bg">
                        <li class="nav-item">
                            <a href="<?php echo url('/') ;?>/sms-flow" class="nav-link active mb-3">
                                Creating Flow
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="flow" class="nav-link mb-3">
                                Flow Template List
                            </a>
                        </li>


                    </ul>
                </div> <!-- end col -->
                <div class="col-xl-4 height-card box-margin">
                    <div class="form-row w-100">
                        <div class="form-group col-md-4">
                            <!-- <label for="exampleInputEmail111" class="pt-2">Condition Reference</label> -->
                        </div>

                        <div class="form-group col-md-8">
                            <!-- <select id="inputState" class="form-control w-100">
                                <option>Select Condition</option>
                                <?php foreach($conditions as $key=>$condition) { ?>
                                <option value="<?php echo $condition->condition_id;?>" title="<?php echo $condition->condition_text;?>"><?php echo $condition->label; ?></option>
                                <?php } ?>
                            </select> -->
                        </div>
                    </div>

                </div> <!-- end col -->
                <div class="col-xl-2 box-margin">

                    <button type="submit" class="btn btn-info mr-2 w-100">Create New Flow Template</button>
                </div> <!-- end col -->

            </div>
            <div class="row">
                <div class="col-xl-4 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Adding Questions</h4>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form action="add-questions" method="post" id="form_question">

                                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                        <div class="form-group">
                                            <label for="exampleInputEmail111">Question Type</label>
                                            <select id="question_type" class="form-control" name="question_type">

                                                <option value="1">Single Question</option>
                                                <option value="2">Multiple Type</option>

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail12">Adding Question</label>
                                            <textarea id="question_text"
                                                class="form-control form-control-md g-resize-none rounded-0" rows="3"
                                                name="question" placeholder="Adding Question"></textarea>
                                        </div>
                                        <div id="multiple_options">
                                        </div>
                                        <div id="copy_div">
                                        <div class="cloned-row w-100" style="display:none">
                                            <div class="form-row">
                                                <div class="form-group col-md-5">
                                                    <label for="exampleInputPassword11">Expected Selection</label>
                                                    <input type="text" class="form-control expected_selection" id="exampleInputPassword11"
                                                        name="expected_selection[]" placeholder="Expected Selection">
                                                </div>
                                                <div class="form-group col-md-1 text-center pt-4">
                                                    <label class="mt-2" for="exampleInputPassword11 ">OR</label><br>


                                                </div>
                                                <div class="form-group col-md-5">
                                                    <label for="exampleInputPassword12">Acceptable Response</label>
                                                    <input type="text" class="form-control acceptable_response" id="exampleInputPassword12"
                                                        name="acceptable_response[]" placeholder="Acceptable Response">
                                                </div>
                                                <div class="form-group col-md-1 text-center pt-4 ">
                                                    <a href="#" class=""><i class="fa fa-trash text-danger font-18 mt-2"></i></a>


                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <input type="button" class="btn btn-info mr-2 btn-sm mb-3" value="Add Response" id="clone" style="display:none">
                                        
                                        <div class="form-group">
                                            <label for="exampleInputEmail111">Save Response To:</label>
                                            <select id="save_response_to" class="form-control" name="save_response_to">
                                            <?php foreach($dataset as $res) { ?>
                                                <option value="<?php echo $res->datasets_id; ?>"><?php echo $res->sub_category; ?></option>
                                            <?php } ?>   
                                            </select>
                                        </div>
                                        <button type="button" id="btn_question" class="btn btn-info mr-2">Submit</button>

                                    </form>
                                </div>
                            </div>


                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col -->

                <div class="col-xl-8 height-card box-margin">
                    <div class="card">
                        <div class="card-body">



                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="form_flow">

                                    <input type="hidden" name="flow_id" id="flow_id" value="<?php echo $data[0]->flow_id; ?>">

                                        <div class="form-group mb-3">
                                            <label for="exampleInputEmail111">Name Flow</label>
                                            <input type="text" id="flow_name" name="flow_name" class="form-control" value="<?php echo $data[0]->flow_name; ?>"
                                                placeholder="Flow Name Goes Here">
                                        </div>

                                        <div class="form-group">
                                            <input name="tags" id="tags"
                                                value="Condition 1,Condition 2,Condition 3,Condition 4" />
                                        </div>
                                        <div id="div_questions">
                                        <?php foreach($data as $question) { ?>
                                            <div class="alert alert-warning" id="div_<?php echo $question->question_id; ?>" role="alert"><?php echo $question->question; ?>
                                            <div class="float-right"><a href="#" data-id="<?php echo $question->question_id; ?>" class="mr-2 edit_question"><i class="fa fa-edit text-info font-18"></i></a>
                                            <input type="hidden" name="question_ids[]" value="<?php echo $question->question_id; ?>">
                                            <a href="#" data-id="<?php echo $question->question_id; ?>" class="delete_question"><i class="fa fa-trash text-danger font-18"></i></a></div></div>
                                       <?php } ?>
                                        </div>

                                        
                                        
                                        <input type="button" class="btn btn-info mr-2 btn-sm mb-3" value="Add condition"
                                            id="clone">


                                    </form>
                                </div> <!-- end col -->


                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <input type="button" class="btn btn-info mr-2 btn-sm mb-3" id="publish_flow" value="Publish Flow">
                                </div>
                            </div>
                            <!-- end row-->
                        </div> <!-- end card-body -->
                    </div> <!-- end card-->
                </div> <!-- end col -->
            </div>


        </div>