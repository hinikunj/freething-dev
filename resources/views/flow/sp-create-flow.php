<style>
    .draggable-main{
        width: 100%;

    }
    .dragbox {
        width: 100%;
    }
    .dragbox:last-child{
        margin: 0px;
    }
    .connected-sortable {
        list-style: none;
        padding: 0px;
        margin: 0px;
    }
    .dragbox ul li{
        width: inherit;
        padding: 15px 20px;


        color: #000;
        font-size: 14px;
        cursor: all-scroll;


    }
    .dragbox ul li:hover {

        background-color: #fff;
        color: #000;
        cursor: all-scroll;
    }
    .dragbox ul li.ui-sortable-helper {
        background-color: #e5e5e5;
        -webkit-box-shadow: 0 0 8px rgba(53, 41, 41, 0.8);
        -moz-box-shadow: 0 0 8px rgba(53, 41, 41, 0.8);
        box-shadow: 0 0 8px rgba(53, 41, 41, 0.8);
        transform: scale(1.015);
        z-index: 100;
    }
    .dragbox ul li.ui-sortable-placeholder {
        background-color: #ddd;
        -moz-box-shadow: inset 0 0 10px #000000;
        -webkit-box-shadow: inset 0 0 10px #000000;
        box-shadow: inset 0 0 10px #000000;
    }
    .create-flow-l-side #draggable-left .remove_question {
        visibility: hidden;
    }
    .create-flow-r-side #draggable-right .remove_question{
        visibility: visible;
    }
</style>
<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-0">Create SMS Flow</h4>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col -->
            </div>
            <div class="row">
                <div class="col-xl-10 height-card box-margin">
                    <ul class="nav nav-pills navtab-bg">
                        <li class="nav-item">
                            <a href="<?= url('/sms-flow') ?>" class="nav-link mb-3">
                                Add Question
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= url('/creating-flow') ?>" class="nav-link active mb-3">
                                Creating Flow
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= url('/flow') ?>" class="nav-link mb-3">
                                Flow Template List
                            </a>
                        </li>
                    </ul>
                </div> <!-- end col -->
            </div>
            <div class="row">
                <div class="col-xl-4 height-card box-margin create-flow-l-side">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Questions</h4>
                            <div class="col-xl-10 height-card box-margin">
                                <div class="form-group w-100">
                                    <input type="text" class="form-control" id="search" name="search" placeholder="Search question">
                                </div>
                            </div>
                            <div class="draggable-main scrolling-wrapper scrollbar scrollbar-warning">
                                <div class="">
                                    <form>
                                        <div class="dragbox">
                                            <ul id="draggable-left" class="connected-sortable draggable-left">
                                                <?php
                                                if (count($flowQues) > 0) {
                                                    $category = false;
                                                    foreach ($flowQues as $key => $flowQue) {
                                                        $category = $flowQue->sub_category;
                                                        if ($key == 0)
                                                            echo "<h6>" . $flowQue->sub_category . "</h6>";
                                                        else if (($key != 0) && ($category != $flowQues[$key - 1]->sub_category))
                                                            echo "<h6>" . $flowQue->sub_category . "</h6>";
                                                        echo '<li id="' . $flowQue->question_id . '" class="alert alert-warning">' . $flowQue->question . '<a href="#" class="remove_question pull-right"><i class="fa fa-trash text-danger font-18"></i></a></li>';
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col -->

                <div class="col-xl-8 height-card box-margin create-flow-r-side">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="form_flow">
                                        <input type="hidden" name="flow_id" id="flow_id" value="<?= (isset($flows->flow_id)) ? $flows->flow_id : 0 ?>">
                                        <div class="form-group mb-3">
                                            <label for="flow_name">Name of Your Flow</label>
                                            <input type="text" id="flow_name" name="flow_name" class="form-control" value="<?= (isset($flows->flow_name)) ? $flows->flow_name : '' ?>" placeholder="Flow Name Goes Here">
                                        </div>
                                        <div class="dragbox">
                                            <ul id="draggable-right" class="connected-sortable draggable-right">
                                                <input type="hidden" id="question_ids" name="question_ids[]" value="">
                                                <li></li>
                                            </ul>
                                        </div>
                                        <div id="div_questions">
                                            <?php
                                            if (isset($flows)) {
                                                $lookup = array_column($flowQues->toArray(), NULL, 'question_id');
                                                $questions = explode(',', $flows->questions);
                                                foreach ($questions as $question) {
                                                    ?>
                                                    <div class="alert alert-warning" id="div_<?php echo $lookup[$question]->question_id; ?>" role="alert"><?php echo $lookup[$question]->question; ?>
                                                        <div class="float-right">
                                                            <input type="hidden" name="question_ids[]" value="<?php echo $lookup[$question]->question_id; ?>">
                                                            <a href="#" data-id="<?php echo $lookup[$question]->question_id; ?>" class="remove_question"><i class="fa fa-trash text-danger font-18"></i></a></div></div>
                                                <?php }
                                            }
                                            ?>
                                        </div>
                                    </form>
                                </div> <!-- end col -->
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <input type="button" class="btn btn-info mr-2 btn-sm mb-3" id="publish_flow" value="Publish Flow">
                                </div>
                            </div>
                            <!-- end row-->
                        </div> <!-- end card-body -->
                    </div> <!-- end card-->
                </div> <!-- end col -->
            </div>
        </div>
        <script>
            var searchRequest = null;
            $(function () {
                var minlength = 0;
                $("#search").keyup(function () {
                    var that = this,
                    value = $(this).val();
                    if (value.length >= minlength) {
                        if (searchRequest != null)
                            searchRequest.abort();
                        searchRequest = $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: '<?php echo url('/') . "/search-question" ?>',
                            data: 'search_keyword=' + value,
                            type: 'POST',
                            dataType: "text",
                            success: function (msg) {
                                //we need to check if the value is the same
                                if (value == $(that).val()) {
                                    $('#draggable-left').html(msg);
                                    //Receiving the result of search here
                                }
                            }
                        });
                    }
                });
            });
            
        </script>