<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-0">Flow Template</h4>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col -->
            </div>
            <div class="row">
                <div class="col-xl-10 height-card box-margin">
                    <ul class="nav nav-pills navtab-bg">
                        <li class="nav-item">
                            <a href="sms-flow" class="nav-link mb-3">
                                Add Question
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="creating-flow" class="nav-link  mb-3">
                                Creating Flow
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="flow" class="nav-link active mb-3">
                                Flow Template List
                            </a>
                        </li>
                    </ul>
                </div> <!-- end col -->
                <div class="col-xl-2 height-card box-margin">
                    <div class="form-group w-100">
                        <input type="text" class="form-control" id="search" name="search" placeholder="Search">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-hover" id="">
                                <tbody id="tblFlow">
                                    <?php
                                    $i = 0;
                                    foreach ($flows as $flow) {
                                        $i++;
                                        ?>
                                        <tr id="div_<?= $flow->flow_id ?>">
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $flow->flow_name ?></td>
                                            <td>
                                                <a href="flow/<?php echo $flow->flow_id ?>" class="mr-2"><i class="fa fa-edit text-info font-18"></i></a>
                                                <a href="#" data-id="<?php echo $flow->flow_id ?>" class="deleteFlow"><i class="fa fa-trash text-danger font-18"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col -->
            </div>
        </div>
        <script>
            var searchRequest = null;
            $(function () {
                var minlength = 0;
                $("#search").keyup(function () {
                    var that = this,
                    value = $(this).val();

                    if (value.length >= minlength ) {
                        if (searchRequest != null) 
                            searchRequest.abort();
                        searchRequest = $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: '<?php echo url('/') . "/search-flow" ?>',
                            data: 'search_keyword=' + value,
                            type: 'POST',
                            dataType: "text",
                            success: function(msg){
                                //we need to check if the value is the same
                                if (value==$(that).val()) {
                                    $('#tblFlow').html(msg);
                                //Receiving the result of search here
                                }
                            }
                        });
                    }
                });
            });
        </script>