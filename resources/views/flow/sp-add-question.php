<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-0">Add Question</h4>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col -->

            </div>
            <div class="row">
                <div class="col-xl-10 height-card box-margin">
                    <ul class="nav nav-pills navtab-bg">
                        <li class="nav-item">
                            <a href="sms-flow" class="nav-link active mb-3">
                                Add Question
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="creating-flow" class="nav-link  mb-3">
                                Creating Flow
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="flow" class="nav-link mb-3">
                                Flow Template List
                            </a>
                        </li>
                    </ul>
                </div> <!-- end col -->
                <div class="col-xl-2 height-card box-margin">
                    <div class="form-group w-100">
                        <input type="text" class="form-control" id="search" placeholder="Search">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Adding Questions</h4>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <form action="add-questions" method="post" id="form_question">
                                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                        <input type="hidden" name="question_id" id="question_id" value="">
                                        <div class="form-group">
                                            <label for="question_type">Question Type</label>
                                            <select id="question_type" class="form-control" name="question_type">
                                                <option value="1">Short Answer</option>
                                                <option value="2">Multiple Choice</option>
                                                <option value="3">Instruction</option>
                                                <!--option>Short Answer</option -->
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label id="addQueLabel" for="question">Adding Question</label>
                                            <textarea id="question_text"
                                                      class="form-control form-control-md g-resize-none rounded-0" rows="2"
                                                      name="question" placeholder="Adding Question"></textarea>
                                        </div>

                                        <div id="multiple_options">
                                        </div>
                                        <div id="copy_div">
                                            <div class="cloned-row w-100" style="display:none">
                                                <div class="d-flex justify-content-start">
                                                    <div class="form-group mr-2">
                                                        <label for="expected_selection">Expected Selection</label>
                                                        <input type="text" class="form-control expected_selection" id="expected_selection"
                                                               name="expected_selection[]" placeholder="Expected Selection">
                                                    </div>

                                                    <div class="form-group mr-2">
                                                        <label for="acceptable_response">Acceptable Response</label>
                                                        <input type="text" class="form-control acceptable_response" id="acceptable_response"
                                                               name="acceptable_response[]" placeholder="Acceptable Response">
                                                    </div>
                                                    <div class="form-group mr-2">
                                                        <label for="widget_bot">Widget Bot</label>
                                                        <input type="text" class="form-control widget_bot" id="widget_bot" name="widget_bot[]" placeholder="Widget Bot">
                                                    </div>
                                                    <div class="form-group mr-2">
                                                        <label for="reply">Reply</label>
                                                        <input type="text" class="form-control reply" id="reply" name="reply[]" placeholder="Reply">
                                                    </div>
                                                    <div class="form-group text-center pt-4 mr-2 ">
                                                        <a href="#" class=""><i class="fa fa-trash text-danger font-18 mt-2"></i></a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <input type="button" class="btn btn-info mr-2 btn-sm mb-3" value="Add Response" id="clone" style="display:none">
                                        <div class="shortAns form-group">
                                            <label id="replyLabel" for="answer">Reply</label>
                                            <textarea id="answer_text" class="form-control form-control-md g-resize-none rounded-0" rows="3" name="answer" placeholder="Add reply to response"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="save_response_to">Save Response To:</label>
                                            <select id="save_response_to" class="form-control" name="save_response_to">
                                                <?php foreach ($dataset as $res) { ?>
                                                    <option value="<?php echo $res->datasets_id; ?>"><?php echo $res->sub_category; ?></option>
                                                <?php } ?>   
                                            </select>
                                        </div>
                                        <button type="button" id="btn_question" class="btn btn-info mr-2">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col -->

                <div class="col-xl-6 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12" id="quesRow">
                                    <form>
                                        <?php
                                        if (count($questions) > 0) {
                                            foreach ($questions as $question) {
                                                ?>
                                                <div class="alert alert-warning" role="alert" id="div_<?= $question->question_id ?>">
                                                    <?= $question->question ?>
                                                    <div class="float-right">
                                                        <a href="#" data-id="<?= $question->question_id ?>" class="edit_question mr-2"><i class="fa fa-edit text-info font-18"></i></a>
                                                        <a href="#" class="delete_question" data-id="<?= $question->question_id ?>"><i class="fa fa-trash text-danger font-18"></i></a>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                        <div id="div_questions">
                                        </div>
                                    </form>
                                </div> <!-- end col -->
                            </div>
                            <!-- end row-->
                        </div> <!-- end card-body -->
                    </div> <!-- end card-->
                </div> <!-- end col -->
            </div>
        </div>
    </div>
    <script>
        var searchRequest = null;
        $(function () {
            var minlength = 0;
            $("#search").keyup(function () {
                var that = this,
                        value = $(this).val();
                if (value.length >= minlength) {
                    if (searchRequest != null)
                        searchRequest.abort();
                    searchRequest = $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '<?php echo url('/') . "/search-label" ?>',
                        data: 'search_keyword=' + value,
                        type: 'POST',
                        dataType: "text",
                        success: function (msg) {
                            //we need to check if the value is the same
                            if (value == $(that).val()) {
                                $('#quesRow').html(msg);
                                //Receiving the result of search here
                            }
                        }
                    });
                }
            });
        });
    </script>