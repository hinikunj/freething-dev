<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script> 

<div class="container-fluid page-body-wrapper">
<!-- Side Menu area -->

<!-- Main Page -->
<div class="main-panel">
<div class="content-wrapper">
    <div class="container-fluid">
        
		
		<div class="row">
			<div class="col-lg-12 height-card box-margin">
                <div class="card">
					<div class="card-body">
						<table id="scroll-horizontal-datatable" class="table w-100 nowrap">
                                <thead>
                                    <tr>
                                        <th>Registered Date</th>
                                        
                                        <th>Company Name</th>                                                       
                                        <th>Industry Category</th>                                                       
                                        <th>Flow Number</th>
                                        <th>Location</th>
                                        <th># of Subscriber</th>
                                        <th>No of Campaigns</th>
                                        
                                        <th>Lifetime ordered / Winnings</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($list as $key => $value) {
                                    if($value->status == 1)
                                    {
                                        $status = 'Active';
                                    }
                                    elseif($value->status == 0)
                                    {
                                         $status = 'Deactivated';
                                    }
                                    else
                                    {
                                         $status = 'Hold';
                                    }
                                        echo '<tr>
                                        <td>'.$value->created_date.'</td>
                                        <td>'.$value->company.'</td>
										<td></td>
                                        <td>765-444-5588</td>
                                        <td>'.$value->location.'</td>
                                        <td>255</td>
                                        <td>50</td>
                                        <td>55/20</td>
                                        <td id="status_'.$value->id.'">'.$status.'</td>
                                        <td>
                                            <div class="dropdown mb-30 mr-2">
                                                
                                                <a href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <button class="dropdown-item userStatus" type="button" status="2" id="'.$value->id.'">Hold</button>
                                                    <button class="dropdown-item userStatus" type="button "  status="1"  id="'.$value->id.'">Activate</button>
                                                    <button class="dropdown-item userStatus" type="button"  status="0"  id="'.$value->id.'">Delete</button>
                                                </div>
                                            </div>
                                        </td>
                                        
                                    </tr>';
                                    }
                                    ?>
                                    
                                  
								
                                </tbody>
                            </table>
					</div>
				</div>
			</div>
		</div>
    </div>

