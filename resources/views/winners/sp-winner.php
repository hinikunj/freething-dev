<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <table id="scroll-horizontal-datatable" class="table w-100 nowrap">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Campaign ID</th>
                                        <th>First Name</th>                                                       
                                        <th>Company Name</th>
                                        <th>Flow Number</th>
                                        <th>Winning ID</th>
                                        <th>Product Name</th>
                                        <th>Product ID</th>
                                        <th>Order / Winning</th>
                                        <th>Price Per Product</th>
                                        <th>Shipping Responsibility</th>
                                        <th>Shipping Method</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($winners) > 0) {
                                        foreach ($winners as $key => $winner) {
                                            //$productPrice = Helper::getcurrency($product[$key]->currency, $product[$key]->displayed_price);
                                            ?>
                                            <tr>
                                                <td><?= ($winner->updated_at) ? $winner->updated_at : $winner->created_at ?></td>
                                                <td><?= $winner->campaign_id ?></td>
                                                <td><?= $winner->first_name ?></td>
                                                <td><?= $winner->company_name ?></td>
                                                <td><?= $winner->flow_number ?></td>
                                                <td><?= $winner->winning_id ?></td>
                                                <td><?= $product[$key]->name; ?></td>
                                                <td><?= $winner->product_id ?></td>
                                                <td>2 Of <?= $product[$key]->quantity ?></td>
                                                <td><?= $winners->symbol . '' . $product[$key]->displayed_price ?></td>
                                                <td><?= $product[$key]->shipping_by ?></td>
                                                <td>USPS</td> 
                                            </tr>           
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
