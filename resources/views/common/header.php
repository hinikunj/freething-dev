<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Required meta tags -->
        <title>Freethngs - <?= (isset($title)) ? $title : 'Dashboard' ?></title>
        <!-- Favicon -->

        <meta name="csrf-token" content="<?php echo csrf_token() ?>">
        <link rel="icon" href="<?= url('/') ?>/resources/assets/img/core-img/favicon.png">
        <!-- <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/default-assets/chartist.css?v=<?= env('FILE_VERSION') ?>"> -->
        
        <!-- Master Stylesheet CSS -->
        <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/default-assets/datatables.bootstrap4.css?v=<?= env('FILE_VERSION') ?>">
        <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/default-assets/modal.css?v=<?= env('FILE_VERSION') ?>">
        <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/default-assets/owl.carousel.min.css?v=<?= env('FILE_VERSION') ?>">
        <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/default-assets/owl.carousel-default.css?v=<?= env('FILE_VERSION') ?>">
        <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/default-assets/jquery.bootstrap-touchspin.min.css?v=<?= env('FILE_VERSION') ?>">
        <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/default-assets/color-picker-bootstrap.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">

        <!-- These plugins only need for the run this page -->
        <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/dropzone.min.css?v=<?= env('FILE_VERSION') ?>">
        <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/default-assets/dropify.min.css?v=<?= env('FILE_VERSION') ?>">
        <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/default-assets/fileupload.css?v=<?= env('FILE_VERSION') ?>">
        <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/default-assets/jquery.tagsinput.min.css?v=<?= env('FILE_VERSION') ?>">
        <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/default-assets/summernote.css?v=<?= env('FILE_VERSION') ?>">
        <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/style.css?v=<?= env('FILE_VERSION') ?>">
        <link rel="stylesheet" href="<?= url('/') ?>/resources/assets/custom.css??v=<?= env('FILE_VERSION') ?>">
        <script src="<?= url('/') ?>/resources/assets/js/jquery.min.js?v=<?= env('FILE_VERSION') ?>"></script>
        <script type="text/javascript" src="<?= url('/') ?>/resources/custom/scripts.js?v=<?= env('FILE_VERSION') ?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script> 
        
        <script>baseurl = '<?= url('/') ?>/';
            productImg = 'resources/assets/uploads/';
        </script>
    </head>
    <body class="sidebar-icon-only">