<!-- Footer Area -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <!-- Footer Area -->
            <footer class="footer-area d-sm-flex justify-content-center align-items-center justify-content-between">
                <!-- Copywrite Text -->
                <div class="copywrite-text">
                    <p>Created by <a href="#">FreeThings</a></p>
                </div>
                <div class="fotter-icon text-center">
                    <a href="#" class="action-item mr-2" data-toggle="tooltip" title="Facebook">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                    <a href="#" class="action-item mr-2" data-toggle="tooltip" title="Twitter">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                    <a href="#" class="action-item mr-2" data-toggle="tooltip" title="Pinterest">
                        <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                    </a>
                    <a href="#" class="action-item mr-2" data-toggle="tooltip" title="Instagram">
                        <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                </div>
            </footer>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
<div id="chat-circle" class="btn btn-raised">
    <img src="<?= url('/') ?>/resources/assets/img/core-img/logo_free_things_shop_-03 (1).png" />

</div>

<div class="chat-box">
    <div class="chat-box-header">
        <span class="chat-box-toggle"><i class="zmdi zmdi-close"></i></span>
        Hi, Welcome to FreeThings.Shop by Hobbima
        <div class="mt-4 chat-box-imgs d-flex justify-content-center">

            <div class="item"><img src="<?= url('/') ?>/resources/assets/img/chat-img/2.png" /></div>
            <div class="item"><img src="<?= url('/') ?>/resources/assets/img/chat-img/1.png" /></div>
            <div class="item"><img src="<?= url('/') ?>/resources/assets/img/chat-img/1.png" /></div>
        </div>
        <p class="mt-3 text-center" style="color:#fff;"><a href="#" class="btn btn-default">More</a></p>
        <p class="mt-0 text-center" style="color:#fff;">Here are your odd numbers:</p>

        <div class="d-flex chat-coupons-code justify-content-center mt-0">
            <div class="item"><h5>FFRMYT</h5></div>
            <div class="item"><h5>FFRMYT</h5></div>
            <div class="item"><h5>FFRMY</h5></div>
            <div class=""><h5 class="refresh-icon"><a href="#"><i class="fa fa-refresh"></i></a></h5></div>
        </div>
        <p class="mt-2 text-center" style="color:#fff;">Text WIN to get started</p>
    </div>
    <div class="chat-box-body">
        <div class="chat-box-overlay">   
        </div>
        <div class="chat-logs">
            <div class="chat-instructions">
                <p class="You have 3 chances to win anyone of these items"></p>
                <div class="card card-body" style="width:75%; margin:0 auto;">					
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <form>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="exampleInputEmail111" placeholder="Enter your phone number to get started">
                                </div>
                                <button type="submit" class="btn btn-info mr-2">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div><!--chat-log -->
    </div>
    <div class="chat-input">      
        <form>
            <input type="text" id="chat-input" placeholder="Send a message..."/>
            <button type="submit" class="chat-submit" id="chat-submit"><i class="material-icons">send</i></button>
        </form>      
    </div>
</div>
<!-- ======================================
********* Page Wrapper Area End ***********
======================================= -->

<!-- Plugins Js -->

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js?v=<?= env('FILE_VERSION') ?>"></script> 
<script src="<?= url('/') ?>/resources/assets/js/popper.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/bootstrap.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/bundle.js?v=<?= env('FILE_VERSION') ?>"></script>

<!-- Active JS -->
<script src="<?= url('/') ?>/resources/assets/js/canvas.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/collapse.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/settings.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/template.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/active.js?v=<?= env('FILE_VERSION') ?>"></script>

<!-- Inject JS 
<script src="<?= url('/') ?>/resources/assets/js/default-assets/chartist.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/chartist-custom.js?v=<?= env('FILE_VERSION') ?>"></script>
-->

<!-- Inject JS -->
<script src="<?= url('/') ?>/resources/assets/js/default-assets/jquery.datatables.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/datatables.bootstrap4.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/datatable-responsive.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/responsive.bootstrap4.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/datatable-button.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/button.bootstrap4.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/button.html5.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/button.flash.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/button.print.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/datatables.keytable.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/datatables.select.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/demo.datatable-init.js?v=<?= env('FILE_VERSION') ?>"></script>

<!-- Inject JS -->
<script src="<?= url('/') ?>/resources/assets/js/default-assets/modal-classes.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/modaleffects.js?v=<?= env('FILE_VERSION') ?>"></script>

<!-- Inject JS -->
<script src="<?= url('/') ?>/resources/assets/js/default-assets/owl.carousel.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/owl.carousel-custom.js?v=<?= env('FILE_VERSION') ?>"></script>

<script src="<?= url('/') ?>/resources/assets/js/default-assets/jquery.bootstrap-touchspin.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/jquery.bootstrap-touchspin.custom.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/bootstrap-datepicker.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<!--	<script src="<?= url('/') ?>/resources/assets/js/default-assets/form-picker.js?v=<?= env('FILE_VERSION') ?>"></script> -->


<!-- Inject JS -->
<script src="<?= url('/') ?>/resources/assets/js/dropzone.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/dropzone-custom.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/dropzone-and-module.min.js?v=<?= env('FILE_VERSION') ?>"></script>

<script src="<?= url('/') ?>/resources/assets/js/default-assets/jquey.tagsinput.min.js?v=<?= env('FILE_VERSION') ?>"></script>

<script src="<?= url('/') ?>/resources/assets/js/default-assets/summernote.min.js?v=<?= env('FILE_VERSION') ?>"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/summernote-active.js?v=<?= env('FILE_VERSION') ?>"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function () {
        $('.js-example-basic-single').select2();
    });
    $(function () {
        var INDEX = 0;
        $("#chat-submit").click(function (e) {
            e.preventDefault();
            var msg = $("#chat-input").val();
            if (msg.trim() == '') {
                return false;
            }
            generate_message(msg, 'self');
            var buttons = [
                {
                    name: 'Existing User',
                    value: 'existing'
                },
                {
                    name: 'New User',
                    value: 'new'
                }
            ];
            setTimeout(function () {
                generate_message(msg, 'user');
            }, 1000)

        })

        function generate_message(msg, type) {
            INDEX++;
            var str = "";
            str += "<div id='cm-msg-" + INDEX + "' class=\"chat-msg " + type + "\">";
            str += "          <span class=\"msg-avatar\">";
            str += "            <img src=\"https:\/\/image.crisp.im\/avatar\/operator\/196af8cc-f6ad-4ef7-afd1-c45d5231387c\/240\/?1483361727745\">";
            str += "          <\/span>";
            str += "          <div class=\"cm-msg-text\">";
            str += msg;
            str += "          <\/div>";
            str += "        <\/div>";
            $(".chat-logs").append(str);
            $("#cm-msg-" + INDEX).hide().fadeIn(300);
            if (type == 'self') {
                $("#chat-input").val('');
            }
            $(".chat-logs").stop().animate({scrollTop: $(".chat-logs")[0].scrollHeight}, 1000);
        }

        function generate_button_message(msg, buttons) {
            /* Buttons should be object array 
             [
             {
             name: 'Existing User',
             value: 'existing'
             },
             {
             name: 'New User',
             value: 'new'
             }
             ]
             */
            INDEX++;
            var btn_obj = buttons.map(function (button) {
                return  "              <li class=\"button\"><a href=\"javascript:;\" class=\"btn btn-info chat-btn\" chat-value=\"" + button.value + "\">" + button.name + "<\/a><\/li>";
            }).join('');
            var str = "";
            str += "<div id='cm-msg-" + INDEX + "' class=\"chat-msg user\">";
            str += "          <span class=\"msg-avatar\">";
            str += "            <img src=\"https:\/\/image.crisp.im\/avatar\/operator\/196af8cc-f6ad-4ef7-afd1-c45d5231387c\/240\/?1483361727745\">";
            str += "          <\/span>";
            str += "          <div class=\"cm-msg-text\">";
            str += msg;
            str += "          <\/div>";
            str += "          <div class=\"cm-msg-button\">";
            str += "            <ul>";
            str += btn_obj;
            str += "            <\/ul>";
            str += "          <\/div>";
            str += "        <\/div>";
            $(".chat-logs").append(str);
            $("#cm-msg-" + INDEX).hide().fadeIn(300);
            $(".chat-logs").stop().animate({scrollTop: $(".chat-logs")[0].scrollHeight}, 1000);
            $("#chat-input").attr("disabled", true);
        }

        $(document).delegate(".chat-btn", "click", function () {
            var value = $(this).attr("chat-value");
            var name = $(this).html();
            $("#chat-input").attr("disabled", false);
            generate_message(name, 'self');
        })

        $("#chat-circle").click(function () {
            $("#chat-circle").toggle('scale');
            $(".chat-box").toggle('scale');
        })

        $(".chat-box-toggle").click(function () {
            $("#chat-circle").toggle('scale');
            $(".chat-box").toggle('scale');
        })

    })
</script>
<script>
    $('.owl-carousel.chat-slider').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    })
    $('.owl-carousel.chat-code-slider').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })
</script>
<script>
    $(function () {
        'use strict';

        $('.datasetFlow').tagsInput({
            'width': '100%',
            'height': '85%',
            'interactive': true,
            'defaultText': 'Add More',
            'removeWithBackspace': true,
            'minChars': 0,
            'maxChars': 20,
            'placeholderColor': '#555'
        });
    });
</script>

<script type="text/javascript">
    $("#dragable tbody").sortable({
        items: "> tr:not(:first)",
        appendTo: "parent",
        helper: "clone"
    }).disableSelection();
</script>
<script>
    $("#clone").click(function () {
        $(".cloned-row:first").clone().insertAfter(".cloned-row:last");
        $('.cloned-row:last').find("input").val("");
    });
</script>
<script>
    /*$(".draggable-left, .draggable-right").sortable({
     connectWith: ".connected-sortable",
     stack: ".connected-sortable ul",
     update: myFunc
     }).disableSelection();*/


    $(".draggable-left").sortable({
        connectWith: ".connected-sortable",
        stack: ".connected-sortable ul",
        update: myFunc,
        helper: function (e, li) {
            this.copyHelper = li.clone().insertAfter(li);

            $(this).data('copied', false);

            return li.clone();
        },
        stop: function () {

            var copied = $(this).data('copied');

            if (!copied) {
                this.copyHelper.remove();
            }

            this.copyHelper = null;
        }
    }).disableSelection();

    $(".draggable-right").sortable({
        connectWith: ".connected-sortable",
        stack: ".connected-sortable ul",
        update: myFunc,
        receive: function (e, ui) {
            ui.sender.data('copied', true);
        }
    }).disableSelection();

    $(".draggable-content").sortable({
        connectWith: ".connected-sortable",
        stack: ".connected-sortable ul"
    }).disableSelection();

    function myFunc(event, ui) {
        var allids = [];
        var b = $("#draggable-right li"); // array of sorted elems
        for (var i = 0; i < b.length; i++)
        {
            if (b[i].id > 0) {
                if (allids.indexOf(b[i].id) === -1) {
                    allids.push(b[i].id);
                }
                //console.log(b[i].id);
            }
        }
        
        /*Covering no order as well*/
        var c = $(".NoReOrder li"); // array of sorted elems
        for (var j = 0; j < c.length; j++)
        {
            if (c[j].id > 0) {
                if (allids.indexOf(c[j].id) === -1) {
                    allids.push(c[j].id);
                }
                //console.log(c[j].id);
            }
        }
        console.log(allids);
        $("#question_ids").val(allids);
    }
</script>


<div id="wait"></div>
<style>
    #wait {
        position:fixed;
        width:100%;
        left:0;right:0;top:0;bottom:0;
        background-color: rgba(255,255,255,0.7);
        z-index:9999;
        display:none;
    }

    @-webkit-keyframes spin {
        from {-webkit-transform:rotate(0deg);}
        to {-webkit-transform:rotate(360deg);}
    }

    @keyframes spin {
        from {transform:rotate(0deg);}
        to {transform:rotate(360deg);}
    }

    #wait::after {
        content:'';
        display:block;
        position:absolute;
        left:48%;top:40%;
        width:40px;height:40px;
        border-style:solid;
        border-color:black;
        border-top-color:transparent;
        border-width: 4px;
        border-radius:50%;
        -webkit-animation: spin .8s linear infinite;
        animation: spin .8s linear infinite;
    }
</style>
<div id="waitss"  style=" background: #e9e9e9;  
     display: none;        
     position: absolute;   
     top: 0;                 
     right: 0;                
     bottom: 0;
     left: 0;
     opacity: 0.5;  
     ">
    <div  style="width:69px;height:89px;position:fixed;top:50%;left:50%;padding:2px;z-index: 9999999;"><img src='<?= url('/') ?>/resources/assets/img/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
</body>

</html>