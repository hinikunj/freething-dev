
<!-- Preloader -->
<div id="preloader-area">
   <div class="lds-ripple">
      <div></div>
      <div></div>
   </div>
</div>
<!-- Preloader -->
<!-- ======================================
   ******* Main Page Wrapper **********
   ======================================= -->
<div class="main-container-wrapper">
<!-- Top bar area -->
<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
   <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
      <a class="navbar-brand brand-logo mr-5" href="/"><img src="<?= url('/') ?>/resources/assets/img/core-img/logo-ft-w1.png" class="mr-2" alt="logo" /></a>
      <a class="navbar-brand brand-logo-mini" href="/"><img src="<?= url('/') ?>/resources/assets/img/core-img/small-logo1.png" alt="logo" /></a>
   </div>
   <div class="navbar-menu-wrapper d-flex align-items-center justify-content-between">
      <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
      <span class="fa fa-bars">
      </span>
      </button>
      <ul class="navbar-nav mr-lg-2">
         <li class="nav-item app-search d-none d-md-block">
            <form role="search" class=""><input type="text" placeholder="Search..." class="form-control">
               <button type="submit" class="search-btn mr-0"><i class="fa fa-search"></i></button>
            </form>
         </li>
      </ul>
      

      <!-- topbar -->

      <ul class="top-navbar-area navbar-nav navbar-nav-right">
                    

                    <li class="nav-item dropdown dropdown-animate">
                        <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="count"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                            <p class="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>
                            <a class="dropdown-item preview-item d-flex align-items-center">
                                <div class="notification-thumbnail">
                                    <div class="preview-icon bg-primary">
                                        <i class="ti-info-alt mx-0"></i>
                                    </div>
                                </div>
                                <div class="notification-item-content">
                                    <h6>Code problem solved.</h6>
                                    <p class="mb-0">
                                        Just now
                                    </p>
                                </div>
                            </a>

                            <a class="dropdown-item preview-item d-flex align-items-center">
                                <div class="notification-thumbnail">
                                    <div class="preview-icon bg-success">
                                        <i class="ti-info-alt mx-0"></i>
                                    </div>
                                </div>
                                <div class="notification-item-content">
                                    <h6>New theme update.</h6>
                                    <p class="mb-0">
                                        02 days ago
                                    </p>
                                </div>
                            </a>

                            <a class="dropdown-item preview-item d-flex align-items-center">
                                <div class="notification-thumbnail">
                                    <div class="preview-icon bg-warning">
                                        <i class="ti-info-alt mx-0"></i>
                                    </div>
                                </div>
                                <div class="notification-item-content">
                                    <h6>Awsome support.</h6>
                                    <p class="mb-0">
                                        02 days ago
                                    </p>
                                </div>
                            </a>
                            <a class="dropdown-item preview-item d-flex align-items-center">
                                <div class="notification-thumbnail">
                                    <div class="preview-icon bg-danger">
                                        <i class="ti-info-alt mx-0"></i>
                                    </div>
                                </div>
                                <div class="notification-item-content">
                                    <h6>Text to build on the card title.</h6>
                                    <p class="mb-0">
                                        03 days ago
                                    </p>
                                </div>
                            </a>

                        </div>
                    </li>

                    <li class="nav-item nav-profile dropdown dropdown-animate">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                            <img src="<?= url('/') ?>/resources/assets/img/member-img/contact-2.jpg" alt="profile" />
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown profile-top" aria-labelledby="profileDropdown">
                          <a href="#" class="dropdown-item"><?= session('first_name'); ?></a>
                            <?php
                            if( session('role')  != 0)
                            {
                              echo '<a href="'.url('/') .'/edit-profile" class="dropdown-item"><i class="zmdi zmdi-account profile-icon" aria-hidden="true"></i> Edit Profile</a>
                            ';
                            }
                            ?>
                            
                            
                            <a href="<?= url('/') ?>/sign-out" class="dropdown-item"><i class="ti-unlink profile-icon" aria-hidden="true"></i> Sign-out</a>
                        </div>
                    </li>
                </ul>

      <!-- end topbar -->
      <button class="navbar-toggler navbar-toggler-right d-xl-none align-self-center" type="button" data-toggle="offcanvas">
      <span class="ti-layout-grid2"></span>
      </button>
   </div>
</nav>
<div class="container-fluid page-body-wrapper">