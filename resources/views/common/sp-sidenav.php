<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <?php
            if (session('role') != 0) {
                echo ' <a class="nav-link" href="' . url('/') . '/dashboard">
                            <i class="fa fa-home menu-icon"></i>
                            <span class="menu-title">Home</span>
                        </a>';
            } else {
                echo ' <a class="nav-link" href="' . url('/') . '/sp-dashboard">
                            <i class="fa fa-home menu-icon"></i>
                            <span class="menu-title">Home</span>
                        </a>';
            }
            ?>
        </li>
        <?php
        if (session('role') != 0) { //User section
            echo '<li class="nav-item">
                        <a class="nav-link" href="' . url('/campaign') . '">
                            <i class="fa fa-user menu-icon"></i>
                            <span class="menu-title">Campaign</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="' . url('/subscribers') . '">
                            <i class="fa fa-user menu-icon"></i>
                            <span class="menu-title">Subscribers</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="' . url('/') . '/history">
                            <i class="fa fa-user menu-icon"></i>
                            <span class="menu-title">Billing</span>
                        </a>
                    </li>';
        }
        if (session('role') == 0) { //Admin section
            echo '<li class="nav-item">
                        <a class="nav-link" href="' . url('/') . '/user-list">
                            <i class="fa fa-user menu-icon"></i>
                            <span class="menu-title">User</span>
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" href="' . url('/') . '/product-list">
                            <i class="fa fa-user menu-icon"></i>
                            <span class="menu-title">Products</span>
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" href="' . url('/') . '/winners">
                            <i class="fa fa-user menu-icon"></i>
                            <span class="menu-title">Winners</span>
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" href="dataset-list">
                            <i class="fa fa-user menu-icon"></i>
                            <span class="menu-title">Dataset</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="sms-flow">
                            <i class="fa fa-user menu-icon"></i>
                            <span class="menu-title">SMS Flow</span>
                        </a>
                    </li>';
        }
        ?>
    </ul>
</nav>