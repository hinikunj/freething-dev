<div class="modal inmodal fade" id="freesmsbundle" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Pricing plan & SMS bundle pricing</h4>
                <a href="#" data-dismiss="modal">Close</a>
            </div>
            <div class="modal-body text-left">
                <div class="row">
                    <div class="col-md-12">

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputCity" class="col-form-label">Pricing plan</label>
                                <select id="inputState" class="form-control select_plan">
                                    <option value="">Select</option>
                                    <?php
                                    $added = array();
                                    foreach ($plans as $key => $value) {
                                        if (!in_array($value->plan, $added)) {
                                            echo '<option  value="' . $value->plan . '" code="' . $value->id . '">' . $value->plan . '</option>';
                                            $added[] = $value->plan;
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputState" class="col-form-label">Your current plan</label>
                                <input type="text" id="example-readonly" class="form-control" readonly="" value="<?= $history[0]->plan ?> Plan">
                            </div>
                            <div class="form-group col-md-3">

                                <label for="inputCity" class="col-form-label mt-4 pt-3">Paid untill <?= date('d, M', strtotime($history[0]->expiry_date)) ?></label>

                            </div>
                            <div class="form-group col-md-2">
                                <label for="inputCity" class="col-form-label mt-4 pt-3"><strong><?= Session::get('symbols'); ?><?= $current_plan_price ?></strong></label>
                            </div>
                        </div>
                        <div class="showplansinbilling" style="display: none">
                            <h4 class="card-title mt-2">Starter Plan SMS Bundle</h4>
                            <fieldset class="form-group mb-20">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-check pl-0 mb-2">
                                            <span class="font-weight-bold text-success showpland">
                                        </div>




                                    </div>
                                    <div class="col-md-2 pl-1">
                                        <strong><?= Session::get('symbols'); ?><span class="font-weight-bold text-success planprice">  </span></strong>
                                    </div>
                                </div>


                            </fieldset>
                        </div>
                        <div class="form-inline">
                            <label class="mr-2" for="inlineFormInputName2">Coupons</label>
                            <input type="text" class="form-control rounded-0 form-control-md mr-sm-3 mb-3 mb-lg-0" id="inlineFormInputName2" placeholder="Jane Doe">


                            <button type="submit" class="btn btn-primary mt-10">Apply</button>
                        </div>
                        <div class="form-row mt-5">

                            <div class="form-group col-md-9">
                                <label for="inputState" class="col-form-label">Total Today</label>

                            </div>

                            <div class="form-group col-md-2">
                                <label for="inputCity" class="col-form-label "><strong class="billingtotal"><?= Session::get('symbols'); ?>0</strong></label>
                            </div>
                        </div>
                        <h4 class="card-title mt-2">Your Saved credit and debit card</h4>
                        <?php
                        foreach ($card_details as $key => $value) {
                            ?>
                            <form role="form"  action="<?= route('stripe.payment') ?>" method="post" class="f_<?= $key ?> dform require-validation "
                                  data-cc-on-file="false"
                                  data-stripe-publishable-key="<?= env('STRIPE_KEY') ?>"  
                                  >
                                <input type="hidden" name="card_id" value="<?= $value->id ?>">
                                <input type="hidden" name="plan_no" >
                                <input type="hidden" name="totalusd" value="20">
                                <input type="hidden" name="typeofend" class="typeofpayment">
                                <input type="hidden" name="billingtotal" class="billingtotal">
                                <input required  type="hidden" name="card-number" autocomplete='off' class="card-number form-control" placeholder="XXXXXXXXXXXXXXXX" value="<?= $value->card_number ?>" size='20'/>

                                <input required type="hidden" name="card-expiry-month"  value="<?= $value->card_expiry_month ?>" class="card-expiry-month form-control" placeholder="MM"  size='2'/>

                                <input required type="hidden" name="card-expiry-year"  value="<?= $value->card_expiry_year ?>" class="card-expiry-year form-control" placeholder="YYYY"  size='4'/>

                                <input required type="hidden" name="card-cvc" size='4'  class="card-cvc form-control" placeholder="XXX">

                                <input type="submit" name="" class="placeorderdata_<?= $key ?>" style="display:none"/>
                            </form>
                            <?php
                        }
                        ?>                          
                        <div class="row">
                            <div class="col-md-7">

                                <?php
                                if (count($card_details) > 0) {
                                    ?>
                                    <table class="table table-bordered table-hover savescard">

                                        <tbody>
                                        <input type="hidden" name="" class="currentid">

                                        <?php
                                        foreach ($card_details as $key => $value) {
                                            ?>

                                            <?php
                                            echo '<tr>
                              <td>
                   <div class="custom-control custom-radio ">
                      <input type="radio" id="' . $key . '" name="customRadio " class="custom-control-input cardselect">
                      <label class="custom-control-label" for="' . $key . '">Visa</label>
                   </div>
                </td>
                              <td>Ending with ' . substr($value->card_number, -4) . '</td>
                              <td>' . $value->name . '</td>
                              <td>' . $value->card_expiry_month . '/' . $value->card_expiry_year . '</td>
                              <td>
                   <div class="form-inline">
                      <div class="form-group">
                        
                         <input type="password" id="inputPassword4" class="form-control rounded-0 form-control-md mx-sm-3 w-25" aria-describedby="passwordHelpInline">
                         <span id="e_' . $key . '"></span>
                      </div>
                   </div>
                </td>
                          </tr>';
                                        }
                                        ?>


                                        </tbody>
                                    </table>
                                    <?php
                                } else {
                                    echo "No Saved Cards";
                                }
                                ?>
                            </div>
                            <div class="col-md-5">
                                <div class="border p-3">
                                    <div class="form-check pl-0 mb-2">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input mr-2 autofill" name="optionsRadios" id="optionsRadios1" value="option1"><span class="ml-3 ">Automatic Refill (<?= Session::get('symbols'); ?>10 Minimum)</span>
                                            <i class="input-helper"></i></label>
                                    </div>
                                    <div class="form-check pl-0 mb-2">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input mr-2 endcampaign" name="optionsRadios" id="optionsRadios1" value="option1" ><span class="ml-3 ">End campaign when monthly SMS is finished</span>
                                            <i class="input-helper"></i></label>
                                    </div>



                                    <button type="button" class="btn btn-primary placeorder">Pay Now</button>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
    $(function () {
        $('.error').css('display', 'none');
        var $form = $(".f.require-validation");
        $('.f.require-validation').bind('submit', function (e) {
            var $form = $(".f.require-validation"),
                    inputSelector = ['input[type=email]', 'input[type=password]',
                        'input[type=text]', 'input[type=file]',
                        'textarea'].join(', '),
                    $inputs = $form.find('.required').find(inputSelector),
                    $errorMessage = $form.find('div.error'),
                    valid = true;
            $errorMessage.addClass('hide');

            $('.has-error').removeClass('has-error');
            $inputs.each(function (i, el) {
                var $input = $(el);
                if ($input.val() === '') {
                    $input.parent().addClass('has-error');
                    $errorMessage.removeClass('hide');
                    e.preventDefault();
                }
            });

            if (!$form.data('cc-on-file')) {
                e.preventDefault();
                Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                Stripe.createToken({
                    number: $('.f .card-number').val(),
                    cvc: $('.f .card-cvc').val(),
                    exp_month: $('.f .card-expiry-month').val(),
                    exp_year: $('.f .card-expiry-year').val()
                }, stripeResponseHandler);
            }

        });

        function stripeResponseHandler(status, response) {
            $('.error').css('display', 'inline');
            if (response.error) {
                $('.error')
                        .removeClass('hide')
                        .find('.alert')
                        .text(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the token into the form so it gets submitted to the server
                $form.find('input[type=text]').empty();
                $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                $form.get(0).submit();
            }
        }

    });

    $(function () {
        $('.error').css('display', 'none');
        var $form = $(".dform.require-validation");
        $('.dform.require-validation').bind('submit', function (e) {
            var $form = $(".dform.require-validation"),
                    inputSelector = ['input[type=email]', 'input[type=password]',
                        'input[type=text]', 'input[type=file]',
                        'textarea'].join(', '),
                    $inputs = $form.find('.required').find(inputSelector),
                    $errorMessage = $form.find('div.error'),
                    valid = true;
            $errorMessage.addClass('hide');

            $('.has-error').removeClass('has-error');
            $inputs.each(function (i, el) {
                var $input = $(el);
                if ($input.val() === '') {
                    $input.parent().addClass('has-error');
                    $errorMessage.removeClass('hide');
                    e.preventDefault();
                }
            });

            if (!$form.data('cc-on-file')) {
                e.preventDefault();
                Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                Stripe.createToken({
                    number: $('.dform .card-number').val(),
                    cvc: $('.dform .card-cvc').val(),
                    exp_month: $('.dform .card-expiry-month').val(),
                    exp_year: $('.dform .card-expiry-year').val()
                }, stripeResponseHandler);
            }

        });

        function stripeResponseHandler(status, response) {
            $('.error').css('display', 'inline');
            if (response.error) {
                $('.error')
                        .removeClass('hide')
                        .find('.alert')
                        .text(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the token into the form so it gets submitted to the server
                $form.find('input[type=text]').empty();
                $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                $form.get(0).submit();
            }
        }

    });
</script>