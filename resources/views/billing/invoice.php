<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Content -->
                    <div class="card mb-30">
                        <div class="card-body p-5">
                            <div class="row align-items-center mb-3">
                                <div class="col">
                                    <!-- Pretitle -->
                                    <h6 class="font-14">Payments</h6>
                                    <!-- Title -->
                                    <h2 class="font-24">Invoice #FREETHNGS Invoice #1</h2>

                                </div>
                                <div class="col-auto">
                                    <!-- Buttons -->
                                    <a href="#!" class="btn btn-success btn-sm mb-2">
                                        Download
                                    </a>

                                </div>
                            </div> <!-- / .row -->

                            <div class="row">
                                <div class="col text-center">
                                    <!-- Logo -->
                                    <img src="<?= url('resources/img/core-img/small-logo1.png') ?>" style="width: 130px" alt="..." class="img-fluid mb-4">

                                    <!-- Title -->
                                    <h5 class="mb-1">Invoice from FREETHNGS</h5>

                                    <!-- Text -->
                                    <p class="mb-30">
                                        Invoice #FREETHNGS Invoice #1
                                    </p>

                                </div>
                            </div> <!-- / .row -->
                            <div class="row">
                                <div class="col-12 col-md-6">

                                    <h6 class="text-uppercase font-12 text-muted">Invoiced from</h6>

                                    <p class="text-muted mb-4">
                                        <strong class="text-body font-16 text-dark">FREETHNGS</strong> <br>
                                        CEO of FREETHNGS <br>
                                        USA
                                    </p>

                                    <h6 class="text-uppercase text-muted font-12">
                                        Invoiced ID
                                    </h6>

                                    <p class="mb-4 text-dark">
                                        #FREETHNGS Invoice #<?= $history[0]->plan_id ?>
                                    </p>

                                </div>
                                <div class="col-12 col-md-6 text-md-right">

                                    <h6 class="text-uppercase font-12 text-muted">
                                        Invoiced to
                                    </h6>

                                    <p class="text-muted mb-4">
                                        <strong class="text-body font-16 text-dark"><?= session('first_name') ?></strong> <br>
                                        <?= session('address') ?> 
                                    </p>

                                    <h6 class="text-uppercase text-muted font-12">
                                        Due date
                                    </h6>

                                    <p class="mb-4 text-dark">
                                        <time datetime="2019-10-15">Nov 15, 2020</time>
                                    </p>

                                </div>
                            </div> <!-- / .row -->
                            <div class="row">
                                <div class="col-12">

                                    <!-- Table -->
                                    <div class="table-responsive">
                                        <table class="table my-4">
                                            <thead>
                                                <tr>
                                                    <th class="bg-transparent border-top-0">
                                                        <span class="font-14">Description</span>
                                                    </th>

                                                    <th class="bg-transparent border-top-0 text-right">
                                                        <span class="font-14">Cost</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="">
                                                        1. subscription - 
                                                        <?= $history[0]->plan ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php
                                                        $fxprice = Helper::getcurrency(session('currency'), $fixprice);
                                                        echo session('symbols') . $fxprice;
                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="">
                                                        2. SMS BUNDLE starter -   <?= $history[0]->totalsms ?> SMS
                                                    </td>

                                                    <td class="text-right">
                                                        <?php
                                                        $sm = Helper::getcurrency(session('currency'), $history[0]->sum);
                                                        echo session('symbols') . $sm;
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                                if ($history[0]->type == 'auto') {
                                                    echo '<tr>
                                                                    <td class="">

                                                                        3. SMS Starter Auto-refill
                                                                    </td>
                                                                    
                                                                    <td class="text-right">
                                                                        $50
                                                                    </td>
                                                                </tr>';
                                                }
                                                ?>

                                                <tr>
                                                    <td class="border-top border-top-2">
                                                        <strong>Total</strong>
                                                    </td>
                                                    <td class="text-right border-top border-top-2">
                                                        <span class="font-20">
                                                            <?php
                                                            $autoprice = ($history[0]->type == 'auto') ? 50 : 0;
                                                            $total = $sm + $fxprice + $autoprice;
                                                            echo session('symbols') . $total;
                                                            ?>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <hr class="my-5">

                                    <!-- Title -->
                                    <h6 class="text-uppercase">
                                        Notes
                                    </h6>

                                    <!-- Text -->
                                    <p class="text-muted mb-0">
                                        We really appreciate your business and if there’s anything else we can do, please let us know! Also, should you need us to add VAT or anything else to this order, it’s super easy since this is a template, so just ask!
                                    </p>

                                </div>
                            </div> <!-- / .row -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
