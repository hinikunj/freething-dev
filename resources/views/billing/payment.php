<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-6">
                                    <h4 class="card-title mb-4">Billing</h4>
                                    <ul class="nav nav-pills navtab-bg">
                                        <li class="nav-item">
                                            <a href="<?= url('/') ?>/history" class="nav-link mb-3">
                                                History
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?= url('/') ?>/payment" class="nav-link active mb-3">
                                                Payment
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xl-2">
                                    <div class="ribbon-wrapper card">
                                        <div class="ribbon ribbon-warning">Subscription type</div>
                                        <p class="ribbon-content">Basic - Monthly</p>
                                        <p class="ribbon-content">Renew - 10/10/2021</p>
                                        <?php
                                        if (!empty($history)) {
                                            echo '<p class="ribbon-content"><a href="#" data-toggle="modal" data-target="#freesmsbundle">Upgrade</a> | <a href="javascript:;" class="cancelsubs"  id="' . $history[0]->id . '" >Cancel Subscription</a></p>';
                                        } else {
                                            echo '<p class="ribbon-content"><a href="javascript:;" class="nodata">Upgrade</a> </p>';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xl-2">
                                    <div class="ribbon-wrapper card">
                                        <div class="ribbon ribbon-warning">Text/SMS</div>
                                        <p class="ribbon-content">1000 of 1500</p>
                                        <p class="ribbon-content">Expires - 10/10/2021</p>
                                        <p class="ribbon-content"><a href="#">Refill</a></p>
                                    </div>
                                </div>
                                <div class="col-xl-2">
                                    <div class="ribbon-wrapper card">
                                        <div class="ribbon ribbon-warning">Offline Campaign</div>
                                        <p class="ribbon-content">Coming Soon</p>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane show active">
                                    <div class="card text-white bg-primary">
                                        <div class="card-header border-bottom-0">Billing Address</div>
                                        <div class="card-body">
                                            <div class="d-flex justify-content-between align-items-center">
                                                <?php
                                                if (empty($address)) {
                                                    echo '<div class="div">
                                                            <h5 class="text-white">No Address Found.
                                                            </h5>
                                                          </div>';
                                                } else {
                                                    echo '<div class="div">
                                                            <h5 class="text-white">
                                                                    ' . $address[0]->title . '
                                                            </h5>
                                                            <p class="card-text text-white">' . $address[0]->billing_address . '</p>
                                                          </div>';
                                                }
                                                ?>
                                                <div class="div">
                                                    <div class="actions ml-3 billing-action-icons">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="card box-margin bg-primary text-white mt-3">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="payment-area">
                                                        <h5 class="card-title mb-30 text-white">Your saved credit card and debit card</h5>
                                                        <div class="table-responsive">
                                                            <?php
                                                            if (count($card_details) > 0) {
                                                                ?>
                                                                <table class="table table-bordered table-hover savescard">
                                                                    <tbody>
                                                                    <input type="hidden" name="" class="currentid">
                                                                    <?php
                                                                    foreach ($card_details as $key => $value) {
                                                                        echo '<tr id="cr_' . $value->id . '">
                                                                            <td> Visa </td>
                                                                            <td>Ending with ' . substr($value->card_number, -4) . '</td>
                                                                            <td>' . $value->name . '</td>
                                                                            <td>' . $value->card_expiry_month . '/' . $value->card_expiry_year . '</td>
                                                                            <td>
                                                                            <div class="actions ml-3">
                                                                                <a href="javascript:;" id="' . $value->id . '" class="action-item mr-2 delCard" title="" data-original-title="Delete">
                                                                                        <i class="fa fa-times"></i>
                                                                                </a>
                                                                            </div>
                                                                            </td>
                                                                          </tr>';
                                                                    }
                                                                    ?>
                                                                    </tbody>
                                                                </table>
                                                                <?php
                                                            } else {
                                                                echo "No Saved Cards";
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col -->
            </div>
        </div>

