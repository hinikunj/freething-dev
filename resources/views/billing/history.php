<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-6">
                                    <h4 class="card-title mb-4">Billing</h4>
                                    <ul class="nav nav-pills navtab-bg">
                                        <li class="nav-item">
                                            <a href="<?= url('/') ?>/history" class="nav-link active mb-3">
                                                History
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?= url('/') ?>/payments" class="nav-link mb-3">
                                                Payment
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xl-2">
                                    <div class="ribbon-wrapper card">
                                        <div class="ribbon ribbon-warning">Subscription type</div>
                                        <p class="ribbon-content">Basic - Monthly</p>
                                        <p class="ribbon-content">Renew - 10/10/2021</p>
                                        <?php
                                        if (!empty($history)) {
                                            echo '<p class="ribbon-content"><a href="#" data-toggle="modal" data-target="#freesmsbundle">Upgrade</a> | <a href="javascript:;" class="cancelsubs"  id="' . $history[0]->id . '" >Cancel Subscription</a></p>';
                                        } else {
                                            echo '<p class="ribbon-content"><a href="javascript:;" class="nodata">Upgrade</a> </p>';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xl-2">
                                    <div class="ribbon-wrapper card">
                                        <div class="ribbon ribbon-warning">Text/SMS</div>
                                        <p class="ribbon-content">1000 of 1500</p>
                                        <p class="ribbon-content">Expires - 10/10/2021</p>
                                        <p class="ribbon-content"><a href="#">Refill</a></p>
                                    </div>
                                </div>
                                <div class="col-xl-2">
                                    <div class="ribbon-wrapper card">
                                        <div class="ribbon ribbon-warning">Offline Campaign</div>
                                        <p class="ribbon-content">Coming Soon</p>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane show active">
                                    <div class="table-responsive">
                                        <?php
                                        if (empty($history)) {
                                            echo 'No Billing History Found.';
                                        } else {
                                            ?>
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Date</th>
                                                        <th>Product type</th>
                                                        <th>Amount</th>
                                                        <th>Description</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($history as $key => $value) {
                                                        $total = $value->featured + $value->sum;
                                                        $total = Helper::getcurrency(session('currency'), $total);
                                                        if($total > 0){
                                                            $detail = ($value->expiry == 30) ? 'Monthly' : 'Yearly';
                                                            $expiry_date = ($value->expiry == 30) ? date('M, d Y', strtotime('+30 Days')) : date('M, d Y', strtotime('+1 years'));
                                                            
                                                            $description = $detail . ' Plan Paid Up Till ' . $expiry_date;
                                                            $invoice = '<a href="' . url('/') . '/invoice/' . $value->id . '"><button type="button" class="btn btn-danger mb-2 mr-2">View Invoice</button></a>';
                                                            $productType = 'SMS Bundle - ' . $value->totalsms;
                                                        } else {
                                                            $description = "-";
                                                            $invoice = $productType = "N/A";
                                                        }

                                                        echo '<tr>
							<th>' . $value->created_date . '</th>
							<td>' . $productType . '</td>
							<td>' . session('symbols') . $total . '</td>
							<td>' . $description . '</td>
							<td>'.$invoice.'</td>
                                                    </tr>';
                                                    }
                                                    ?>



                                                </tbody>
                                            </table>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end card-->
                </div> <!-- end col -->
            </div>
        </div>