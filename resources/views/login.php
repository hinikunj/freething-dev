<script src="<?= url('/') ?>/resources/assets/js/jquery.min.js"></script>
<body class="login-area">

    <!-- Preloader -->
    <div id="preloader-area">
        <div class="lds-ripple">
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- Preloader -->

    <!-- ======================================
    ******* Page Wrapper Area Start **********
    ======================================= -->
    <div class="main-content- h-100vh">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center">
                <div class="hero">
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                </div>
                <div class="col-sm-10 col-md-8 col-lg-5">
                    <!-- Middle Box -->
                    <div class="middle-box">
					
                        <div class="card">
							
                            <div class="card-body p-4">
								<div class="text-center"><img src="<?= url('/') ?>/resources/assets/img//core-img/logo-ft-w1.png" /></div>
                                <!-- Logo -->
                                <h4 class="font-24 mb-30">Login</h4>

                                <form action="" method="post" id="login">
                                    <div class="message"></div>
                                    <div class="form-group">
                                        <input class="form-control phoneFields" type="phone" name="phone" id="emailaddress" required="" placeholder="Enter your email">
                                    </div>

                                    <div class="form-group">
                                        <a href="forget-password.html" class="text-dark float-right"></a>
                                        <input class="form-control login" type="password" required="" name="password" id="password" placeholder="Enter your password">
                                    </div>

                                    <div class="form-group d-flex justify-content-between align-items-center mb-3">
                                        <div class="checkbox d-inline mb-0">
                                            <input type="checkbox" name="checkbox-1" id="checkbox-8">
                                            <label for="checkbox-8" class="cr mb-0 font-13">Remember me</label>
                                        </div>
                                        <span><a class="font-12 text-success" href="<?= url('/') ?>/forget-password">Forgot your password?</a></span>
                                    </div>

                                    <div class="form-group mb-0">
                                        <button class="btn btn-info btn-block" type="submit"> Log In </button>
                                    </div>

                                    <div class="text-center mt-15"><span class="mr-2 font-12">Don't have an account?</span><a class="font-12" href="<?= url('/').'/register' ?>">Sign up</a></div>
									
									<div class="text-center mt-15">
										<a href="javascript:;" class="google_auth"><img src="<?= url('/') ?>/resources/assets/img//core-img/google-login.png" style="width:100%;	" /></a>
									</div>

                                </form>

                                <!-- end card -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ======================================
    ********* Page Wrapper Area End ***********
    ======================================= -->

    <!-- Plugins Js -->
    <script src="<?= url('/') ?>/resources/assets/js/popper.min.js"></script>
    <script src="<?= url('/') ?>/resources/assets/js/bootstrap.min.js"></script>
    <script src="<?= url('/') ?>/resources/assets/js/bundle.js"></script>

    <!-- Active JS -->
    <script src="<?= url('/') ?>/resources/assets/js/default-assets/active.js"></script>


  <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
  <script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-app.js"></script>

  <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
  <script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-analytics.js"></script>

  <!-- Add Firebase products that you want to use -->
  <script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-auth.js"></script>
  <script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-firestore.js"></script>

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyDl-PSB1IxAMNvg6DBc31UdEUFHeadZQ7k",
    authDomain: "signin-test-216405.firebaseapp.com",
    databaseURL: "https://signin-test-216405.firebaseio.com",
    projectId: "signin-test-216405",
    storageBucket: "signin-test-216405.appspot.com",
    messagingSenderId: "602679727849",
    appId: "1:602679727849:web:4de9130e4e2af5b5de0278",
    measurementId: "G-NCSKFD7849"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  // firebase.analytics();
</script>
 <script>
$(document).ready(function(){
   
 $('.google_auth').click(function(){ 
        var provider = new firebase.auth.GoogleAuthProvider();
// firebase.auth().signInWithRedirect(provider);
         firebase.auth().signInWithPopup(provider).then(function(result) {
          // This gives you a Google Access Token. You can use it to access the Google API.
          var token = result.credential.accessToken;
          // The signed-in user info.
          var user = result.user;
          console.log(user);

                $.ajax({
                url: baseurl+'google-register',
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                data: 'email='+user.email+'&first_name='+user.displayName+'&phone='+user.phoneNumber,
                
                type: 'POST',
                success: function (result) {
                   console.log(result);
                result = jQuery.parseJSON(result);
                console.log(result.description);
    //                             ph = '';
                $('.message').html('').removeClass('alert alert-success').removeClass('alert alert-danger');
                if(result.success == true)
                {
                  if(result.role == 0)
                   {
                    window.location.href = baseurl + "sp-dashboard";
                   }
                   else
                   {
                    window.location.href = baseurl + "dashboard";
                     
                   }
                   
                    
                                        
                }
                else
                {
                    $('.message').html(result.description).addClass('alert alert-danger');
                }
                
                }
                }) 
                return false;
          // ...
        }).catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // The email of the user's account used.
          var email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          var credential = error.credential;
          // ...
        });



    })

})
</script>


</body>

</html>