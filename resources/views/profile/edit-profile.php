<?php
$rolesArr = array('Super Admin', 'C-Level', 'Marketing', 'Founder', 'Manager', 'Product manager', 'Sales Representative');
?>
<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 box-margin height-card">
                    <div class="card card-body">
                        <h4 class="card-title">Account Settings - Edit Profile</h4>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <form action="" method="post" id="gnrlProfile">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail111">First Name</label>
                                                <input type="text" name="first_name" value="<?= $list[0]->first_name ?>" class="form-control" id="exampleInputEmail111" placeholder="First Name">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail12">last Name</label>
                                                <input type="text" name="last_name"  value="<?= $list[0]->last_name ?>" class="form-control" id="exampleInputEmail12" placeholder="last Name">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="exampleInputPassword11">Role</label>
                                                <select class="custom-select mt-3" name="role" >
                                                    <option value="">Select Role</option>
                                                    <?php
                                                    foreach ($rolesArr as $key => $value) {
                                                        if ($key != 0) {
                                                            $slct = ($key == $list[0]->role) ? 'selected' : '';
                                                            echo '<option value="' . $key . '" ' . $slct . '>' . $value . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="exampleInputPassword12">Phone Number</label>
                                                <input type="text" name="phone" value="<?= $list[0]->phone ?>" class="form-control" id="exampleInputPassword12" required placeholder="Phone Number">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="exampleInputPassword12">Website</label>
                                                <input type="text" name="website" value="<?= $list[0]->website ?>"  class="form-control" id="exampleInputPassword12" placeholder="Website">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="exampleInputPassword12">Company</label>
                                                <input type="text" name="company" value="<?= $list[0]->company ?>"  class="form-control" id="exampleInputPassword12" placeholder="Company">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-info mr-2">Save Changes</button>
                                    <span class="message"></span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 box-margin height-card">
                    <div class="card card-body">
                        <h4 class="card-title">Change Email</h4>
                        <p>You can change your email at anytime. But you will have to confirm the email after the change.</p>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <form method="post" action="" id="change-email">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="email">Change Email Address</label>
                                                <input type="email" name="email" value="<?= $list[0]->email ?>"  class="form-control" id="exampleInputEmail111" placeholder="Enter your new email address">
                                            </div>
                                            <div class="otpmessage"></div>
                                            <div class="form-group otpblock">
                                                <label for="otp">Enter OTP</label>
                                                <input type="text" name="otp"   class="form-control" id="exampleInputEmail111" placeholder="Enter OTP">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-info mr-2 otpsub">Save Changes</button>
                                    <span class="emailmessage"></span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 box-margin height-card">
                    <div class="card card-body">
                        <h4 class="card-title">Change Password</h4>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <form method="post" id="resetpwd" action="">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="password">New Password</label>
                                                <input type="text" name="password" class="form-control" id="exampleInputEmail111" placeholder="New Password">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="confirm_password">Confirm Password</label>
                                                <input type="text" name="confirm_password" class="form-control" id="exampleInputEmail111" placeholder="Confirm Password">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-info mr-2">Save Changes</button>
                                    <span class="messagepwd"></span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 box-margin height-card">
                    <div class="card card-body">
                        <h4 class="card-title">Your store location</h4>
                        <p>if you oprate in multiple nations, please create a separate FREETHNGS store for each location website. We are bringing FREETHNGS to other locations. If your location is not listed, Please contact us. We may have a special arrangements in your area. https://freethngs.crisp.help</p>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <form method="post" action="" id="profileloc">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="location">Location</label>
                                                <select class="custom-select mt-3" <?= ($list[0]->location == '') ? false : 'disabled' ?> required name="location">
                                                    <option selected="">Select your location</option>
                                                    <?php
                                                    foreach ($location as $key => $value) {
                                                        $chk = (strtolower($list[0]->location) == strtolower($value->location)) ? 'selected' : '';
                                                        echo '<option ' . $chk . '>' . $value->location . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    if ($list[0]->location == '') {
                                        echo ' <button type="submit" class="btn btn-info mr-2">Save Changes</button>';
                                    }
                                    ?>
                                    <span class="locmessage"></span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 box-margin height-card">
                    <div class="card card-body">
                        <h4 class="card-title">Change Language</h4>
                        <p>Choose the FREETHNGS store innterface language. We're currently making translations to different languages. If you'd like to help  with new translations, Please message our support team at  https://freethngs.crisp.help</p>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <form method="post" action="" id="langProfile">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label for="languages">Location</label>
                                                <select class="custom-select mt-3" name="languages" required>
                                                    <option selected="">Select language</option>
                                                    <?php
                                                    foreach (Config::get('constants.languages') as $key => $value) {
                                                        $chk = (strtolower($list[0]->languages) == strtolower($value)) ? 'selected' : '';
                                                        echo '<option ' . $chk . '>' . $value . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-info mr-2">Save Changes</button>
                                    <span class="langmessage"></span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 box-margin height-card">
                    <div class="card card-body">
                        <h4 class="card-title">Industry Category</h4>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <form action="" method="post" id="industryAdd">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <select class="custom-select" name="industry" required>
                                                <?php
                                                foreach (Config::get('constants.industry') as $key => $value) {

                                                    $chk = (strtolower($list[0]->industry) == strtolower($value)) ? 'selected' : '';
                                                    echo '<option ' . $chk . '>' . $value . '</option>';
                                                }
                                                ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-info mr-2">Save Changes</button>
                                    <span class="indmessage"></span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 box-margin height-card">
                    <div class="card card-body">
                        <h4 class="card-title">Delete account</h4>
                        <p>Please delete if you want to remove your account and delete all your personal data and all widgets.</p>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <form>
                                    <button type="button" class="btn btn-info mr-2 delete-acc">Delete Account</button>
                                    <span class="delmessage"></span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>