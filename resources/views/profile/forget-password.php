<script src="<?= url('/') ?>/resources/assets/js/jquery.min.js"></script>

<body class="login-area">
   <!-- Preloader -->
    <div id="preloader-area">
        <div class="lds-ripple">
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- Preloader -->

    <!-- ======================================
    ******* Page Wrapper Area Start **********
    ======================================= -->
    <div class="main-content- h-100vh">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center">
                <div class="hero">
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                </div>
                <div class="col-sm-10 col-md-8 col-lg-5">
                    <!-- Middle Box -->
                    <div class="middle-box">
                        <div class="card">
                            <div class="card-body p-4">
							<div class="text-center"><img src="<?= url('/') ?>/resources/assets/img/core-img/logo-ft-w1.png" /></div>
                                <h4 class="font-24 mb-30">Forgot Password ?</h4>

                                <form action="" method="post" id="forget-password">

                                    <div class="form-group">
                                        <label class="lock-text text-dark">Email</label>
                                        <input type="text" name="email" class="form-control height-50" id="examplePassword1" placeholder="Email or Phone">
                                        <h3>OR</h3>
                                        <label for="fullname">Phone Number</label>
                                        <div data-label="Example" class="mt-example demo-forms">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <select name="phonecode" id="inputState" class="form-control">
                                               <option>+001</option>
                                                <option>+91</option>
                                                <?= Config::get('constants.phonecodes'); ?>
                                                
                                            </select>
                                            </div>
                                            <input id="inputPhoneNumber" type="text" class="form-control" name="phone" border-left-0" placeholder="Enter phone number">
                                        </div>
                                    </div>
                                
                                    </div>
									
                                    <div class="form-group mb-0">
                                        <button class="btn btn-primary btn-block" type="submit">Send Password</button>
                                    </div>

                                    <div>
                                         
                                    </div>
									<div class="text-center mt-15"><span class="mr-2 font-13">Don't have an account?</span><a class="font-12" href="register.html">Sign up</a></div>
									<div class="text-center mt-15"><span class="mr-2 font-13">Already have an account?</span><a class="font-13" href="login.html">Sign in</a></div>
                                </form>
                                <!-- end card -->

                                <!-- OTP Block -->
                                 <form action="" id="resetForgetPwd" method="post">
                                    <div class="otpmsg"></div>
                                    <div class="crtext"></div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <!-- <label for="fullname">Phone Verification</label> -->
                                                 <input type="hidden" value="" name="email_to_verify">
                                                 <input type="hidden" value="" name="code_to_verify">
                                                <input type="hidden" value="" name="mobile_to_verify">
                                                <input class="form-control txtField" type="text"  name="otp" id="f" placeholder="Enter OTP" required>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                           <div class="form-group">
                                        <label for="password">Password</label>
                                        <input class="form-control"  type="password" required name="password" id="password" placeholder="Enter your password">
                                    </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                        <label for="password">Confirm Password</label>
                                        <input class="form-control" type="password" name="confirm_password" required id="confirm_password" placeholder="Enter your password">
                                    </div>
                                        </div>
                                    </div>
                                     <div class="form-group mb-0 mt-15">
                                                <button class="btn btn-info btn-block" type="submit">Save</button>
                                            </div>
                                </form>
                               <!-- END OTP Bllock --> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ======================================
    ********* Page Wrapper Area End ***********
    ======================================= -->

    <!-- Plugins  Js -->
    
    <script src="<?= url('/') ?>/resources/assets/js/popper.min.js"></script>
    <script src="<?= url('/') ?>/resources/assets/js/bootstrap.min.js"></script>
    <script src="<?= url('/') ?>/resources/assets/js/bundle.js"></script>

    <!-- Active JS -->
    <script src="<?= url('/') ?>/resources/assets/js/default-assets/active.js"></script>

</body>

</html>