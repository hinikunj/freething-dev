<script src="<?= url('/') ?>/resources/assets/js/jquery.min.js"></script>
<body class="login-area">
    <!-- Preloader -->
    <div id="preloader-area">
        <div class="lds-ripple">
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- Preloader -->

    <!-- ======================================
    ******* Page Wrapper Area Start **********
    ======================================= -->
    <div class="main-content- h-100vh">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center">
                <div class="hero">
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                    <div class="cube"></div>
                </div>
                <div class="col-md-8 col-lg-6">
                    <!-- Middle Box -->
                    <div class="middle-box">
                        <div class="card">
                            <div class="card-body p-4">
                                <div class="text-center mb-5"><img src="<?= url('/') ?>/resources/assets/img/core-img/logo-ft-w1.png" /></div>
                                <h4 class="font-24 mb-30 crtext">Create account.</h4>

                                <form action="" id="register" method="post">
                                    <div class="message"></div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="fullname">First Name</label>
                                                <input class="form-control txtField" type="text"  name="first_name" id="first_name" placeholder="First name" required>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="fullname">Last Name</label>
                                                <input class="form-control txtField"  type="text" name="last_name" id="last_name" placeholder="Last name" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="fullname">For varification</label>
                                                <div data-label="Example" class="mt-example demo-forms">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <select name="phonecode" id="inputState" class="form-control">
                                                                <?= Config::get('constants.phonecodes'); ?>
                                                            </select>
                                                        </div>
                                                        <input id="inputPhoneNumber" type="text" class="form-control" name="phone" border-left-0" placeholder="Enter phone number">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="emailaddress">Country</label>
                                                <select class="form-control" name="location" id="location">
                                                    <option value="US">United States</option>
                                                    <option value="GB">United Kingdom</option>
                                                    <option value="CA">Canada</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="emailaddress">Email address</label>
                                                <input class="form-control emailFields"  type="email" name="email" id="email" required placeholder="Enter your email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input class="form-control"  type="password" required name="password" id="password" placeholder="Enter your password">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="password">Confirm Password</label>
                                                <input class="form-control" type="password" name="confirm_password" required id="confirm_password" placeholder="Enter your password">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="indstry">Company Name</label>
                                                <input class="form-control" name="company" type="text" required="" id="company name" placeholder="Enter your company name">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="indstry">Industry Category</label>
                                                <select class="custom-select" name="industry">
                                                    <option selected="">Select</option>
                                                    <?php
                                                    foreach (Config::get('constants.industry') as $key => $value) {
                                                        echo '<option>' . $value . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mb-0 mt-15">
                                        <button class="btn btn-info btn-block" type="submit">Create my account</button>
                                    </div>
                                    <div class="text-center mt-15"><span class="mr-2 font-13">Already have an account?</span><a class="font-13" href="<?= url('/') . '/login' ?>">Sign in</a></div>
                                    <div class="text-center mt-15">
                                        <a href="javascript:;" class="google_auth"><img src="<?= url('/') ?>/resources/assets/img/core-img/google-login.png" style="width:100%;	" /></a>
                                    </div>
                                </form>
                                <!-- OTP Block -->
                                <form action="" id="phoneOtpVerify" method="post">
                                    <div class="otpmsg"></div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <!-- <label for="fullname">Phone Verification</label> -->
                                                <input type="hidden" value="" name="code_to_verify">
                                                <input type="hidden" value="" name="mobile_to_verify">
                                                <input class="form-control txtField" type="text"  name="otp" id="f" placeholder="Enter OTP" required>
                                            </div>
                                            <div class="form-group mb-0 mt-15">
                                                <button class="btn btn-info btn-block" type="submit">Verify</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- END OTP Bllock -->
                                <!-- end card -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ======================================
    ********* Page Wrapper Area End ***********
    ======================================= -->

    <!-- Plugins  Js -->
    <script src="<?= url('/') ?>/resources/assets/js/popper.min.js"></script>
    <script src="<?= url('/') ?>/resources/assets/js/bootstrap.min.js"></script>
    <script src="<?= url('/') ?>/resources/assets/js/bundle.js"></script>
    <!-- Active JS -->
    <script src="<?= url('/') ?>/resources/assets/js/default-assets/active.js"></script>
    <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-app.js"></script>
    <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
    <script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-analytics.js"></script>
    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.17.1/firebase-firestore.js"></script>

    <script>
        // Your web app's Firebase configuration
        var firebaseConfig = {
            apiKey: "AIzaSyDl-PSB1IxAMNvg6DBc31UdEUFHeadZQ7k",
            authDomain: "signin-test-216405.firebaseapp.com",
            databaseURL: "https://signin-test-216405.firebaseio.com",
            projectId: "signin-test-216405",
            storageBucket: "signin-test-216405.appspot.com",
            messagingSenderId: "602679727849",
            appId: "1:602679727849:web:4de9130e4e2af5b5de0278",
            measurementId: "G-NCSKFD7849"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
        // firebase.analytics();
    </script>
    <script>
        $(document).ready(function () {
            $('.google_auth').click(function () {
                var provider = new firebase.auth.GoogleAuthProvider();
   // firebase.auth().signInWithRedirect(provider);
                firebase.auth().signInWithPopup(provider).then(function (result) {
                    // This gives you a Google Access Token. You can use it to access the Google API.
                    var token = result.credential.accessToken;
                    // The signed-in user info.
                    var user = result.user;
                    $.ajax({
                        url: baseurl + 'google-register',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: 'email=' + user.email + '&first_name=' + user.displayName + '&phone=' + user.phoneNumber,
                        type: 'POST',
                        success: function (result) {
                            result = jQuery.parseJSON(result);
                            $('.message').html('').removeClass('alert alert-success').removeClass('alert alert-danger');
                            if (result.success == true)
                            {
                                if (result.role == 0) {
                                    window.location.href = baseurl + "product-list";
                                } else {
                                    window.location.href = baseurl + "dashboard";
                                }
                            } else {
                                $('.message').html(result.description).addClass('alert alert-danger');
                            }
                        }
                    })
                    return false;
                    // ...
                }).catch(function (error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    // The email of the user's account used.
                    var email = error.email;
                    // The firebase.auth.AuthCredential type that was used.
                    var credential = error.credential;
                    // ...
                });
            })
        })
    </script>
</body>
</html>