<!-- Main Page -->
<div class="main-panel">
	<div class="content-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-12 height-card box-margin">
					<div class="card">
						<div class="card-body">
							<h3 class="card-title mb-4">Campaigns</h3>
							
							<p>
								<div class="div">
									<div class="badge badge-success badge-pill">Running Period</div>
								</div>							
							</p>
								<div class="div">
									Start date: <?= date('M j, Y',strtotime($campaign_detail[0]->created_date)); ?>
								</div>
							<div class="tab-content">
								<div class="tab-pane show active" id="previouscamp">
									<div class="table-responsive">
										
										<table id="basic-datatable" class="table dt-responsive nowrap w-100">
									<thead>
												<tr>
													<th>Created On</th>
													<th>Product in campaign</th>
													<th>No of products won</th>
													<th>Left in stock</th>
													<th>Won by</th>
													<th>Total cost</th>
													<th>End date</th>
													<th>Status</th>
												</tr>
											</thead>
											<tbody>
											<?php 
												if(isset($campaign_detail) && count($campaign_detail) > 0){
												foreach($campaign_detail as $campaign) { 
													$productDetails = json_decode($campaign->product_details);
											?>
												<tr>
													<th><?= date('M j, Y',strtotime($campaign->created_date)) ?></th>
													<td>
														<a href="<?= url('/campaign/'.$campaign->id) ?>">
															<?= count($productDetails); ?>
														</a>
													</td>
													<td><?= $productDetails[0]->winning_freq ?></td>
													<td><?= ($productDetails[0]->winning - $productDetails[0]->winning_freq) ?></td>
													<td><?= $productDetails[0]->winning ?> Subscribers</td>
													<td>$<?= $productDetails[0]->shipping_cost ?></td>
													<td><?= ($campaign->deleted_at) ? date('M j, Y',strtotime($campaign->deleted_at)) : '-'; ?></td>
													<td><?= ($campaign->deleted_at) ? "Previous" : 'Running'; ?></td>
												</tr>
												<?php } } ?>
											</tbody>
								</table>
							</div>
								</div>
							</div>
						</div>
					</div> <!-- end card-->
				</div> <!-- end col -->
			</div>
		</div>
	</div>
<div>