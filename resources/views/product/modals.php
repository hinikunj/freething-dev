<style>
    .img-wrap {
        position: relative;
    }
    .img-wrap .close {
        position: absolute;
        top: 2px;
        right: 2px;
        z-index: 100;
    }
</style>
<div class="modal inmodal fade text-left" id="each-product-detail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Products</h4>
            </div>
            <form action="" method="post" id="update-product" enctype="multipart/form-data">
                <input type="hidden" name="id">

                <div class="modal-body">

                    <div class="row flex-row flex-nowrap mt-4">
                        <div class="col-xl-6 col-md-6">
                            <div class="form-group row g-mb-25">
                                <input type="hidden" name="attach">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">File Upload</label>
                                <div class="col-sm-9">
                                    <div class="showimg"></div>
                                    <!-- <form action="#" class="dropzone dz-clickable"> -->
                                    <div  class="dropzone dz-clickable fileattach">

                                        <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                    </div>
                                    <!-- </form> -->
                                </div>
                            </div>
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Custom Store By Location</label>
                                <div class="col-sm-9 selectloc">

                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio1a" name="location_type" class="custom-control-input location_type" value="locsearch" required>
                                        <label class="custom-control-label" for="customRadio1a">Specific Country</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio2a" name="location_type" class="custom-control-input location_type" value="comsearch" required>
                                        <label class="custom-control-label" for="customRadio2a">Specific Company</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg locsearch box">Select Location</label>
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg comsearch box">Search Company</label>
                                <div class="col-sm-9">
                                    <select class="form-control rounded-0 locsearch box locationbox" id="exampleSelect1" name="location" required>

                                    </select>
                                    <input type="text" name="company" class="form-control form-control-lg rounded-0 comsearch box search-company" id="lgFormGroupInput1" placeholder="Search Company" >
                                    <div class="companylist"></div>
                                </div>
                            </div>

                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Product Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-lg rounded-0 name" required name="name" id="lgFormGroupInput1" placeholder="Product Name">
                                </div>
                            </div>
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Product Description</label>
                                <div class="col-sm-9">
                                    <textarea id="summernote" class="form-control description" name="description" required placeholder="Your Message Here ..."></textarea>
                                </div>
                            </div>
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Product Category Target</label>
                                <div class="col-sm-9">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input category" value="B2B"  name="category[]" id="customCheck11">
                                        <label class="custom-control-label" for="customCheck11">B2B</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input category" value="B2C"  name="category[]" id="customCheck12">
                                        <label class="custom-control-label" for="customCheck12">B2C</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" value="All (B2B & B2C)" class="custom-control-input category"  name="category[]" id="customCheck13">
                                        <label class="custom-control-label" for="customCheck13">All (B2B & B2C)</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Product Type</label>
                                <div class="col-sm-9">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input type" value="E Items"  name="type[]" id="customCheck14">
                                        <label class="custom-control-label" for="customCheck14">E Items</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input type" value="Physical"  name="type[]" id="customCheck15">
                                        <label class="custom-control-label" for="customCheck15">Physical</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Shipping Responsibility</label>
                                <div class="col-sm-9">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input shipping_by" value="freethings"  name="shipping_by[]" id="customCheck16">
                                        <label class="custom-control-label" for="customCheck16">FREETHNGS</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input shipping_by" value="store owner"  name="shipping_by[]" id="customCheck17">
                                        <label class="custom-control-label" for="customCheck17">Store owner</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Coupons or Promo upload</label>
                                <div class="col-sm-9">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input coupons" value="1"  name="coupons" id="customCheck18">
                                        <label class="custom-control-label" for="customCheck18">Requires coupons upload</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-md-6">
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Quantity</label>
                                <div class="col-sm-9"> 
                                    <div class="input-group">
                                        <input class="px-2 form-control quantity" type="number" required name="quantity" style="width: 3.2rem;" value="1" required>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text q_addons" id="basic-addon1">0</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Price/Items</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <input type="text" class="form-control price" name="price" required placeholder="Price/Items" aria-label="Username" aria-describedby="basic-addon1">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><span class="symbol">$</span><span class="p_addons">0</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Shipping Cost</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <input type="text" name="shipping_cost" class="form-control shipping_cost" required placeholder="Price/Items" aria-label="Username" aria-describedby="basic-addon1">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><span class="symbol">$</span><span class="s_addons">0</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Charges After win</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <input type="text" class="form-control price_after_win" name="price_after_win" required placeholder="Charges After win" aria-label="Username" aria-describedby="basic-addon1">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><span class="symbol">$</span><span class="sc_addons">0</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Product Prize Value</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <input type="text" class="form-control prize" name="prize" required placeholder="$" aria-label="Username" aria-describedby="basic-addon1">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><span class="symbol">$</span><span class="pz_addons">0</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Price Value/Displayed Price</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <input type="text" class="form-control displayed_price" name="displayed_price" required placeholder="$" aria-label="Username" aria-describedby="basic-addon1">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><span class="symbol">$</span><span class="d_addons">0</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Who determine odd of winning</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <select id="inputState" class="form-control determine_winning" required name="determine_winning">
                                            <option>Select</option>
                                            <option value="freethings">Freethings </option>
                                            <option value="store owner">Store Owner</option>
                                        </select>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text w_addons" id="basic-addon1">FREETHNGS</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Choose Odds</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <select id="inputState" class="form-control odds" required name="odds">
                                            <option value="">Select</option>
                                            <option>1:15</option>
                                            <option>1:25</option>
                                            <option>1:200</option>
                                            <option>1:500</option>
                                            <option>1:1000</option>
                                            <option>1:5000</option>
                                            <option>1:10,000</option>
                                            <option>1:20,000</option>
                                        </select>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text o_addons" id="basic-addon1">Select</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Product Provided By</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <select id="inputState" class="form-control provide_by" required name="provide_by">
                                            <option>Select</option>
                                            <option value="freethings">Freethings </option>
                                            <option value="store owner">Store Owner</option>
                                        </select>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text pr_addons" id="basic-addon1">Select</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Flow Template</label>
                                <div class="col-sm-9">
                                    <div class="input-group"> 
                                        <select id="flow_template" class="form-control flow_template template" required name="template">
                                            <option>Select</option>
                                            <?php
                                            if (isset($flows) && count($flows) > 0) {
                                                foreach ($flows as $flow) {
                                                    echo '<option data-questions="' . $flow['questions'] . '" value="' . $flow['flow_id'] . '">' . $flow['flow_name'] . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text t_addons" id="basic-addon1">Select</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row g-mb-25">
                                <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Data to be captured after win</label>
                                <div class="col-sm-9">
                                    <div>
                                        <input name="data" class="datasetFlow" id="tags_addons" value="New York,Texas,Florida,New Mexico"  required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info btnsubmit">Submit</button>
                    <span class="message"></span>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- View Product in Checkput processs for user -->

<div class="modal inmodal fade" id="product-info" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Product Information</h4>
            </div>
            <div class="modal-body product-info-box">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
        /*Dataset based on flow template*/
        $(document).on("change", ".flow_template", function (e) {
            var questions = $(this).find(':selected').data('questions');
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: baseurl + 'get-datasset',
                dataType: 'json',
                data: 'question_ids=' + questions,
                success: function (response) {
                    if (response.success) {
                        $('#tags_addons').val(response.data);
                        $('.datasetFlow').tagsInput('refresh');
                    } else {
                        $('#tags_addons').html('Last name,Phone number,First Name');
                    }
                }
            });
        });
</script>