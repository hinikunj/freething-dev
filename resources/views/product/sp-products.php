<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12 col-sm-12 col-xl-12">
                    <!-- Card -->
                    <div class="card box-margin">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <button type="button" class="btn btn-secondary mb-2 mr-2">Calender</button> <button type="button" class="btn btn-secondary mb-2 mr-2 add-location-modal"  >Location</button>
                                </div>
                                <div class="col-auto">
                                    <button type="button" class="btn btn-danger mb-2 mr-2 addproductpopup" >Add Products</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive cart-area">
                                <table id="datatable-buttons" class="table table-nowrap table-striped table-centered nowrap w-100">
                                    <thead>
                                        <tr>
                                            <th>Sr. No</th>
                                            <th>Product & Price</th>
                                            <th>Shipping</th>
                                            <th>Odd of Winning</th>
                                            <th>Quantity In Store</th>
                                            <th>Charges After Win</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 0;
                                        foreach ($list as $key => $value) {
                                            $imageArr = explode(',', $value->attach);
                                            echo '<tr id="pr_' . $value->id . '">
                           <td>
                              ' . ++$i . '
                           </td>
                           <td class="text-left">
                              <img src="' . url('/') . '/resources/assets/uploads/' . $imageArr[0] . '" alt="contact-img" title="contact-img" class="round mr-3 product-thumb" />
                              <p class="m-0 d-inline-block align-middle font-16">
                                 <a href="apps-ecommerce-products-details.html" class="text-body">' . $value->name . '</a>
                                 <br>
                                 <small class="mr-2 disprice_' . $value->id . '"><b>' . $value->price . '</b> </small>
                              </p>
                           </td>
                           <td class="disshipping_' . $value->id . '">
                             ' . $value->shipping_cost . '
                           </td>
                           <td class="disodds_' . $value->id . '">
                              TBD by ' . $value->odds . '
                           </td>
                           <td>
                     <div class="input-group">
                           <input type="text" name="quantities" id="' . $value->id . '" value="' . $value->quantity . '" class="form-control" style="width: 90px;" placeholder="Qty" aria-label="Username" aria-describedby="basic-addon1">
                           
                        </div>
                             <span class="q_' . $value->id . '"></span>
                           </td>
                           <td class="disafterwin_' . $value->id . '">
                              ' . $value->price_after_win . '
                           </td>
                           <td>
                              <div class="actions">
                                 <a href="javascript:;" class="action-item mr-2 edit-product" id="' . $value->id . '">
                                 <i class="fa fa fa-pencil"></i>
                                 </a>
                                 <a href="javascript:;" class="action-item mr-2 view-product-details" data-toggle="tooltip" title="" data-original-title="Quick view" id="' . $value->id . '">
                                 <i class="fa fa fa-check"></i>
                                 </a>
                                 <a href="javascript:;" class="action-item mr-2 delProduct" data-toggle="tooltip" id="' . $value->id . '" title="" data-original-title="Move to trash">
                                 <i class="fa fa-times"></i>
                                 </a>
                              </div>
                           </td>
                        </tr>';
                                        }
                                        ?>


                                    </tbody>
                                </table>
                            </div>
                            <!-- end table-responsive-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal fade text-left" id="addproductpopup" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Products</h4>
                    </div>
                    <form action="" method="post" id="add-product" enctype="multipart/form-data">
                        <div class="modal-body">

                            <div class="row flex-row flex-nowrap mt-4">
                                <div class="col-xl-6 col-md-6">
                                    <div class="form-group row g-mb-25">
                                        <input type="hidden" name="attach">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">File Upload</label>
                                        <div class="col-sm-9">
                                            <!-- <form action="#" class="dropzone dz-clickable"> -->
                                            <div  class="dropzone dz-clickable fileattach">

                                                <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                            </div>
                                            <!-- </form> -->
                                        </div>
                                    </div>
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Custom Store By Location</label>
                                        <div class="col-sm-9 selectloc">

                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="location_type" class="custom-control-input" value="locsearch" required>
                                                <label class="custom-control-label" for="customRadio1">Specific Country</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="location_type" class="custom-control-input" value="comsearch" required>
                                                <label class="custom-control-label" for="customRadio2">Specific Company</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg locsearch box">Select Location</label>
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg comsearch box">Search Company</label>
                                        <div class="col-sm-9">
                                            <select class="form-control rounded-0 locsearch box locationbox" id="exampleSelect1" name="location" required>

                                            </select>
                                            <input type="text" class="form-control form-control-lg rounded-0 comsearch box search-company" id="lgFormGroupInput1" placeholder="Search Company" >
                                            <div class="companylist"></div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="currency" id="currencyfield">
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Product Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control form-control-lg rounded-0" required name="name" id="lgFormGroupInput1" placeholder="Product Name">
                                        </div>
                                    </div>
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Product Description</label>
                                        <div class="col-sm-9">
                                            <textarea id="summernote" class="form-control" name="description" required placeholder="Your Message Here ..."></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Product Category Target</label>
                                        <div class="col-sm-9">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" value="B2B"  name="category[]" id="customCheck1">
                                                <label class="custom-control-label" for="customCheck1">B2B</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" value="B2C"  name="category[]" id="customCheck2">
                                                <label class="custom-control-label" for="customCheck2">B2C</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" value="All (B2B & B2C)" class="custom-control-input"  name="category[]" id="customCheck3">
                                                <label class="custom-control-label" for="customCheck3">All (B2B & B2C)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Product Type</label>
                                        <div class="col-sm-9">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" value="E Items"  name="type[]" id="customCheck4">
                                                <label class="custom-control-label" for="customCheck4">E Items</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" value="Physical"  name="type[]" id="customCheck5">
                                                <label class="custom-control-label" for="customCheck5">Physical</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Shipping Responsibility</label>
                                        <div class="col-sm-9">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" value="freethings"  name="shipping_by[]" id="customCheck6">
                                                <label class="custom-control-label" for="customCheck6">FREETHNGS</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" value="store owner"  name="shipping_by[]" id="customCheck7">
                                                <label class="custom-control-label" for="customCheck7">Store owner</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Coupons or Promo upload</label>
                                        <div class="col-sm-9">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" value="1"  name="coupons" id="customCheck8">
                                                <label class="custom-control-label" for="customCheck8">Requires coupons upload</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-md-6">
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Quantity</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <input class="px-2 form-control" type="number" required name="quantity" style="width: 3.2rem;" value="1" required>
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text q_addons" id="basic-addon1">0</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Price/Items</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="price" required placeholder="Price/Items" aria-label="Username" aria-describedby="basic-addon1">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1"><span class="symbol">$</span><span class="p_addons">0</span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Shipping Cost</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <input type="text" name="shipping_cost" class="form-control" required placeholder="Price/Items" aria-label="Username" aria-describedby="basic-addon1">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1"><span class="symbol">$</span><span class="s_addons">0</span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Charges After win</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <input type="text" readonly="readonly" class="form-control" name="price_after_win" required placeholder="Charges After win" aria-label="Username" aria-describedby="basic-addon1">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1"><span class="symbol">$</span><span class="sc_addons">0</span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Product Prize Value</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="prize" required placeholder="$" aria-label="Username" aria-describedby="basic-addon1">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1"><span class="symbol">$</span><span class="pz_addons">0</span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Price Value/Displayed Price</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="displayed_price" required placeholder="$" aria-label="Username" aria-describedby="basic-addon1">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1"><span class="symbol">$</span><span class="d_addons">0</span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Who determine odd of winning</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <select id="inputState" class="form-control" required name="determine_winning">
                                                    <option>Select</option>
                                                    <option value="freethings">Freethings </option>
                                                    <option value="store owner">Store Owner</option>
                                                </select>
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text w_addons" id="basic-addon1">Select</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row g-mb-25 odds-section">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Choose Odds</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <select id="inputState" required class="form-control" required name="odds">
                                                    <option value="">Select</option>
                                                    <option>1:15</option>
                                                    <option>1:25</option>
                                                    <option>1:200</option>
                                                    <option>1:500</option>
                                                    <option>1:1000</option>
                                                    <option>1:5000</option>
                                                    <option>1:10,000</option>
                                                    <option>1:20,000</option>
                                                </select>
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text o_addons" id="basic-addon1">Select</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Product Provided By</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <select id="inputState" required class="form-control" required name="provide_by">
                                                    <option value="">Select</option>
                                                    <option value="freethings">Freethings </option>
                                                    <option value="store owner">Store Owner</option>
                                                </select>
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text pr_addons" id="basic-addon1">Select</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Flow Template</label>
                                        <div class="col-sm-9">
                                            <div class="input-group"> 
                                                <select id="flow_template" required class="form-control flow_template" required name="template">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (count($flows) > 0) {
                                                        foreach ($flows as $flow) {
                                                            echo '<option data-questions="' . $flow['questions'] . '" value="' . $flow['flow_id'] . '">' . $flow['flow_name'] . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text t_addons" id="basic-addon1">Select</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row g-mb-25">
                                        <label for="lgFormGroupInput1" class="col-sm-3 col-form-label col-form-label-lg">Data to be captured after win</label>
                                        <div class="col-sm-9">
                                            <div>
                                                <input name="data" class="datasetFlow" id="tags" value="<?= $data_capture; ?>"  required/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer justify-content-center">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info">Submit</button>
                            <span class="message"></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal inmodal fade text-left" id="add-location" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Locations</h4>
                </div>
                <form action="" method="post" id="add-locations">

                    <div class="modal-body">
                        <div class="message"></div>
                        <div class="row flex-row flex-nowrap mt-4">


                            <div class="col-xl-12 col-md-12">
                                <div class="form-group row g-mb-25">
                                    <label for="lgFormGroupInput1" class="col-sm-4 col-form-label col-form-label-lg">Location Name</label>
                                    <div class="col-sm-8">
                                        <select class="form-control locselectd" >
                                            <?php
                                            foreach ($location as $key => $value) {
                                                echo '<option value="' . $value->country . '" code="' . $value->code . '" symbol="' . $value->symbol . '">' . $value->country . ' ( ' . $value->code . '-' . $value->symbol . ' ) ' . '</option>';
                                            }
                                            ?>
                                        </select>

                                    </div>
                                </div>
                                <input type="hidden" name="location" >
                                <input type="hidden" name="currency" >

                                <input type="hidden" name="symbol" >

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer justify-content-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info">Add Location</button>
                    </div>
            </div>
        </div>

        </form>
    </div>
</div>
</div>
<script>
        /*Dataset based on flow template*/
        $(document).on("change", ".flow_template", function (e) {
            var questions = $(this).find(':selected').data('questions');
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: baseurl + 'get-datasset',
                dataType: 'json',
                data: 'question_ids=' + questions,
                success: function (response) {
                    if (response.success) {
                        $('#tags').val(response.data);
                        $('.datasetFlow').tagsInput('refresh');
                    } else {
                        $('#tags').html('Last name,Phone number,First Name');
                    }
                }
            });
        });
</script>