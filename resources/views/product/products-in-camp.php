<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title mb-4">Campaigns</h3>
                            <a href="<?= url('/campaign') ?>" class="btn btn-info"> Back to campaigns</a>
                            <p>
                            <div class="div">
                                <div class="badge badge-success badge-pill"><?= ($campaign_detail->deleted_at) ? "Previous" : 'Running Period'; ?></div>
                            </div>							
                            </p>
                            <div class="div">
                                <b>Start date:</b> <?= date('M j, Y', strtotime($campaign_detail->created_date)); ?>
                                <?= ($campaign_detail->deleted_at) ? " <b>End Date:</b> " . date('M j, Y', strtotime($campaign_detail->deleted_at)) : '' ?>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane show active" id="currentcamp">
                                    <div class="row">
                                        <?php
                                        if (isset($product_details) && count($product_details) > 0) {
                                            foreach ($product_details as $product) {
                                                if (!empty($product->productdetails->attach)) {
                                                    $productImages = explode(',', $product->productdetails->attach);
                                                }
                                                ?>

                                                <div class="col-sm-6 col-lg-6 col-xl-3">
                                                    <div class="single-product-item mb-30">
                                                        <div class="product-card12">
<!--                                                            <a class="product-thumb" href="product-details.html"><img src="<?= url('/resources/assets/uploads/' . $productImages[0]) ?>" alt="Product"></a>-->
                                                            <?php 
                                                            if(!empty($productImages)){
                                                                echo '<div class="owl-carousel owl-theme full-width">';
                                                                foreach($productImages as $image){
                                                                    echo '<div class="item">
                                                                    <img src="'.url('/resources/assets/uploads/' . $image).'" alt="product image" />
                                                                </div>';
                                                                }
                                                                echo '</div>';
                                                            }
                                                            ?>
                                                            <!-- Product -->
                                                            <h3 class="product font-17 mb-15 mt-20"><?= $product->productdetails->name ?></h3>
                                                            <div class="d-flex justify-content-between align-items-center">
                                                                <div class="div">
                                                                    <div class="badge badge-success badge-pill">Ordered 250 (<?= $product->productdetails->quantity ?>)</div>
                                                                </div>
                                                                <div class="div">
                                                                    <div class="badge badge-success badge-pill">Won <?= $product->winning_freq ?></div>
                                                                </div>
                                                            </div>
                                                            <?php if(isset($product->coupon_image)) {  ?>
                                                                <div class="d-flex justify-content-between align-items-center mt-3">
                                                                        <div class="div">
                                                                            <div data-src="<?= url('/resources/assets/uploads/' . $product->coupon_image) ?>" class="badge badge-success badge-pill pop" style="cursor: pointer;">Coupons : Image</div>
                                                                        </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php }
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
    </div>
    
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" data-dismiss="modal">
    <div class="modal-content"  >              
      <div class="modal-body">
      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>
<script>
$(function() {
    $('.pop').on('click', function() {
            $('.imagepreview').attr('src', $(this).attr('data-src'));
            $('#imagemodal').modal('show');   
    });		
});
</script>