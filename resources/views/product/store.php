<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="#">
        <title>FreeThngs Store</title>
        <!-- Bootstrap core CSS -->
        <link href="<?= url('/') ?>/resources/assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= url('/') ?>/resources/assets/css/material-design-iconic-font.min.css" rel="stylesheet">


        <!-- Custom styles for this template -->

        <link href="https://fonts.googleapis.com/css2?family=Comic+Neue:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <link href="<?= url('/') ?>/resources/assets/css/custom.css" rel="stylesheet">
        <style>
            .winning-item-box {
                border: 0px solid #ccc;
                background: #fff;
                border-radius: 8px;
            }
            .winning-item-details h3{
                font-size: 20px;
                font-weight: 600;
            }
            .faq-wrapper h5{
                margin: 35px 0 11px 0;
                font-size: 25px;
                font-weight: bold;
            }
            .faq-wrapper p{
                font-size: 16px;
                color: #656565;
            }
            .faq-wrapper{
                border-bottom: 1px solid #f2f2f2;
                margin: 0 0 26px 0;
                padding: 0 0 8px 0;
            }

            /* product carousel */
            #product-img-car .carousel-item{
                text-align:center;
            }
            #product-img-car .carousel-item img{
                width:100%;
            }
            .winning-item-details{
                margin: 20px 0 0 0;
            }
            .winning-item-details h3 {
                font-size: 20px;
                font-weight: 600;
                min-height: 51px;
            }
            #product-img-car .carousel-control-next-icon {
                background-image: url(<?= url('/') ?>/resources/assets/images/right.png);
            }
            #product-img-car .carousel-control-prev-icon {
                background-image: url(<?= url('/') ?>/resources/assets/images/left.png);
            }
            #product-img-car .carousel-control-next-icon, #product-img-car .carousel-control-prev-icon {
                width: 20px;
                height: 35px;
            }
            /* product carousel */

            /* Right Side bar Toggel */
            .right-box{
                float:right;
                overflow: hidden;
                background: #f0e68c;
                display:none;
            }
            /* Add padding and border to inner content
            for better animation effect */
            .right-box-inner{
                width: 400px;
                padding: 10px;
                border: 1px solid #a29415;
            }
            /* Right Side bar Toggel */
			.logosubline{
				display: block;
    font-size: 12px;
    color: #fff;
    font-style: italic;
			}
			.chat-msg.system {
    background: #f1562a87;
    padding: 8px;
    border-radius: 25px;
    width: 80%;
    margin-top: 10px;
    margin-bottom: 10px;
	color:#fff;
			}
			.chat-msg.self {
    background: #F1562A;
    padding: 8px;
    width: 80%;
    border-radius: 25px;
	float:left;
	color:#fff;
	margin-top: 10px;
}
        </style>
        
    </head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-orange">
            <div class="container">
                <!-- Brand -->
                <a class="navbar-brand" href="/">
                    <img src="<?= url('/') ?>/resources/assets/images/store-logo.png" class="navbar-brand-img" alt="">
                    <span class="logosubline">Brought to you by <?= (isset($settings->store_name)) ? $settings->store_name :'Missing company Name' ?></span>
                </a>
                <!-- Collapse -->
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <!-- Navigation -->
                    <div class="ml-auto">
                    </div>
                    <?php
                    if (isset($settings->store_url)) {
                        if (filter_var($settings->store_url, FILTER_VALIDATE_URL)) {
                            $storeURL = $settings->store_url;
                        } else {
                            $storeURL = 'http://' . $settings->store_url;
                        }
                    } else {
                        $storeURL = "#";
                    }
                    ?>
                    <a class="navbar-btn btn btn-md btn-outline-primary lift ml-auto" href="<?= $storeURL ?>" target="_blank">
                        Go to <?= (isset($settings->store_name)) ? $settings->store_name : '' ?> Site
                    </a>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row mt-3 mb-3 p-0">
                <div class="col-md-12">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php
                            if (!empty($settings->store_promo_slider)) {
                                $storeImages = explode(',', $settings->store_promo_slider);
                                if (!empty($storeImages)) {
                                    $active = ' active';
                                    foreach ($storeImages as $sImage) {
                                        echo '<div class="carousel-item' . $active . '">
                                                <img class="d-block w-100" src="' . url('/resources/assets/uploads/' . $sImage) . '" alt="Store image">
                                            </div>';
                                        $active = false;
                                    }
                                }
                            } else {
                                echo '<div class="carousel-item active">
                                        <img class="d-block w-100" src="' . url('/') . '/resources/assets/uploads/16043387786.png" alt="First slide">
                                      </div>
                                      <div class="carousel-item">
                                        <img class="d-block w-100" src="' . url('/') . '/resources/assets/uploads/16043387787.png" alt="First slide">
                                      </div>
                                      <div class="carousel-item">
                                        <img class="d-block w-100" src="' . url('/') . '/resources/assets/uploads/16043387798.png" alt="First slide">
                                      </div>';
                            }
                            ?>
                        </div>
                        <?php if (!empty($storeImages) && count($storeImages) > 1) { ?>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-md-12">
                    <div class="card flex-md-row mb-4">
                        <div class="card-body text-center">
                            
                            <div class="mb-1 winning-num">You have 3 chances to win.</div>
                            <div class="mb-1 winning-num">Each chance you get 3 odd numbers.</div>
                            <div class="mb-1 winning-num">Your odd numbers. Use widget below.</div>
                            <p class="card-text d-block mb-auto mt-3 code-tags">
                                <?php
                                if (count($odd_codes) > 0) {
                                    $i = 1;
                                    foreach ($odd_codes as $code) {
                                        echo '<span><small id="code' . $i . '">' . $code . '</small><sup><a href="javascript:void(0);" data-original-title="Copy to clipboard" data-toggle="tooltip" data-placement="top" class="fa fa-copy copyClip" id="copyCode' . $i . '" data-id="' . $code . '">Copy</a></sup></span>';
                                        $i++;
                                        if ($i == 4)
                                            break;
                                    }
                                }
                                ?>
                                <a href="javascript:void(0);" onClick="addtext();"><i class="fi-xwsuxl-reload"></i></a>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="pb-3 mb-4 text-center">
                        You could win following items
                    </h3>
                </div>
            </div>
        </div>
        <section style="background:#F6F6F6;">
            <main role="main" class="container" >
                <div class="row pt-5">
                    <?php
                    if (isset($product_details) && count($product_details) > 0) {
                        foreach ($product_details as $product) {
                            if (isset($product->productdetails)) {
                                if (!empty($product->productdetails->attach)) {
                                    $productImages = explode(',', $product->productdetails->attach);
                                }
                                ?>

                                <div class="col-md-4 mb-4">
                                    <div class="winning-item-box p-4">
                                        <div class="winning-image text-center">
                                            <div id="product-img-car" class="carousel slide" data-ride="carousel">
                                                <div class="carousel-inner">
                                                    <?php
                                                    if (!empty($productImages)) {
                                                        $active = ' active';
                                                        foreach ($productImages as $image) {
                                                            echo '<div class="carousel-item' . $active . '">
                                                                <a class="user-to-view-product-images" href="javascript:void(0);" id="'.$product->id.'">
                                                            <img class="d-inline" src="' . url('/resources/assets/uploads/' . $image) . '" alt="First product">
                                                                </a>
                                                        </div>';
                                                            $active = false;
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                                <?php if (!empty($productImages) && count($productImages) > 1) { ?>
                                                    <a class="carousel-control-prev" href="#product-img-car" role="button" data-slide="prev">
                                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="carousel-control-next" href="#product-img-car" role="button" data-slide="next">
                                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                <?php } ?>

                                            </div>
                                        </div>
                                        <div class="winning-item-details">
                                            <h3 class="product font-17 mb-15 mt-20" ><a href="javascript:void(0);"  class="user-to-view-product-details" id="<?= $product->id ?>"><?= $product->productdetails->name ?></a></h3>
                                            
                                            <p class="winning-item-value">value : $<?= $product->productdetails->displayed_price ?></p>
                                            <p class="winning-item-value">Winning Chances : <?= $product->winning_freq ?> of <?= $product->winning ?></p>
                                            <p class="winning-item-value">Winning left : 5 of <?= $product->today ?></p>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                    }
                    ?>
                </div><!-- /.row -->

            </main><!-- /.container -->
        </section>
        <section style="background:#fff;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center mt-5 pt-5">
                        <h2>Frequently Asked Questions</h2>
                        <p style="font-size:15px;">Aliquam a augue suscipit, luctus neque purus ipsum neque undo dolor primis<br> libero tempus, blandit a cursus varius at magna tempor</p>
                    </div>
                    <div class="col-md-12 text-left">
                        <div class="faq-wrapper">
                            <h5>
                                1. Getting started with Treko?
                            </h5>
                            <p>Maecenas gravida porttitor nunc, quis vehicula magna luctus tempor. Quisque vel laoreet turpis. Urna augue, viverra a augue eget, dictum tempor diam. Sed pulvinar consectetur nibh, vel imperdiet varius viverra. Laoreet augue ac massa lorem. Fusce eu pretium a blandit posuere ligula varius magna cursus non nulla vitae massa</p>
                        </div>
                        <div class="faq-wrapper">
                            <h5>
                                2.  Do you have a free trial?
                            </h5>
                            <p>Maecenas gravida porttitor nunc, quis vehicula magna luctus tempor. Quisque vel laoreet turpis. Urna augue, viverra a augue eget, dictum tempor diam. Sed pulvinar consectetur nibh, vel imperdiet varius viverra. Laoreet augue ac massa lorem. Fusce eu pretium a blandit posuere ligula varius magna cursus non nulla vitae massa</p>
                        </div>
                        <div class="faq-wrapper">
                            <h5>
                                3.  How to sign up for a FreeThngs account?
                            </h5>
                            <p>Maecenas gravida porttitor nunc, quis vehicula magna luctus tempor. Quisque vel laoreet turpis. Urna augue, viverra a augue eget, dictum tempor diam. Sed pulvinar consectetur nibh, vel imperdiet varius viverra. Laoreet augue ac massa lorem. Fusce eu pretium a blandit posuere ligula varius magna cursus non nulla vitae massa</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<input type="hidden" value="0" id="step"/>
<input type="hidden" value="0" id="endchat"/>
<input type="hidden" value="0" id="winningstep"/>
        <footer class="blog-footer">
            <p>&copy; <?= date('Y') ?> FreeThngs</p>
            <p class="social-icons">

                <?php
                if (isset($settings->facebook_link)) {
                    if (filter_var($settings->facebook_link, FILTER_VALIDATE_URL)) {
                        echo "<a href='" . $settings->facebook_link . "'><i class='fi-snsuxl-facebook'></i></a>";
                    } else {
                        echo "<a href='https://" . $settings->facebook_link . "'><i class='fi-snsuxl-facebook'></i></a>";
                    }
                }

                if (isset($settings->linked_link)) {
                    if (filter_var($settings->linked_link, FILTER_VALIDATE_URL)) {
                        echo "<a href='" . $settings->linked_link . "'><i class='fi-snsuxl-linkedin'></i></a>";
                    } else {
                        echo "<a href='https://" . $settings->linked_link . "'><i class='fi-snsuxl-linkedin'></i></a>";
                    }
                }

                if (isset($settings->twitter_link)) {
                    if (filter_var($settings->twitter_link, FILTER_VALIDATE_URL)) {
                        echo "<a href='" . $settings->twitter_link . "'><i class='fi-snsuxl-twitter'></i></a>";
                    } else {
                        echo "<a href='https://" . $settings->twitter_link . "'><i class='fi-snsuxl-twitter'></i></a>";
                    }
                }

                if (isset($settings->instagram_link)) {
                    if (filter_var($settings->instagram_link, FILTER_VALIDATE_URL)) {
                        echo "<a href='" . $settings->instagram_link . "'><i class='fi-snsuxl-instagram'></i></a>";
                    } else {
                        echo "<a href='https://" . $settings->instagram_link . "'><i class='fi-snsuxl-instagram'></i></a>";
                    }
                }
                ?>
            </p>
            <p>
                <a href="#" class="text-white">Back to top</a>
            </p>
        </footer>
        <div id="chat-circle" class="btn btn-raised">
    
	<img src="<?= url('/') ?>/resources/assets/images/chat-logo.png" class="" alt="" style="width:100%;">

</div>

<div class="chat-box">
    <div class="chat-box-header">
        <span class="chat-box-toggle"><i class="zmdi zmdi-close-circle-o"></i></span>
        Hi, Welcome to FreeThings.Shop by Hobbima
        <div class="mt-4 chat-box-imgs d-flex justify-content-center">

            <div class="item"><img src="<?= url('/') ?>/resources/assets/images/2.png" class="" alt="" style="width:100%;"></div>
            <div class="item"><img src="<?= url('/') ?>/resources/assets/images/1.png" class="" alt="" style="width:100%;"></div>
            <div class="item"><img src="<?= url('/') ?>/resources/assets/images/1.png" class="" alt="" style="width:100%;"></div>
        </div>
        <p class="mt-3 mb-0 text-center" style="color:#fff;"><a href="#" class="morelink">More</a></p>
        <p class="mt-2 mb-2 text-center" style="color:#fff;">Here are your odd numbers:</p>

        <div class="d-flex chat-coupons-code justify-content-center mt-0">
		<?php
		    if (count($odd_codes) > 0) {
                                    $i = 1;
                                    foreach ($odd_codes as $code) {
										echo '<div class="item"><h5 id="codes' . $i . '"><span id="codewidget' . $i . '">'.$code.'</span><sup style="top: -0.9em;"><a href="javascript:void(0);" data-original-title="Copy to clipboard" data-toggle="tooltip" data-placement="top" class="fa fa-copy copyClip" id="copyCodewidget' . $i . '" data-id="' . $code . '">Copy</a></sup></h5></div>';
                                        //echo '<span><small id="code' . $i . '">' . $code . '</small><sup><a href="javascript:void(0);" data-original-title="Copy to clipboard" data-toggle="tooltip" data-placement="top" class="fa fa-copy copyClip" id="copyCode' . $i . '" data-id="' . $code . '">Copy</a></sup></span>';
                                        $i++;
                                        if ($i == 4)
                                            break;
                                    }
                                }
		?>
            <!--<div class="item"><h5>FFRMYT</h5></div>
            <div class="item"><h5>FFRMYT</h5></div>
            <div class="item"><h5>FFRMY</h5></div> -->
            <div class=""><h5 class="refresh-icon"><a href="#null" onclick="addtextwidget();"><i class="zmdi zmdi-refresh-alt"></i></a></h5></div>
        </div>
        <p class="mt-2 text-center" style="color:#fff;">Text WIN to get started</p>
    </div>
    <div class="chat-box-body">
        <div class="chat-box-overlay">   
        </div>
        <div class="chat-logs">
            <div class="chat-instructions">
                <p class="You have 3 chances to win anyone of these items"></p>
                <div class="card card-body" style="width:100%; margin:0 auto;">					
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <form>
                                <div class="form-group">
								<p style="display:none;" id="verificationtext">Please enter verification code that sent to your phone number.</p>
                                    <input type="text" class="form-control" id="phonenumber" placeholder="Enter your phone number to get started">
                                    <input type="text" class="form-control" id="verificationcode" placeholder="Enter Code" style="display:none;">
									<input type="text" class="form-control" id="firstname" placeholder="First Name" style="display:none;">
									<input type="hidden" value="0" id="isverify"/>
									<input type="hidden" value="0" id="isverifyfirstname"/>
									<span id="changenumber" style="cursor: pointer;text-decoration: underline;color: blue;font-size: 12px;display:none;">Change Mobile Number</span>
									<span id="resendcode" style="cursor: pointer;text-decoration: underline;color: blue;font-size: 12px;float: right;margin-top: 5px;display:none">Resend Code</span>
                                    
                                </div>
                                <button type="button" class="btn btn-info mr-2" id="sbtmtbutton">Submit</button>
                                <button type="button" class="btn btn-info mr-2" id="sbtmtverification" style="display:none;">Verifiy</button>
                                <button type="button" class="btn btn-info mr-2" id="sbtmfirstname" style="display:none;">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div><!--chat-log -->
    </div>
    <div class="chat-input">      
        <form>
            <input type="text" id="chat-input" placeholder="Send a message..."/>
            <button type="submit" class="chat-submit" id="chat-submit"><i class="material-icons">send</i></button>
        </form>      
    </div>
</div>
        <!-- Side bar Toggle -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <script src="http://code.jquery.com/jquery-3.1.0.js"></script>

        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
        <script type="text/javascript" src="<?= url('/') ?>/resources/custom/scripts.js?v=<?= env('FILE_VERSION') ?>"></script>
        <script>baseurl = '<?= url('/') ?>/';
            productImg = 'resources/assets/uploads/';
        </script>
        <script src="<?= url('/') ?>/resources/assets/js/jquery.min.js?v=<?= env('FILE_VERSION') ?>"></script>
        <script src="<?= url('/') ?>/resources/assets/js/bootstrap.min.js"></script>
        <script src="<?= url('/') ?>/resources/assets/js/mask.js"></script>
        <script defer src="https://friconix.com/cdn/friconix.js"></script>
        <script defer src="https://unpkg.com/@popperjs/core@2.6.0/dist/umd/popper.min.js"></script>
        <script>
                                    $(document).ready(function () {
										$("#chat-input").attr("disabled",true);
										$("#step").val(0);
										$("#endchat").val(0);
										$("#isverifyfirstname").val(0);
                                        $(".slide-toggle").click(function () {
                                            $(".box").animate({
                                                width: "toggle"
                                            });
                                        });

                                        $('a.copyClip').on('click', function (e) {
                                            $('.copyClip').attr('data-original-title', 'Copy to clipboard');
                                            var t = $(this);
                                            copyToClipboard(t.data('id'));
                                            t.attr('data-original-title', 'Copied').tooltip('show');
                                        });
                                        
                        

                                    });

                                    //Copy URL
                                    function copyToClipboard(code) {
                                        console.log('Copied', code);
                                        //window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
                                        if (window.clipboardData && window.clipboardData.setData) {
                                            // IE specific code path to prevent textarea being shown while dialog is visible.
                                            return clipboardData.setData("Text", code);

                                        } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
                                            var textarea = document.createElement("textarea");
                                            textarea.textContent = code;
                                            textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
                                            document.body.appendChild(textarea);
                                            textarea.select();
                                            try {
                                                return document.execCommand("copy");  // Security exception may be thrown by some browsers.
                                            } catch (ex) {
                                                console.warn("Copy to clipboard failed.", ex);
                                                return false;
                                            } finally {
                                                document.body.removeChild(textarea);
                                            }
                                        }
                                    }
                                    function addtext() {
                                        $randomword = <?php echo json_encode($odd_codes); ?>;
                                        $('#code1').text($randomword[getRandomInt(0, 15)]);
                                        $('#copyCode1').attr('data-id', $('#code1').text());
                                        $('#code2').text($randomword[getRandomInt(0, 15)]);
                                        $('#copyCode2').attr('data-id', $('#code2').text());
                                        $('#code3').text($randomword[getRandomInt(0, 15)]);
                                        $('#copyCode3').attr('data-id', $('#code3').text());
                                    }
									function addtextwidget() {
                                        $randomword = <?php echo json_encode($odd_codes); ?>;
                                        $('#codewidget1').text($randomword[getRandomInt(0, 15)]);
                                        $('#copyCodewidget1').attr('data-id', $('#codewidget1').text());
                                        $('#codewidget2').text($randomword[getRandomInt(0, 15)]);
                                        $('#copyCodewidget2').attr('data-id', $('#codewidget2').text());
                                        $('#codewidget3').text($randomword[getRandomInt(0, 15)]);
                                        $('#copyCodewidget3').attr('data-id', $('#codewidget3').text());
                                    }
                                    function getRandomInt(min, max) {
                                        return Math.floor(Math.random() * (max - min)) + min;
                                    }
        </script>
		<script>
		function selectoption(str){
			
			$("#chat-input").val(str);
			$("#chat-submit").trigger('click');
		}
    $(function () {
		// $(".chat-logs").append("<ul class='options'><li onclick='selectoption(\"Option A\")'>Option A</li><li onclick='selectoption(\"Option B\")'>Option B</li><li onclick='selectoption(\"Option C\")'>Option C</li><li onclick='selectoption(\"Option D\")'>Option D</li></ul>");
        var INDEX = 0;
        $("#chat-submit").click(function (e) {
			
            e.preventDefault();
            var msg = $("#chat-input").val();
            var step = eval($("#step").val());
			var phonenumber = $("#phonenumber").val();
            if (msg.trim() == '') {
                return false;
            }
			var endchat = $("#endchat").val();
			if(endchat == 1){
				
				$("#chat-input").val('');
				return false;
			}
			
            generate_message(msg, 'self');
            var buttons = [
                {
                    name: 'Existing User',
                    value: 'existing'
                },
                {
                    name: 'New User',
                    value: 'new'
                }
            ];
           var winningstep = $("#winningstep").val();
			
			if(winningstep == 3){
				
				return;
			}
			
			if($("#isverify").val() == 0){
				
				 setTimeout(function () {
				 msgsystem = "Please verify your phone number first.";
                generate_messagesystem(msgsystem, 'system');
            }, 1000);
			}else if($("#isverifyfirstname").val() != 1){
				 setTimeout(function () {
				savefirstname(msg,phonenumber);
				$("#isverifyfirstname").val(1)
            }, 1000);
				
			}else{
				/*if(step == 0 && msg.toUpperCase() != 'WIN'){
					
					 msgsystem = "Please send WIN to get started.";
					setTimeout(function () {
				
                generate_messagesystem(msgsystem, 'system');
            }, 1000);
					return;
				}else if(step == 0 && msg.toUpperCase() == 'WIN'){
					
				msgsystem = "Please send odd code.";
				$("#step").val(eval(step+1));
                setTimeout(function () {
				 
                generate_messagesystem(msgsystem, 'system');
            }, 1000);
				return;
				} */
				var step = $("#step").val();
				$.ajax({
           type: "POST",
           url: '<?php echo url('/'); ?>/startgame',
           data: {msg:msg,step:step,phone:phonenumber,"_token": "<?php echo csrf_token(); ?>"},
           success: function( msgsystem ) {
              if(msgsystem == 'notmatch'){
			 
			 //generate_messagesystem("Sorry, we could not recognize the odd code.", 'system');
			 setTimeout(function () {
				
                generate_messagesystem("Sorry, we could not recognize the odd code.", 'system');
            }, 1000);
			  }else if(msgsystem == 'notready'){
				  var winningstep = $("#winningstep").val();
				  
				  if(winningstep == 0){
				  
					//generate_messagesystem("Sorry, this is not a winning code, you have 2 more chances, please try again. Enter the odd code.", 'system');
					setTimeout(function () {
				
                generate_messagesystem("Sorry, this is not a winning code, you have 2 more chances, please try again. Enter the odd code.", 'system');
            }, 1000);
					$("#winningstep").val(1);
				  
				  }else if(winningstep == 1){
					  
					//generate_messagesystem("Sorry, this is not a winning code, you have 1 more chance, please try again. Enter the odd code.", 'system');
					setTimeout(function () {
				
                generate_messagesystem("Sorry, this is not a winning code, you have 1 more chance, please try again. Enter the odd code.", 'system');
            }, 1000);
					
				    $("#winningstep").val(2);
				  }else{
					  
					 // generate_messagesystem("Sorry, this is not a winning code.", 'system');
					 setTimeout(function () {
				
                generate_messagesystem("Sorry, this is not a winning code.", 'system');
            }, 1000);
					  $("#winningstep").val(3);
				  }
			  }else{
				  var step = eval($("#step").val());
				  var step = eval(step+1);
				  $("#step").val(step);
				 
				  if(step == 2){
				   //console.log(msgsystem);
				    var step = eval(step+1);
					$("#step").val(step);
				   var msgsystem = JSON.parse(msgsystem);
				   setTimeout(function () {
				   generate_messagesystem("Thank you, one moment while we check.", 'system');
				   }, 1000);
				   setTimeout(function () {
					   
				   generate_messagesystem("Congratulation! You Won "+msgsystem.name+"", 'system');
				   }, 3000); 
				   
				   
				   //generate_message('<p style="visibility:hidden">test test test</p> ', 'self');
				   setTimeout(function () {
				   generate_messagesystem(msgsystem.questionstr, 'system');
				   }, 4000);
				   //$("#step").val(0);
				  }else{
				   //console.log(msgsystem);
				   var msgsystem = JSON.parse(msgsystem);
				   setTimeout(function () {
				   generate_messagesystem(msgsystem.questionstr, 'system');
				   }, 1000);
				   if (typeof msgsystem.end !== 'undefined') {
						$("#endchat").val(1);
					}
				   //$("#step").val(0);
				  }
			 }
           }
       });
				
			}
			
        })

        function generate_message(msg, type) {
            INDEX++;
            var str = "";
            str += "<div id='cm-msg-" + INDEX + "' class=\"chat-msg " + type + "\">";
           /* str += "          <span class=\"msg-avatar\">";
            str += "            <img src=\"https:\/\/image.crisp.im\/avatar\/operator\/196af8cc-f6ad-4ef7-afd1-c45d5231387c\/240\/?1483361727745\">";
            str += "          <\/span>"; */
            str += "          <div class=\"cm-msg-text\">";
            str += msg;
            str += "          <\/div>";
            str += "        <\/div>";
            $(".chat-logs").append(str);
            $("#cm-msg-" + INDEX).hide().fadeIn(300);
            if (type == 'self') {
                $("#chat-input").val('');
            }
            $(".chat-logs").stop().animate({scrollTop: $(".chat-logs")[0].scrollHeight}, 1000);
        }
		
		function generate_messagesystem(msg, type) {
            INDEX++;
            var str = "";
            str += "<div id='cm-msg-" + INDEX + "' class=\"chat-msg " + type + "\" style='float:right;text-align:right;font-size:14px;'>";
           /* str += "          <span class=\"msg-avatar\">";
            str += "            <img src=\"https:\/\/image.crisp.im\/avatar\/operator\/196af8cc-f6ad-4ef7-afd1-c45d5231387c\/240\/?1483361727745\">";
            str += "          <\/span>"; */
            str += "          <div class=\"cm-msg-text\">";
            str += msg;
            str += "          <\/div>";
            str += "        <\/div>";
            $(".chat-logs").append(str);
            $("#cm-msg-" + INDEX).hide().fadeIn(300);
            if (type == 'self') {
                $("#chat-input").val('');
            }
            $(".chat-logs").stop().animate({scrollTop: $(".chat-logs")[0].scrollHeight}, 1000);
        }

        function generate_button_message(msg, buttons) {
            /* Buttons should be object array 
             [
             {
             name: 'Existing User',
             value: 'existing'
             },
             {
             name: 'New User',
             value: 'new'
             }
             ]
             */
            INDEX++;
            var btn_obj = buttons.map(function (button) {
                return  "              <li class=\"button\"><a href=\"javascript:;\" class=\"btn btn-info chat-btn\" chat-value=\"" + button.value + "\">" + button.name + "<\/a><\/li>";
            }).join('');
            var str = "";
            str += "<div id='cm-msg-" + INDEX + "' class=\"chat-msg user\">";
            str += "          <span class=\"msg-avatar\">";
            str += "            <img src=\"https:\/\/image.crisp.im\/avatar\/operator\/196af8cc-f6ad-4ef7-afd1-c45d5231387c\/240\/?1483361727745\">";
            str += "          <\/span>";
            str += "          <div class=\"cm-msg-text\">";
            str += msg;
            str += "          <\/div>";
            str += "          <div class=\"cm-msg-button\">";
            str += "            <ul>";
            str += btn_obj;
            str += "            <\/ul>";
            str += "          <\/div>";
            str += "        <\/div>";
            $(".chat-logs").append(str);
            $("#cm-msg-" + INDEX).hide().fadeIn(300);
            $(".chat-logs").stop().animate({scrollTop: $(".chat-logs")[0].scrollHeight}, 1000);
            $("#chat-input").attr("disabled", true);
        }

        $(document).delegate(".chat-btn", "click", function () {
            var value = $(this).attr("chat-value");
            var name = $(this).html();
            $("#chat-input").attr("disabled", false);
            //generate_message(name, 'self');
        });

        $("#chat-circle").click(function () {
            $("#chat-circle").toggle('scale');
            $(".chat-box").toggle('scale');
        });

        $(".chat-box-toggle").click(function () {
            $("#chat-circle").toggle('scale');
            $(".chat-box").toggle('scale');
        });
		
		$("#verificationcode").val('');
		$("#phonenumber").val('');
		$("#sbtmtbutton").click(function (){
			$("#verificationtext").hide();
			var phonenumber = $("#phonenumber").val();
			
			if(phonenumber != ''){
				
				$.ajax({
           type: "POST",
           url: '<?php echo url('/'); ?>/verifyphone',
           data: {phonenumber:phonenumber,"_token": "<?php echo csrf_token(); ?>"},
           success: function( msg ) {
			 
              if(msg == 'yes'){
			  $("#sbtmtverification").show();
			  $("#sbtmtbutton").hide();
			  $("#phonenumber").hide();
			  $("#verificationcode").show();
			  $("#verificationtext").show();
			  $("#changenumber").show();
			  $("#resendcode").show();
			  }else{
				  $("#verificationtext").html("You have entered invalid number, Please enter your mobile number.");
				  $("#verificationtext").show();
				  //alert("You have entered invalid number, Please enter your mobile number.");
			  }
           }
       });
				
			}
			
			
		});
		
		
		$("#resendcode").click(function (){
			
			var phonenumber = $("#phonenumber").val();
			
			if(phonenumber != ''){
				
				$.ajax({
           type: "POST",
           url: '<?php echo url('/'); ?>/verifyphone',
           data: {phonenumber:phonenumber,"_token": "<?php echo csrf_token(); ?>"},
           success: function( msg ) {
			  
              if(msg == 'yes'){
				  alert("Verification code is sent again.");
			  $("#sbtmtverification").show();
			  $("#sbtmtbutton").hide();
			  $("#phonenumber").hide();
			  $("#verificationcode").show();
			  $("#verificationtext").show();
			  $("#changenumber").show();
			  $("#resendcode").show();
			  }else{
				  $("#verificationtext").html("You have entered invalid number, Please enter your mobile number.");
				  $("#verificationtext").show();
				  //alert("You have entered invalid number, Please enter your mobile number.");
			  }
           }
       });
				
			}
			
			
		});
		
		$("#changenumber").click(function (){
			$("#sbtmtverification").hide();
			  $("#sbtmtbutton").show();
			  $("#phonenumber").show();
			  $("#verificationcode").hide();
			  $("#verificationtext").hide();
			  $("#changenumber").hide();
			  $("#resendcode").hide();
			
		});
		
		$("#sbtmtverification").click(function (){
			
			var verificationcode = $("#verificationcode").val();
			var phonenumber = $("#phonenumber").val();
			if(verificationcode != ''){
				
				$.ajax({
           type: "POST",
           url: '<?php echo url('/'); ?>/verificationcode',
           data: {verificationcode:verificationcode,phonenumber:phonenumber,"_token": "<?php echo csrf_token(); ?>"},
           success: function( msg ) {
              
			  if(msg == 'yes'){
				 
				// $("#verificationtext").html("Your phone number is verified! Please send WIN to get started.");
				 //$("#verificationtext").html("Your phone number is verified! Please enter first name.");
				 $("#verificationtext").html("Your phone number is verified!");
				 
				 $("#isverify").val(1);
				 
				 $("#verificationcode").hide();
				 
				  $("#sbtmtverification").hide();
				  //$("#sbtmfirstname").show();
				  //$("#firstname").show();
				  $("#chat-input").attr("disabled",false);
				  
				  $("#changenumber").hide();
			      
				  $("#resendcode").hide();
				  $("#firstname").val(1);
				setTimeout(function () {
				   generate_messagesystem("So we know how to address you, what is your first name, please?", 'system');
				   }, 2000);
			  }else if(msg == 'namenotrequired'){
				  var step =  $("#step").val();
					 $("#step").val(eval(step+1));
					 $("#isverifyfirstname").val(1);
					  $("#verificationtext").html("Your phone number is verified!");
				 
				 $("#isverify").val(1);
				 
				 $("#verificationcode").hide();
				 
				  $("#sbtmtverification").hide();
				  //$("#sbtmfirstname").show();
				  //$("#firstname").show();
				  $("#chat-input").attr("disabled",false);
				  
				  $("#changenumber").hide();
			      
				  $("#resendcode").hide();
				  $("#firstname").val(1);
					 setTimeout(function () {
					generate_messagesystem("What is the odd number ?", 'system');
				   }, 2000);
			  }else{
				  $("#verificationtext").html("Invalid code!");
				  //alert("Invalid code!");
				    $("#verificationtext").show();
			  
			  }
           }
       });
				
			}
			
			
		});
		
		
		
		$("#phonenumber").mask("999-999-9999");
		function savefirstname(firstname,phonenumber){
		
			
			if(firstname != ''){
				
				$.ajax({
           type: "POST",
           url: '<?php echo url('/'); ?>/savefirstname',
           data: {firstname:firstname,phonenumber:phonenumber,"_token": "<?php echo csrf_token(); ?>"},
           success: function( msg ) {
              
			  if(msg == 'yes'){
				 setTimeout(function () {
				   generate_messagesystem("Thank you "+firstname, 'system')
				   }, 1000);
				 setTimeout(function () {
					 var step =  $("#step").val();
					 $("#step").val(eval(step+1));
				generate_messagesystem("What is the odd number?", 'system')
				 }, 2000);
			  }else{
				  
			  
			  }
           }
       });
				
			}
	}
    });
	
	
</script>
<style>
ul.options{
	
	list-style: none;
}
ul.options li{
margin-bottom: 5px;
background: #ccc;

padding: 8px;
cursor: pointer;
}
#chat-circle {
    position: fixed;
    bottom: 50px;
    right: 50px;
    background: #f1562a;
    width: 100px;
    height: 100px;  
    border-radius: 50%;
    color: white;
    padding: 0;
    cursor: pointer;
    box-shadow: 0px 3px 16px 0px rgba(0, 0, 0, 0.6), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
    z-index: 999;
}
#chat-circle .icon_chat_alt{
    font-size: 30px;
}

.btn#my-btn {
    background: white;
    padding-top: 13px;
    padding-bottom: 12px;
    border-radius: 45px;
    padding-right: 40px;
    padding-left: 40px;
    color: #5865C3;
}
.refresh-icon a{
	color:#fff;
}
#chat-overlay {
    background: rgba(255,255,255,0.1);
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border-radius: 50%;
    display: none;
}


.chat-box {
    display:none;
    background: #efefef;
    position:fixed;
    right:30px;
    bottom:50px;  
    width:355px;
    max-width: 185vw;
    max-height:100vh;
    border-radius:5px;  
    /*   box-shadow: 0px 5px 35px 9px #464a92; */
    box-shadow: 0px 5px 35px 9px #ccc;
    z-index: 99;
}
.chat-box-toggle {
    float: right;
    margin-right: 0;
    cursor: pointer;
    margin: 0 0 0 26px;
}
.chat-box-header {
    background: #f1562a;
    height: auto;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    color: white;
    text-align: left;
    font-size: 20px;
    padding: 15px;
}
.chat-box-body {
    position: relative;  
    height:370px;  
    height:auto;
    border:1px solid #ccc;  
    overflow: hidden;
}
.chat-box-body:after {
    content: "";
    background-image: url('data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDIwMCAyMDAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTAgOCkiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+PGNpcmNsZSBzdHJva2U9IiMwMDAiIHN0cm9rZS13aWR0aD0iMS4yNSIgY3g9IjE3NiIgY3k9IjEyIiByPSI0Ii8+PHBhdGggZD0iTTIwLjUuNWwyMyAxMW0tMjkgODRsLTMuNzkgMTAuMzc3TTI3LjAzNyAxMzEuNGw1Ljg5OCAyLjIwMy0zLjQ2IDUuOTQ3IDYuMDcyIDIuMzkyLTMuOTMzIDUuNzU4bTEyOC43MzMgMzUuMzdsLjY5My05LjMxNiAxMC4yOTIuMDUyLjQxNi05LjIyMiA5LjI3NC4zMzJNLjUgNDguNXM2LjEzMSA2LjQxMyA2Ljg0NyAxNC44MDVjLjcxNSA4LjM5My0yLjUyIDE0LjgwNi0yLjUyIDE0LjgwNk0xMjQuNTU1IDkwcy03LjQ0NCAwLTEzLjY3IDYuMTkyYy02LjIyNyA2LjE5Mi00LjgzOCAxMi4wMTItNC44MzggMTIuMDEybTIuMjQgNjguNjI2cy00LjAyNi05LjAyNS0xOC4xNDUtOS4wMjUtMTguMTQ1IDUuNy0xOC4xNDUgNS43IiBzdHJva2U9IiMwMDAiIHN0cm9rZS13aWR0aD0iMS4yNSIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIi8+PHBhdGggZD0iTTg1LjcxNiAzNi4xNDZsNS4yNDMtOS41MjFoMTEuMDkzbDUuNDE2IDkuNTIxLTUuNDEgOS4xODVIOTAuOTUzbC01LjIzNy05LjE4NXptNjMuOTA5IDE1LjQ3OWgxMC43NXYxMC43NWgtMTAuNzV6IiBzdHJva2U9IiMwMDAiIHN0cm9rZS13aWR0aD0iMS4yNSIvPjxjaXJjbGUgZmlsbD0iIzAwMCIgY3g9IjcxLjUiIGN5PSI3LjUiIHI9IjEuNSIvPjxjaXJjbGUgZmlsbD0iIzAwMCIgY3g9IjE3MC41IiBjeT0iOTUuNSIgcj0iMS41Ii8+PGNpcmNsZSBmaWxsPSIjMDAwIiBjeD0iODEuNSIgY3k9IjEzNC41IiByPSIxLjUiLz48Y2lyY2xlIGZpbGw9IiMwMDAiIGN4PSIxMy41IiBjeT0iMjMuNSIgcj0iMS41Ii8+PHBhdGggZmlsbD0iIzAwMCIgZD0iTTkzIDcxaDN2M2gtM3ptMzMgODRoM3YzaC0zem0tODUgMThoM3YzaC0zeiIvPjxwYXRoIGQ9Ik0zOS4zODQgNTEuMTIybDUuNzU4LTQuNDU0IDYuNDUzIDQuMjA1LTIuMjk0IDcuMzYzaC03Ljc5bC0yLjEyNy03LjExNHpNMTMwLjE5NSA0LjAzbDEzLjgzIDUuMDYyLTEwLjA5IDcuMDQ4LTMuNzQtMTIuMTF6bS04MyA5NWwxNC44MyA1LjQyOS0xMC44MiA3LjU1Ny00LjAxLTEyLjk4N3pNNS4yMTMgMTYxLjQ5NWwxMS4zMjggMjAuODk3TDIuMjY1IDE4MGwyLjk0OC0xOC41MDV6IiBzdHJva2U9IiMwMDAiIHN0cm9rZS13aWR0aD0iMS4yNSIvPjxwYXRoIGQ9Ik0xNDkuMDUgMTI3LjQ2OHMtLjUxIDIuMTgzLjk5NSAzLjM2NmMxLjU2IDEuMjI2IDguNjQyLTEuODk1IDMuOTY3LTcuNzg1LTIuMzY3LTIuNDc3LTYuNS0zLjIyNi05LjMzIDAtNS4yMDggNS45MzYgMCAxNy41MSAxMS42MSAxMy43MyAxMi40NTgtNi4yNTcgNS42MzMtMjEuNjU2LTUuMDczLTIyLjY1NC02LjYwMi0uNjA2LTE0LjA0MyAxLjc1Ni0xNi4xNTcgMTAuMjY4LTEuNzE4IDYuOTIgMS41ODQgMTcuMzg3IDEyLjQ1IDIwLjQ3NiAxMC44NjYgMy4wOSAxOS4zMzEtNC4zMSAxOS4zMzEtNC4zMSIgc3Ryb2tlPSIjMDAwIiBzdHJva2Utd2lkdGg9IjEuMjUiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIvPjwvZz48L3N2Zz4=');
    opacity: 0.1;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    height:100%;
    position: absolute;
    z-index: -1;   
}

#chat-input {
    background: #f4f7f9;
    width:100%; 
    position:relative;
    height:47px;  
    padding-top:10px;
    padding-right:50px;
    padding-bottom:10px;
    padding-left:15px;
    border:none;
    resize:none;
    outline:none;
    border:1px solid #ccc;
    color:#888;
    border-top:none;
    border-bottom-right-radius:5px;
    border-bottom-left-radius:5px;
    overflow:hidden;  
}
.chat-input > form {
    margin-bottom: 0;
}
#chat-input::-webkit-input-placeholder { /* Chrome/Opera/Safari */
    color: #ccc;
}
#chat-input::-moz-placeholder { /* Firefox 19+ */
    color: #ccc;
}
#chat-input:-ms-input-placeholder { /* IE 10+ */
    color: #ccc;
}
#chat-input:-moz-placeholder { /* Firefox 18- */
    color: #ccc;
}
.chat-submit {  
        position: absolute;
    bottom: 3px;
    right: 10px;
    background: transparent;
    box-shadow: none;
    border: none;
    border-radius: 50%;
    color: #5A5EB9;
    width: 50px;
    height: 35px;
    font-size: 15px;
}
.chat-logs {
    padding:15px; 
    height:244px;
    overflow-y:scroll;
}

.chat-logs::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

.chat-logs::-webkit-scrollbar
{
    width: 5px;  
    background-color: #F5F5F5;
}

.chat-logs::-webkit-scrollbar-thumb
{
    background-color: #5A5EB9;
}
.chat-coupons-code{

}
.chat-coupons-code .item{
    margin: 0 3px;
    background: #fff;
    border-radius: 4px;
} 
.chat-coupons-code .item h5{
       margin: 0;
    padding: 8px 10px;
    font-size: 14px;
    color: #000;
} 
.chat-box-imgs .item {
    margin: 0 20px;
}
.morelink{
	    font-size: 15px;
    color: #fff;
}
</style>
    </body>
</html>
