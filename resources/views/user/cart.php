<?php $uploadAction = false; ?>
<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="checkout-process-wrapper">

                        <div class="row">
                            <div class="col-xl-6">
                                <div class="justify-content-between bd-highlight mb-3">

                                    <div class="bd-highlight">
                                        <ul class="nav nav-pills navtab-bg flex-column flex-sm-row">
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Select
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link active mb-3 pl-5 pr-5 text-center">
                                                    Cart
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link  mb-3 pl-5 pr-5 text-center">
                                                    Checkout
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Settings
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Done
                                                </a>
                                            </li>

                                        </ul>
                                    </div>


                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="bd-highlight">
                                    <div class="btn-group mb-2 show float-right">

                                        <a href="<?= url('/') ?>/all-product-list"><button type="button" class="btn btn-danger btn-info"  aria-expanded="true">Continue Shopping</button></a>

                                    </div>
                                    <div class="btn-group mb-2 mr-3 show float-right">

                                        <button type="button" class="btn btn-warning dropdown-toggle cart-button" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="true"><i class="zmdi zmdi-shopping-cart"></i><span class="cart-bubble"><?= $cartcount ?></span></button>
                                        <div class="dropdown-menu dropdown-menu-lg-right p-2 cart-bubble-prod-list" style="min-width:20rem;">
                                            <ul class="list-group cartname">
                                                <?php
                                                if (!empty($list)) {
                                                    foreach ($list as $key => $value) {
                                                        $path = Config::get('constants.product_img_path');
                                                        $imageArr = explode(',', $value->attach);
                                                        echo '<li class="list-group-item "><img src="' . url('/') . $path . $imageArr[0] . '" width="50"/> ' . $value->name . '</li>';
                                                    }
                                                } else {
                                                    echo '<li class="list-group-item">Cart is empty.</li>';
                                                }
                                                ?>


                                            </ul>
                                            <a href="<?= url('/') ?>/cart" class="btn btn-info">Continue to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content">


                            <div class="tab-pane show active" id="carttab">
                                <form enctype="multipart/form-data" method="post" id="checkoutform">
                                    <div class="row">
                                        <?php
                                        if ($success == false) {
                                            echo ' <div class="col-md-8">
                                       <div class="card box-margin">
                                          <div class="card-body">';
                                            echo "Cart id empty";
                                            echo '</div></div></div>';
                                        } else {
                                            ?>
                                            <div class="col-md-8">
                                                <div class="card box-margin">
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="">
                                                                    <div class="checkout-area mb-50">
                                                                        <h4 class="card-title mt-0 mb-3">My Cart Items</h4>

                                                                        <div class="table-responsive">

                                                                            <table class="table table-nowrap table-bordered table-centered mb-0">
                                                                                <thead class="thead-light">
                                                                                    <tr>
                                                                                        <th>Product</th>
                                                                                        <th>Winning<br>Frequency/Quantity</th>
                                                                                        <th>Price</th>
                                                                                        <th>Shipping</th>
                                                                                        <th>Charges <br>per winning</th>
                                                                                        <th></th>
                                                                                        <th>Today</th>
                                                                                        <th>Clear</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody class="text-center">
                                                                                    <?php
                                                                                    $total_charges = 0;
                                                                                    $uploadAction = url('/upload-image');
                                                                                    foreach ($list as $key => $value) {
                                                                                        $imageArr = explode(',', $value->attach);
                                                                                        $symbol = Config::get('constants.' . $value->currency);
                                                                                        $symbol = '<i class="' . $symbol . '" aria-hidden="true"></i>';
                                                                                        Session::put('use_symbol', $symbol);

                                                                                        $winning = $value->price + $value->shipping_cost;
                                                                                        echo '   <tr id="c_' . $value->product_id . '">
                                    <td class="text-left">
                                       <img src="' . url('/') . Config::get('constants.product_img_path') . $imageArr[0] . '" alt="contact-img" title="contact-img" class="round mr-3 product-thumb">
                                       <p class="m-0 d-inline-block align-middle font-16">
                                          <a href="javascript:;" id="' . $value->product_id . '" class="text-body user-to-view-product-details">' . ucwords($value->name) . '</a>
                                          <br>';
                                                                                        if ($value->odds != '' && $value->odds != '0') {
                                                                                            echo '<small class="mr-2"><b>Odds of winning:</b> <br>' . ucwords($value->odds) . ' </small>';
                                                                                        }

                                                                                        echo '  </p>';
                                                                                        echo '<input type="hidden" name="id[]" value="' . $value->product_id . '"/>';
                                                                                        if ($value->coupons == 1) {
                                                                                            echo '<div class="form-group mt-3">
                    <input type="hidden" name="coupon_image[]" id="hidden' . $key . '" value=""/>
                           <label>Please upload coupon image<br> for winners</label>
                           <input type="file" name="couponImage" class="form-control" id="couponImage' . $key . '">
                           <div class="input-group col-xs-12">
                              <span class="input-group-append">
                                 <button data-imageid="' . $key . '" class="file-upload-browse btn btn-info uploadCouponImage" type="button">Upload</button>
                              </span>
                           </div>
                        </div>';
                                                                                        }


                                                                                        echo ' 
                                    </td>
                                    <td>
                                       <input type="number" class="wwfreq" key="' . $key . '" name="winning_freq[]" min="1" value="' . $value->winning_freq . '" id="winning_freq_' . $key . '" class="form-control" placeholder="Qty" style="width: 90px;">
                                    </td>
                                    <td>
                                       <input type="hidden" name="price[]" value="' . $value->price . '"/>
                                       ' . $symbol . $value->price . '
                                    </td>
                                    <td>
                                     <input type="hidden" name="shipping_cost[]" id="shipping_cost_' . $key . '" value="' . $value->shipping_cost . '"/>
                                    
                                       ' . $symbol . $value->shipping_cost . '
                                    </td>
                                    <td>
                                        <input type="hidden" name="winning[]" id="winning_' . $key . '" value="' . $winning . '"/>
                                       ' . $symbol . $winning . '
                                    </td>
                                    <td class="text-left">
                                       <div class="custom-control custom-radio ">
                                       <input type="radio" id="Addtostore' . $key . '" name="storecheck' . $key . '" class="custom-control-input storecheck" key="' . $key . '" value="addtostore">
                                       <label class="custom-control-label" for="Addtostore' . $key . '">Add to store</label>
                                    </div>
                                    <div class="custom-control custom-radio">
                                       <input type="radio" checked id="Addtostore1' . $key . '" name="storecheck' . $key . '" class="custom-control-input storecheck"  key="' . $key . '"  value="buynow">
                                       <label class="custom-control-label" for="Addtostore1' . $key . '">Buy Now</label>
                                    </div>
                                    </td>
                                    <td>
                                    <input type="hidden"class="today_cal" name="today[]" id="today_' . $key . '" value="' . $winning . '"/>
                                      ' . $symbol . ' <span class="today_' . $key . '">' . $winning . '</span>
                                    </td>
                      
                                    <td>
                                       <div class="actions ml-3">
                                          <a href="javascript:;" class="action-item mr-2 user-to-view-product-details" data-toggle="tooltip" title="" data-original-title="Quick view" id="' . $value->product_id . '">
                                          <i class="fa fa-external-link"></i>
                                          </a>
                                          <a href="javascript:;" id="' . $value->product_id . '" class="action-item mr-2 delcartitem" data-toggle="tooltip" title="" data-original-title="Move to trash">
                                          <i class="fa fa-times"></i>
                                          </a>
                                       </div>
                                    </td>
                                 </tr>';
                                                                                        $total_charges += $winning;
                                                                                    }
                                                                                    echo '<tr class="consolidate">
                                             <td></td>
                                             <td></td>
                                             <td></td>
                                             <td></td>
                                             <td></td>
                                             <td>Total </td>
                                             <td class="distot">' . $total_charges . '</td>
                                             <td></td>
                                          </tr>';
                                                                                    ?>

                                                                                <input type="hidden" name="totalcharges" value="<?= $total_charges ?>">
                                                                                </tbody>
                                                                            </table>
                                                                            <input type="hidden" name="onetimecost" value="<?= $onetimecost ?>">
                                                                            <input type="hidden" class="totalusd" name="totalusd" value="<?= $onetimecost + $total_charges; ?>">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 order-md-2 box-margin">
                                                <div class="card box-margin">
                                                    <div class="card-body">
                                                        <h4 class="d-flex justify-content-between align-items-center mb-3">
                                                            <span class="card-title mb-0">Price Details</span>
                                                        </h4>

                                                        <ul class="list-group mb-3">
                                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                                <div>
                                                                    <h6 class="mb-0 font-14">1 Time setup Fees</h6>
                                                                    <p>30 Days Money Back Guarantee</p>
                                                                </div>
                                                                <span><?= Session::get('symbols'); ?><span class="font-weight-bold text-success one"><?= $onetimecost ?></span></span>
                                                            </li>
                                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                                <div>
                                                                    <h6 class="mb-0 font-14">Sales Tax</h6>
                                                                </div>
                                                                <span class="font-weight-bold text-success"></span>
                                                            </li>
                                                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                                <div>
                                                                    <h6 class="mb-0 font-14">Today's Charges </h6>
                                                                </div>

                                                                <span class="font-weight-bold  text-success"><?= Session::get('symbols'); ?><span class="  totalchargesdisplay two"><?= $total_charges ?></span></span>
                                                            </li>
                                                            <li class="list-group-item d-flex justify-content-between">
                                                                <span class="text-dark font-weight-bold">Total </span>

                                                                <span><?= Session::get('symbols'); ?><strong class="text-danger totalusddisplay"><?= $onetimecost + $total_charges; ?></strong></span>

                                                            </li>
                                                        </ul>

                                                        <div class="input-group">
                                                            <button type="submit" tabindex="-1" class="btn btn-info chkoutbtn">Continue To Checkout</button>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="card ">
                                                    <div class="card-body">
                                                        <div class="single-smart-card d-flex">
                                                            <div class="icon mr-3">
                                                                <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                            </div>
                                                            <div class="text">

                                                                <p>If you choose to BUY NOW, the cost of winning will remain the same. No charges at the time of winning.
                                                                </p></div>
                                                        </div>
                                                        <div class="single-smart-card d-flex">
                                                            <div class="icon mr-3">
                                                                <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                            </div>
                                                            <div class="text">

                                                                <p>If you choose to ADD TO STORE, we will charge your credit card on file at the time of winning and issue an invoice. 
                                                                </p></div>
                                                        </div>
                                                        <div class="single-smart-card d-flex">
                                                            <div class="icon mr-3">
                                                                <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                            </div>
                                                            <div class="text">
                                                                <p>Product availability is not guaranteed when you choose to ADD TO STORE. You may see the number of products you added to your store reduce based on what we have left in stock. 
                                                                </p></div>
                                                        </div>
                                                        <div class="single-smart-card d-flex">
                                                            <div class="icon mr-3">
                                                                <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                            </div>
                                                            <div class="text">
                                                                <p>Your Store will remain active even after your product in stock runs out. We will remind you when product in stock is running low. 
                                                                </p></div>
                                                        </div>
                                                        <div class="single-smart-card d-flex">
                                                            <div class="icon mr-3">
                                                                <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                            </div>
                                                            <div class="text">
                                                                <p>You can delete and restart a campaign anytime. If you have products you "BUY NOW", you will always have those available in stock even after you delete a campaign
                                                                </p></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- end card-->
                </div>
            </div>
        </div>
        <script>
            $(function () {

                $(".uploadCouponImage").click(function () {
                    var uniqueID = $(this).attr("data-imageid");
                    //uniqueID = uniqueID.replace("i", "");
                    var file_data = $('#couponImage' + uniqueID).prop('files')[0];
                    if (!file_data) {
                        $.alert({
                            title: 'No Image!',
                            content: 'Please select an image!',
                        });
                        return false;
                    }
                    var form_data = new FormData();
                    form_data.append('file', file_data);

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: '<?php echo $uploadAction; ?>',
                        dataType: 'json', // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (output) {
                            console.log(output, output.data);
                            $("#hidden" + uniqueID).val(output.data);
                        }
                    });
                });
            });
        </script>
