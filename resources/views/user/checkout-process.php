<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="checkout-process-wrapper">

                        <div class="row">
                            <div class="col-xl-6">
                                <div class="justify-content-between bd-highlight mb-3">

                                    <div class="bd-highlight">
                                        <ul class="nav nav-pills navtab-bg flex-column flex-sm-row">
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link active mb-3 pl-5 pr-5 text-center">
                                                    Select
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Cart
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link  mb-3 pl-5 pr-5 text-center">
                                                    Checkout
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Settings
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Done
                                                </a>
                                            </li>

                                        </ul>
                                    </div>


                                </div>
                            </div>

                            <div class="col-xl-6">          
                                <div class="bd-highlight">             
                                    <div class="btn-group mb-2 show float-right">
                                        <button type="button" class="btn btn-danger dropdown-toggle btn-info" data-toggle="dropdown" aria-expanded="true">Select Business</button>
                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                                            <a class="dropdown-item typefilter" href="javascript:void(0)">ALL</a>
                                            <a class="dropdown-item typefilter" href="javascript:void(0)">B2B</a>
                                            <a class="dropdown-item typefilter" href="javascript:void(0)">B2C</a>

                                        </div>
                                    </div>
                                    <div class="btn-group mb-2 mr-3 show float-right">

                                        <button type="button" class="btn btn-warning dropdown-toggle cart-button" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="true"><i class="zmdi zmdi-shopping-cart"></i><span class="cart-bubble"><?= $cartcount ?></span></button>
                                        <div class="dropdown-menu dropdown-menu-lg-right p-2 cart-bubble-prod-list" style="min-width:20rem;">
                                            <ul class="list-group cartname">
                                                <?php
                                                if (!empty($cart_details)) {
                                                    foreach ($cart_details as $key => $value) {
                                                        $path = Config::get('constants.product_img_path');
                                                        $imageArr = explode(',', $value->attach);
                                                        echo '<li class="list-group-item "><img src="' . url('/') . $path . $imageArr[0] . '" width="50"/> ' . $value->name . '</li>';
                                                    }
                                                } else {
                                                    echo '<li class="list-group-item">Cart is empty.</li>';
                                                }
                                                ?>

                                            </ul>
                                            <?php if (!empty($cart_details)) { ?>
                                                <a href="<?= url('/') ?>/cart" class="btn btn-info">Continue to cart</a>
                                            <?php } ?>
                                            </div>
                                        </div>



                                        <form action="" method="post" id="typefilterform" style="display: none">
                                            <input type="hidden" name="typefilter">
                                            <input type="submit" class="typefilterform">
                                            <?= csrf_field(); ?>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane show active" id="selecttab">
                                    <div class="row">
                                        <?php
                                        if (empty($list)) {
                                            echo '<div class="col-sm-6 col-lg-6 col-xl-3">
                        <div class="single-product-item mb-30">No Products.</div></div>';
                                        } else {
                                            ?>
                                            <!-- Single product -->
                                            <?php
                                            foreach ($list as $key => $value) {
                                                $imageArr = explode(',', $value->attach);
                                                $symbol = Config::get('constants.' . $value->currency);
                                                $symbol = '<i class="' . $symbol . '" aria-hidden="true"></i>';
                                                echo '<div class="col-sm-6 col-lg-6 col-xl-3">
                           <div class="single-product-item mb-30">
                              <div class="product-card">
                                 <a class="product-thumb user-to-view-product-details" href="javascript:void(0);" id="' . $value->id . '"><img src="' . url('/') . '/resources/assets/uploads/' . $imageArr[0] . '" alt="Product"></a>
                                 <!-- Product -->
                                 <h3 class="product font-17 mb-15 mt-20 "><a href="javascript:;" class="user-to-view-product-details" id="' . $value->id . '">' . ucwords($value->name) . '</a></h3>
                                 <div class="d-flex justify-content-between align-items-center">
                                    <h4 class="product-price mb-0 mt-0">' . $symbol . '' . $value->price . '</h4>
                                 </div>';
                                                if (strtolower($value->determine_winning) != Config::get('constants.freethings')) {
                                                    echo '<div class="d-flex justify-content-between align-items-center">
                                    <p class="mt-15 mb-0">Odds of winning</p>
                                    <div class="div mt-15">
                                       <div class="form-group col-md-4">
                                                    
                                                    <select name="odds" id="inputState" class="form-control" style="width: 97px;">
                                                        <option value="">Select</option>
                                                         <option>1:15</option>
                                                         <option>1:25</option>
                                                         <option>1:200</option>
                                                         <option>1:500</option>
                                                         <option>1:1000</option>
                                                         <option>1:5000</option>
                                                         <option>1:10,000</option>
                                                         <option>1:20,000</option>
                                                        
                                                    </select>
                                                </div>
                                    </div>
                                 </div>';
                                                }

                                                echo'   <div class="d-flex justify-content-between align-items-center">
                                    <p class="mt-15 mb-0">Winning Frequency/Month</p>
                                    <div class="div mt-15">
                                       <input class="px-2 form-control mr-2" type="number" name="winning_freq" style="width: 3.2rem;" value="1" required="">
                                    </div>
                                 </div>
                                 <div class="product-buttons">
                                    <a class="btn btn-rounded btn-info btn-light mt-30 addtocart cartbtn_' . $value->id . '" href="#" id="' . $value->id . '">Add to Cart</a>
                                 </div>
                              </div>
                           </div>
                        </div>';
                                            }
                                        }
                                        ?>


                                    </div>
                                    <?php if (!empty($cart_details)) { ?>
                                    <div class="row mt-5">
                                        <div class="col-md-12 text-center">
                                            <a href="<?= url('/') ?>/cart">
                                                <button type="button" class="btn btn-rounded btn-danger mb-2 mr-2 btn-lg">Continue To Cart</button>
                                            </a>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="tab-pane" id="checkouttab">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="card box-margin bg-primary text-white">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="payment-area">
                                                                <h5 class="card-title mb-30 text-white">Your saved credit card and debit card</h5>

                                                                <div class="table-responsive">
                                                                    <table class="table table-bordered table-hover savescard">

                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="custom-control custom-radio">
                                                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                                                        <label class="custom-control-label" for="customRadio1">Visa</label>
                                                                                    </div>
                                                                                </td>
                                                                                <td>Ending with 9345</td>
                                                                                <td>Christina Martinez</td>
                                                                                <td>10/2020</td>
                                                                                <td>
                                                                                    <div class="form-inline">
                                                                                        <div class="form-group">
                                                                                            <label for="inputPassword4">CVV</label>
                                                                                            <input type="password" id="inputPassword4" class="form-control rounded-0 form-control-md mx-sm-3" aria-describedby="passwordHelpInline">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="custom-control custom-radio">
                                                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                                                        <label class="custom-control-label" for="customRadio1">Visa</label>
                                                                                    </div>
                                                                                </td>
                                                                                <td>Ending with 9345</td>
                                                                                <td>Christina Martinez</td>
                                                                                <td>10/2020</td>
                                                                                <td>
                                                                                    <div class="form-inline">
                                                                                        <div class="form-group">
                                                                                            <label for="inputPassword4">CVV</label>
                                                                                            <input type="password" id="inputPassword4" class="form-control rounded-0 form-control-md mx-sm-3" aria-describedby="passwordHelpInline">
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card box-margin">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-8">
                                                            <div class="payment-area">
                                                                <h5 class="card-title mb-30">Select payment method</h5>
                                                                <div class="nav-tabs-top">
                                                                    <ul class="nav nav-tabs border-none">
                                                                        <li class="nav-item">
                                                                            <a class="nav-link active btn btn-info" data-toggle="tab" href="#payment-methods-cc">
                                                                                <img class="card-visa-thumb" src="<?= url('/') ?>/resources/assets/img/shop-img/card.png" alt="">
                                                                            </a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link btn btn-info" data-toggle="tab" href="#payment-methods-paypal">
                                                                                <img class="card-visa-thumb" src="<?= url('/') ?>/resources/assets/img/shop-img/visa.png" alt="">
                                                                            </a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link btn btn-info" data-toggle="tab" href="#payment-methods-visa">
                                                                                <img class="card-visa-thumb" src="<?= url('/') ?>/resources/assets/img/shop-img/visa-2.png" alt="">
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                    <div class="tab-content">
                                                                        <div class="tab-pane card-body fade show active" id="payment-methods-cc">
                                                                            <div class="form-group">
                                                                                <label>
                                                                                    <span class="form-label text-dark mb-0">Card Number</span>
                                                                                </label>
                                                                                <input type="text" class="form-control" placeholder="XXXX-XXXX-XXXX-XXXX">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="form-label text-dark">Card Holder</label>
                                                                                <input type="text" class="form-control" placeholder="Name On Card">
                                                                            </div>
                                                                            <div class="form-row">
                                                                                <div class="col">
                                                                                    <label class="form-labe text-dark">Exp. Date</label>
                                                                                    <input type="text" class="form-control" placeholder="MM/YY">
                                                                                </div>
                                                                                <div class="col">
                                                                                    <label class="form-label text-dark">CVC</label>
                                                                                    <input type="text" class="form-control" placeholder="XXX">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane card-body fade" id="payment-methods-paypal">
                                                                            <div class="border p-3 mb-30">
                                                                                <h6>Add Paypal Form</h6>
                                                                                <p class="mb-0">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words.</p>
                                                                            </div>
                                                                            <button type="button" class="btn btn-lg btn-info btn-block">
                                                                                Continue with <strong><em>PayPal</em></strong>
                                                                            </button>
                                                                        </div>
                                                                        <div class="tab-pane card-body fade" id="payment-methods-visa">
                                                                            <div class="border p-3 mb-30">
                                                                                <h6>Add Visa Form</h6>
                                                                                <p class="mb-0">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words.</p>
                                                                            </div>
                                                                            <button type="button" class="btn btn-lg btn-info btn-block">
                                                                                Continue with <strong><em>PayPal</em></strong>
                                                                            </button>
                                                                        </div>
                                                                        <div class="form-group ml-3">
                                                                            <div class="checkbox d-inline">
                                                                                <input type="checkbox" name="checkbox-1" id="checkbox-2">
                                                                                <label for="checkbox-2" class="cr">Save payment info</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-30">
                                                                    <button type="button" class="btn btn-info">
                                                                        Add Card &nbsp;→
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 order-md-2 box-margin">
                                            <div class="card box-margin">
                                                <div class="card-body">
                                                    <h4 class="d-flex justify-content-between align-items-center mb-3">
                                                        <span class="card-title mb-0">Price Details</span>
                                                    </h4>

                                                    <ul class="list-group mb-3">
                                                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                            <div>
                                                                <h6 class="mb-0 font-14">1 Time setup Fees</h6>
                                                                <p>30 Days Money Back Guarantee</p>
                                                            </div>
                                                            <span><?= Session::get('symbols'); ?><span class="font-weight-bold text-success">$12</span></span>
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                            <div>
                                                                <h6 class="mb-0 font-14">Sales Tax</h6>
                                                            </div>
                                                            <span class="font-weight-bold text-success">$12</span>
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                            <div>
                                                                <h6 class="mb-0 font-14">Shipping</h6>
                                                            </div>
                                                            <span><?= Session::get('symbols'); ?><span class="font-weight-bold text-success">$12</span></span>
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between">
                                                            <span class="text-dark font-weight-bold">Total (USD)</span>
                                                            <strong class="text-danger">$410</strong>
                                                        </li>
                                                    </ul>
                                                    <form class="d-flex justify-content-between">
                                                        <div class="input-group">
                                                            <button type="submit" class="btn btn-info">Add to Store</button>
                                                        </div>
                                                        <div class="input-group">
                                                            <button type="submit" class="btn btn-info">Edit Order</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="card box-margin">
                                                <div class="card-body">
                                                    <div class="single-smart-card d-flex">
                                                        <div class="icon mr-3">
                                                            <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                        </div>
                                                        <div class="text">

                                                            <p>If you choose to BUY NOW, the cost of winning will remain the same. No charges at the time of winning.
                                                            </p></div>
                                                    </div>
                                                    <div class="single-smart-card d-flex">
                                                        <div class="icon mr-3">
                                                            <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                        </div>
                                                        <div class="text">

                                                            <p>If you choose to ADD TO STORE, we will charge your credit card on file at the time of winning and issue an invoice. 
                                                            </p></div>
                                                    </div>
                                                    <div class="single-smart-card d-flex">
                                                        <div class="icon mr-3">
                                                            <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                        </div>
                                                        <div class="text">

                                                            <p>Product availability is not guaranteed when you choose to ADD TO STORE. you may see the number of products you added to your store reduce based on what we have left in stock. 
                                                            </p></div>
                                                    </div>
                                                    <div class="single-smart-card d-flex">
                                                        <div class="icon mr-3">
                                                            <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                        </div>
                                                        <div class="text">

                                                            <p>Your Store will remain active even after your product in stock runs out. We will remind you when product i stock is running low. 
                                                            </p></div>
                                                    </div>
                                                    <div class="single-smart-card d-flex">
                                                        <div class="icon mr-3">
                                                            <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                        </div>
                                                        <div class="text">

                                                            <p>You can delete and restart a campaign anytime. If you have products you "BUY NOW", you will always have those available in stock even after you delete a campaign"Please add these two here:
                                                            </p></div>
                                                    </div>
                                                    <div class="single-smart-card d-flex">
                                                        <div class="icon mr-3">
                                                            <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                        </div>
                                                        <div class="text">

                                                            <p>Your Store will remain active even after your product in stock runs out. We will remind you when product i stock is running low
                                                            </p></div>
                                                    </div>
                                                    <div class="single-smart-card d-flex">
                                                        <div class="icon mr-3">
                                                            <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                        </div>
                                                        <div class="text">

                                                            <p>You can delete and restart a campaign anytime. If you have products you "BUY NOW", you will always have those available in stock even after you delete a campaign
                                                            </p></div>
                                                    </div>
                                                </div>




                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane" id="carttab">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="card box-margin">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="card-body">
                                                                <div class="checkout-area mb-50">
                                                                    <h4 class="card-title mt-0 mb-3">My Cart Items</h4>
                                                                    <div class="table-responsive cart-area">
                                                                        <table class="table table-nowrap table-bordered table-centered mb-0">
                                                                            <thead class="thead-light">
                                                                                <tr>
                                                                                    <th>Product</th>
                                                                                    <th>Winning<br>Frequency</th>
                                                                                    <th>Price</th>
                                                                                    <th>Shipping</th>
                                                                                    <th>Charges <br>per winning</th>
                                                                                    <th>Today</th>
                                                                                    <th></th>
                                                                                    <th>Clear</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody class="text-center">
                                                                                <tr>
                                                                                    <td class="text-left">
                                                                                        <img src="<?= url('/') ?>/resources/assets/img/shop-img/1.png" alt="contact-img" title="contact-img" class="round mr-3 product-thumb">
                                                                                        <p class="m-0 d-inline-block align-middle font-16">
                                                                                            <a href="apps-ecommerce-products-details.html" class="text-body">Netflix Subscription</a>
                                                                                            <br>
                                                                                            <small class="mr-2"><b>Odds of winning:</b> <br>25 To 5000 </small>

                                                                                        </p>

                                                                                        <div class="form-group mt-3">
                                                                                            <label>Please upload coupon image<br> for winners</label>
                                                                                            <input type="file" name="img[]" class="file-upload-default">
                                                                                            <div class="input-group col-xs-12">
                                                                                                <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                                                                                                <span class="input-group-append">
                                                                                                    <button class="file-upload-browse btn btn-info" type="button">Upload</button>
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="number" min="1" value="5" class="form-control" placeholder="Qty" style="width: 90px;">
                                                                                    </td>
                                                                                    <td>
                                                                                        $5.99
                                                                                    </td>
                                                                                    <td>
                                                                                        $5.99
                                                                                    </td>
                                                                                    <td>
                                                                                        $743.30
                                                                                    </td>
                                                                                    <td>
                                                                                        $0
                                                                                    </td>
                                                                                    <td class="text-left">
                                                                                        <div class="custom-control custom-radio">
                                                                                            <input type="radio" id="Addtostore" name="customRadio" class="custom-control-input">
                                                                                            <label class="custom-control-label" for="Addtostore">Add to store</label>
                                                                                        </div>
                                                                                        <div class="custom-control custom-radio">
                                                                                            <input type="radio" id="Addtostore" name="customRadio" class="custom-control-input">
                                                                                            <label class="custom-control-label" for="Addtostore">Buy Now</label>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div class="actions ml-3">
                                                                                            <a href="#" class="action-item mr-2" data-toggle="tooltip" title="" data-original-title="Quick view">
                                                                                                <i class="fa fa-external-link"></i>
                                                                                            </a>
                                                                                            <a href="#" class="action-item mr-2" data-toggle="tooltip" title="" data-original-title="Move to trash">
                                                                                                <i class="fa fa-times"></i>
                                                                                            </a>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="text-left">
                                                                                        <img src="<?= url('/') ?>/resources/assets/img/shop-img/2.png" alt="contact-img" title="contact-img" class="round mr-3 product-thumb">
                                                                                        <p class="m-0 d-inline-block align-middle font-16">
                                                                                            <a href="apps-ecommerce-products-details.html" class="text-body">Controlar</a>
                                                                                            <br>
                                                                                            <small class="mr-2"><b>Odds of winning:</b><br>25 To 5000 </small>
                                                                                        </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="number" min="1" value="5" class="form-control" placeholder="Qty" style="width: 90px;">
                                                                                    </td>
                                                                                    <td>
                                                                                        $5.99
                                                                                    </td>
                                                                                    <td>
                                                                                        $0
                                                                                    </td>
                                                                                    <td>
                                                                                        $0
                                                                                    </td>

                                                                                    <td>
                                                                                        $0
                                                                                    </td>
                                                                                    <td class="text-left">
                                                                                        <div class="custom-control custom-radio">
                                                                                            <input type="radio" id="Addtostore" name="customRadio" class="custom-control-input">
                                                                                            <label class="custom-control-label" for="Addtostore">Add to store</label>
                                                                                        </div>
                                                                                        <div class="custom-control custom-radio">
                                                                                            <input type="radio" id="Addtostore" name="customRadio" class="custom-control-input">
                                                                                            <label class="custom-control-label" for="Addtostore">Buy Now</label>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div class="actions ml-3">
                                                                                            <a href="#" class="action-item mr-2" data-toggle="tooltip" title="" data-original-title="Quick view">
                                                                                                <i class="fa fa-external-link"></i>
                                                                                            </a>
                                                                                            <a href="#" class="action-item mr-2" data-toggle="tooltip" title="" data-original-title="Move to trash">
                                                                                                <i class="fa fa-times"></i>
                                                                                            </a>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="text-left">
                                                                                        <img src="<?= url('/') ?>/resources/assets/img/shop-img/3.png" alt="contact-img" title="contact-img" class="round mr-3 product-thumb">
                                                                                        <p class="m-0 d-inline-block align-middle font-16">
                                                                                            <a href="apps-ecommerce-products-details.html" class="text-body">Head Phone</a>
                                                                                            <br>
                                                                                            <small class="mr-2"><b>Odds of winning:</b><br>25 To 5000 </small>
                                                                                        </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="number" min="1" value="5" class="form-control" placeholder="Qty" style="width: 90px;">
                                                                                    </td>
                                                                                    <td>
                                                                                        $99.00
                                                                                    </td>
                                                                                    <td>
                                                                                        $5.99
                                                                                    </td>
                                                                                    <td>
                                                                                        $0
                                                                                    </td>
                                                                                    <td>
                                                                                        $0
                                                                                    </td>
                                                                                    <td class="text-left">
                                                                                        <div class="custom-control custom-radio">
                                                                                            <input type="radio" id="Addtostore" name="customRadio" class="custom-control-input">
                                                                                            <label class="custom-control-label" for="Addtostore">Add to store</label>
                                                                                        </div>
                                                                                        <div class="custom-control custom-radio">
                                                                                            <input type="radio" id="Addtostore" name="customRadio" class="custom-control-input">
                                                                                            <label class="custom-control-label" for="Addtostore">Buy Now</label>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div class="actions ml-3">
                                                                                            <a href="#" class="action-item mr-2" data-toggle="tooltip" title="" data-original-title="Quick view">
                                                                                                <i class="fa fa-external-link"></i>
                                                                                            </a>
                                                                                            <a href="#" class="action-item mr-2" data-toggle="tooltip" title="" data-original-title="Move to trash">
                                                                                                <i class="fa fa-times"></i>
                                                                                            </a>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="text-left">
                                                                                        <img src="<?= url('/') ?>/resources/assets/img/shop-img/4.png" alt="contact-img" title="contact-img" class="round mr-3 product-thumb">
                                                                                        <p class="m-0 d-inline-block align-middle font-16">
                                                                                            <a href="apps-ecommerce-products-details.html" class="text-body">Phone</a>
                                                                                            <br>
                                                                                            <small class="mr-2"><b>Odds of winning:</b><br>25 To 5000 </small>
                                                                                        </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="number" min="1" value="5" class="form-control" placeholder="Qty" style="width: 90px;">
                                                                                    </td>
                                                                                    <td>
                                                                                        $99.00
                                                                                    </td>
                                                                                    <td>
                                                                                        $5.99
                                                                                    </td>
                                                                                    <td>
                                                                                        $0
                                                                                    </td>
                                                                                    <td>
                                                                                        $0
                                                                                    </td>
                                                                                    <td class="text-left">
                                                                                        <div class="custom-control custom-radio">
                                                                                            <input type="radio" id="Addtostore" name="customRadio" class="custom-control-input">
                                                                                            <label class="custom-control-label" for="Addtostore">Add to store</label>
                                                                                        </div>
                                                                                        <div class="custom-control custom-radio">
                                                                                            <input type="radio" id="Addtostore" name="customRadio" class="custom-control-input">
                                                                                            <label class="custom-control-label" for="Addtostore">Buy Now</label>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div class="actions ml-3">
                                                                                            <a href="#" class="action-item mr-2" data-toggle="tooltip" title="" data-original-title="Quick view">
                                                                                                <i class="fa fa-external-link"></i>
                                                                                            </a>
                                                                                            <a href="#" class="action-item mr-2" data-toggle="tooltip" title="" data-original-title="Move to trash">
                                                                                                <i class="fa fa-times"></i>
                                                                                            </a>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="text-left">
                                                                                        <img src="<?= url('/') ?>/resources/assets/img/shop-img/5.png" alt="contact-img" title="contact-img" class="round mr-3 product-thumb">
                                                                                        <p class="m-0 d-inline-block align-middle font-16">
                                                                                            <a href="apps-ecommerce-products-details.html" class="text-body">TV Remot</a>
                                                                                            <br>
                                                                                            <small class="mr-2"><b>Odds of winning:</b><br>25 To 5000 </small>
                                                                                        </p>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="number" min="1" value="5" class="form-control" placeholder="Qty" style="width: 90px;">
                                                                                    </td>
                                                                                    <td>
                                                                                        $49.99
                                                                                    </td>
                                                                                    <td>
                                                                                        $5.99
                                                                                    </td>
                                                                                    <td>
                                                                                        $0
                                                                                    </td>
                                                                                    <td>
                                                                                        $0
                                                                                    </td>
                                                                                    <td class="text-left">
                                                                                        <div class="custom-control custom-radio">
                                                                                            <input type="radio" id="Addtostore" name="customRadio" class="custom-control-input">
                                                                                            <label class="custom-control-label" for="Addtostore">Add to store</label>
                                                                                        </div>
                                                                                        <div class="custom-control custom-radio">
                                                                                            <input type="radio" id="Addtostore" name="customRadio" class="custom-control-input">
                                                                                            <label class="custom-control-label" for="Addtostore">Buy Now</label>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div class="actions ml-3">
                                                                                            <a href="#" class="action-item mr-2" data-toggle="tooltip" title="" data-original-title="Quick view">
                                                                                                <i class="fa fa-external-link"></i>
                                                                                            </a>
                                                                                            <a href="#" class="action-item mr-2" data-toggle="tooltip" title="" data-original-title="Move to trash">
                                                                                                <i class="fa fa-times"></i>
                                                                                            </a>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="text-left">
                                                                                        <img src="<?= url('/') ?>/resources/assets/img/shop-img/7.png" alt="contact-img" title="contact-img" class="round mr-3 product-thumb">
                                                                                    <p class="m-0 d-inline-block align-middle font-16">
                                                                                        <a href="apps-ecommerce-products-details.html" class="text-body">Heade Phone</a>
                                                                                        <br>
                                                                                        <small class="mr-2"><b>Odds of winning:</b><br>25 To 5000 </small>
                                                                                    </p>
                                                                                </td>
                                                                                <td>
                                                                                    <input type="number" min="1" value="5" class="form-control" placeholder="Qty" style="width: 90px;">
                                                                                </td>
                                                                                <td>
                                                                                    $129.99
                                                                                </td>
                                                                                <td>
                                                                                    $5.99
                                                                                </td>
                                                                                <td>
                                                                                    $0
                                                                                </td>
                                                                                <td>
                                                                                    $0
                                                                                </td>
                                                                                <td class="text-left">
                                                                                    <div class="custom-control custom-radio">
                                                                                        <input type="radio" id="Addtostore" name="customRadio" class="custom-control-input">
                                                                                        <label class="custom-control-label" for="Addtostore">Add to store</label>
                                                                                    </div>
                                                                                    <div class="custom-control custom-radio">
                                                                                        <input type="radio" id="Addtostore" name="customRadio" class="custom-control-input">
                                                                                        <label class="custom-control-label" for="Addtostore">Buy Now</label>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="actions ml-3">
                                                                                        <a href="#" class="action-item mr-2" data-toggle="tooltip" title="" data-original-title="Quick view">
                                                                                            <i class="fa fa-external-link"></i>
                                                                                        </a>
                                                                                        <a href="#" class="action-item mr-2" data-toggle="tooltip" title="" data-original-title="Move to trash">
                                                                                            <i class="fa fa-times"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 order-md-2 box-margin">
                                        <div class="card box-margin">
                                            <div class="card-body">
                                                <h4 class="d-flex justify-content-between align-items-center mb-3">
                                                    <span class="card-title mb-0">Price Details</span>
                                                </h4>

                                                <ul class="list-group mb-3">
                                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                        <div>
                                                            <h6 class="mb-0 font-14">1 Time setup Fees</h6>
                                                            <p>30 Days Money Back Guarantee</p>
                                                        </div>
                                                        <span class="font-weight-bold text-success">$12</span>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                        <div>
                                                            <h6 class="mb-0 font-14">Sales Tax</h6>
                                                        </div>
                                                        <span class="font-weight-bold text-success">$12</span>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                        <div>
                                                            <h6 class="mb-0 font-14">Shipping</h6>
                                                        </div>
                                                        <span class="font-weight-bold text-success">$12</span>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between">
                                                        <span class="text-dark font-weight-bold">Total (USD)</span>
                                                        <strong class="text-danger">$410</strong>
                                                    </li>
                                                </ul>
                                                <form>
                                                    <div class="input-group">
                                                        <button type="submit" class="btn btn-info">Continue To Checkout</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="card ">
                                            <div class="card-body">
                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>If you choose to BUY NOW, the cost of winning will remain the same. No charges at the time of winning.
                                                        </p></div>
                                                </div>
                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>If you choose to ADD TO STORE, we will charge your credit card on file at the time of winning and issue an invoice. 
                                                        </p></div>
                                                </div>
                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>Product availability is not guaranteed when you choose to ADD TO STORE. you may see the number of products you added to your store reduce based on what we have left in stock. 
                                                        </p></div>
                                                </div>
                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>Your Store will remain active even after your product in stock runs out. We will remind you when product i stock is running low. 
                                                        </p></div>
                                                </div>
                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>You can delete and restart a campaign anytime. If you have products you "BUY NOW", you will always have those available in stock even after you delete a campaign"Please add these two here:
                                                        </p></div>
                                                </div>
                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>Your Store will remain active even after your product in stock runs out. We will remind you when product i stock is running low
                                                        </p></div>
                                                </div>
                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>You can delete and restart a campaign anytime. If you have products you "BUY NOW", you will always have those available in stock even after you delete a campaign
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="settingstab">
                                <div class="row">
                                    <div class="col-md-4 height-card box-margin">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">Link Tracking</h4>
                                                <p class="card-text">As the conclusion of the SMS flow, we will send a message thanking your visitor for taking the chance to win. THis is a chance to re target your customers.</p>
                                                <p class="card-text">Thanks for cisiting us today. Please visit us at {{your unique tracking number}}</p>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label for="inputPassword4">Enter Unique link</label>
                                                        <input type="password" id="inputPassword4" class="form-control rounded-0 form-control-md mx-sm-3" aria-describedby="passwordHelpInline">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 height-card box-margin">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">Net Promoter Score</h4>
                                                <p class="card-text">Choose from one the the Net Promoter Score. We will deliver this message after completing their chances of after they win and we collect the lead.</p>
                                                <div class="form-inline">
                                                    <div class="form-group">
                                                        <label for="inputPassword4">Your website name</label>
                                                        <input type="password" id="inputPassword4" class="form-control rounded-0 form-control-md mx-sm-3" aria-describedby="passwordHelpInline">
                                                    </div>
                                                    <div class="mt-3">
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                            <label class="custom-control-label justify-content-start" for="customRadio1">How was your experience with visiting {{your website name}} today? 1 for good and 2 for not good.</label>
                                                        </div>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                            <label class="custom-control-label justify-content-start" for="customRadio2">On the scale of 1 to 10, how likely are you to recommend {{your website name}} to someone like you?</label>
                                                        </div>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                            <label class="custom-control-label justify-content-start" for="customRadio2">Not interested</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 height-card box-margin">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">Second Chance</h4>
                                                <p class="card-text">Let a subscriber get a second chance if they choose to provide more information about themself or refer someone to your FREETHNGS store. The information provided about themself will be only data not found in the subscriber's profile on the dashboard. Subscribers can play once every 3 days.</p>
                                                <div class="mt-3">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label justify-content-start" for="customRadio3">Refer someone</label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label justify-content-start" for="customRadio4">Collect data</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 height-card box-margin">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">Reminder (Re-targeting)</h4>
                                                <p class="card-text">When a winner wins a % or $ coupon off a product or free shipping, we will send a reminder for them to use their coupons.</p>
                                                <div class="mt-3">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                        <label class="custom-control-label" for="customCheck1">Just a reminder from {{store name}} not to forget to use your winning coupon. We will not send another reminder. To stop receiving text messages from us, please reply to STOP. <br>{{Store owner's unique link}}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 height-card box-margin">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">Store updates (Re-targeting)</h4>
                                                <p class="card-text">Allow subscribers to get reminded when new opportunities to win are available. This gives you a shot at getting your subscribers back to your website and convert them. When you add new products to your FREETHNGS store, customers will get a text message letting then know they can come back and play.</p>
                                                <div class="mt-3">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label justify-content-start" for="customRadio3"><strong>New Campaign</strong><br>Hi, {{First Name}} we want to remind you that they are new opportunities to win free items at {{store name}} at {{website link}}. Thank you. To stop getting this reminder, please text STOP.</label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label justify-content-start" for="customRadio4"><strong>Weekly Reminder</strong><br> Hi, {{First Name}} Justa reminder that you can now try your luck to win {{store name}} at {{website link}}. Thank you. To stop getting this reminder, please text STOP.</label>
                                                        <div class="form-inline mt-5">
                                                            <div class="form-group">
                                                                <label for="inputPassword4">Your website name</label>
                                                                <input type="password" id="inputPassword4" class="form-control rounded-0 form-control-md mx-sm-3" aria-describedby="passwordHelpInline">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 height-card box-margin">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">Get Discovered</h4>
                                                <p class="card-text">Add your store to our explore page and get discovered by consumers. Our discovery page is SEO friendly and you may find qualified lead that will lead to more sale.</p>
                                                <div class="mt-3">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                        <label class="custom-control-label" for="customCheck1">I am in, get me discovered</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 height-card box-margin">
                                        <div class="form-group">
                                            <div class="checkbox d-inline">
                                                <input type="checkbox" name="checkbox-1" id="checkbox-13">
                                                <label for="checkbox-13" class="cr">Accept terms and conditions</label>
                                            </div>
                                        </div>
                                        <div class="">
                                            <button type="submit" class="btn btn-info">Submit</button>	
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="completetab">
                                <div class="row">
                                    <div class="col-md-12 height-card box-margin">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">Congratulations</h4>
                                                <h6 class="card-title">Your store is not ready</h6>
                                                <p class="card-text">Your FREETHNGS&copy; store SMS flow number is {{Get Twillio Number}}. What you can after this:</p>
                                                <ul>
                                                    <li>Add your store to your website, Your store link is https://freethings.com/website_name or install the widget below</li>
                                                    <li>Test Your SMS flow</li>
                                                    <li>Edit store by adding or removing more items to your FREETHNGS store.</li>
                                                    <li>Refer a friend and get a full month of subscription free. Your referral link is https://freethings.com?grsf=58qs07</li>
                                                    <li>Contact us if you need help at https://freethings.com?grsf=58qs07</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end card-->
                </div>
            </div>
        </div>