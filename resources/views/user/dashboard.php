<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-6 col-xl">
                    <!-- Card -->
                    <div class="card box-margin">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col">
                                    <!-- Title -->
                                    <h4 class="my-0">65</h4>
                                    <!-- Heading -->
                                    <span class="font-18 text-dark mb-0">
                                        Number of messages
                                    </span>
                                </div>
                                <div class="col-auto">
                                    <!-- Icon -->
                                    <div class="icon">
                                        <i class="fa fa-id-badge font-30"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end card-->
                </div>
                <div class="col-12 col-sm-6 col-xl">
                    <!-- Card -->
                    <div class="card box-margin">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col">
                                    <!-- Title -->
                                    <h4 class="my-0">25K</h4>
                                    <!-- Heading -->
                                    <span class="font-18 text-dark mb-0">
                                        Number of subscribers
                                    </span>
                                </div>
                                <div class="col-auto">
                                    <!-- Icon -->
                                    <div class="icon">
                                        <i class="fa fa-id-badge font-30"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-xl">
                    <!-- Card -->
                    <div class="card box-margin">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col">
                                    <!-- Title -->
                                    <h4 class="my-0">55</h4>
                                    <!-- Heading -->
                                    <span class="font-18 text-dark mb-0">
                                        Winning
                                    </span>

                                </div>

                                <div class="col-auto">
                                    <!-- Icon -->
                                    <div class="icon">
                                        <i class="fa fa-id-badge font-30"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-xl">
                    <!-- Card -->
                    <div class="card box-margin">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col">
                                    <!-- Title -->
                                    <h4 class="my-0">40K</h4>
                                    <!-- Heading -->
                                    <span class="font-18 text-dark">
                                        Store Visit
                                    </span>
                                </div>
                                <div class="col-auto">
                                    <!-- Icon -->
                                    <div class="icon">
                                        <i class="fa fa-id-badge font-30"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="row pb-4">
                                <div class="col-lg-7">
                                    <!-- Donut Fill -->
                                    <div id="panel-21" class="panel">
                                        <h4 class="card-title">Top Winning Items</h4>
                                    </div>
                                </div>
                                <div class="col-lg-5 text-xl-right">
                                    <div class="btn-group mb-2 mr-2">
                                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Winners
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <button class="dropdown-item" type="button">Top Winning Items</button>
                                            <button class="dropdown-item" type="button">Top Subscribers</button>
                                            <button class="dropdown-item" type="button">Top Winners</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5">
                                    <!-- Donut Fill -->
                                    <div id="panel-21" class="panel">

                                        <div class="panel-container show">
                                            <div class="panel-content">
                                                <div id="donutFill" class="ct-chart"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7">
                                    <div class="template-demo">
                                        <table class="table mb-0">
                                            <tbody>
                                                <tr>
                                                    <td>Free Shipping</td>
                                                    <td class="text-right">
                                                        <div class="badge badge-primary">&nbsp;&nbsp;&nbsp;</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Donation</td>
                                                    <td class="text-right">
                                                        <div class="badge badge-info">&nbsp;&nbsp;&nbsp;</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Netflix</td>
                                                    <td class="text-right">
                                                        <div class="badge badge-danger">&nbsp;&nbsp;&nbsp;</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Free Month Sub</td>
                                                    <td class=" text-right">
                                                        <div class="badge badge-success">&nbsp;&nbsp;&nbsp;</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Phone</td>
                                                    <td class=" text-right">
                                                        <div class="badge badge-warning">&nbsp;&nbsp;&nbsp;</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <!-- Donut Fill -->
                                    <div id="panel-21" class="panel">
                                        <h4 class="card-title">Campaign Settings</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table mb-0">
                                        <tbody>
                                            <tr>
                                                <td>Campaign Goals</td>
                                                <td class="text-right">
                                                    <a href="#" class="btn btn-info btn-sm mb-2 mr-2" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#setupgoal">Setup Goal</a>
                                                    <?php
                                                    if (!empty($existGoal)) {
                                                        echo '<span class="badge badge-info"><i class="fa fa-smile-o"></i> Goal Set</span>';
                                                    } else
                                                        echo '<span class="badge badge-danger"><i class="fa fa-frown-o"></i> Goal Not Set</span>';
                                                    ?>
                                                    <div class="modal inmodal fade text-left" id="setupgoal" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog modal-xl">
                                                            <form method="post" name="frmGoal">
                                                                <div class="modal-content" id="lead-goals">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Lets Setup Your Lead Goals</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>Please select the type of leads you will like to capture. you will see the types of the items you can add to your store as you select. Please select carefully.</p>
                                                                        <div class="row">
                                                                            <div class="col-md-8">
                                                                                <div id="checkboxes" class="scrolling-wrapper scrollbar scrollbar-warning row mt-4 pb-5 mb-5">
                                                                                    <!-- Dataset section starts -->
                                                                                    <?php
                                                                                    if (!empty($existGoal)) {
                                                                                        $savedGoal = json_decode($existGoal->set_goals);
                                                                                    } else
                                                                                        $savedGoal = array();
                                                                                    foreach ($setGoal as $category => $subcategory) {
                                                                                        //Category section
                                                                                        echo '<div class="col-xl-4 col-md-6">'
                                                                                        . '<h5 class="card-title">' . $category . '</h5><hr>';
                                                                                        //Subcategry section
                                                                                        foreach ($subcategory as $k => $v) {
                                                                                            $speakHtml = $chkGoal = false;
                                                                                            $exp = explode('::', $v);
                                                                                            $v = $exp[0]; //sub_category Label
                                                                                            $w = $exp[1]; //sub_category ID
                                                                                            $x = $exp[2]; //question count
                                                                                            $y = $exp[3]; //$questionID
                                                                                            $z = $exp[4]; //is_default?

                                                                                            $disabled = $checked = false;
                                                                                            if ($x > 0 || strpos($v, '(auto-captured)') !== false) {
                                                                                                $speakHtml = '<a href="#" data-goal="' . $v . '" data-id="' . $w . '" class="setupgoal-speak"><img src="' . url('/resources/assets/img/core-img/speech-bubble.png') . '" style="width:20px;" /></a>';
                                                                                                $chkGoal = 'chkGoal';
                                                                                            } else {
                                                                                                $disabled = ' disabled';
                                                                                            }
                                                                                            $vSpace = str_replace(' ', '_', $v);
                                                                                            //if ($v == 'First Name' || $v == 'Phone number' || $v == 'Last name' || $v == 'Email') {
                                                                                            if($z == 1){
                                                                                                $checked = 'checked';
                                                                                                $disabled = ' disabled';
                                                                                            } else if (isset($savedGoal->$v)) {
                                                                                                $checked = 'checked';
                                                                                            } else if (isset($savedGoal->$vSpace)) {
                                                                                                $checked = 'checked';
                                                                                            }

                                                                                            echo '<div class="form-group">
                                                                                            <div class="checkbox checkbox-danger d-inline">
                                                                                                <input class="'.$chkGoal.'" data-queid="' . $y . '" data-goal="' . $v . '" type="checkbox" name="checkbox-' . $v . '" id="checkbox-' . $category . '-' . $k . '" ' . $checked . $disabled . '>
                                                                                                <label data-queid="' . $y . '" data-goal="' . $v . '"  for="checkbox-' . $category . '-' . $k . '" class="cr '.$chkGoal.'">' . $v . '</label>
                                                                                                ' . $speakHtml . '
                                                                                            </div>
                                                                                        </div>';
                                                                                        }
                                                                                        echo '</div>';
                                                                                    }
                                                                                    ?>
                                                                                    <!-- Dataset section Ends -->
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="setup-goal-side">
                                                                                    <h4>Pick Question Modal</h4>
                                                                                </div>
                                                                                <div class="form-check pl-0 mb-2">
                                                                                    <label class="form-check-label">
                                                                                        <input type="radio" class="form-check-input mr-2 question_order" name="question_order" value="random" checked=""><span class="ml-3">Randomly</span>
                                                                                    </label>
                                                                                    <label class="form-check-label">
                                                                                        <input type="radio" class="form-check-input mr-2 question_order" name="question_order" value="order" checked=""><span class="ml-3">Order</span>
                                                                                    </label>

                                                                                </div>
                                                                                <h4>No Re-order</h4>
                                                                                <div class="no-dragable-box scrolling-wrapper-reorder scrollbar-reorder scrollbar-warning-reorder" >
                                                                                    <ul class="NoReOrder">
                                                                                        <?php
                                                                                            foreach ($setGoal as $category => $subcategory) {
                                                                                                foreach ($subcategory as $k => $v) {
                                                                                                    $exp = explode('::', $v);
                                                                                                    $v = $exp[0]; //sub_category Label
                                                                                                    //$w = $exp[1]; //sub_category ID
                                                                                                    //$x = $exp[2]; //question count
                                                                                                    $y = $exp[3]; //$questionID
                                                                                                    $z = $exp[4]; //is_default?
                                                                                                    if($z == 1){
                                                                                                        echo '<li id="'.$y.'" class="alert alert-warning">'.$v.'</li>';
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        ?>
                                                                                        <!--li id="0" class="alert alert-warning">First Name</li>
                                                                                        <li id="0" class="alert alert-warning">Last Name</li>
                                                                                        <li id="69" class="alert alert-warning">Email</li>
                                                                                        <li id="0" class="alert alert-warning">Phone Number</li>
                                                                                        <li class="alert alert-warning">IP country (auto-captured)</li>
                                                                                        <li class="alert alert-warning">IP country (auto-captured)</li>
                                                                                        <li class="alert alert-warning">IP country (auto-captured)</li>
                                                                                        <li class="alert alert-warning">IP country (auto-captured)</li -->

                                                                                    </ul>
                                                                                </div>
                                                                                <h4>Re-order</h4>
                                                                                <div class="draggable-main scrolling-wrapper-reorder scrollbar-reorder scrollbar-warning-reorder" >
                                                                                    <p>Please select dataset category to add here.</p>
                                                                                    <div class="">
                                                                                        <input type="hidden" id="question_ids" name="question_ids[]" value="">
                                                                                        <!--                                                                                        <form>-->
                                                                                        <div class="dragbox">

                                                                                            <ul id="draggable-right" class="connected-sortable draggable-right">
                                                                                                <?php
                                                                                                if (!empty($existGoal)) {
                                                                                                    $savedGoal = json_decode($existGoal->set_goals);
                                                                                                    if(isset($existGoal->questions)){
                                                                                                        $existQuestions = explode(',',$existGoal->questions);
                                                                                                        if(!empty($existQuestions))
                                                                                                        foreach ($existQuestions as $v) {
                                                                                                            echo '<input type="hidden" name="question_ids[]" value="'.$v.'">';
                                                                                                        }
                                                                                                    }
                                                                                                    foreach ($savedGoal as $key => $value) {
                                                                                                        echo '<li id="0" class="alert alert-warning">' . $key . '
                                                                                                    <div class="float-right">
                                                                                                        <a href="#" class="remove_question delete_question">
                                                                                                            <i class="fa fa-trash text-danger font-18"></i>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                </li>';
                                                                                                    }
                                                                                                } ?>
                                                                                            </ul>
                                                                                        </div>
                                                                                        <!--                                                                                        </form>-->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer justify-content-center">
                                                                        <button type="button" class="btn btn-danger closefirstmodal">Close</button>
                                                                        <?php
                                                                        if (!empty($existGoal) && count($campaign_detail) > 0 && $campaign_detail[0]->payment_status == 1) {
                                                                            $setGoaldisabled = ' disabled';
                                                                        } else {
                                                                            $setGoaldisabled = false;
                                                                        }
                                                                        ?>
                                                                        <button type="submit" <?= $setGoaldisabled; ?> class="btn btn-info">Setup Complete</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                            <div class="modal-content" id="flow-questions" style="display:none;">
                                                                <div class="modal-header flex-column">
                                                                    <h4 id="setupgoalSpeakTitle" class="modal-title">Question</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <table class="table nowrap">
                                                                        <tbody id="questionContent">
                                                                            <tr>
                                                                                <td style="font-weight:bold;">Question</td>
                                                                                <td><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Short Answers</td>
                                                                                <td><p>Wait for response</p></td>
                                                                            </tr>
                                                                    </table>
                                                                </div>
                                                                <div class="modal-footer justify-content-center">
                                                                    <input type="hidden" id="hid_dataset_id" />
                                                                    <button class="btn btn-danger backToGoal">Back</button>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- Modal With Warning -->
                                                    <div id="Warning" class="modal fade text-center" role="dialog" data-backdrop="false">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    <p>Do you want to close setup goal?</p>
                                                                    <button type="button" class="btn btn-danger confirmclosed">Confirm Close</button>
                                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Campaign</td>

                                                <td class="text-right">
                                                    <?php
                                                    $createCamp = url('/all-product-list');
                                                    $widgetLinkID = false;
                                                    if (count($campaign_detail) > 0 && $campaign_detail[0]->payment_status == 1) {  //Zero campaign_detail
                                                        ?>
                                                        <span id="runningCamp">
                                                            <div class="runingCampaign">
                                                                <a href="#" class="btn btn-info btn-sm mb-2 mr-2 getwidget" data-toggle="modal" data-target="#getwidget">Get Widget</a>
                                                                <a href="<?= url('/') . "/delete-campaign" ?>" class="btn btn-info btn-sm mb-2 mr-2 deleteCamp">Delete Campaign</a>
                                                                <?php
                                                                $camprow = $campaign_detail->first();
                                                                $widgetLinkID = url("/store/CPX" . $camprow->id . "XBUX" . $camprow->user_id);
                                                                if ($camprow->status == 2)
                                                                    $label = "Retrive";
                                                                else
                                                                    $label = "Pause";
                                                                ?>
                                                                <a href="<?= url('/') . "/change-campaign" ?>" class="btn btn-info btn-sm mb-2 mr-2 changeCamp"><span id="changeCamp"><?= $label ?></span> Campaign</a>
                                                            </div>
                                                        </span>
                                                    <?php } else if (count($campaign_detail) < 1 || $campaign_detail[0]->payment_status == 0) { ?>
                                                        <a href="<?= url('/') ?>/all-product-list" class="btn btn-info btn-sm mb-2 mr-2">Create Campaign</a>
<?php } ?>
                                                    <span id="createCamp"></span>
                                                    <div class="modal inmodal fade text-left" id="getwidget" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title">FreeThngs Store Widget</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>Copy and paste this code into desired place of your website. (HTML editor, Website template, theme etc)</p>
<<<<<<< HEAD
                                                                    <p>Store Link:: 
                                                                        <a target="_blank" id="storeLink" class="btn btn-info btn-sm mb-2 mr-2">Visit Store Page</a>
=======
                                                                    <p>Store Link::
                                                                        <a href="#" target="_blank" id="storeLink" class="btn btn-info btn-sm mb-2 mr-2">Visit Store Page</a>
>>>>>>> 7118af6acd45084ca766897004db814332729a97
                                                                    </p>
                                                                    <p>Store Widget</p>
                                                                    <textarea class="form-control form-control-md u-textarea-expandable rounded-0">
                                                                        <blockquote class="embedly-card">
                                                                        <h4>
                                                                        <a href="http://www.google.com">Google</a>
                                                                        </h4>
                                                                        <p>Search the world's information, including webpages, images, videos and more. Google has many special features to help you find exactly what you're looking for.</p>
                                                                        </blockquote>
                                                                        <script async src="//cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
                                                                    </textarea>

                                                                    <p class="mt-5">Chat Widget</p>
                                                                    <textarea class="form-control form-control-md u-textarea-expandable rounded-0"><blockquote class="embedly-card"><h4><a href="http://www.google.com">Google</a></h4><p>Search the world's information, including webpages, images, videos and more. Google has many special features to help you find exactly what you're looking for.</p></blockquote>
<script async src="//cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script></textarea>
																		</div>

																		<div class="modal-footer">
																			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
																			<button type="button" class="btn btn-info">Save changes</button>
																		</div>
																	</div>
																</div>
															</div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                </div>
                        </div>

                                    </div>
                                </div>
                            </div>
						</div>
						<div class="row">
							<div class="col-lg-12 height-card box-margin">
                                <div class="card">
                                    <div class="card-body">
                                        <table id="scroll-horizontal-datatable" class="table w-100 nowrap">
                                            <thead>
                                                    <tr>
                                                        <th>Date</th>
                                                        <th>Campaign ID</th>
                                                        <th>Winning ID</th>

                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Phone Number</th>
                                                        <th>Product Won</th>
                                                        <th>Product in stock</th>
                                                        <th>Shipping Required</th>
                                                        <th>Invoice Amount</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                    <?php
                                    if (isset($campaign_report) && count($campaign_report) > 0) {
                                        foreach ($campaign_report as $campaign) {
                                            ?>
                                                                                                                                                                                                                                    <tr>
                                                                                                                                                                                                                                        <td><?= $campaign->created_date ?></td>
                                                                                                                                                                                                                                        <td><?= $campaign->id ?></td>
                                                                                                                                                                                                                                        <td>PF7JF</td>
                                                                                                                                                                                                                                        <td><?= $userdetails->first_name ?></td>
                                                                                                                                                                                                                                        <td><?= $userdetails->last_name ?></td>
                                                                                                                                                                                                                                        <td><?= $userdetails->phonecode . '-' . $userdetails->phone ?></td>
                                                                                                                                                                                                                                        <td>Phone Grip</td>
                                                                                                                                                                                                                                        <td>2 Of 5</td>
                                                                                                                                                                                                                                        <td>No</td>
                                                                                                                                                                                                                                        <td><?= $userdetails->symbol . $campaign->totalcharges ?></td>
                                                                                                                                                                                                                                    </tr>

                                            <?php
                                        }
                                    }
                                    ?>
                                                </tbody>
                                            </table>
                                    </div>
				</div>
							</div>
						</div>
                    </div>
<link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/default-assets/new/sweetalert-2.min.css">
<script src="<?= url('/') ?>/resources/assets/js/default-assets/sweetalert-init.js"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/sweetalert2.min.js"></script>
<script>

    $(function () {
        $('a.getwidget').on('click', function (e) {
            var href = '<?= $widgetLinkID ?>';
            //var storeLink = '<a href="' + href + '" class="btn btn-info btn-sm mb-2 mr-2">Visit Store Page</a>';
            $("#storeLink").attr('href', href);
        });
        //Delete campaign
        $('a.deleteCamp').confirm({
            title: 'Delete Campaign?',
            content: 'You will not recover your campaign again.',
            autoClose: 'cancel|8000',
            buttons: {
                deleteCampaign: {
                    text: 'delete campaign',
                    action: function () {
                        //location.href = this.$target.attr('href');
                        return $.ajax({
                            url: this.$target.attr('href'),
                            dataType: 'json',
                            method: 'get'
                        }).done(function (response) {

                            if (response.success) {
                                var href = '<?php echo $createCamp; ?>';
                                var createCampHtml = '<a href="' + href + '" class="btn btn-info btn-sm mb-2 mr-2">Create Campaign</a>';
                                $('#runningCamp').hide();
                                $('#createCamp').html(createCampHtml);
                            }
                        }).fail(function () {
                            //self.setContent('Something went wrong.');
                        });
                    }
                },
                cancel: function () {
                    //$.alert('action is canceled');
                }
            }
        });
        //Play-Pause campaign
        $('a.changeCamp').confirm({
            title: 'Are you sure?',
            content: 'It will change Campaign status!',
            autoClose: 'cancel|7000',
            buttons: {
                changeCampaign: {
                    text: 'change campaign',
                    action: function () {
                        //location.href = this.$target.attr('href');
                        return $.ajax({
                            url: this.$target.attr('href'),
                            dataType: 'json',
                            method: 'get'
                        }).done(function (response) {

                            if (response.success) {
                                if ($('#changeCamp').text() == 'Pause')
                                    label = "Retrive";
                                else
                                    label = "Pause";

                                $('#changeCamp').text(label);
                            }
                        }).fail(function () {
                            //self.setContent('Something went wrong.');
                        });
                    }
                },
                cancel: function () {
                    //$.alert('action is canceled');
                }
            }
        });


        /*============= Set Goal =========== */

        $('form').on('submit', function (e) {
            e.preventDefault();
            var selected = [];
            $('#checkboxes input:checked').each(function () {
                selected.push($(this).attr('name'));
            });

            if (selected.length < 1) {
                $.alert({
                    title: 'Set Goal!',
                    content: 'Please select atleast one goal!',
                });
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: '<?php echo url('/') . "/set-goal" ?>',
                data: $('form').serialize(),
                success: function () {
                    $.alert('Goal has been set successfully!');
                    //console.log("res::",response);
                    location.href = '<?php echo url('/') . "/dashboard" ?>';
                }
            });
        });

        $('.closefirstmodal').click(function () { //Close Button on Form Modal to trigger Warning Modal
            $('#Warning').modal('show').on('show.bs.modal', function () { //Show Warning Modal and use `show` event listener
            });
        });

        $('.confirmclosed').on('click', function (e) {
            $('.modal').modal('hide');
        });

    });

    /*Question on goal*/
    $('.setupgoal-speak').on('click', function (e) {
        var goal_id = $(this).data('id');
        var goal_label = $(this).data('goal');
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: '<?php echo url('/') . "/get-questions" ?>',
            dataType: 'json',
            data: 'goal_id=' + goal_id + '&goal_label=' + goal_label,
            success: function (response) {
                $("#hid_dataset_id").val(goal_id);
                $('#setupgoalSpeakTitle').html(goal_label);
                if (response.success) {
                    $('#questionContent').html(response.data);
                } else {
                    $('#questionContent').html('<tr><td>No Question added.</td></tr>');
                }
                /*Show Hide Popups*/
                $('#lead-goals').hide();
                $('#flow-questions').show();
            }
        });
    });
    $('.backToGoal').on('click', function (e) {
        $('#lead-goals').show();
        $('#flow-questions').hide();
    });

    $('.chkGoal').on('change', function (e) {
        var outputHtml = '';
        var checkedGoal = $(this).data('goal');
        var checkedQue = $(this).data('queid');
        if (this.checked) {
            outputHtml += '<li id="' + checkedQue + '" class="alert alert-warning">' + checkedGoal;
            outputHtml += '<div class="float-right">';
            outputHtml += '<a href="#" data-id="' + checkedQue + '" class="remove_question delete_question">';
            outputHtml += '<i class="fa fa-trash text-danger font-18"></i>';
            outputHtml += '</a>';
            outputHtml += '</div></li>';
            if (checkedGoal.indexOf("(auto-captured)") > -1)
                $('.NoReOrder').append(outputHtml);
            else
                $('#draggable-right').append(outputHtml);
        } else {
            $('#' + checkedQue).remove();
        }
        myFunc();
    });

    $(document).on("click", "a.remove_question", function () {
        $(this).parent().parent().remove();
        var dataVal = $(this).data('id');
        $('input:checkbox[data-queid="'+dataVal+'"]').attr('checked', false);
        myFunc();
    });

    function updateQuestion(question_id){
        question_link = $("#question_link").val();
        var text_entry = $("#text_entry").val();
		var suggested_edit = $("textarea[name='suggested_edit']").val();
                dataset_id = $("#hid_dataset_id").val();
        if($("#question_link").data("required")=="required" && question_link==""){
            Swal.fire({title:"Link is required",confirmButtonClass:"btn btn-confirm mt-2"});
            return;
        }
        if(typeof $("#question_link").val() !== 'undefined' && question_link!="" && !validURL(question_link)){
            Swal.fire({title:"Invalid Link. Link must be start with 'https'",confirmButtonClass:"btn btn-confirm mt-2"});
            return;
        }
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        $.ajax({
            url: 'updateQuestion',
            type: 'POST',
            "dataType": "json",
            data:{question_link:question_link,question_id:question_id, suggested_edit:suggested_edit, text_entry:text_entry,dataset_id:dataset_id},
            success: function(data) {
                if(data.success == false) {
                  Swal.fire({title:data.msg,confirmButtonClass:"btn btn-confirm mt-2"});
                } else {
                   Swal.fire({title:"Data submitted successfully. ", type: "success"});
                   $('#lead-goals').show();
                    $('#flow-questions').hide();
                }
            },
            error: function(e) {
            }
        });
    }
    function validURL(str) {
        var pattern = new RegExp('^(https:\\/\\/)((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
          '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
          '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
          '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
          '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        return !!pattern.test(str);
      }

                                    </script>
