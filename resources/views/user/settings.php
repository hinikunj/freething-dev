<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script> 
<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 height-card box-margin">

                    <div class="checkout-process-wrapper">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="justify-content-between bd-highlight mb-3">

                                    <div class="bd-highlight">
                                        <ul class="nav nav-pills navtab-bg flex-column flex-sm-row">
                                            <li class="nav-item">
                                                <?php #url('/') . "/all-product-list" ?>
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Select
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>
                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Cart
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link  mb-3 pl-5 pr-5 text-center">
                                                    Checkout
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link active mb-3 pl-5 pr-5 text-center">
                                                    Settings
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Done
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="bd-highlight">
                                    <div class="btn-group mb-2 show float-right">
                                        <button type="button" class="btn btn-danger dropdown-toggle btn-info" data-toggle="dropdown" aria-expanded="true">Select Business</button>
                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                                            <a class="dropdown-item" href="javascript:void(0)">ALL</a>
                                            <a class="dropdown-item" href="javascript:void(0)">B2B</a>
                                            <a class="dropdown-item" href="javascript:void(0)">B2C</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane show active" id="settingstab">
                                <form method="post" enctype="multipart/form-data" name="settings" id="settings">
                                    <input type="hidden" name="camp_id" value="<?= $camp_id; ?>" />
                                    <div class="row">
                                        <div class="col-md-4 height-card box-margin">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="card-title">Link Tracking</h4>
                                                    <p class="card-text">As the conclusion of the SMS flow, we will send a message thanking your visitor for taking the chance to win. THis is a chance to re target your customers.</p>
                                                    <p class="card-text">Thanks for cisiting us today. Please visit us at {{your unique tracking number}}</p>
                                                    <div class="form-inline">
                                                        <div class="form-group">
                                                            <label for="unique_link">Enter Unique link</label>
                                                            <input type="text" name="unique_link" id="unique_link" class="form-control rounded-0 form-control-md mx-sm-3">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 height-card box-margin">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="card-title">Net Promoter Score</h4>
                                                    <p class="card-text">Choose from one the the Net Promoter Score. We will deliver this message after completing their chances of after they win and we collect the lead.</p>
                                                    <div class="form-inline">

                                                        <div class="mt-3">
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio" id="net_promoter_score1" name="net_promoter_score" class="custom-control-input" value="1">
                                                                <label class="custom-control-label justify-content-start" for="net_promoter_score1">How was your experience with visiting {{your website name}} today? 1 for good and 2 for not good.</label>
                                                            </div>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio" id="net_promoter_score2" name="net_promoter_score" class="custom-control-input" value="2">
                                                                <label class="custom-control-label justify-content-start" for="net_promoter_score2">On the scale of 1 to 10, how likely are you to recommend {{your website name}} to someone like you?</label>
                                                            </div>

                                                            <div class="custom-control custom-radio">
                                                                <input type="radio" id="net_promoter_score3" name="net_promoter_score" class="custom-control-input" value="3">
                                                                <label class="custom-control-label justify-content-start" for="net_promoter_score3">Not interested</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 height-card box-margin">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="card-title">Second Chance</h4>
                                                    <p class="card-text">Let a subscriber get a second chance if they choose to provide more information about themself or refer someone to your FREETHNGS store. The information provided about themself will be only data not found in the subscriber's profile on the dashboard. Subscribers can play once every 3 days.</p>
                                                    <div class="mt-3">
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" id="second_chance1" name="second_chance" class="custom-control-input" value="1">
                                                            <label class="custom-control-label justify-content-start" for="second_chance1">Refer someone</label>
                                                        </div>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" id="second_chance2" name="second_chance" class="custom-control-input" value="2">
                                                            <label class="custom-control-label justify-content-start" for="second_chance2">collect data</label>
                                                        </div>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" id="second_chance3" name="second_chance" class="custom-control-input" value="3">
                                                            <label class="custom-control-label justify-content-start" for="second_chance3">Not interested</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 height-card box-margin">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="card-title">Play Reminder</h4>
                                                    <p class="card-text">When a winner wins a % or $ coupon off a product or free shipping, we will send a reminder for them to use their coupons.</p>
                                                    <div class="mt-3">
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" id="play_reminder1" name="play_reminder" class="custom-control-input"  value="1">
                                                            <label class="custom-control-label justify-content-start" for="play_reminder1"><strong>Weekly Reminder</strong><br> Hi, {{First Name}} Justa reminder that you can now try your luck to win {{store name}} at {{website link}}. Thank you. To stop getting this reminder, please text STOP.</label>

                                                        </div>

                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" id="play_reminder2" name="play_reminder" class="custom-control-input"  value="2">
                                                            <label class="custom-control-label" for="play_reminder2"><strong>Countdown-to-play</strong>
                                                                <br>Ask your subscribers if they want to be reminded to play once they is another chance to play. 
                                                                "You play every 48 hours, will you like to be reminded when you can play next?</label>
                                                        </div>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" id="play_reminder3" name="play_reminder" class="custom-control-input"  value="3">
                                                            <label class="custom-control-label" for="play_reminder3">Not interested</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 height-card box-margin">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="card-title">Campaign Reminder</h4>
                                                    <p class="card-text">Allow subscribers to get reminded when new opportunities to win are available. This gives you a shot at getting your subscribers back to your website and convert them. When you add new products to your FREETHNGS store, customers will get a text message letting then know they can come back and play.</p>
                                                    <div class="mt-3">
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" id="campaign_reminder1" name="campaign_reminder" class="custom-control-input"  value="1">
                                                            <label class="custom-control-label justify-content-start" for="campaign_reminder1"><strong>New Campaign</strong><br>Hi, {{First Name}} we want to remind you that they are new opportunities to win free items at {{store name}} at {{website link}}. Thank you. To stop getting this reminder, please text STOP.</label>
                                                        </div>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" id="campaign_reminder2" name="campaign_reminder" class="custom-control-input"  value="2">
                                                            <label class="custom-control-label" for="campaign_reminder2"><strong>Coupon Reminder</strong><br>Just a reminder from {{store name}} not to forget to use your winning coupon. We will not send another reminder. To stop receiving text messages from us, please reply to STOP. <br>{{Store owner's unique link}}</label>
                                                        </div>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" id="campaign_reminder3" name="campaign_reminder" class="custom-control-input"  value="3">
                                                            <label class="custom-control-label justify-content-start" for="campaign_reminder3">Not interested</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 height-card box-margin">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="card-title">Get Discovered</h4>
                                                    <p class="card-text">Add your store to our explore page and get discovered by consumers. Our discovery page is SEO friendly and you may find qualified lead that will lead to more sale.</p>
                                                    <div class="form-group mt-3">
                                                        <label>Please customize your Freethings.shop website and widge to match your brand. If you do not charge, it will be our default orange color.</label>
                                                        <div class="d-flex">
                                                            <input type="color" id="colorpicker" class="form-control col-md-8" name="get_discover_color" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" value="#bada55"> 
                                                            <input type="text" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" class="form-control col-md-4" value="#bada55" name="get_discover_text" id="hexcolor"></input>
                                                        </div>
                                                    </div>
                                                    <div class="mt-3">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" name="get_discover" value="1" id="customCheck1">
                                                            <label class="custom-control-label" for="customCheck1">I am in, get me discovered</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 height-card box-margin">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="card-title">Social Media Link</h4>

                                                    <div class="form-group mt-3">

                                                        <input type="text" name="facebook_link" class="form-control" id="facebook_link" placeholder="Facebook link">
                                                        <input type="text" name="instagram_link" class="form-control mt-2" id="instagram_link" placeholder="Instagram link">
                                                        <input type="text" name="twitter_link" class="form-control mt-2" id="twitter_link" placeholder="Twitter link">
                                                        <input type="text" name="linked_link" class="form-control mt-2" id="linked_link" placeholder="LinkedIn link">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 height-card box-margin">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="card-title">Store Promo Slider</h4>
                                                    <div class="form-group mt-3">
                                                        <div  class="dropzone dz-clickable store_promo_slider">
                                                            <div class="dz-default dz-message">
                                                                <span>Drop files here to upload</span>
                                                            </div>
                                                        </div>
                                                        <p>Up to 3 images</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 height-card box-margin">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="card-title">Store identity</h4>
                                                    <div class="form-group mt-3">
                                                        <input type="text" name="store_name" class="form-control" placeholder="Your website name - ex. Walmart" id="store_name">
                                                        <input type="text" name="store_url" class="form-control" placeholder="Your website url - ex. www.walmart.com" id="store_url">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 height-card box-margin">
                                            <div class="form-group">
                                                <div class="checkbox d-inline">
                                                    <input type="checkbox" value="1" name="terms_condition" id="terms_condition">
                                                    <label for="terms_condition" class="cr">Accept terms and conditions</label>
                                                </div>
                                            </div>
                                            <div class="">
                                                <button type="submit" class="btn btn-info">Submit</button>	
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- end card-->
                </div>
            </div>
        </div>
        <script>
$(function () {
    $('form').on('submit', function (e) {
        e.preventDefault();
        var terms_condition = $('#terms_condition').is(':checked');
        if (!terms_condition) {
            $.alert({
                title: 'Accept terms!',
                content: 'Please accept terms and conditions!',
            });
            return false;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'post',
            url: '<?php echo url('/') . "/settings-save" ?>',
            data: $('form').serialize(),
            success: function () {
                location.href = '<?php echo url('/') . "/complete" ?>';
            }
        });
    });


    /*Upload store image slider up to 3*/
    $(".store_promo_slider").dropzone({
        url: baseurl + 'upload-store-image',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (file, response) {
            if (file.height < 483 || file.width < 1200)
            {
                 $.confirm({
                               title: 'Invalid',
                               content: 'Image must be of 1200 x 483',
                               type: 'blue',
                               typeAnimated: true,
                               buttons: {
                                        close: function () {
                                            jconfirm.instances[0].close();
                                            }
                                 }
                      });
                this.removeFile(file);
                return false;
            }
            /*if (file.size < 2000) {
              $.confirm({
                        title: 'Invalid',
                        content: 'Image must be of more than 100 MB.',
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                             close: function () {
                                 jconfirm.instances[0].close();
                                 }
                          }
               });
             this.removeFile(file);
             return false;
             }*/
            result = jQuery.parseJSON(response);
            if (result.success == true)
            {
                if ($('[name="attach"]').val() == '')
                {
                    $('[name="attach"]').val(result.filename);
                } else
                {
                    $('[name="attach"]').val($('[name="attach"]').val() + ',' + result.filename);
                }
            } else
            {
                $(this).remove();
            }

        }
    });

    /*Color picker*/
    $('#colorpicker').change(function() {
        $('#hexcolor').val($(this).val());
    });
});
        </script>