

<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="checkout-process-wrapper">

                        <div class="row">
                            <div class="col-xl-6">
                                <div class="justify-content-between bd-highlight mb-3">

                                    <div class="bd-highlight">
                                        <ul class="nav nav-pills navtab-bg flex-column flex-sm-row">
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Select
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Cart
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link active  mb-3 pl-5 pr-5 text-center">
                                                    Checkout
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Settings
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>

                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Done
                                                </a>
                                            </li>

                                        </ul>
                                    </div>


                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="bd-highlight">
                                    <div class="btn-group mb-2 show float-right">
                                        <a href="<?= url('/') ?>/all-product-list"><button type="button" class="btn btn-danger btn-info"  aria-expanded="true">Continue Shopping</button></a>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="tab-content">


                            <div class="tab-pane show active" id="checkouttab">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="card box-margin bg-primary text-white">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="payment-area">
                                                            <h5 class="card-title mb-30 text-white">Your saved credit card and debit card</h5>

                                                            <div class="table-responsive">
                                                                <?php
                                                                foreach ($card_details as $key => $value) {
                                                                    ?>
                                                                    <form role="form"  action="<?= route('stripe.payment') ?>" method="post" class="f_<?= $key ?> dform require-validation "
                                                                          data-cc-on-file="false"
                                                                          data-stripe-publishable-key="<?= env('STRIPE_KEY') ?>"
                                                                          >
                                                                        <input type="hidden" name="card_id" value="<?= $value->id ?>">
                                                                        <input type="hidden" name="plan_no" >
                                                                        <input type="hidden" name="totalusd" >

                                                                        <input required  type="hidden" name="card-number" autocomplete='off' class="card-number form-control" placeholder="XXXXXXXXXXXXXXXX" value="<?= $value->card_number ?>" size='20'/>

                                                                        <input required type="hidden" name="card-expiry-month"  value="<?= $value->card_expiry_month ?>" class="card-expiry-month form-control" placeholder="MM"  size='2'/>

                                                                        <input required type="hidden" name="card-expiry-year"  value="<?= $value->card_expiry_year ?>" class="card-expiry-year form-control" placeholder="YYYY"  size='4'/>

                                                                        <input required type="hidden" name="card-cvc" size='4'  class="card-cvc form-control" placeholder="XXX">

                                                                        <input type="submit" name="" class="placeorderdata_<?= $key ?>" style="display:none"/>
                                                                    </form>
                                                                    <?php
                                                                }
                                                                ?>

                                                                <?php
                                                                if (count($card_details) > 0) {
                                                                    ?>
                                                                    <table class="table table-bordered table-hover savescard">

                                                                        <tbody>
                                                                        <input type="hidden" name="" class="currentid">

                                                                        <?php
                                                                        foreach ($card_details as $key => $value) {
                                                                            ?>

                                                                            <?php
                                                                            echo '<tr>
                              <td>
                   <div class="custom-control custom-radio ">
                      <input type="radio" id="' . $key . '" name="customRadio " class="custom-control-input cardselect">
                      <label class="custom-control-label" for="' . $key . '">Visa</label>
                   </div>
                </td>
                              <td>Ending with ' . substr($value->card_number, -4) . '</td>
                              <td>' . $value->name . '</td>
                              <td>' . $value->card_expiry_month . '/' . $value->card_expiry_year . '</td>
                              <td>
                   <div class="form-inline">
                      <div class="form-group">
                         <label for="inputPassword4">CVV</label>
                         <input type="password" id="inputPassword4" class="form-control rounded-0 form-control-md mx-sm-3" aria-describedby="passwordHelpInline">
                         <span id="e_' . $key . '"></span>
                      </div>
                   </div>
                </td>
                          </tr>';
                                                                        }
                                                                        ?>


                                                                        </tbody>
                                                                    </table>
                                                                    <?php
                                                                } else {
                                                                    echo "No Saved Cards";
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card box-margin">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-8">
                                                        <form role="form" action="" method="post" id="add_address">

                                                            <div class="payment-area">
                                                                <h5 class="card-title mb-30">Add Billing Address</h5>
                                                                <div class="nav-tabs-top">


                                                                    <div class="tab-content">
                                                                        <div class="tab-pane card-body fade show active" id="payment-methods-cc">
                                                                            <form method="post" id="add_address">
                                                                                <div class="addmsg"></div>
                                                                                <div class="form-group">
                                                                                    <label>
                                                                                        <span class="form-label text-dark mb-0">Title</span>
                                                                                    </label>
                                                                                    <input type="text" name="title" autocomplete='off' class="form-control" placeholder="Title" size='20'>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>
                                                                                        <span class="form-label text-dark mb-0">Address</span>
                                                                                    </label>
                                                                                    <input type="text" name="address" autocomplete='off' class="form-control" placeholder="Address" size='20'>
                                                                                </div>
                                                                                <button type="submit" class="btn btn-info">
                                                                                    Add&nbsp;→
                                                                                </button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card box-margin">
                                            <div class="card-body">
                                                <div class="row">

                                                    <div class="col-lg-8">
<?php
if (Session::has('success')) {
    ?>
                                                            <div class="alert alert-success text-center">
                                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                                                <p><?= Session::get('success') ?></p>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>


                                                        <form role="form" action="<?= route('stripe.post') ?>" method="post" class="f require-validation "
                                                              data-cc-on-file="false"
                                                              data-stripe-publishable-key="<?= env('STRIPE_KEY') ?>"
                                                              id="payment-form">

                                                            <div class="payment-area">
                                                                <h5 class="card-title mb-30">Pay Here</h5>
                                                                <div class="nav-tabs-top">


                                                                    <div class="tab-content">
                                                                        <div class="tab-pane card-body fade show active" id="payment-methods-cc">
                                                                            <div class="form-group">
                                                                                <label>
                                                                                    <span class="form-label text-dark mb-0">Card Number</span>
                                                                                </label>
                                                                                <input type="text" name="card-number" autocomplete='off' class="card-number form-control" placeholder="XXXXXXXXXXXXXXXX" size='20'>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="form-label text-dark">Card Holder</label>
                                                                                <input type="text" name="name" class="form-control" placeholder="Name On Card">
                                                                            </div>
                                                                            <div class="form-row">
                                                                                <div class="col">
                                                                                    <label class="form-labe text-dark">Exp. Month</label>
                                                                                    <input type="text" name="card-expiry-month" class="card-expiry-month form-control" placeholder="MM"  size='2'>
                                                                                </div>
                                                                                <div class="col">
                                                                                    <label class="form-labe text-dark">Exp. Year</label>
                                                                                    <input type="text" name="card-expiry-year" class="card-expiry-year form-control" placeholder="YYYY"  size='4'>
                                                                                </div>
                                                                                <div class="col">
                                                                                    <label class="form-label text-dark">CVV</label>
                                                                                    <input type="text" name="card-cvc" size='4' class="card-cvc form-control" placeholder="XXX">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane card-body fade" id="payment-methods-paypal">
                                                                            <div class="border p-3 mb-30">
                                                                                <h6>Add Paypal Form</h6>
                                                                                <p class="mb-0">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words.</p>
                                                                            </div>
                                                                            <button type="button" class="btn btn-lg btn-info btn-block">
                                                                                Continue with <strong><em>PayPal</em></strong>
                                                                            </button>
                                                                        </div>
                                                                        <div class="tab-pane card-body fade" id="payment-methods-visa">
                                                                            <div class="border p-3 mb-30">
                                                                                <h6>Add Visa Form</h6>
                                                                                <p class="mb-0">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words.</p>
                                                                            </div>
                                                                            <button type="button" class="btn btn-lg btn-info btn-block">
                                                                                Continue with <strong><em>PayPal</em></strong>
                                                                            </button>
                                                                        </div>
                                                                        <!-- <div class="form-group ml-3">
                                                                           <div class="checkbox d-inline">
                                                                              <input type="checkbox" name="save" id="checkbox-2">
                                                                              <label for="checkbox-2" class="cr">Save payment info</label>
                                                                           </div>
                                                                        </div> -->
                                                                    </div>
                                                                </div>

                                                                <div class="mt-30">
                                                                    <button type="submit" class="btn btn-info">
                                                                        Place Order &nbsp;→
                                                                    </button>

                                                                    <span class='col-md-12 error form-group hide e'>
                                                                        <span class='alert-danger alert'></span>
                                                                    </span>

                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 order-md-2 box-margin">
                                        <div class="card box-margin">
                                            <div class="card-body">
                                                <h4 class="d-flex justify-content-between align-items-center mb-3">
                                                    <span class="card-title mb-0">Price Details</span>
                                                </h4>

                                                <ul class="list-group mb-3">
                                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                        <div>
                                                            <h6 class="mb-0 font-14">1 Time setup Fees</h6>
                                                            <p>30 Days Money Back Guarantee</p>
                                                        </div>
                                                        <span><?= Session::get('symbols'); ?><span class="font-weight-bold text-success"><?= $res[0]->onetimecost ?></span></span>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                        <div>
                                                            <h6 class="mb-0 font-14">Sales Tax</h6>
                                                        </div>
                                                        <span class="font-weight-bold text-success"></span>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                        <div>
                                                            <h6 class="mb-0 font-14">Total </h6>
                                                        </div>
                                                        <span><?= Session::get('symbols'); ?><span class="font-weight-bold text-success"><?= $res[0]->totalcharges ?></span></span>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                        <div>
                                                            <h6 class="mb-0 font-14">Current Plan (Free plan)</h6>
                                                            <p>Paid until Dec 30th 2020<br><a href="#">See Plan</a></p>

                                                        </div>
                                                        <span class="font-weight-bold text-success">
                                                            <select name="plan" class="form-control rounded-0 form-control-md mx-sm-3 select_plan">
                                                                <option value="">Select</option>
<?php
$added = array();
foreach ($plans as $key => $value) {
    if (!in_array($value->plan, $added)) {
        echo '<option  value="' . $value->plan . '" code="' . $value->id . '">' . $value->plan . '</option>';
        $added[] = $value->plan;
    }
}
?>


                                                            </select></span>
                                                        <span class="font-weight-bold text-success fixprice">  </span>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                        <div>
                                                            <h6 class="mb-0 font-14">Available TEXT\SMS</h6>
                                                            <p>900 SMS</p>
                                                        </div>
                                                        <span class="font-weight-bold text-success showpland">
<?php
// foreach ($plans as $key => $value) {
//  echo '<input style="display:none" shtype="'.$value->plan.'" id="pd_'.$value->id.'" sum="'.$value->sum.'" type="radio" name="drone" class="slctplan"><span shtype="'.$value->plan.'" style="display:none"> '.$value->totalsms.'/'.$value->price.'/sms'.'<br></span>';
// }
?>

                                                        </span>
                                                        <span><?= Session::get('symbols'); ?><span class="font-weight-bold text-success planprice">  </span></span>
                                                    </li>
                                                    <li class="list-group-item d-flex justify-content-between">
                                                        <span class="text-dark font-weight-bold">Total (USD)</span>
                                                        <span><?= Session::get('symbols'); ?> <strong class="text-danger tusd"><?= $res[0]->totalusd ?></strong></span>
                                                    </li>
                                                </ul>
                                                <form class="d-flex justify-content-between ">
                                                    <div class="input-group">
                                                        <button type="button" class="btn btn-info placeorder">Place Order</button>
                                                    </div>
                                                    <div class="input-group">

                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="card box-margin">
                                            <div class="card-body">
                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>If you choose to BUY NOW, the cost of winning will remain the same. No charges at the time of winning.
                                                        </p></div>
                                                </div>
                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>If you choose to ADD TO STORE, we will charge your credit card on file at the time of winning and issue an invoice. 
                                                        </p></div>
                                                </div>
                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>Product availability is not guaranteed when you choose to ADD TO STORE. You may see the number of products you added to your store reduce based on what we have left in stock. 
                                                        </p></div>
                                                </div>
                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>Your Store will remain active even after your product in stock runs out. We will remind you when product in stock is running low. 
                                                        </p></div>
                                                </div>


                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-check-all font-40 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>You can delete and restart a campaign anytime. If you have products you "BUY NOW", you will always have those available in stock even after you delete a campaign
                                                        </p></div>
                                                </div>
                                            </div>




                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                    <!-- end card-->
                </div>
            </div>
        </div>
        <div class="modal inmodal fade" id="product-info" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Product Information</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-lg-4 col-xl-4">
                                <div class="product-name mb-20">
                                    <h3>Free Shipping</h3>
                                </div>
                                <div class="product-gallery" style="width:100%;">
                                    <img src="<?= url('/') ?>/resources/assets/img/shop-img/1.png" alt="Product" class="" style="width:100%;">
                                </div>
                                <div class="product-name mb-20">
                                    <h4 class="font-18 mt-20">Description :</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla amet magni velit dicta, quia ipsa vero ipsum. Pariatur blanditiis, quia.</p>
                                    <h4 class="font-18 mt-20">Product Prize Value :</h4>
                                    <p>Unknown</p>
                                    <h4 class="font-18 mt-20">Odd Of Winning :</h4>
                                    <p>TBD by website owner</p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-4 col-xl-4">
                                <div class="product-name mb-20">
                                    <h4 class="font-18 mt-20">Delivery Method :</h4>
                                    <p>SMS and Email</p>
                                    <h4 class="font-18 mt-20">Product Category</h4>
                                    <p>B2B</p>
                                    <h4 class="font-18 mt-20">Product Charges</h4>
                                    <p>$0</p>
                                    <h4 class="font-18 mt-20">Product Shipping</h4>
                                    <p>$0</p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-4 col-xl-4">
                                <div class="product-name mb-20">
                                    <h4 class="font-18 mt-20">Product provided by</h4>
                                    <p>Store and Website owner</p>
                                    <h4 class="font-18 mt-20">Product fullment by</h4>
                                    <p>FREETHNGS, SMS flow text with coupon/promo codes</p>
                                    <h4 class="font-18 mt-20">Product location</h4>
                                    <p>USA</p>
                                    <h4 class="font-18 mt-20">Lead Capture</h4>
                                    <p><a href="#" class="download-link badge badge-success badge-pill">Download</a> <a href="#" class="download-link badge badge-success badge-pill">Download</a> <a href="#" class="download-link badge badge-success badge-pill">Download</a> <a href="#" class="download-link badge badge-success badge-pill">Download</a> <a href="#" class="download-link badge badge-success badge-pill">Download</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

        <script type="text/javascript">
            $(function () {
                $('.error').css('display', 'none');
                var $form = $(".f.require-validation");
                $('.f.require-validation').bind('submit', function (e) {
                    var $form = $(".f.require-validation"),
                            inputSelector = ['input[type=email]', 'input[type=password]',
                                'input[type=text]', 'input[type=file]',
                                'textarea'].join(', '),
                            $inputs = $form.find('.required').find(inputSelector),
                            $errorMessage = $form.find('div.error'),
                            valid = true;
                    $errorMessage.addClass('hide');

                    $('.has-error').removeClass('has-error');
                    $inputs.each(function (i, el) {
                        var $input = $(el);
                        if ($input.val() === '') {
                            $input.parent().addClass('has-error');
                            $errorMessage.removeClass('hide');
                            e.preventDefault();
                        }
                    });

                    if (!$form.data('cc-on-file')) {
                        e.preventDefault();
                        Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                        Stripe.createToken({
                            number: $('.f .card-number').val(),
                            cvc: $('.f .card-cvc').val(),
                            exp_month: $('.f .card-expiry-month').val(),
                            exp_year: $('.f .card-expiry-year').val()
                        }, stripeResponseHandler);
                    }

                });

                function stripeResponseHandler(status, response) {
                    $('.error').css('display', 'inline');
                    if (response.error) {
                        $('.error')
                                .removeClass('hide')
                                .find('.alert')
                                .text(response.error.message);
                    } else {
                        // token contains id, last4, and card type
                        var token = response['id'];
                        // insert the token into the form so it gets submitted to the server
                        $form.find('input[type=text]').empty();
                        $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                        $form.get(0).submit();
                    }
                }

            });

            $(function () {
                $('.error').css('display', 'none');
                var $form = $(".dform.require-validation");
                $('.dform.require-validation').bind('submit', function (e) {
                    var $form = $(".dform.require-validation"),
                            inputSelector = ['input[type=email]', 'input[type=password]',
                                'input[type=text]', 'input[type=file]',
                                'textarea'].join(', '),
                            $inputs = $form.find('.required').find(inputSelector),
                            $errorMessage = $form.find('div.error'),
                            valid = true;
                    $errorMessage.addClass('hide');

                    $('.has-error').removeClass('has-error');
                    $inputs.each(function (i, el) {
                        var $input = $(el);
                        if ($input.val() === '') {
                            $input.parent().addClass('has-error');
                            $errorMessage.removeClass('hide');
                            e.preventDefault();
                        }
                    });

                    if (!$form.data('cc-on-file')) {
                        e.preventDefault();
                        Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                        Stripe.createToken({
                            number: $('.dform .card-number').val(),
                            cvc: $('.dform .card-cvc').val(),
                            exp_month: $('.dform .card-expiry-month').val(),
                            exp_year: $('.dform .card-expiry-year').val()
                        }, stripeResponseHandler);
                    }

                });

                function stripeResponseHandler(status, response) {
                    $('.error').css('display', 'inline');
                    if (response.error) {
                        $('.error')
                                .removeClass('hide')
                                .find('.alert')
                                .text(response.error.message);
                    } else {
                        // token contains id, last4, and card type
                        var token = response['id'];
                        // insert the token into the form so it gets submitted to the server
                        $form.find('input[type=text]').empty();
                        $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                        $form.get(0).submit();
                    }
                }

            });
        </script>