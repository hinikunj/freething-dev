<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="checkout-process-wrapper">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="justify-content-between bd-highlight mb-3">
                                    <div class="bd-highlight">
                                        <ul class="nav nav-pills navtab-bg flex-column flex-sm-row">
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Select
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>
                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Cart
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>
                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link  mb-3 pl-5 pr-5 text-center">
                                                    Checkout
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>
                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link mb-3 pl-5 pr-5 text-center">
                                                    Settings
                                                </a>
                                            </li>
                                            <li class="nav-item d-none d-sm-block">
                                                <p class="mt-2 pl-2 pr-2">
                                                    <i class="ti-angle-right"></i>
                                                </p>
                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link active mb-3 pl-5 pr-5 text-center">
                                                    Done
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="bd-highlight">
                                    <div class="btn-group mb-2 show float-right">
                                        <button type="button" class="btn btn-danger dropdown-toggle btn-info" data-toggle="dropdown" aria-expanded="true">Select Business</button>
                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                                            <a class="dropdown-item" href="javascript:void(0)">ALL</a>
                                            <a class="dropdown-item" href="javascript:void(0)">B2B</a>
                                            <a class="dropdown-item" href="javascript:void(0)">B2C</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane show active" id="completetab">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="animated-alert">
                                            <div class="alert bg-primary">

                                                <h3 class="text-white">Congratulation</h3>
                                                <h5 class="text-white">Your store is now ready</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12">

                                        <div class="card ">
                                            <div class="card-body">
                                                <h3 class="card-text mb-4">Your FREETHNGS&copy; store SMS flow number is {{Get Twillio Number}}.</h3>
                                                <h4 class="card-text mb-3">What you can after this:</h4>
                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-circle font-20 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>Add your store to your website, Your store link is <span style="color:blue"><?php echo $widgetLinkID = url('/')."/store/CPX".$camp_id."XBUX".$user_id; ?></span> or <a href="#"><strong>install the widget</strong></a>
                                                        </p></div>
                                                </div>
                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-circle font-20 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>Test Your SMS flow
                                                        </p></div>
                                                </div>

                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-circle font-20 text-primary"></i>
                                                    </div>
                                                    <div class="text">

                                                        <p>Refer a friend and get a full month of subscription free. Your referral link is <span style="color:blue"><?php echo $widgetLinkID = url('/')."/store/CPX".$camp_id."XBUX".$user_id; ?></span>
                                                        </p></div>
                                                </div>
                                                <div class="single-smart-card d-flex">
                                                    <div class="icon mr-3">
                                                        <i class="zmdi zmdi-circle font-20 text-primary"></i>
                                                    </div>
                                                    <div class="text">
                                                        <p>Contact us if you need help at <span style="color:blue"><?php echo $widgetLinkID = url('/')."/store/CPX".$camp_id."XBUX".$user_id; ?></span>
                                                        </p></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end card-->
                </div>
            </div>
        </div>