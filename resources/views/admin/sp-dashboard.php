<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-6 col-xl">
                    <!-- Card -->
                    <div class="card box-margin">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col">
                                    <!-- Title -->
                                    <h4 class="my-0">65</h4>

                                    <!-- Heading -->
                                    <span class="font-18 text-dark mb-0">
                                        Number of users
                                    </span>
                                </div>

                                <div class="col-auto">
                                    <!-- Icon -->
                                    <div class="icon">
                                        <i class="fa fa-id-badge font-30"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end card-->
                </div>

                <div class="col-12 col-sm-6 col-xl">
                    <!-- Card -->
                    <div class="card box-margin">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col">
                                    <!-- Title -->
                                    <h4 class="my-0">25K</h4>
                                    <!-- Heading -->
                                    <span class="font-18 text-dark mb-0">
                                        Number of Contacts
                                    </span>
                                </div>
                                <div class="col-auto">
                                    <!-- Icon -->
                                    <div class="icon">
                                        <i class="fa fa-id-badge font-30"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-xl">
                    <!-- Card -->
                    <div class="card box-margin">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col">
                                    <!-- Title -->
                                    <h4 class="my-0">55</h4>
                                    <!-- Heading -->
                                    <span class="font-18 text-dark mb-0">
                                        Number of Winnings
                                    </span>

                                </div>

                                <div class="col-auto">
                                    <!-- Icon -->
                                    <div class="icon">
                                        <i class="fa fa-id-badge font-30"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-xl">
                    <!-- Card -->
                    <div class="card box-margin">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col">
                                    <!-- Title -->
                                    <h4 class="my-0">40K</h4>
                                    <!-- Heading -->
                                    <span class="font-18 text-dark">
                                        Running Campaigns
                                    </span>
                                </div>
                                <div class="col-auto">
                                    <!-- Icon -->
                                    <div class="icon">
                                        <i class="fa fa-id-badge font-30"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <table id="sp-dashboard-datatable" class="table w-100 nowrap">
                                <thead>
                                    <tr>
                                        <th style="display:none">#</th>
                                        <th>Date</th>
                                        <th>Campaign ID</th>
                                        <th>Company Name</th>                                                       
                                        <th>Flow Number</th>
                                        <th>Winning ID</th>
                                        <th>product Name</th>
                                        <th>Product ID</th>
                                        <th>No of Winings</th>
                                        <th>Price Per Product</th>
                                        <th>Winning Frequency Times</th>
                                        <th>Pause Limit</th>
                                        <th>Set Winning Frequency</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $pause_cnt = 0;
                                    if (count($campaign_detail) > 0) {  //Zero campaign_detail 
                                        foreach ($campaign_detail as $key => $campaign) {
                                            $campID = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 5);
                                            foreach ($campaign->productdetails as $k => $product) {
                                                $countdown_id = false;
                                                $countdown_id = (isset($campaign->countdown[$k])) ? $campaign->countdown[$k]->id : 0;
                                                $is_pause = (isset($campaign->countdown[$k]) && $campaign->countdown[$k]->winning_status=="1") ? 1 : 0;
                                                $pause_cnt += $is_pause;
                                                $uniqueRow = $campaign->id . $campaign->user_id . $product->id;
                                                if (isset($campaign->countdown[$k])) {
                                                    $info = 'Every ' . $campaign->countdown[$k]->every_time . ' ' . $campaign->countdown[$k]->frequency . ' from ' . date("F j, Y", strtotime($campaign->countdown[$k]->startdate));
                                                    $info = '<a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="' . $info . '"><i class="fa fa-info-circle"></i></a>';
                                                } else {
                                                    $info = false;
                                                }
                                                ?>
                                                <tr>
                                                    <td style="display:none"><?= $key + 1; ?></td>
                                                    <td><?= $campaign->created_date ?></td>
                                                    <td><?= $campaign->campaign_id ?></td>
                                                    <td><?= $campaign->company ?></td>
                                                    <td><?= $campaign->phone ?></td>
                                                    <td><a id="getWinners" class="getWinners" href="#" data-toggle="modal" data-prod-id="<?= $product->id ?>" data-id="<?= $campaign->id ?>" data-target="#winning-table">View</a></td>
                                                    <td><?= $product->name ?></td>
                                                    <td><?= $campID . $product->id ?></td>
                                                    <td><?= $product->winning_freq . '/' . $product->winning ?></td>                                                    
                                                    <td><?= $product->displayed_price ?></td>
                                                    <td class="timerCol">
                                                        <?php
                                                        if ($campaign->deleted_at) {
                                                            echo "<p style='color:red'>Campaign Deleted</p>";
                                                        } else if ($campaign->payment_status == 0) {
                                                            echo "<p style='color:#ff8000'>Payment not Done</p>";
                                                        } else if ($campaign->status == 2) {
                                                            echo "<p style='color:#800080'>Paused</p>";
                                                        } else if ($product->winning_freq >= $product->winning) {
                                                            echo "<p style='color:#800080'>Stopped</p>";
                                                        } else if (!$campaign->deleted_at && $countdown_id > 0) {
                                                            if (isset($campaign->countdown[$k]->startdate)) {
                                                                echo '<span class="' . $uniqueRow . '" id="' . $uniqueRow . '"></span>';
                                                                echo $info;
                                                            }
                                                        } else {
                                                            echo "<p>Not Set</p>";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><span class="product_pause_usage" data-ispause="<?= $is_pause; ?>" data-pauselimit="<?= $campaign->product_pause_limit; ?>">-</span>/<?= $campaign->product_pause_limit; ?></td>
                                                    <td>
                                                        <?php if (!$campaign->deleted_at) { ?>
                                                            <a id="setCampTime" class="setCampTime" href="#" data-toggle="modal" data-countdown-id="<?= $countdown_id ?>" data-prod-id="<?= $product->id ?>" data-id="<?= $campaign->id ?>" data-target="#settime">Set time</a>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal fade text-left" id="settime" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <form method="post" name="frmSetTime"> 
                    <input type="hidden" id="countdown_id" name="countdown_id" value="" />
                    <input type="hidden" id="camp_id" name="camp_id" value="" />
                    <input type="hidden" id="prod_id" name="prod_id" value="" />
                    <div class="modal-content">
                        <div class="modal-header">

                            <h4 class="modal-title">Set the winning frequency time. countdown is going to start. you can pause winning frequeny anytime.</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="startdate">From</label>
                                <input type="text" name="startdate" id="startdate" class="form-control" data-provide="datepicker" placeholder="Select Date">
                            </div>
                            <div class="form-group">
                                <label for="every_time">Every</label>
                                <input id="every_time" type="text" name="demo3">
                            </div>
                            <div class="form-group">
                                <label for="frequency">Time frequency</label>
                                <select id="frequency" name="frequency" class="form-control">
                                    <option value="">Time frequency</option>
                                    <option value="seconds">Secs</option>
                                    <option value="minutes">Mins</option>
                                    <option value="hours">Hours</option>
                                    <!-- option value="days">Days</option -->
                                </select>
                            </div>

                        </div>

                        <div class="modal-footer d-block">
                            <div class="row">
                                <div class="col-4 col-sm-4">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" name="winning_status" id="winning_status" value="1">
                                        <label class="custom-control-label" for="winning_status">Pause winning</label>
                                    </div>
                                </div>
                                <div class="col-8 col-sm-8 text-right">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-info">Save changes</button>
                                </div>
                                <!-- Default switch -->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!--        Winning Model-->
        <div class="modal inmodal fade text-left" id="winning-table" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Winning IDs</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-striped"  id='WinnerList'>
                            <tr>
                                <th>Date and Time</th>
                                <th>Winnign ID</th>
                                <th>Subscriber Name</th>
                                <th>Subscriber Phone Number</th>
                            </tr>
                            <div>
<!--                                dynamic content-->
                            </div>
                        </table>
                    </div>

                    <div class="modal-footer d-block">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(".product_pause_usage").html("<?= $pause_cnt ?>");
            var countdowns = [
                <?php
                foreach ($campaign_detail as $campaign) {
                    foreach ($campaign->productdetails as $k => $product) {
                        $countdown_id = (isset($campaign->countdown[$k])) ? $campaign->countdown[$k]->id : 0;
                        $uniqueRow = $campaign->id . $campaign->user_id . $product->id;
                        ?>
                        {
                            id: <?= $uniqueRow ?>,
                            pid: <?= $product->id ?>,
                            cid: <?= $campaign->id ?>,
                            countdown_id: <?= $countdown_id ?>,
                            winning_status: <?= (isset($campaign->countdown[$k])) ? $campaign->countdown[$k]->winning_status : 0 ?>,
                            date: new Date("<?php echo (isset($campaign->countdown[$k])) ? date('M j, Y H:i:s', strtotime($campaign->countdown[$k]->startdate . ' 23:59:59')) : null; ?>").getTime(),
                            start_count_date: new Date("<?php echo (isset($campaign->countdown[$k])) ? date('M j, Y H:i:s', strtotime($campaign->countdown[$k]->startdate . ' 00:00:00')) : null; ?>").getTime(),
                            every_time: '<?= (isset($campaign->countdown[$k])) ? $campaign->countdown[$k]->every_time : 0 ?>',
                            frequency: '<?= (isset($campaign->countdown[$k])) ? $campaign->countdown[$k]->frequency : 0 ?>',
                            createdDate: new Date("<?php echo date('M j, Y H:i:s', strtotime($campaign->created_date)); ?>").getTime()
                        },
                    <?php
                    }
                }
                ?>
            ];
            $(function () {

                /*Jquery countdown configuration*/
                initDataTable();
                getCountdownTime();

                /*Datepicker custom configuration*/
                $("#startdate").datepicker({
                    startDate: '+0d',
                    autocomplete: 'off'
                });

                /*==============Get Campaing information ==========*/
                $('a.setCampTime').on('click', function (e) {
                    var camp_id = $(this).data('id');
                    var prod_id = $(this).data('prod-id');
                    var countdown_id = $(this).data('countdown-id');
					$("#winning_status").attr("disabled",true);
                    e.preventDefault();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'post',
                        url: '<?php echo url('/') . "/get-time" ?>',
                        dataType: 'json',
                        data: 'camp_id=' + camp_id + '&prod_id=' + prod_id + '&countdown_id=' + countdown_id,
                        success: function (data) {
                            if (data.status) {
                                //$("input[id=camp_id]").val(data.campData.id);
                                $("#camp_id").val(camp_id);
                                $("#prod_id").val(prod_id);
                                $("#countdown_id").val(countdown_id);
                                $("#startdate").val(data.campData.startdate);
                                $("#every_time").val(data.campData.every_time);
                                $("#frequency").val(data.campData.frequency);
								pause_used = parseInt($(".product_pause_usage").html());
								pause_limit = parseInt($(".product_pause_usage").data("pauselimit"));
								if(pause_used>=pause_limit){
									$("#winning_status").attr("disabled",true);
								} else {
									$("#winning_status").removeAttr("disabled");
								}
                                if (data.campData.winning_status == '1') {
                                    $('#winning_status')[0].checked = true; //$("#winning_status").attr('checked', true);
                                } else {
                                    $('#winning_status')[0].checked = false; //$("#winning_status").attr('checked', false);                                    
                                }
                            } else {
                                $("#camp_id").val(camp_id);
                                $("#prod_id").val(prod_id);
                            }
                        }
                    });
                });
                /*============= Set Campaign countdown =========== */

                $('form').on('submit', function (e) {
                    e.preventDefault();
                    var every = $('#every_time').val();
                    if (every < 1) {
                        $.alert({
                            title: 'Time frequency!',
                            content: 'Please select frequency number!',
                        });
                        return false;
                    }

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'post',
                        url: '<?php echo url('/') . "/set-time" ?>',
                        data: $('form').serialize(),
                        success: function () {
                            $.alert('Time has been set successfully!');
                            $('#settime').modal('hide');
                            location.href = '<?php echo url('/') . "/sp-dashboard" ?>';
                        }
                    });
                })
            
            /*==============Get Winner information ==========*/
                $('a.getWinners').on('click', function (e) {
                    var camp_id = $(this).data('id');
                    var prod_id = $(this).data('prod-id');
                    e.preventDefault();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'post',
                        url: '<?php echo url('/') . "/get-winners" ?>',
                        dataType: 'json',
                        data: 'camp_id=' + camp_id + '&prod_id=' + prod_id,
                        success: function (data) {
                            if (data.status) {
                                $('#WinnerList').html('<tr><th>Date and Time</th><th>Winnign ID</th><th>Subscriber Name</th><th>Subscriber Phone Number</th></tr>');
                                $(data.winData).each(function (k, v) {
                                    $('#WinnerList').append(
                                        '<tr><td><?= $campaign->created_date ?></td><td>'+v.winning_id+'</td><td>'+v.first_name+'</td><td>'+v.flow_number+'</td></tr>'
                                    );
                                })
                            } else {
                                $('#WinnerList').html('<tr><td>No Winner yet!</td></tr>');
                            }
                        }
                    });
                });
            
            });

            // rowCallback is what you need
            function initDataTable() {
                $('#sp-dashboard-datatable').DataTable({
                    rowCallback: function (nRow) {
                        /* This is your code */
                        // Set the date we're counting down to
                        

                        // Update the count down every 1 second
                        var timer = setInterval(function () {
                            // Get todays date and time
                            var now = Date.now();
                            var index = countdowns.length - 1;
                            // we have to loop backwards since we will be removing
                            // countdowns when they are finished
                            while (index >= 0) {
                                var countdown = countdowns[index];
                                // Find the distance between now and the count down date
                                var distance = countdown.start_count_date - now;
                                // Time calculations for days, hours, minutes and seconds
                                var subscribeDays = Math.round((now - countdown.createdDate) / (1000 * 60 * 60 * 24));
                                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                                // If the count down is over, write some text
                                if (countdown.winning_status) {
                                    $("." + countdown.id).text('Paused').css("color", "#800080");
                                    // this timer is done, remove it
                                    countdowns.splice(index, 1);
                                } else if (subscribeDays > 30) {
                                    $("." + countdown.id).text("Subscribtion hold");
                                    countdowns.splice(index, 1);
                                } else if (countdown.start_count_date < 0){
                                    $("." + countdown.id).text("Not Set");
                                    countdowns.splice(index, 1);
                                } else if (distance < 0) {
                                    //$("." + countdown.id).text("Ready").css("color", "#008000");
                                    $("." + countdown.id).addClass("in_ready");
                                    $("." + countdown.id).data("cid",countdown.cid);
                                    $("." + countdown.id).data("pid",countdown.pid);                                                                        
                                    $("." + countdown.id).data("sdate",countdown.start_count_date);                                                                        
                                    $("." + countdown.id).data("every",countdown.every_time);                                                                        
                                    $("." + countdown.id).data("freq",countdown.frequency);                                                                        
                                    if (days > 0) {
                                        hours = hours + (days * 24);
                                    }
                                    if (hours > 48) {
                                        //$("." + countdown.id).text("Upcoming");
                                    } else {
                                        //$("." + countdown.id).text(hours + "h " + minutes + "m " + seconds + "s ");
                                    }
                                } else {
                                    $("." + countdown.id).text("Upcoming").css("color", "#008000");;
                                }                                
                                index -= 1;
                            }

                            // if all countdowns have finished, stop timer
                            if (countdowns.length < 1) {
                                clearInterval(timer);
                            }
                        }, 1000);
                    },
                    scrollX: !0,
                    language: {paginate: {previous: "<i class='arrow_carrot-left'>", next: "<i class='arrow_carrot-right'>"}},
                    drawCallback: function () {
                        $(".dataTables_paginate > .pagination").addClass("pagination-rounded");
                        getCountdownTime();
                    },
                    //iDisplayLength: 10
                });
            }
            ;
            function getCountdownTime(){
                var data_obj = [];
                $(".in_ready").each(function(e){
                    console.log($(this).data());
                    var obj = {};
                    obj.id = $(this).attr("id");
                    obj.cid = $(this).data("cid");
                    obj.pid = $(this).data("pid");
                    obj.sdate = $(this).data("sdate");
                    obj.freq = $(this).data("freq");
                    obj.every = $(this).data("every");
                    data_obj.push(obj);
                });
                if(data_obj.length>0){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'post',
                        url: '<?php echo url('/') . "/get-countdown-time" ?>',
                        dataType: 'json',
                        data: {
                            current_timer: data_obj,
                            silent: 1
                        },
                        success: function (data) {
                            $(data.winData).each(function(c,v){
                                $("."+v.id).data("iscountdown",v.is_countdown);
                                if(v.is_countdown=="1"){
                                    $("."+v.id).data("lasttime",v.last_time);
                                }
                            });
                            checkTimer();
                        }
                    });
                }
                /**/
            }
            function checkTimer(){
                var system_now = new Date();
                var now = new Date(system_now.toLocaleString('en-US', { timeZone: 'UTC' }));
                $(".in_ready").each(function(e){
                    if($(this).data("iscountdown")=="1"){
                        start_count_date = new Date($(this).data("lasttime")).getTime();
                        var distance = start_count_date - now;
                        
                        // Time calculations for days, hours, minutes and seconds
                        if(distance>0){
                            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                            if (days > 0) {
                                hours = hours + (days * 24);
                            }
                            if (hours > 24) {
                                $(this).text("Upcoming");
                            } else {
                                $(this).text(hours + "h " + minutes + "m " + seconds + "s ");                            
                            }
                        }else{
                            $(this).text("Ready").css("color", "#008000");
                        }
                    }else{
                        $(this).text("Ready").css("color", "#008000");
                    }
                });
            }
            var check_timer = setInterval(checkTimer,1000);
            var get_countdown_time = setInterval(getCountdownTime,60000);
            setTimeout(getCountdownTime,2000);
            
        </script>