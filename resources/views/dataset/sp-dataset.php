<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

<?php
// echo "<pre>";
// print_r($list);
?>
<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-0 float-left">Table and Data Setting</h4>

                            <button type="button" data-toggle="modal" data-target="#adddataset"
                                    class="btn btn-warning mb-2 mr-2 float-right">Add Dataset</button>
                            <div id="" class="dataTables_filter float-right mr-5"></div>
                            <div class="modal inmodal fade" id="adddataset" tabindex="-1" role="dialog"
                                 aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Adding Dataset Category</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12 col-lg-12 col-xl-12">
                                                    <form id="form_dataset" action="dataset/save" method="POST">
                                                        <input type="hidden" name="dataset_id" id="dataset_id">
                                                        <div class="form-group selectloc">
                                                            <label for="exampleInputEmail111">Should this apply to a specific company</label>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio" id="yes" name="is_company_specific"
                                                                       class="custom-control-input"
                                                                       value="spec-com-search">
                                                                <label class="custom-control-label"
                                                                       for="yes">Yes</label>


                                                            </div>
                                                            <div class="custom-control custom-radio">


                                                                <input type="radio" id="no" name="is_company_specific"
                                                                       value="0" checked class="custom-control-input">
                                                                <label class="custom-control-label" for="no">no</label>
                                                            </div>
                                                        </div>
                                                        <div class="row g-mb-25">
                                                            <div class="form-group col-md-12 mb-0">
                                                                <label for="lgFormGroupInput1"
                                                                       class="col-form-label col-form-label-lg spec-com-search box">Search
                                                                    Company</label>

                                                            </div>

                                                            <div class="form-group col-md-12">

                                                <!-- <input type="text"
                                                   class="form-control form-control-lg rounded-0 spec-com-search box"
                                                   id="lgFormGroupInput1"
                                                   placeholder="Search Company"> -->
                                                                <select class="form-control spec-com-search box" name="company_id" id="company_id">
                                                                    <?php foreach ($company as $comp) { ?>
                                                                        <option value="<?php echo $comp->id; ?>"><?php echo $comp->company_name; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="form-group col-md-12 mb-0">
                                                                <label for="inputEmail4" class="col-form-label">Dataset
                                                                    Category</label>

                                                            </div>
                                                            <div class="form-group col-md-6">

                                                                <select id="old_category" name="old_category" class="form-control">
                                                                    <option value="0">Choose</option>
                                                                    <?php foreach ($category as $cat) { ?>
                                                                        <option value="<?php echo $cat->category; ?>"><?php echo $cat->category; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="inputPassword4"
                                                                       class="col-form-label"></label>
                                                                <button type="button"
                                                                        class="btn btn-outline-warning mb-2 mr-2"
                                                                        id="btn_add_category">Add Dataset Category</button>
                                                            </div>
                                                        </div>
                                                        <div class="form-group cloned-row-x " id="div_new_category" style="display:none">
                                                            <label for="category">New Dataset Category</label>
                                                            <input type="test" class="form-control" name="category" id="category" placeholder="New Dataset Category">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="sub_category">Dataset SubCategory</label>
                                                            <input type="text" class="form-control"
                                                                   id="sub_category" name="sub_category"
                                                                   placeholder="Dataset SubCategory">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="sub_category">Explanation</label>
                                                            <textarea id="summernote" name="explanation" class="form-control" placeholder="Your Message Here ..." style="min-height:300px;"></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class=" d-inline">
                                                                <input type="checkbox" id="link_required" name="link_required" />
                                                                <label for="link_required" class="cr">Link Required</label>
                                                            </div>


                                                        </div>
                                                        <div class="form-group">
                                                            <div class=" d-inline">
                                                                <input type="checkbox" id="text_entry" name="text_entry" />
                                                                <label for="text_entry" class="cr">Text Entry Required</label>
                                                            </div>


                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary mr-2" id="btn_submit">Submit</button>
                                            <button type="submit" class="btn btn-danger"
                                                    data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end card-->
                </div>
                <!-- end col -->
            </div>
            <div class="row">
                <div class="col-xl-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body table-responsive">



                            <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>Organize</th>
                                        <th>Dataset Category</th>
                                        <th>Sub Categories</th>
                                        <th>For</th>
                                        <th title="Default selected in setup goal">Set Default</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($list as $val) { ?>
                                        <tr>
                                            <td><?= $val->datasets_id ?></td>
                                            <td><?= $val->category ?></td>
                                            <td><?= $val->sub_category ?></td>
                                            <td><?= ($val->is_company_specific) ? $val->company_name : 'ALL' ?></td>
                                            <td>
                                                <?php ($val->is_default) ? $checked = 'checked' : $checked = false ?>
                                                <input type="checkbox" <?= $checked ?> class="isDefaultCheck" data-id="<?= $val->datasets_id ?>" title="Default selected in setup goal" name="is_default" id="is_default" value="1">
                                            </td>
                                            <td class="action<?= $val->datasets_id ?>">
                                                <?php
                                                if ($val->is_default == 1) {
                                                    echo 'Default';
                                                } else {
                                                    ?>
                                                    <a href="#" data-toggle="modal" data-target="#adddataset" data-id="<?= $val->datasets_id ?>" data-company="<?= $val->company_id ?>" data-category="<?= $val->category ?>" data-iscompany="<?= $val->is_company_specific ?>" data-subcat="<?= $val->sub_category ?>" data-explanation='<?php $val->explanation ?>' data-linkrequired="<?= $val->link_required ?>" data-textentry="<?= $val->text_entry ?>" class="mr-2 dataset_edit"><i class="fa fa-edit text-info font-18"></i></a>
                                                    <a href="#" class="dataset_delete" data-id="<?= $val->datasets_id ?>" ><i class="fa fa-trash text-danger font-18"></i></a>
                                        <?php } ?>
                                            </td>
                                        </tr>
<?php } ?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end card-->
                </div>
                <!-- end col -->
            </div>
        </div>
