<?php
$existGoals = json_decode($existGoal->set_goals, true);
$existGoalArr = array_keys($existGoals);
?>
<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="card mb-30">
                        <div class="card-body text-center">
                            <div class="profile--tumb">
                                <img src="<?= url('/') ?>/resources/img/member-img/1.png" alt="">
                            </div>
                            <h6 class="font-20 mb-1"><?= $subscriber->first_name . ' ' . $subscriber->last_name ?></h6>
                            <p class="font-13 text-dark">Last Updated: <?= ($subscriber->updated_at) ? $subscriber->updated_at : $subscriber->created_at ?></p>
                            <p class="description px-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime mollitia.</p>
                        </div>
                    </div>
                    <!-- ./profile -->

                    <div class="card address mb-30">
                        <div class="card-body">
                            <h4 class="font-16 mb-15">Communication :</h4>
                            <div class="mt-0 d-flex align-items-center">
                                <i class="fa fa-home pr-2"></i>
                                <h6 class="font-14 mb-0">example@.com</h6>
                            </div>
                            <div class="mt-3 d-flex align-items-center">
                                <i class="fa fa-phone pr-2"></i>
                                <h6 class="font-14 mb-0"><?= $subscriber->phone ?></h6>
                            </div>

                            <!-- Icon -->
                            <div class="profile-social-icon mt-15 justify-content-left d-flex">
                                <a href="#" data-toggle="tooltip" title="Facebook">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                                <a href="#" data-toggle="tooltip" title="Dribbble">
                                    <i class="fa fa-dribbble" aria-hidden="true"></i>
                                </a>
                                <a href="#" data-toggle="tooltip" title="Twitter">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                                <a href="#" data-toggle="tooltip" title="Instagram">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- ./address -->
                </div>

                <div class="col-12 col-md-8">
                    <div class="profile-crm-area">
                        <div class="card mb-30">
                            <div class="card-body">
                                <h6 class="card-title">Personal</h6>
                                <div class="col-xl-7 col-sm-9">
                                    <span class="profile-info"><sup><a href="#" data-toggle="modal" data-target="#winning-table">History</a></sup></span>

                                    <div class="modal inmodal fade text-left" id="winning-table" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">First Name <!-- {{Dataset Category Name}} --></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-striped">
                                                        <tr>
                                                            <th>Date</th>
                                                            <th>Response</th>

                                                        </tr>
                                                        <tr>
                                                            <td>02/12/2020 10:00:05 AM</td>
                                                            <td>Pranav Prashar</td>

                                                        </tr>
                                                        <tr>
                                                            <td>02/12/2020 10:00:05 AM</td>
                                                            <td>Pranav Prashar</td>

                                                        </tr>
                                                    </table>
                                                </div>

                                                <div class="modal-footer d-block">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if (isset($setGoal['Personal'])) {
                                    foreach ($setGoal['Personal'] as $key => $value) {
                                        if ($value == 'First Name')
                                            $colValue = $subscriber->first_name;
                                        if ($value == 'Age')
                                            $colValue = (isset($subscriber->age)) ? $subscriber->age : '-';
                                        if ($value == 'Gender')
                                            $colValue = $subscriber->gender;
                                        /*$vSpace = false;
                                        $vSpace = str_replace(' ', '_', $value);
                                        if(in_array($value, $existGoalArr) || in_array($vSpace, $existGoalArr)){ */
                                            echo '<div class="row profile-row">
                                                    <div class="col-xs-5 col-sm-3">
                                                        <span class="profile-cat">' . $value . '</span>
                                                    </div>
                                                    <div class="col-xl-7 col-sm-9">
                                                        <span class="profile-info">' . $colValue . '</span>
                                                    </div>
                                                </div>';
                                        //}
                                    }
                                    unset($setGoal['Personal']);
                                    unset($setGoal['Communication']);
                                }
                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <?php
                            foreach ($setGoal as $category => $subcategory) {
                                $subCatHtml = ''; $i = 0;
                                //Category section
                                $catHtml = '<div class="col-xl-6 height-card box-margin">'
                                . '<div class="card">
                                            <div class="card-body">'
                                . '<h6 class="card-title">' . $category . '</h6>';
                                //Subcategry section
                                foreach ($subcategory as $k => $v) {
                                    $vSpace = false;
                                    $vSpace = str_replace(' ', '_', $v);
                                    if(in_array($v, $existGoalArr) || in_array($vSpace, $existGoalArr)){
                                        $i++;
                                        $subCatHtml .= '<div class="row profile-row">
                                                <div class="col-xs-5 col-sm-3">
                                                    <span class="profile-cat">' . $v . '</span>
                                                </div>
                                                <div class="col-xl-7 col-sm-9">
                                                    <span class="profile-info">-</span>
                                                </div>
                                            </div>';
                                    }
                                }
                                if($i > 0){
                                    echo $catHtml;
                                    echo $subCatHtml;
                                    echo '</div>'
                                    . '</div>'
                                    . '</div>';
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
