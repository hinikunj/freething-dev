<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<link rel="stylesheet" href="<?= url('/') ?>/resources/assets/css/default-assets/new/sweetalert-2.min.css">
<script src="<?= url('/') ?>/resources/assets/js/default-assets/sweetalert-init.js"></script>
<script src="<?= url('/') ?>/resources/assets/js/default-assets/sweetalert2.min.js"></script>
<script>

$(document).ready(function() {
    //$('.js-example-basic-single').select2();
});


$('#btn_submit').click(function() {

      var is_company_specific = $("input[name='is_company_specific']:checked").val();
    var company_id = $('#company_id').val();
    var old_category = $('#old_category').val();
    if(old_category != '0') {
      var category = old_category;
    } else {
      var category = $('#category').val();
    }
    explanation = $("#summernote").summernote("code");
    if($('#link_required').is(":checked")) {
      var link_required = "1";
    } else {
      var link_required  = "0";
    }
    if($('#text_entry').is(":checked")) {
      var text_entry = "1";
    } else {
      var text_entry  = "0";
    }
    if(category.trim() == '') {
      alert('Please provide category');
    } else {

      var sub_category = $('#sub_category').val();
      var dataset_id =  $('#dataset_id').val();
      
          $.ajax({
        url: 'dataset/save',
        type: 'POST',
        "dataType": "json",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        //data: {"is_company_specific":is_company_specific,"company_id":company_id,"category":category,"sub_category":sub_category},
        data:{"is_company_specific":is_company_specific,"company_id":company_id,"category":category,"sub_category":sub_category,"explanation":explanation,"link_required":link_required,"text_entry":text_entry,"dataset_id":dataset_id},
        success: function(data) {
        //called when successful
        
        if(data.success == false) {
          //alert('Duplicate Dataset Entry')
          Swal.fire({title:"Duplicate Dataset Entry!",confirmButtonClass:"btn btn-confirm mt-2"});
        } else {
        location.reload();
        }
        },
        error: function(e) {
        //called when there is an error
        //console.log(e.message);
        }
      });
  }
});

$('.isDefaultCheck').click(function() {
    var id = $(this).data('id');
    //Play-Pause campaign
        $.confirm({
            title: 'Are you sure?',
            content: 'It will change default status!',
            autoClose: 'cancel|7000',
            buttons: {
                changeCampaign: {
                    text: 'change dataset',
                    action: function () {
                        //location.href = this.$target.attr('href');
                        return $.ajax({
                            url: baseurl + 'set-default-goal/' + id,
                            dataType: 'json',
                            method: 'get'
                        }).done(function (response) {
                            if (response.success) {
                                //if(response.data)
                                    //$('.action'+id).text('Default');
                                $.alert(response.message);
                            }
                        }).fail(function () {
                            //self.setContent('Something went wrong.');
                        });
                    }
                },
                cancel: function () {
                    //$.alert('action is canceled');
                }
            }
        });
    });


$('.dataset_edit').click(function() {
  var is_company_specific = $(this).data('iscompany');
  var company = $(this).data('company');
  var category = $(this).data('category');
  var subcat = $(this).data('subcat');
  var explanation = $(this).data('explanation');
  var link_required = $(this).data('linkrequired');
  var text_entry = $(this).data('textentry');
  var id = $(this).data('id');

  $('#dataset_id').val(id); 

  if(is_company_specific == 1) {
    $('#yes').click();
  } else {
    $('#no').click();
  }
  $('#company_id').val(company); 
  $('#category').val(category); 
  $('#old_category').val(category);
  $('#sub_category').val(subcat); 
  $("#summernote").summernote("code", explanation);
  if(link_required == "1") {
    $('#link_required').prop("checked",true);
  } else {
    $('#link_required').prop("checked",false);
  }
  
  if(text_entry == "1")
    $('#text_entry').prop("checked",true);
  else
    $('#text_entry').prop("checked",false);
});

$('.dataset_delete').click(function() {
  
  var id = $(this).data('id');
  delete_dataset(id);
});

function delete_dataset(id) {
  Swal.fire({
        title:"Are you sure?",
        text:"You won't be able to revert this!",
        type:"warning",
        showCancelButton:!0,
        confirmButtonColor:"#3085d6",
        cancelButtonColor:"#d33",
        confirmButtonText:"Yes, delete it!"
    }).then(function(t)
        {   
          
          if(t.value) {
            $.ajax({
              url: 'dataset/delete_dataset',
              type: 'POST',
              "dataType": "json",
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              //data: {"is_company_specific":is_company_specific,"company_id":company_id,"category":category,"sub_category":sub_category},
              data:{"dataset_id":id},
              success: function(data) {
              //called when successful
              location.reload();
              },
              error: function(e) {
              //called when there is an error
              //console.log(e.message);
              }
            });   
          }
          //t.value&&Swal.fire("Deleted!","Your file has been deleted.","success")
        })
}

$('#btn_add_category').click(function() {
  $('#div_new_category').show();
});

$('#old_category').change(function() {
if($(this).val() != '0') {
  $('#div_new_category').hide();
} else {
  $('#div_new_category').show();
}
});
</script>