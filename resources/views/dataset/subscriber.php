<!-- Main Page -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 height-card box-margin">
                    <div class="card">
                        <div class="card-body">
                            <table id="scroll-horizontal-datatable" class="table w-100 nowrap">
                                <thead>
                                    <tr>
                                        <th>Last Update</th>
                                        <th>Feeling</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Phone Number</th>
                                        <th>Winning Count</th>
                                        <th>Play Reminder</th>
                                        <th>Campaign Reminder </th>
                                        <th>Can play again in</th>
                                        <th>NPS/Comment</th>
                                        <th>Referral Count</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($subscribers) > 0) {
                                        foreach ($subscribers as $subscriber) {
                                            ?>
                                            <tr>
                                                <td><?= ($subscriber->updated_at) ? $subscriber->updated_at : $subscriber->created_at ?></td>
                                                <td><i class="zmdi zmdi-mood" style="font-size: 20px; color: #f1562a;"></i></td>
                                                <td><?= $subscriber->first_name ?></td>
                                                <td><?= $subscriber->last_name ?></td>
                                                <td><?= $subscriber->phone ?></td>
                                                <td>2</td>
                                                <td><?= (($settings->play_reminder == 1) ? 'Weekly' : ($settings->play_reminder == 2)) ? 'Countdown-to-play' : 'Unsubscribed' ?></td>
                                                <td><?= (($settings->campaign_reminder == 1) ? 'Subscribed New Campaign' : ($settings->campaign_reminder == 2)) ? 'Coupon reminder' : 'Unsubscribed' ?></td>
                                                <td>47.59</td>
                                                <td class="npscomment">Good <a href="#" data-toggle="modal" data-target="#npscomment"><i class="zmdi zmdi-eye"></i></a></td>
                                                <td>2</td>
                                                <td><a href="<?= url('/subscriber-profile/' . $subscriber->id) ?>" class="btn btn-info">View Profile</a></td>
                                            </tr>           
                                        <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal inmodal fade text-left" id="npscomment" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">NPS Comment</h4>
                    </div>
                    <div class="modal-body">
                        <table id="scroll-horizontal-datatable" class="table w-100 nowrap">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>NPS</th>
                                </tr>
                            </thead>
                            <tbody>                                                    
                                <tr>
                                    <td>10/18/2019</td>
                                    <td><h6>Good</h6> <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></td>
                                </tr>
                                <tr>
                                    <td>10/18/2019</td>
                                    <td><h6>Not Good</h6> <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></td>
                                </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>