<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
/* Login */
Route::any('/', 'LoginController@login');

/* Register */
Route::any('/register', 'LoginController@index');

/* Login */
Route::any('/login', 'LoginController@login');



/////////////////////// PRODUCT /////////////////////////////////////////////////
/* product list */
Route::any('/product-list', 'ProductController@list');

/* add product */
Route::post('/add-product', 'ProductController@add_product');

/* upload product image */
Route::post('/upload-product', 'ProductController@upload_product');

/* search company */
Route::post('/search-company', 'ProductController@search_company');

/* add location */
Route::post('/add-location', 'ProductController@add_location');

/* get locations */
Route::post('/get-locations', 'ProductController@get_locations');

/* get product detail */
Route::get('/get-product-detail/{id}', 'ProductController@get_product_detail');

/* get product detail */
Route::get('/delete-product/{id}', 'ProductController@delete_product');

/* delete product immage */
Route::get('{id}/delete', 'ProductController@destroy')->name('file.delete');

/* edit product */
Route::post('/update-product', 'ProductController@update_product');
Route::post('/get-datasset', 'ProductController@get_datasset');

/////////////////// PROFILE ///////////////////////////////////////////////
/* EDIT PROFILE */
Route::any('/edit-profile', 'LoginController@edit_profile');

/* RESET PASSWORD */
Route::any('/reset-password', 'LoginController@reset_pwd');

/* Update Location */
Route::any('/update-profile-location', 'LoginController@update_profile_location');

/* Update Language */
Route::any('/update-profile-lang', 'LoginController@update_profile_lang');

/* Update Industry */
Route::any('/update-profile-industry', 'LoginController@update_profile_industry');

/* Delete Account */
Route::get('/delete-account', 'LoginController@delete_account');

/* Update Email */
Route::post('/update-email', 'LoginController@update_email');

/* Verify Phone */
Route::post('/verify-phone', 'LoginController@verify_phone');


/* sign out */
Route::get('/sign-out', 'LoginController@sign_out');

/* Google Auth */
Route::post('/google-register', 'LoginController@google_register');

/* FORGOT PASSWORD */
Route::any('/forget-password', 'LoginController@forget_password');

/* RESET FORGOT PASSWORD */
Route::any('/reset-forget-password', 'LoginController@reset_forget_password');
/* Admin Dashboard */
/* User Dashboard */
Route::get('/sp-dashboard', 'UserController@admin_dashboard');

/////////////////// USER ///////////////////////////////////////////////
/* User Dashboard */
Route::get('/dashboard', 'UserController@dashboard');

/* product list for User */
Route::any('/all-product-list', 'UserController@all_product_list');

/* Add to cart */
Route::any('/add-to-cart/{id}', 'UserController@add_to_cart');

/* cart */
Route::any('/cart', 'UserController@cart');

/* delete cart item */
Route::get('/delete-cart-product/{id}', 'UserController@delete_cart_product');

/* checkout */

Route::any('/checkout_process', 'UserController@checkout_process');
Route::any('/checkout', 'UserController@checkout');
Route::any('/payment', 'UserController@payment')->name('stripe.payment');
Route::any('/checkout-stripe', 'UserController@stripe')->name('stripe.post');
Route::any('/settings', 'UserController@settings');
Route::any('/settings-save', 'UserController@settings_save');
Route::any('/complete', 'UserController@complete');
Route::any('/payment-done', 'UserController@payment_done');


/////////////////// USER MANAGEMENT ///////////////////////////////////////////////
/* User list */
Route::any('/user-list', 'UserManagementController@user_list');

/* Updtae user status */
Route::any('/update-user-status/{id}/{status}', 'UserManagementController@update_user_status');

////////////////////DATASET//////////////////
Route::any('/dataset-list', 'DatasetController@list');
Route::any('/dataset/save', 'DatasetController@save');
Route::any('/dataset/delete_dataset', 'DatasetController@delete_dataset');

Route::any('/sms-flow', 'FlowController@flow');


Route::any('/add-questions', 'FlowController@add_question');
Route::any('/getsymbol/{price}', 'UserController@getsymbol');
Route::any('/delete-questions', 'FlowController@delete_questions');
Route::any('/edit-questions', 'FlowController@edit_questions');
Route::any('/get-questions', 'FlowController@get_questions');
Route::any('/flow', 'FlowController@list');
Route::any('/creating-flow', 'FlowController@creating_flow');
Route::any('/add-flow', 'FlowController@add_flow');
Route::any('/flow/{id}', 'FlowController@edit_flow');
Route::any('/delete-flow', 'FlowController@delete_flow');
Route::any('/drafts', 'FlowController@drafts');
Route::any('/search-flow', 'FlowController@search_flow');
Route::any('/search-question', 'FlowController@search_question');
Route::any('/updateQuestion', 'FlowController@update_question');


Route::any('/plan-details/{plan}', 'UserController@plan_details');

////////////////////// BILLING ////////////////////////
/* history */
Route::any('/history', 'BillingController@history');
/* Payment */
Route::any('/payments', 'BillingController@payment');
/* Invoice */
Route::any('/invoice/{id}', 'BillingController@invoice');
Route::any('/cancel-subscription/{id}', 'BillingController@cancel_subscription');


/* Delete card */
Route::any('/delete-card/{id}', 'BillingController@delete_card');

/* Add Address */
Route::any('/add-address', 'BillingController@add_address');

/* Done routing by Nikunj for campaign */
Route::get('/delete-campaign', 'CampaignController@delete_campaign');
Route::get('/change-campaign', 'CampaignController@change_campaign');
Route::any('/campaign', 'CampaignController@index');
Route::any('/campaign/{id}', 'CampaignController@campaign_product');
Route::any('/upload-image', 'UserController@fileUpload');
Route::any('/subscribers', 'SubscribersController@index');
Route::any('/subscriber-profile/{id}', 'SubscribersController@profile');
Route::any('/set-goal', 'UserController@save_goal');
Route::any('/set-default-goal/{id}', 'DatasetController@set_default_goal');
Route::any('/winners', 'WinnersController@index');
Route::any('/get-winners', 'WinnersController@get_winners');
Route::any('/get-countdown-time', 'WinnersController@get_countdown_time');
Route::any('/set-time', 'UserController@set_time');
Route::any('/get-time', 'UserController@get_time');
Route::any('/store/{id}', 'CampaignController@store');
Route::post('/upload-store-image', 'UserController@upload_store_image');

Route::post('/verifyphone', 'UserController@verifyphone');
Route::post('/verificationcode', 'UserController@verificationcode');
Route::post('/resendcode', 'UserController@resendcode');
Route::post('/startgame', 'UserController@startgame');
Route::post('/savefirstname', 'UserController@savefirstname');
/* Twilio */
Route::any('/incomingsms', 'TwilioController@incomingsms');